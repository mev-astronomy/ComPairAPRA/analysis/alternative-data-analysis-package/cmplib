#include "CmpBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"

#include "DstBranch.h"
#include "SitBranch.h"
#include "CztBranch.h"
#include "CsIBranch.h"

#include "TFile.h"
#include "TMath.h"

ClassImp( CmpBranch )

CmpBranch::CmpBranch( void )
{
  Clear();
}

CmpBranch::~CmpBranch()
{
}

Double_t CmpBranch::GetRadDist( void ) const
{
  if ( fP1 == CmpLib::fVnotValid ) return CmpLib::fDnotValid;
  if ( fP2 == CmpLib::fVnotValid ) return CmpLib::fDnotValid;

  return (fP1-fP2).Mag();
}

Int_t CmpBranch::Process( Int_t sel )
{
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return -1;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return -1;
  
  FindNhitSit();
  FindNhitCzt();
  FindNhitCsI();

  if ( fNhitSit == 0 ) {
    if ( fNhitCzt == 1 && fNhitCsI == 1 ) {
      fP1 = fHposCzt;
      fE1 = fEdepCzt;
      
      fP2 = fHposCsI;
      fE2 = fEdepCsI;
      
      fSingle  = kTRUE;
      fPairing = CmpLib::CZTCSI;
    }
    else return 0;
  }
  else if ( fNhitSit == 1 ) {
    if ( fHitSlice.size() != 1 ) return 0;
    
    fP1 = fHpos[fHitSlice[0]];
    fE1 = fEdep[fHitSlice[0]];

    
    if ( fNhitCzt == 0 ) {
      if ( fNhitCsI != 1 ) return 0;
      fP2 = fHposCsI;
      fE2 = fEdepCsI;
      fSingle  = kTRUE;
      fPairing = CmpLib::SITCSI;
    }
    else if ( fNhitCzt == 1 ) {
      fP2 = fHposCzt;
      fE2 = fEdepCzt;
      fSingle  = kTRUE;
      fPairing = CmpLib::SITCZT;
    }
    else return 0;
  }
  else if ( fNhitSit == 2 ) {
    if ( fHitSlice.size() != 2 ) return 0;
    
    if ( fHitSlice[1]-fHitSlice[0] == 1 ) {
      fP1 = fHpos[fHitSlice[1]];
      fE1 = fEdep[fHitSlice[1]]+fEdep[fHitSlice[0]];
      
      if ( fNhitCzt == 0 ) {
	if ( fNhitCsI != 1 ) return 0;
	fP2 = fHposCsI;
	fE2 = fEdepCsI;
	fSingle  = kTRUE;
	fPairing = CmpLib::SITCSI;
      }
      else if ( fNhitCzt == 1 ) {
	fP2 = fHposCzt;
	fE2 = fEdepCzt;
	fSingle  = kTRUE;
	fPairing = CmpLib::SITCZT;
      }
      else return 0;
    }
    else {
      fP1 = fHpos[fHitSlice[1]];
      fE1 = fEdep[fHitSlice[1]];
      
      fP2 = fHpos[fHitSlice[0]];
      fE2 = fEdep[fHitSlice[0]];
      
      fDouble  = kTRUE;
      fPairing = CmpLib::SITSIT;
    }
  }
  else return 0;
  
  TVector3 vect( fP1.X()-fP2.X(), fP1.Y()-fP2.Y(), fP1.Z()-fP2.Z() );
  TVector3 unit = vect.Unit();

  fConeZen = vect.Theta();
  fConeAzi = vect.Phi  ();
  fConeAng = TMath::ACos( 1.0 - mec2*fE1/(fE2*(fE1+fE2)) );
  
  return 1;
}

void CmpBranch::SetConeAng( void )
{
  fConeAng = TMath::ACos( 1.0 - mec2*fE1/((fE0-fE1)*fE0) );
  //  fConeAng = TMath::ACos( 1.0 - mec2*(fE0-fE2)/(fE2*fE0) );
}

TVector3 CmpBranch::FindP0( Double_t phi )
{
  Double_t x1 = TMath::Sin( fConeAng )*TMath::Cos( phi );
  Double_t y1 = TMath::Sin( fConeAng )*TMath::Sin( phi );
  Double_t z1 = TMath::Cos( fConeAng );

  Double_t x2 =
    +x1*TMath::Cos(fConeAzi)*TMath::Cos(fConeZen)
    -y1*TMath::Sin(fConeAzi)
    +z1*TMath::Cos(fConeAzi)*TMath::Sin(fConeZen);
  Double_t y2 =
    +x1*TMath::Sin(fConeAzi)*TMath::Cos(fConeZen)
    +y1*TMath::Cos(fConeAzi)
    +z1*TMath::Sin(fConeAzi)*TMath::Sin(fConeZen);
  Double_t z2 =
    -x1*TMath::Sin(fConeZen)
    +z1*TMath::Cos(fConeZen);

  fP0.SetXYZ( x2, y2, z2 );

  return fP0;
}

TVector3 CmpBranch::FindP0( TVector3 pos )
{
  Double_t x = pos.X();
  Double_t y = pos.Y();
  Double_t z = pos.Z();

  Double_t x0 =
    +x*TMath::Cos(fConeZen)*TMath::Cos(fConeAzi)
    +y*TMath::Cos(fConeZen)*TMath::Sin(fConeAzi)
    -z*TMath::Sin(fConeZen);
  Double_t y0 =
    -x*TMath::Sin(fConeAzi)
    +y*TMath::Cos(fConeAzi);
  /*
  Double_t z0 =
    +x*TMath::Sin(fConeZen)*TMath::Cos(fConeAzi)
    +y*TMath::Sin(fConeZen)*TMath::Sin(fConeAzi)
    +z*TMath::Cos(fConeZen);
  */
  
  Double_t phi = 0.0;
  if ( x0 == 0.0 ) {
    if      ( y0 > 0.0 ) phi = +TMath::Pi()/2.0;
    else if ( y0 < 0.0 ) phi = -TMath::Pi()/2.0;
    else                 phi = 0.0;
  }
  else if ( x0 > 0.0 ) {
    phi = TMath::ATan( y0/x0 );
  }
  else if ( x0 < 0.0 ) {
    if      ( y0 > 0.0 ) phi = TMath::ATan( y0/x0 ) + TMath::Pi();
    else if ( y0 < 0.0 ) phi = TMath::ATan( y0/x0 ) - TMath::Pi();
    else                 phi = -TMath::Pi();
  }

  return FindP0( phi );
}

void CmpBranch::FindNhitSit( void )
{
  if ( !gCmpParams ) return;
  CmpParams *p = gCmpParams;
  
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return;

  fNhitSit = 0;
  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    Int_t lyr_x = slice*2+CmpLib::COORD_X;
    Int_t lyr_y = slice*2+CmpLib::COORD_Y;
    Int_t nhit_x = dst->GetNhits( CmpLib::D_SIT, lyr_x );
    Int_t nhit_y = dst->GetNhits( CmpLib::D_SIT, lyr_y );
    if ( nhit_x >= 1 || nhit_y >= 1 ) {
      fNhitSit++;
      if ( nhit_x == 1 && nhit_y == 1 ) {
	fHitSlice.push_back( slice );
	HitBranch *hitx = dst->GetHit( dst->GetHitID( CmpLib::D_SIT, lyr_x, 0 ) );
	HitBranch *hity = dst->GetHit( dst->GetHitID( CmpLib::D_SIT, lyr_y, 0 ) );
	// fEdep[slice] = 0.5*(hitx->GetHitEne()+hity->GetHitEne());
	fEdep[slice] = hity->GetHitEne();
	Int_t idx_x = hitx->GetIndex();
	Int_t idx_y = hity->GetIndex();
	fHpos[slice].SetXYZ( p->GetOrigin( idx_x ).X(), p->GetOrigin( idx_y ).Y(), p->GetOrigin( idx_x ).Z() );
      }
    }
  }

  return;
}

void CmpBranch::FindNhitCzt( void )
{
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return;
  
  fNhitCzt = dst->GetNhits( CmpLib::D_CZT );

  if ( fNhitCzt == 1 ) {
    Int_t hid = dst->GetHitID( CmpLib::D_CZT, 0, 0 );
    HitBranch *hit = dst->GetHit( hid );
    //    if ( !hit || hit->GetHitSta() < 4 ) return;
    if ( !hit ) return;
    fEdepCzt = hit->GetHitEne();
    fHposCzt = hit->GetHitPos();
  }
  
  return;
}

void CmpBranch::FindNhitCsI( void )
{
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return;
  
  fNhitCsI = dst->GetNhits( CmpLib::D_CSI );
  
  if ( fNhitCsI == 1 ) {
    for ( Int_t lyr = 0; lyr < CsILib::N_LAYR; lyr++ ) {
      Int_t hid = dst->GetHitID( CmpLib::D_CSI, lyr, 0 );
      if ( hid < 0 ) continue;
      HitBranch *hit = dst->GetHit( hid );
      if ( !hit ) return;
      fEdepCsI = hit->GetHitEne();
      fHposCsI = hit->GetHitPos();
    }
  }

  return;
}

void CmpBranch::Clear( Option_t *option )
{
  fP0 = CmpLib::fVnotValid;
  fP1 = CmpLib::fVnotValid;
  fP2 = CmpLib::fVnotValid;

  fE0 = 0.0;
  fE1 = 0.0;
  fE2 = 0.0;

  fSingle = kFALSE;
  fDouble = kFALSE;
  
  fPairing = -1;
  fNhitSit = 0;
  fNhitCzt = 0;
  fNhitCsI = 0;

  fConeZen = CmpLib::fDnotValid;
  fConeAzi = CmpLib::fDnotValid;
  fConeAng = CmpLib::fDnotValid;
  fConePhi = CmpLib::fDnotValid;

  fHitSlice.clear();
  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    fEdep[slice] = 0.0;
    fHpos[slice] = CmpLib::fVnotValid;
  }

  fEdepCzt = 0.0;
  fEdepCsI = 0.0;
  fHposCzt = CmpLib::fVnotValid;
  fHposCsI = CmpLib::fVnotValid;
}

void CmpBranch::Print( Option_t *option ) const
{
}
