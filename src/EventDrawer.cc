#include "EventDrawer.h"
#include "CmpTree.h"
#include "CmpLib.h"
#include "CmpParams.h"
#include "SitParams.h"
#include "DstBranch.h"
#include "TrkBranch.h"
#include "CmpBranch.h"
#include "SimBranch.h"

#include "TGraphErrors.h"
#include "TLatex.h"
#include "TColor.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TChain.h"
#include "TBox.h"
#include "TVector3.h"
#include "TF1.h"
#include "TH2.h"
#include "TMath.h"
#include "TMarker.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TGaxis.h"

#include <iostream>

ClassImp( EventDrawer )

EventDrawer::EventDrawer()
{
  iEvent = -1;
  Clear();

  fTrees = new TChain( "ttree" );
  fCtree = new CmpTree;

  fNumberOfColors = TColor::GetNumberOfColors();

  fWorld[0] = new TH2D( "world_x", "X-Z View", 1, -100., 300., 1, -200., 200. );
  //  fWorld[0] = new TH2D( "world_x", "X-Z View", 1, -100., 100., 1, -100., 0. );
  fWorld[0]->GetXaxis()->SetLabelFont( 22 );
  fWorld[0]->GetYaxis()->SetLabelFont( 22 );
  fWorld[0]->GetXaxis()->SetLabelSize( 0.040 );
  fWorld[0]->GetYaxis()->SetLabelSize( 0.040 );
  fWorld[0]->SetStats( 0 );
  
  fWorld[1] = new TH2D( "world_y", "Y-Z View", 1, -100., 300., 1, -200., 200. );
  //  fWorld[1] = new TH2D( "world_y", "Y-Z View", 1, -100., 100., 1, -100., 0. );
  fWorld[1]->GetXaxis()->SetLabelFont( 22 );
  fWorld[1]->GetYaxis()->SetLabelFont( 22 );
  fWorld[1]->GetXaxis()->SetLabelSize( 0.040 );
  fWorld[1]->GetYaxis()->SetLabelSize( 0.040 );
  fWorld[1]->SetStats( 0 );

  fLine[0] = new TLine ( 200.0, -10.0, 200.0, 190.0 );
  fLine[0]->SetLineColor( kBlue );
  fAxis[0] = new TGaxis( 120.0, -10.0, 280.0, -10.0, -0.8, 0.8, 504 );
  fAxis[0]->SetLabelFont( 22 );
  fAxis[0]->SetLabelSize( 0.035 );
  fAxis[0]->SetLabelColor( kBlue );
  fAxis[0]->SetLineColor ( kBlue );
  fAxis[0]->SetTitleColor( kBlue );
  fAxis[0]->SetTitleSize( 0.035 );
  fAxis[0]->SetTitleFont( 22 );
  fAxis[0]->SetTitle ( "[mm]" );
    
  fLine[1] = new TLine ( 200.0, -10.0, 200.0, 190.0 );
  fLine[1]->SetLineColor( kBlue );
  fAxis[1] = new TGaxis( 120.0, -10.0, 280.0, -10.0, -0.8, 0.8, 504 );
  fAxis[1]->SetLabelFont( 22 );
  fAxis[1]->SetLabelSize( 0.035 );
  fAxis[1]->SetLabelColor( kBlue );
  fAxis[1]->SetLineColor ( kBlue );
  fAxis[1]->SetTitleColor( kBlue );
  fAxis[1]->SetTitleSize( 0.035 );
  fAxis[1]->SetTitleFont( 22 );
  fAxis[1]->SetTitle ( "[mm]" );

  SetGeometry();
}

EventDrawer::~EventDrawer()
{
  for ( Int_t i = 0; i < CmpLib::N_GEOM; i++ ) {
    for ( Int_t coord = 0; coord < CmpLib::N_COORD; coord++ ) {
      if ( fSensor[coord][i] ) delete fSensor[coord][i];
    }
  }
  
  delete fLine[0];
  delete fLine[1];
  delete fAxis[0];
  delete fAxis[1];
  delete fWorld[0];
  delete fWorld[1];
  delete fTrees;
  delete fCtree;
}

Bool_t EventDrawer::AddFile( const char *in_fname )
{
  Long_t id, flags, mtime;
  Long64_t size;
  if ( gSystem->GetPathInfo( in_fname, &id, &size, &flags, &mtime ) ) {
    std::cout << "File : " << in_fname << " was not found." << std::endl;
    return kFALSE;
  }

  fTrees->Add( in_fname );
  return kTRUE;
}

Int_t EventDrawer::InitTree( void )
{
  fCtree->SetAddr( fTrees );
  return fTrees->GetEntries();
}


void EventDrawer::SetGeometry( void )
{
  if ( !gCmpParams ) CmpParams::ReadFile();
  CmpParams *p = gCmpParams;

  for ( Int_t i = 0; i < CmpLib::N_GEOM; i++ ) {
    Int_t did = p->GetDetID( i );
    Int_t pid = p->GetPriID( i );
    //    Int_t sid = p->GetSecID( i );
    TVector3 origin = p->GetOrigin( i );
    TVector3 dimens = p->GetDimens( i );
    for ( Int_t coord = 0; coord < CmpLib::N_COORD; coord++ ) {
      if ( did == CmpLib::D_SIT && pid%2 != coord ) {
	fSensor[coord][i] = 0;
      }
      else {
	Double_t orig = ( coord == 0 ) ? origin.X() : origin.Y();
	Double_t dime = ( coord == 0 ) ? dimens.X() : dimens.Y();
	fSensor[coord][i] = new TBox( orig - dime/2.0, origin.Z() - dimens.Z()/2.0,
				      orig + dime/2.0, origin.Z() + dimens.Z()/2.0 );
	if ( did == CmpLib::D_CSI )
	  fSensor[coord][i]->SetLineColor( kGreen );
	else if ( did == CmpLib::D_CZT )
	  fSensor[coord][i]->SetLineColor( kMagenta );
	else if ( did == CmpLib::D_SIT )
	  fSensor[coord][i]->SetLineColor( kBlue );
	fSensor[coord][i]->SetLineWidth( 1 );
	fSensor[coord][i]->SetFillStyle( 0 );
      }
    }
  }
}

void EventDrawer::DrawFrame( Int_t coord )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return;

  if ( !gCmpParams ) CmpParams::ReadFile();
  CmpParams *p = gCmpParams;

  if ( !gSitParams ) SitParams::ReadFile();
  SitParams *sitp = gSitParams;
  
  fWorld[coord]->Draw( "AIXS" );
  fLine [coord]->Draw( "same" );
  fAxis [coord]->Draw( "same" );

  for ( Int_t i = 0; i < CmpLib::N_GEOM; i++ ) {
    Int_t did = p->GetDetID( i );
    Int_t pid = p->GetPriID( i );
    Int_t sid = p->GetSecID( i );
    if ( fSensor[coord][i] ) {
      if ( did == CmpLib::D_CZT && ( coord == 0 && sid/4 != 0 ) ) continue;
      if ( did == CmpLib::D_CZT && ( coord == 1 && sid%4 != 0 ) ) continue;
      if ( did == CmpLib::D_CZT && ( (pid % 4) == 0 || pid > 12 ) ) continue;
      if ( did == CmpLib::D_SIT && ( pid >= 2*sitp->GetNslice() ) ) continue;
      fSensor[coord][i]->Draw( "same" );
    }
  }
}

void EventDrawer::GetNext( Int_t tsels, Int_t tmode, Double_t thre )
{
  Int_t fPairing = -1;
  
  UInt_t Eid = 0;
  Double_t ene = 0.0;
  do {
    iEvent++;
    if ( iEvent >= fTrees->GetEntries() ) return;
    fTrees->GetEntry( iEvent );
    DstBranch *dst = fCtree->GetDst();
    Eid = fCtree->GetEventID();
    ene = dst->GetEvtEne();
    CmpBranch *cmp = fCtree->GetCmp();
    fPairing = cmp->GetPairing();
  } while ( !TrigMode( tsels, tmode ) || ene < thre || Eid == 0 || fPairing < 0 );
    //  } while ( !TrigMode( tsels, tmode ) || ene < thre  || !BackSplash() );
}

void EventDrawer::GetPrev( Int_t tsels, Int_t tmode, Double_t thre )
{
  Double_t ene = 0.0;
  do {
    iEvent--;
    if ( iEvent < 0 ) return;
    fTrees->GetEntry( iEvent );
    DstBranch *dst = fCtree->GetDst();
    ene = dst->GetEvtEne();
  } while ( !TrigMode( tsels, tmode ) || ene < thre );
}

void EventDrawer::JumpEntry( Int_t entry )
{
  if ( entry >= fTrees->GetEntries() ) return;
  iEvent = entry;
  fTrees->GetEntry( iEvent );
}

Bool_t EventDrawer::TrigMode( Int_t tsels, Int_t tmode )
{
  Int_t fTrigPat = 0;

  //  UInt_t eventid = fCtree->GetEventID();
  UInt_t hitpatt = fCtree->GetHitPatt();
  for ( Int_t i = 0; i < 12; i++ ) {
    if ( hitpatt & BIT(i)    ) fTrigPat |= B_SIT;
    if ( hitpatt & BIT(i+16) ) fTrigPat |= B_SIT;
  }
  if ( hitpatt & BIT(14) ) fTrigPat |= B_CSI;
  if ( hitpatt & BIT(13) ) fTrigPat |= B_CZT;

  //  std::cout << eventid << ": " << fTrigPat << " " << tmode << " " << std::endl;

  if      ( tsels == 3 ) return kTRUE;
  else if ( tsels == 2 && ( ( fTrigPat & tmode ) == tmode ) ) return kTRUE;
  else if ( tsels == 1 && ( fTrigPat == tmode ) ) return kTRUE;
  
  return kFALSE;
}

Bool_t EventDrawer::BackSplash ( void )
{
  SimBranch *sim = fCtree->GetSim();
  if ( !sim ) return kFALSE;

  DstBranch *dst = fCtree->GetDst();
  if ( !dst ) return kFALSE;
  Int_t nhit_czt = dst->GetNhits( CmpLib::D_CZT );
  Int_t nhit_csi = dst->GetNhits( CmpLib::D_CSI );

  if ( nhit_czt == 0 || nhit_csi == 0 ) return kFALSE;

  Int_t np = sim->GetNpoint();
  Bool_t flag = kFALSE;
  Int_t prev_id = -1;
  for ( Int_t ip = 0; ip < np; ip++ ) {
    Int_t t_id = sim->GetTrackID ( ip );
    //    Int_t p_id = sim->GetParentID( ip );
    //    if ( prev_id != t_id && prev_id != p_id ) flag = kFALSE;
    if ( prev_id != t_id ) flag = kFALSE;
    prev_id = t_id;
    //    Double_t ekine = sim->GetTrajEkine( ip );
    TVector3 point = sim->GetTrajPoint( ip );
    if ( point.Z() < -100.0 ) flag = kTRUE;
    if ( flag ) {
      if ( point.Z() < -64.0 || point.Z() > -44.0 ) continue;
      if ( point.X() < -60.0 || point.X() >  30.0 ) continue;
      if ( point.Y() < -30.0 || point.Y() >  60.0 ) continue;
      return kTRUE;
    }
  }
  return kFALSE;
}

void EventDrawer::DrawEvent( Int_t coord, Int_t mode )
{
  if ( !gCmpParams ) CmpParams::ReadFile();
  CmpParams *p = gCmpParams;

  if ( iEvent < 0 ) GetNext();
  Clear();
  
  DrawFrame( coord );

  Double_t dmax[CmpLib::N_DET] = { 2048.0, 2048.0, 0.64, 32.0, 16.0 };
  
  DstBranch *dst = fCtree->GetDst();
  if ( dst ) {
    Int_t fNhits = dst->GetNhits();
    for ( Int_t i = 0; i < fNhits; i++ ) {
      HitBranch *hit = dst->GetHit( i );
      if ( !hit ) continue;
      Int_t did = hit->GetDetID();
      Int_t pid = hit->GetPriID();
      Int_t sid = hit->GetSecID();
      Double_t edep = hit->GetHitEne();
      TVector3 hpos = hit->GetHitPos();
      TVector3 perr = hit->GetPosErr();
      
      Int_t idx = p->GetIndex( did, pid, sid );
      if ( idx < 0 ) continue;
      if ( !fSensor[coord][idx] ) continue;

      if      ( did == CmpLib::D_CSI ) {
	fNhitCsiBar++;
	fCsiTotEdep += edep;
	if ( fCsiMaxEdep < edep ) fCsiMaxEdep = edep;
      }
      else if ( did == CmpLib::D_CZT ) {
	fNhitCztBar++;
	fCztTotEdep += edep;
	if ( fCztMaxEdep < edep ) fCztMaxEdep = edep;
      }
      else if ( did == CmpLib::D_SIT ) {
	fNhitSitStr[pid/2]++;
	fSitTotEdep[pid/2] += edep;
	if ( fSitMaxEdep[pid/2] < edep ) fSitMaxEdep[pid/2] = edep;
      }
      
      Int_t icol = TMath::FloorNint( fNumberOfColors*edep/dmax[did] );
      if ( icol > fNumberOfColors ) icol = fNumberOfColors;

      Int_t ipal = TColor::GetColorPalette( icol );
      
      if ( did == CmpLib::D_SIT ) {
	TVector3 origin = p->GetOrigin( idx );
	TVector3 dimens = p->GetDimens( idx );
	Double_t orig = ( coord == 0 ) ? origin.X() : origin.Y();
	Double_t dime = ( coord == 0 ) ? dimens.X() : dimens.Y();
	TBox *fHit = new TBox( orig - dime/2.0, origin.Z() - edep*10.0,
			       orig + dime/2.0, origin.Z() + edep*10.0 );
	fHit->SetLineColor( kWhite );
	fHit->SetLineWidth( 1 );
	fHit->SetFillStyle( 0 );
	fHit->Draw( "same" );
	fHitArray.AddLast( fHit );
      }
      else if ( did == CmpLib::D_CSI ) {
	TVector3 origin = p->GetOrigin( idx );
	TVector3 dimens = p->GetDimens( idx );
	if ( (pid+1)%2 == coord ) {
	  Double_t orig = ( coord == 0 ) ? origin.X() : origin.Y();
	  Double_t dime = ( coord == 0 ) ? dimens.X() : dimens.Y();
	  TBox *fHit = new TBox( orig - dime/2.05, origin.Z() - dimens.Z()/2.05,
				 orig + dime/2.05, origin.Z() + dimens.Z()/2.05 );
	  fHit->SetFillColor( ipal );
	  fHit->SetFillStyle( 1001 );
	  fHit->SetLineColor( ipal );
	  fHit->SetLineWidth( 1 );
	  fHit->Draw( "same" );
	  fHitArray.AddLast( fHit );
	}
	else {
	  Double_t orig = ( coord == 0 ) ? hpos.X() : hpos.Y();
	  Double_t dime = ( coord == 0 ) ? dimens.Y() : dimens.X();
	  TBox *fHit = new TBox( orig - dime/2.05, origin.Z() - dimens.Z()/2.05,
				 orig + dime/2.05, origin.Z() + dimens.Z()/2.05 );
	  fHit->SetFillColor( ipal );
	  fHit->SetFillStyle( 1001 );
	  fHit->SetLineColor( ipal );
	  fHit->SetLineWidth( 1 );
	  fHit->Draw( "same" );
	  fHitArray.AddLast( fHit );
	}
      }
      else if ( did == CmpLib::D_CZT ) {
	TVector3 origin = p->GetOrigin( idx );
	TVector3 dimens = p->GetDimens( idx );
	Double_t orig = ( coord == 0 ) ? origin.X() : origin.Y();
	Double_t dime = ( coord == 0 ) ? dimens.X() : dimens.Y();
	//	TBox *fHit = new TBox( orig - dime/2.5, origin.Z() - edep,
	//			       orig + dime/2.5, origin.Z() + edep );
	//	fHit->SetLineWidth( 3 );
	/*
	TMarker *fMrk = ( coord == CmpLib::COORD_X )
	  ? new TMarker( hpos.X(), hpos.Z(), 20 )
	  : new TMarker( hpos.Y(), hpos.Z(), 20 );
	fMrk->SetMarkerColor( (i==0) ? kWhite : kOrange );
	fMrk->SetMarkerSize( 1.0 );
	fMrk->Draw( "same" );
	*/
	//	std::cout << Form( "%lf %lf %lf %lf", edep, hpos.X(), hpos.Y(), hpos.Z() ) << std::endl;
	TBox *fHit = new TBox( orig - dime/5.0, origin.Z() - edep/2.0,
			       orig + dime/5.0, origin.Z() + edep/2.0 );
	fHit->SetLineWidth( 3 );
	fHit->SetLineColor( kWhite );
	fHit->SetFillStyle( 0 );
	fHit->Draw( "same" );
	fHitArray.AddLast( fHit );
	//	TBox *fHit = new TBox( orig - dime/2.1, origin.Z() - dimens.Z()/2.1,
	//			       orig + dime/2.1, origin.Z() + dimens.Z()/2.1 );
	//	TBox *fHit = new TBox( orig - dime/2.0, origin.Z() - dimens.Z()/2.0,
	//			       orig + dime/2.0, origin.Z() + dimens.Z()/2.0 );
	//	fHit->SetFillColor( ipal );
	//	fHit->SetFillStyle( 1001 );
	//	fHit->SetLineColor( ipal );
	//	fHit->SetLineColor( kWhite );
	//	fHit->SetLineWidth( 2 );
	//	fHit->Draw( "same" );
	fHitArray.AddLast( fHit );
      }
    }
  }

  DrawTrack( coord, mode );
  DrawInfo ( coord );
}

void EventDrawer::DrawInfo( Int_t coord )
{
  UInt_t fEventID = fCtree->GetEventID();  
  
  DrawText(  80.0, -60.0, Form( "EventID: 0x%8X",     fEventID    ), 0.025, kOrange );

  DrawText(  85.0, -75.0, Form( "Name" ), 0.025, kOrange );
  DrawText( 145.0, -75.0, Form( "#Hit" ), 0.025, kOrange, 31 );
  DrawText( 220.0, -75.0, Form( "Maxim Edep" ), 0.025, kOrange, 31 );
  DrawText( 300.0, -75.0, Form( "Total Edep" ), 0.025, kOrange, 31 );

  Double_t ypos = -80.0;
  for ( Int_t slice = CmpLib::N_SLICE - 1; slice >= 0; slice-- ) {
    ypos -= 10.0;
    DrawText(  85.0, ypos, Form( "Si-%X", slice ), 0.025, kOrange );
    DrawText( 145.0, ypos, Form( "%3d",          fNhitSitStr[slice] ), 0.025, kOrange, 31 );
    DrawText( 220.0, ypos, Form( "%8.3lf [MeV]", fSitMaxEdep[slice] ), 0.025, kOrange, 31 );
    DrawText( 300.0, ypos, Form( "%8.3lf [MeV]", fSitTotEdep[slice] ), 0.025, kOrange, 31 );
  }

  ypos -= 10.0;
  DrawText(  85.0, ypos, Form( "CdZnTe" ), 0.025, kOrange );
  DrawText( 145.0, ypos, Form( "%3d",          fNhitCztBar ), 0.025, kOrange, 31 );
  DrawText( 220.0, ypos, Form( "%8.3lf [MeV]", fCztMaxEdep ), 0.025, kOrange, 31 );
  DrawText( 300.0, ypos, Form( "%8.3lf [MeV]", fCztTotEdep ), 0.025, kOrange, 31 );

  ypos -= 10.0;
  DrawText(  85.0, ypos, Form( "CsI" ), 0.025, kOrange );
  DrawText( 145.0, ypos, Form( "%3d",          fNhitCsiBar ), 0.025, kOrange, 31 );
  DrawText( 220.0, ypos, Form( "%8.3lf [MeV]", fCsiMaxEdep ), 0.025, kOrange, 31 );
  DrawText( 300.0, ypos, Form( "%8.3lf [MeV]", fCsiTotEdep ), 0.025, kOrange, 31 );
}

void EventDrawer::DrawText( Double_t x, Double_t y, const char *str,
			    Double_t size, Int_t col, Int_t align,
			    Double_t angle )
{
  TLatex *tx = new TLatex( x, y, str );
  tx->SetTextAlign( align );
  tx->SetTextSize ( size  );
  tx->SetTextColor( col   );
  tx->SetTextAngle( angle );
  tx->Draw();
}

void EventDrawer::DrawTrack( Int_t coord, Int_t mode )
{
  fHitArray.Clear();
  
  DrawSimTrk( coord, mode );

  if ( mode == 1 ) {
    TLine *fit_track = FitTrack( coord );
    if ( fit_track ) {
      fit_track->SetLineColor( kRed );
      fit_track->SetLineStyle( 1 );
      fit_track->Draw( "same" );
      
      TGraphErrors *gr = new TGraphErrors();
      gr->SetMarkerStyle( 20 );
      gr->SetMarkerSize ( 0.5 );
      gr->SetLineColor( kRed );
      gr->SetMarkerColor( kRed );
      
      for ( it_type it = fRes.begin(); (it != fRes.end()); ++it ) {
	Int_t n = gr->GetN();
	gr->SetPoint( n, it->second*100.0+200.0, it->first );
	gr->SetPointError( n, 0.15*100, 1.0 );
      }
      gr->Draw( "PL" );
    }
  }
}

TLine *EventDrawer::FitTrack( Int_t coord )
{
  DstBranch *dst = fCtree->GetDst();
  if ( !dst ) return (TLine *)NULL;
  
  TrkBranch *trk = fCtree->GetTrk();
  if ( trk ) {
    if ( !trk->Found( coord ) ) return (TLine *)NULL;

    for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
      Int_t hid = trk->GetHitID( coord, slice );
      if ( hid < 0 ) continue;
      HitBranch *hit = dst->GetHit( hid );
      if ( !hit ) continue;
      TVector3 hpos = hit->GetHitPos();
      TVector3 perr = hit->GetPosErr();
      Double_t pos = hpos.Z();
      Double_t res = ( coord == 0 ) ? perr.X()   : perr.Y();
      fRes.insert( std::multimap<Double_t, Double_t>::value_type( pos, res ) );
    }
    
    Double_t fA = trk->GetA( coord );
    Double_t fB = trk->GetB( coord );

    Double_t zup =  200.0;
    Double_t zdn = -200.0;
    
    Double_t xup = fA*zup+fB;
    Double_t xdn = fA*zdn+fB;

    if ( xup < -75.0 ) {
      xup = -75.0;
      zup = (xup-fB)/fA;
    }
    else if ( xup > 75.0 ) {
      xup = 75.0;
      zup = (xup-fB)/fA;
    }
    
    if ( xdn < -75.0 ) {
      xdn = -75.0;
      zdn = (xdn-fB)/fA;
    }
    else if ( xdn > 75.0 ) {
      xdn = 75.0;
      zdn = (xdn-fB)/fA;
    }

    TLine *track = new TLine( xup, zup, xdn, zdn );
    return track;
  }
  else return (TLine *)NULL;
}

void EventDrawer::DrawSimTrk( Int_t coord, Int_t mode )
{
  if ( mode == 1 ) {
    TLine *sim_track = SimTrack( coord );
    if ( sim_track ) {
      sim_track->SetLineColor( kBlue );
      sim_track->SetLineStyle( 2 );
      //      sim_track->Draw( "same" );
    }
  }
  else if ( mode == 2 ) {
    SimBranch *sim = fCtree->GetSim();
    if ( sim ) {
      UInt_t npoint = sim->GetNpoint();
      Int_t pre_tid = -1;
      TObjArray *garray = new TObjArray();
      Int_t col[4] = { kBlue, kGreen, kRed, kOrange };
      for ( UInt_t i = 0; i < npoint; i++ ) {
	Int_t    tid   = sim->GetTrackID( i );
	//	const char *pname = sim->GetPartName( i );
	TString pname = sim->GetPartName( i );
	//	Double_t ene   = sim->GetTrajEkine( i );
	TVector3 pos   = sim->GetTrajPoint( i );
	Double_t r = ( coord == 0 ) ? pos.X() : pos.Y();
	TString hitpr = sim->GetProcName( i );
	//	std::cout << Form( "%2d: %lf %lf %lf %lf %s", i, ene, pos.X(), pos.Y(), pos.Z(), hitpr.Data() ) << std::endl;
	if ( pre_tid != tid ) {
	  pre_tid = tid;
	  TGraph *gr = new TGraph();
	  gr->SetMarkerStyle( 20 );
	  gr->SetMarkerSize( 0.1 );
	  Int_t pid = 0;
	  if   ( pname.CompareTo( "gamma" ) == 0 ) pid = 1;
	  else if ( pname.CompareTo( "e-" ) == 0 ) pid = 2;
	  else if ( pname.CompareTo( "e+" ) == 0 ) pid = 3;
	  gr->SetMarkerColor( col[pid] );
	  gr->SetLineColor  ( col[pid] );
	  gr->SetLineStyle  ( 2 );
	  garray->Add( gr );
	}
	TGraph *gr = (TGraph *)garray->Last();
	gr->SetPoint( gr->GetN(), r, pos.Z() );
      }
      for ( Int_t i = 0; i < garray->GetEntries(); i++ ) {
	TGraph *gr = (TGraph *)garray->At( i );
	if ( gr ) gr->Draw("PL");
      }
    }
  }
}

TLine *EventDrawer::GetTrack( Int_t coord, Double_t zup, Double_t zdn, TVector3 pos, TVector3 mom )
{
  Double_t r = ( coord == 0 ) ? pos.X() : pos.Y();
  Double_t m = ( coord == 0 ) ? mom.X() : mom.Y();

  if ( m == 0.0 ) {
    TLine *track = new TLine( r, zup, r, zdn );
    return track;
  }
  else if ( mom.Z() == 0.0 ) {
    TLine *track = new TLine( -100.0, pos.Z(), 100.0, pos.Z() );
    return track;
  }
  else {
    Double_t a = m / mom.Z();
    Double_t b = r - a*pos.Z();
      
    Double_t xup = a*zup+b;
    Double_t xdn = a*zdn+b;
    
    if ( xup < -100.0 ) {
      xup = -100.0;
      zup = (xup-b)/a;
    }
    else if ( xup > 100.0 ) {
      xup = 100.0;
      zup = (xup-b)/a;
    }
    
    if ( xdn < -100.0 ) {
      xdn = -100.0;
      zdn = (xdn-b)/a;
    }
    else if ( xdn > 100.0 ) {
      xdn = 100.0;
      zdn = (xdn-b)/a;
    }
    
    TLine *track = new TLine( xup, zup, xdn, zdn );
    return track;
  }
}

TLine *EventDrawer::SimTrack( Int_t coord )
{
  SimBranch *sim = fCtree->GetSim();
  if ( sim ) {
    TVector3 pos = sim->GetPosition();
    TVector3 mom = sim->GetMomentum();
    
    return GetTrack( coord, 200.0, -200.0, pos, mom );
  }
  else return (TLine *)NULL;
}

Int_t EventDrawer::GetEventID( void )
{
  //  return fCtree->GetEventID();
  return iEvent;
}

void EventDrawer::Clear( Option_t *option )
{
  fRes.clear();

  for ( Int_t i = 0; i < CmpLib::N_SLICE; i++ ) {
    fNhitSitStr[i] = 0;
    fSitMaxEdep[i] = 0.0;
    fSitTotEdep[i] = 0.0;
  }

  fNhitCztBar = 0;
  fCztMaxEdep = 0.0;
  fCztTotEdep = 0.0;

  fNhitCsiBar = 0;
  fCsiMaxEdep = 0.0;
  fCsiTotEdep = 0.0;
}
