#include "DataConvert.h"

#include "TSystem.h"
#include "TObjString.h"

#include <iostream>
#include <signal.h>

Int_t DataConvert::fKill = 0;

DataConvert::DataConvert( void )
{
  fAtrb   = 0;
  time( &fTstart );
  fPacket = new BasePacket();
  fFileSize = 0.0;
  fEidFname = "cmp_eid.dat";
  fOutFname = "cmp_cvt.dat";
  Clear();
}

DataConvert::DataConvert( UShort_t atrb )
{
  fAtrb = atrb;
  time( &fTstart );
  fPacket = new BasePacket();
  fFileSize = 0.0;
  Clear();
}

DataConvert::~DataConvert()
{
  if ( fPacket ) delete fPacket;
}

void DataConvert::SetEventID( void )
{
  Int_t a = 0;
  fEventID = fIdBuff[a+3]*256*256*256+fIdBuff[a+2]*256*256+fIdBuff[a+1]*256+fIdBuff[a+0];
}

void DataConvert::SetEpoch  ( void )
{
  Int_t a = 4;
  fEpoch = fIdBuff[a+3]*256*256*256+fIdBuff[a+2]*256*256+fIdBuff[a+1]*256+fIdBuff[a+0];
}

void DataConvert::SetEpochNS( void )
{
  Int_t a = 8;
  fEpochNS = fIdBuff[a+3]*256*256*256+fIdBuff[a+2]*256*256+fIdBuff[a+1]*256+fIdBuff[a+0];
}

Int_t DataConvert::Add( const char *in_fname )
{
  // get info of raw data file
  Long_t id, flags, mtime;
  Long64_t size;
  if ( gSystem->GetPathInfo( in_fname, &id, &size, &flags, &mtime ) ) {
    std::cout << "Raw data file, \"" << in_fname << "\""
	      << " does not exist." << std::endl;
    return 0;
  }

  if ( fTstart > (time_t)mtime ) fTstart = mtime;
  
  fFileSize += size/1024.0/1024.0;
  fList.Add( new TObjString( in_fname ) );
  
  return 1;
}

Int_t DataConvert::Open( const char *id_fname, const char *tr_fname )
{
  fEidFname = id_fname;
  fEID.open( id_fname, std::ios::in|std::ios::binary );
  if ( !fEID.is_open() ) return -1;
  
  fOutFname = tr_fname;
  fOUT.open( tr_fname, std::ios::out|std::ios::binary );
  if ( !fOUT.is_open() ) return -1;

  std::cout << "EvendID file \"" << id_fname << "\" open." << std::endl;
  std::cout << "Output file \"" << tr_fname << "\" open." << std::endl;
  fERR.open( "dump.bin", std::ios::out|std::ios::binary );
  if ( !fERR.is_open() ) return -1;
  std::cout << "Errout file \"" << "dump.bin" << "\" open." << std::endl;

  return 0;
}

Int_t DataConvert::Loop( Int_t nproc )
{
  Int_t nevt = 0;

  UInt_t fEvntID = 0;
  UInt_t fPrevID = 0;
  
  // set signal handler
  signal( SIGINT,  SigHandler );
  signal( SIGTERM, SigHandler );

  while ( nproc <= 0 || ( nevt < nproc ) ) {
    if ( fKill ) {
      std::cout << std::endl << "*** break ***, process interrupted" << std::endl;
      break;
    }

    // open raw data file
    if ( !fIN.is_open() ) {
      TObjString *str = (TObjString *)fList.At( 0 );
      if ( !str ) break;
      fIN.open( str->GetName(), std::ios::binary|std::ios::in );
      if ( !fIN.is_open() ) {
	std::cout << "Error in opening raw data file: "
		  << str->GetName() << std::endl;
	continue;
      }
      std::cout << "Open raw data file: " << str->GetName() << std::endl;
    }

    if ( int ret = Read() ) {
      if ( ret < 0 ) break;
      fEvntID = fPacket->GetEvid();
      if ( fPacket->GetEpch() > 10*24*60*60 && fTstart > (time_t)fPacket->GetEpch() )
	fTstart = (time_t)fPacket->GetEpch();
      nevt++;
      if ( fPacket->GetEvid() != 0 && fPacket->GetEvid() != 1 && fPacket->GetEvid() < 0x80000000 )
	std::cout << Form( "[Error] Event ID is less than 0x80000000: 0x%08x (0x%08x)", fEvntID, fPrevID+1 ) << std::endl;
      else
	fPacket->WritePacket( &fOUT );
      if ( nevt%1000000 == 1 ) std::cout << Form( "      %8d event processed", nevt ) << std::endl;
      if ( fEvntID >= 0x80000000 ) fPrevID = fEvntID;
    }
    else {
      if ( fIN.is_open() && fIN.eof() ) {
	fIN.close();
	fList.RemoveFirst();
      }
    }
  }

  std::cout << Form( "Total %8d event processed", nevt ) << std::endl;
  // reset signal handler
  signal( SIGINT,  SIG_DFL );
  signal( SIGTERM, SIG_DFL );

  return 1;
}

Int_t DataConvert::Close( void )
{
  if ( !fOUT.is_open() ) return -1;
  if ( Rename() != 0 )   return -1;
  fOUT.close();
  if ( !fERR.is_open() ) return -1;
  fERR.close();
  return 0;
}

Int_t DataConvert::Rename( void )
{
  std::size_t pos = fOutFname.find( ".dat" );
  std::string bfn = fOutFname.substr( 0, pos );

  struct tm *ptr = localtime( &fTstart );
  int year = ptr->tm_year+1900;
  int mon  = ptr->tm_mon+1;
  int mday = ptr->tm_mday;
  int hour = ptr->tm_hour;
  int min  = ptr->tm_min;

  std::string name = Form( "%s-%04d-%02d%02d-%02d%02d", bfn.c_str(),
			   year, mon, mday, hour, min );
  int file_num = 1;
  std::string full_name;

  int fDout;
  do {
    full_name = name+Form( "-%03d.dat", file_num++ );
    fDout = open( full_name.c_str(), O_RDONLY );
  } while( fDout > 0 );

  return std::rename( fOutFname.c_str(), full_name.c_str() );
}

void DataConvert::Clear( Option_t *option )
{
  fSize    = 0;
  fEventID = 0;
  fEpoch   = 0;
  fEpochNS = 0;
}

void DataConvert::SigHandler( int sig )
{
  fKill = 1;
}
