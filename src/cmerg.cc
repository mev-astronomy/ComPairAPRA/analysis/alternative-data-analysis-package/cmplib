#include "Packet.h"
#include "CmpLib.h"
#include "DataMerger.h"
#include "SitMerger.h"

#include "TString.h"

#include <iostream>
#include <fstream>
#include <string>

UShort_t Nattr( std::string attr )
{
  if      ( attr == "TRG" ) return PKT_ATRB_TRG;
  else if ( attr == "CZT" ) return PKT_ATRB_CZT;
  else if ( attr == "SIT" ) return PKT_ATRB_SIT;
  else if ( attr == "SI0" ) return PKT_ATRB_SI0;
  else if ( attr == "SI1" ) return PKT_ATRB_SI1;
  else if ( attr == "SI2" ) return PKT_ATRB_SI2;
  else if ( attr == "SI3" ) return PKT_ATRB_SI3;
  else if ( attr == "SI4" ) return PKT_ATRB_SI4;
  else if ( attr == "SI5" ) return PKT_ATRB_SI5;
  else if ( attr == "SI6" ) return PKT_ATRB_SI6;
  else if ( attr == "SI7" ) return PKT_ATRB_SI7;
  else if ( attr == "SI8" ) return PKT_ATRB_SI8;
  else if ( attr == "SI9" ) return PKT_ATRB_SI9;
  else if ( attr == "CSI" ) return PKT_ATRB_CSI;
  else return 0;
}

void usage( void )
{
  std::cout << std::endl;
  std::cout << "Usage: cmerg (options) [raw_file].." << std::endl;
  std::cout << std::endl;
  std::cout << "Merge modes:" << std::endl;
  std::cout << "  SIT        SiT                                 " << std::endl;
  std::cout << "  CMP        CMP                                 " << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "  -o FILE       set output file name as FILE     " << std::endl;
  std::cout << "  -a ATTR FILE  set input file name as FILE      " << std::endl;
  std::cout << "  -n N          process only N events            " << std::endl;
  std::cout << "  -h            show this help                   " << std::endl;
  std::cout << std::endl;
}

int main( int argc, char *argv[] )
{
  std::cout << "======================================================" << std::endl;
  std::cout << " ComPair CZT ROOT converter (ebuild)                  " << std::endl;
  std::cout << Form( " $Revision: 1.1.1.1 $, last update: %s %s", __DATE__, __TIME__ ) << std::endl;
  std::cout << "======================================================" << std::endl;

  // set default parameters
  std::string ifn = "trg_cvt.dat";
  std::string ofn = "cmp_cvt.dat";
  int nevt = 0;
  std::string attr;

  // create main object
  DataMerger *cmerg;

  // argument analysis
  std::string mode = argv[1];

  if      ( mode == "CMP" ) {
    cmerg = new DataMerger();
    ofn   = "cmp_cvt.dat";
  }
  else if ( mode == "SIT" ) {
    cmerg = new SitMerger();
    ofn   = "sit_cvt.dat";
  }
  
  if ( argc == 2 && argv[1][0] == '-' && argv[1][1] == 'h' ) {
    usage();
    return 0;
  }
  else if ( argc < 2 ) {
    std::cout << std::endl;
    std::cout << "You need at least two arguments. " << std::endl;
    std::cout << "Try \"ebuild -h\" for more information" << std::endl;
    return 0;
  }

  // option analysis
  for ( int iarg = 2; iarg < argc; iarg++ ) {
    if ( argv[iarg][0] == '-' && argv[iarg][1] == 'o' ) {
      if ( argc > iarg+1 ) ofn = argv[++iarg];
    }
    
    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'n' ) {
      if ( argc > iarg+1 ) nevt = atoi( argv[++iarg] );
    }
    
    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'a' ) {
      if ( argc > iarg+1 ) attr = argv[++iarg];
      if ( argc > iarg+1 ) ifn  = argv[++iarg];
      cmerg->Add( Nattr( attr ), ifn.c_str() );
    }
    
    else if ( argv[iarg][0] == '-' )
      std::cout << Form( "Unknown option: %s", argv[iarg] ) << std::endl;
  }
  
  if ( nevt > 0 ) std::cout << Form( ", Nevt=%d", nevt );
  std::cout << std::endl;

  // open files
  if ( cmerg->Open( ofn.c_str() ) < 0 ) return -1;

  // start process
  cmerg->Loop( nevt );
  cmerg->Close();
  
  return 0;
}
