#include "CsILib.h"

#include "TString.h"

ClassImp( CsILib )

Int_t    CsILib::fThreshold = 0;
TVector3 CsILib::fVnotValid = TVector3( -99999.9, -99999.9, -99999.9 );
Double_t CsILib::fDnotValid = -99999.9;
Float_t  CsILib::fFnotValid = -99999.9;

Double_t CsILib::fCsIWidth  = 16.7;
Double_t CsILib::fCsIHeight = 16.7;
Double_t CsILib::fCsILength = 100.0;

CsILib::CsILib()
{
}

CsILib::~CsILib()
{
}
