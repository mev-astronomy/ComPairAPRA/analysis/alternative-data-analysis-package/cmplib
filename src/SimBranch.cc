#include "SimBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"

ClassImp( SimBranch )

SimBranch::SimBranch( void )
{
  Clear();
}

SimBranch::~SimBranch()
{
}

void SimBranch::SetHitPrName( Int_t idx, TString name )
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return;
  fHitPrName[idx] = name;
}

void SimBranch::Clear( Option_t *option )
{
  fParticle = -1;
  fAtomNum  = -1;
  fAtomMass = -1;
  fEkinetic = CmpLib::fDnotValid;
  fPosition = CmpLib::fVnotValid;
  fMomentum = CmpLib::fVnotValid;
  fHitPrName.clear();
  
  fTrackID.clear();
  fParentID.clear();
  fProcName.clear();
  fPartName.clear();
  fTrajEkine.clear();
  fTrajPoint.clear();
}

void SimBranch::Print( Option_t *option ) const
{
}
