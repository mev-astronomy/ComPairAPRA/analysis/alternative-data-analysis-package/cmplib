#include "CztEventID.h"

#include "TMath.h"
#include "TCanvas.h"
#include "TGraph.h"
#include "TH2.h"

#include <iostream>

CztEventID::CztEventID( void )
{
  fReqTime  = 10.0;
  fSysClock = 10.0e-9;
  
  fAtrb = PKT_ATRB_CZT;
  fCztPacket = new CztPacket();
}

CztEventID::CztEventID( UShort_t atrb ) : DataEventID( atrb )
{
  fReqTime = 10.0;
  fSysClock = 10.0e-9;

  fCztPacket = new CztPacket();
}

CztEventID::~CztEventID()
{
  if ( fCztPacket ) delete fCztPacket;
}

void CztEventID::SetTimeOffset( double oft )
{
  fCztPacket->SetToffset( oft );
}

Int_t CztEventID::Branch( void )
{
  if ( !fTree ) return -1;
  fTree->Branch( "fUtcTime", &fUtcTime, "fUtcTime/D" );
  fTree->Branch( "fEventID", &fEventID, "fEventID/i" );
  fTree->Branch( "fTmStamp", &fTmStamp, "fTmStamp/i" );
  fTree->Branch( "fTimePps", &fTimePps, "fTimePps/i" );
  return 0;
}

Int_t CztEventID::Process( void )
{
  fTree->GetEvent( fTree->GetEntries()-1 );
  Double_t fTended = fUtcTime;

  std::vector<Int_t> ipoint;
  ipoint.clear();

  // GetRang
  Double_t pre_time = -1.0;
  UInt_t pre_pps = 0;
  Bool_t first = kTRUE;
  for ( Int_t i = 0; i < fTree->GetEntries(); i++ ) {
    fTree->GetEvent( i );
    if ( first || pre_pps != fTimePps ) {
      ipoint.push_back( i );
      pre_pps = fTimePps;
      pre_time = fUtcTime;
      first = kFALSE;
    }
  }
  
  Int_t enough = TMath::Nint( ( fTended - pre_time ) );
  if ( !enough ) {
    ipoint.pop_back();
  }

  Int_t np = ipoint.size();
  if ( np == 0 ) np = 1;

  Int_t *fIevt = new Int_t[np+1];
  for ( Int_t ip = 0; ip < np; ip++ ) {
    fIevt[ip] = ipoint[ip];
  }
  fIevt[np] = fTree->GetEntries();

  // GetSync
  Int_t  *fB   = new Int_t [np];
  UInt_t *fPps = new UInt_t[np];
  
  for ( Int_t ip = 0; ip < np; ip++ ) {
    fTree->GetEvent( fIevt[ip] );
    Double_t sync = 0.0;
    UInt_t   nclk = 0;
    UInt_t   pps0 = fTimePps;
    Double_t tim0 = fUtcTime;
    for ( Int_t ievt = fIevt[ip]; ievt < fIevt[ip+1]; ievt++ ) {
      fTree->GetEvent( ievt );
      UInt_t   fraw = fTmStamp;
      Double_t time = fUtcTime;
      UInt_t   pps1 = fTimePps;

      if ( fraw < pps1 && pps1-fraw < 0x10000 ) continue;

      if ( fraw < pps0 ) nclk = 0x100000000 + fraw - pps0;
      else               nclk = fraw - pps0;
      Double_t diff = time - tim0 - nclk*fSysClock;
      if ( diff < sync ) sync = diff;
    }
    sync += tim0;
    fB  [ip] = TMath::Nint( sync );
    fPps[ip] = pps0;
  }

  // GetCorr
  Double_t *fA    = new Double_t[np];
  UInt_t   *fNpps = new UInt_t  [np];

  for ( Int_t ip = 1; ip < np; ip++ ) {
    fNpps[ip] = 0;
    UInt_t nclk = fPps[ip]-fPps[ip-1];
    fNpps[ip] = TMath::Nint(nclk*fSysClock);
    fA[ip] = ( fNpps[ip] == 0 || nclk == 0 ) ? 1.0 : fNpps[ip]/(nclk*fSysClock);
  }

  if ( np <= 1 ) {
    fNpps[0] = 0;
    fA   [0] = 0.0;
  }
  else {
    fNpps[0] = fNpps[1];
    fA   [0] = fA   [1];
  }
  
  TGraph *gr = new TGraph();
  
  for ( Int_t ip = 0; ip < np; ip++ ) {
    for ( Int_t ievt = fIevt[ip]; ievt < fIevt[ip+1]; ievt++ ) {
      fTree->GetEvent( ievt );

      fVevid.push_back( fEventID );

      // UInt_t nclk = ( fTmStamp > fPps[ip] ) ? fTmStamp-fPps[ip] : 0x100000000+fTmStamp-fPps[ip];
      Int_t  mclk = ( fTmStamp + 0x10000 > fPps[ip] ) ? fTmStamp-fPps[ip] : 0x100000000+fTmStamp-fPps[ip];
      Double_t fUtcCorr = ( fA[ip] == 0.0 ) ? fUtcTime*1.0e+9 : mclk*fSysClock*fA[ip]*1.0e+9+fB[ip]*1.0e+9;
      UInt_t fEpoch   = TMath::FloorNint( fUtcCorr*1.0e-9 );
      UInt_t fEpochNS = TMath::FloorNint( fUtcCorr - fEpoch*1.0e+9 );
      
      fVepch.push_back( fEpoch );
      fVepns.push_back( fEpochNS );
      
      gr->SetPoint( gr->GetN(), ievt, fUtcTime-fEpoch-fEpochNS*1.0e-9 );
      if ( TMath::Abs( fUtcTime-fEpoch-fEpochNS*1.0e-9 ) > 20.0 ) {
	std::cout << Form( "%8d: %10u %10u %10u %10.8lf %9d %18.9lf %18.9lf",
			   ip, fTmStamp, fPps[ip], fTmStamp-fPps[ip], fA[ip], fB[ip], fUtcTime, fUtcTime-fEpoch-fEpochNS*1.0e-9 ) << std::endl;
      }
    }
  }

  /*
  TCanvas *c1 = new TCanvas( "c1", "c1" );
  TH2D *hist = new TH2D( "hist", "Time diff. [sec]", 1, -1000.0, fIevt[np]+1000.0, 1, -1.0, 1.0 );
  hist->SetStats( 0 );
  hist->GetXaxis()->SetTitle( "Event in file" );
  hist->Draw();
  gr->SetMarkerStyle( 20 );
  gr->SetMarkerSize( 0.25 );
  gr->Draw("P");
  c1->SaveAs( "diff.png" );
  delete c1;
  delete hist;
  */

  delete gr;
  
  ipoint.clear();
  
  delete [] fIevt;
  delete [] fA;
  delete [] fNpps;
  delete [] fB;
  delete [] fPps;
  return 0;
}

void CztEventID::SetUtcTime( void )
{
  fUtcTime = fCztPacket->GetEpoch()*1.0 + fCztPacket->GetEpochNS()*1.0e-9;
}

void CztEventID::SetEventID( void )
{
  fEventID = fCztPacket->GetEventID();
}

void CztEventID::SetTmStamp( void )
{
  fTmStamp = fCztPacket->GetTmStamp();
}

void CztEventID::SetTimePps( void )
{
  fTimePps = fCztPacket->GetTimePps();
}

Int_t CztEventID::Read( void )
{
  Clear();

  fSize = fCztPacket->ReadPacket( &fIN );

  if ( fSize <= 0 ) return fSize;

  SetUtcTime();
  SetEventID();
  SetTmStamp();
  SetTimePps();
  
  //  std::cout << Form( "%8x: %8x %8x %18.9lf", fEventID, fTmStamp, fTimePps, fUtcTime ) << std::endl;
  return 1;
}

void CztEventID::Clear( Option_t *option )
{
  fUtcTime = 0.0;
  fEventID = 0;
  fTmStamp = 0;
  fTimePps = 0;
}
