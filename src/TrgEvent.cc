#include "TrgEvent.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"

#include "TFile.h"

ClassImp( TrgEvent )

TrgEvent::TrgEvent( void )
{
  for ( Int_t i = 0; i < N_DATA; i++ ) fEvtPrev[i] = 0;
  Clear();
}

TrgEvent::~TrgEvent()
{
}

Int_t TrgEvent::Fill( BasePacket *pkt )
{
  unsigned char *a = pkt->GetData();
  SetEvent( (unsigned int *)(a+10) );
  return 1;
}

void TrgEvent::Print( Option_t *option ) const
{
}

TrgEvent& TrgEvent::operator= (const TrgEvent &trg)
{
  fEventID = trg.fEventID;
  fNtrgLck = trg.fNtrgLck;
  fNclkPps = trg.fNclkPps;
  fNclkRaw = trg.fNclkRaw;
  fNclkEna = trg.fNclkEna;
  fNclkLiv = trg.fNclkLiv;
  fNgpsPPS = trg.fNgpsPPS;
  fCoHitRw = trg.fCoHitRw;
  fCoHitCD = trg.fCoHitCD;
  fHitPatt = trg.fHitPatt;
  //  fTsecond = trg.fTsecond;
  //  fTmicros = trg.fTmicros;
  fNtrgAck = trg.fNtrgAck;
  fNswtAck = trg.fNswtAck;
  fNextAck = trg.fNextAck;
  fNlpcAck = trg.fNlpcAck;
  fNhpcAck = trg.fNhpcAck;
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    fNcoMode[i] = trg.fNcoMode[i];
    fNcdMode[i] = trg.fNcdMode[i];
  }
  fNclkACD = trg.fNclkACD;
  fNclkCZT = trg.fNclkCZT;
  fNclkCs1 = trg.fNclkCs1;
  fNclkCs2 = trg.fNclkCs2;
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNclkSiT[i] = trg.fNclkSiT[i];
  }
  fNhitACD = trg.fNhitACD;
  fNoneSiT = trg.fNoneSiT;
  fNandSiT = trg.fNandSiT;
  fNtwoSiT = trg.fNtwoSiT;
  fNhitCZT = trg.fNhitCZT;
  fN1stCsI = trg.fN1stCsI;
  fN2ndCsI = trg.fN2ndCsI;
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNhitXsi[i] = trg.fNhitXsi[i];
    fNhitYsi[i] = trg.fNhitYsi[i];
  }

  memcpy( (void *)&fEvtPrev, (void *)&trg.fEvtPrev, N_DATA*4 );
  return *this;
}

unsigned int TrgEvent::GetNtrgLck( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[1];
  else return fNtrgLck;
}

unsigned int TrgEvent::GetNclkPps( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[2];
  else return fNclkPps;
}

unsigned int TrgEvent::GetNclkRaw( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[3];
  else return fNclkRaw;
}

unsigned int TrgEvent::GetNclkEna( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[4];
  else return fNclkEna;
}

unsigned int TrgEvent::GetNclkLiv( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[5];
  else return fNclkLiv;
}


unsigned int TrgEvent::GetNgpsPPS( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[6];
  else return fNgpsPPS;
}

unsigned int TrgEvent::GetNtrgAck( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[9];
  else return fNtrgAck;
}

unsigned int TrgEvent::GetNswtAck( Bool_t raw ) const
{
  if ( raw ) return GetLSI(fEvtPrev[10]);
  else return fNswtAck;
}

unsigned int TrgEvent::GetNextAck( Bool_t raw ) const
{
  if ( raw ) return GetMSI(fEvtPrev[10]);
  else return fNextAck;
}

unsigned int TrgEvent::GetNlpcAck( Bool_t raw ) const
{
  if ( raw ) return GetLSI(fEvtPrev[11]);
  else return fNlpcAck;
}

unsigned int TrgEvent::GetNhpcAck( Bool_t raw ) const
{
  if ( raw ) return GetMSI(fEvtPrev[11]);
  else return fNhpcAck;
}

unsigned int TrgEvent::GetNcoMode( int mode, Bool_t raw ) const
{
  if ( mode < 0 || mode >= N_MODE ) return 0;
  if ( raw ) return GetLSI( fEvtPrev[12+mode] );
  else return fNcoMode[mode];
}

unsigned int TrgEvent::GetNcdMode( int mode, Bool_t raw ) const
{
  if ( mode < 0 || mode >= N_MODE ) return 0;
  if ( raw ) return GetMSI( fEvtPrev[12+mode] );
  else return fNcdMode[mode];
}

unsigned int TrgEvent::GetNclkACD( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[28];
  else return fNclkACD;
}

unsigned int TrgEvent::GetNclkCZT( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[29];
  else return fNclkCZT;
}

unsigned int TrgEvent::GetNclkCs1( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[30];
  else return fNclkCs1;
}

unsigned int TrgEvent::GetNclkCs2( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[31];
  else return fNclkCs2;
}

unsigned int TrgEvent::GetNclkSiT( int sitr, Bool_t raw ) const
{
  if ( sitr < 0 || sitr >= N_SITR ) return 0;
  if ( raw ) return fEvtPrev[32+sitr];
  else return fNclkSiT[sitr];
}

unsigned int TrgEvent::GetNhitACD( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[44];
  else return fNhitACD;
}

unsigned int TrgEvent::GetNoneSiT( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[45];
  else return fNoneSiT;
}

unsigned int TrgEvent::GetNandSiT( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[46];
  else return fNandSiT;
}

unsigned int TrgEvent::GetNtwoSiT( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[47];
  else return fNtwoSiT;
}

unsigned int TrgEvent::GetNhitCZT( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[48];
  else return fNhitCZT;
}

unsigned int TrgEvent::GetN1stCsI( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[49];
  else return fN1stCsI;
}

unsigned int TrgEvent::GetN2ndCsI( Bool_t raw ) const
{
  if ( raw ) return fEvtPrev[50];
  else return fN2ndCsI;
}

unsigned int TrgEvent::GetNhitXsi( int sitr, Bool_t raw ) const
{
  if ( sitr < 0 || sitr >= N_SITR ) return 0;
  if ( raw ) return GetLSI( fEvtPrev[51+sitr] );
  else return fNhitXsi[sitr];
}

unsigned int TrgEvent::GetNhitYsi( int sitr, Bool_t raw ) const
{
  if ( sitr < 0 || sitr >= N_SITR ) return 0;
  if ( raw ) return GetMSI( fEvtPrev[51+sitr] );
  else return fNhitYsi[sitr];
}

void TrgEvent::SetEvent( unsigned int *data )
{
  fEventID = *(data+1);
  fNtrgLck = *(data+1)-fEvtPrev[1];
  if ( fNtrgLck > 4294900000.0 ) {
    std::cout << "*(data+1) = " << *(data+1) << ", fEvtPref[1] = " << fEvtPrev[1] << std::endl;
  }
  fNclkPps = *(data+2)-fEvtPrev[2];
  fNclkRaw = *(data+3)-fEvtPrev[3];
  fNclkEna = *(data+4)-fEvtPrev[4];
  fNclkLiv = *(data+5)-fEvtPrev[5];
  fNgpsPPS = *(data+6)-fEvtPrev[6];
  fCoHitRw = GetLSI( *(data+7) );
  fCoHitCD = GetMSI( *(data+7) );
  fHitPatt = *(data+8);
  //  fTsecond = *(data+9);
  //  fTmicros = *(data+10);
  fNtrgAck = *(data+9) - fEvtPrev[9];
  fNswtAck = ( 0xFFFF & ( GetLSI(*(data+10)) - GetLSI(fEvtPrev[10]) ) );
  fNextAck = ( 0xFFFF & ( GetMSI(*(data+10)) - GetMSI(fEvtPrev[10]) ) );
  fNlpcAck = ( 0xFFFF & ( GetLSI(*(data+11)) - GetLSI(fEvtPrev[11]) ) );
  fNhpcAck = ( 0xFFFF & ( GetMSI(*(data+11)) - GetMSI(fEvtPrev[11]) ) );
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    fNcdMode[i] = (0xFFFF & (GetMSI( *(data+12+i) ) - GetMSI( fEvtPrev[12+i] )) );
    fNcoMode[i] = (0xFFFF & (GetLSI( *(data+12+i) ) - GetLSI( fEvtPrev[12+i] )) );
  }
  fNclkACD = *(data+28)-fEvtPrev[28];
  fNclkCZT = *(data+29)-fEvtPrev[29];
  fNclkCs1 = *(data+30)-fEvtPrev[30];
  fNclkCs2 = *(data+31)-fEvtPrev[31];
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNclkSiT[i] = *(data+32+i) - fEvtPrev[32+i];
  }
  fNhitACD = *(data+44)-fEvtPrev[44];
  fNoneSiT = *(data+45)-fEvtPrev[45];
  fNandSiT = *(data+46)-fEvtPrev[46];
  fNtwoSiT = *(data+47)-fEvtPrev[47];
  fNhitCZT = *(data+48)-fEvtPrev[48];
  fN1stCsI = *(data+49)-fEvtPrev[49];
  fN2ndCsI = *(data+50)-fEvtPrev[50];
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNhitXsi[i] = ( 0xFFFF & ( GetLSI(*(data+51+i)) - GetLSI(fEvtPrev[51+i]) ) );
    fNhitYsi[i] = ( 0xFFFF & ( GetMSI(*(data+51+i)) - GetMSI(fEvtPrev[51+i]) ) );
  }
  
  memcpy( (void *)&fEvtPrev, (void *)data, N_DATA*4 );
}

void TrgEvent::Clear( Option_t *option )
{
  fEventID = 0;
  fNtrgLck = 0;
  fNclkPps = 0;
  fNclkRaw = 0;
  fNclkEna = 0;
  fNclkLiv = 0;
  fNgpsPPS = 0;
  fCoHitRw = 0;
  fCoHitCD = 0;
  fHitPatt = 0;
  //  fTsecond = 0;
  //  fTmicros = 0;
  fNtrgAck = 0;
  fNswtAck = 0;
  fNextAck = 0;
  fNlpcAck = 0;
  fNhpcAck = 0;
  for ( Int_t i = 0; i < N_MODE; i++ ) {
    fNcoMode[i] = 0;
    fNcdMode[i] = 0;
  }
  fNclkACD = 0;
  fNclkCZT = 0;
  fNclkCs1 = 0;
  fNclkCs2 = 0;
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    fNclkSiT[i] = 0;
  }
  fNhitACD = 0;
  fNoneSiT = 0;
  fNandSiT = 0;
  fNtwoSiT = 0;
  fNhitCZT = 0;
  fN1stCsI = 0;
  fN2ndCsI = 0;
  for ( Int_t i = 0; i < N_SITR; i++ )
    fNhitXsi[i] = fNhitYsi[i] = 0;
}

void TrgEvent::WriteCsv( std::ofstream& myFile, TString fTime )
{
  myFile << Form( "%s, "   , fTime.Data() );                // fTime
  myFile << Form( "%10u, " , fEventID | 1 << 31);           // fEventID
  myFile << Form( "%10u, " , fEvtPrev[1] );                 // fNtrgLck
  myFile << Form( "%10u, " , fNclkPps );                    // fNclkPps
  myFile << Form( "%10u,"  , fEvtPrev[3] );                 // fNclkRaw
  myFile << Form( "%10u,"  , fEvtPrev[4] );                 // fNclkEna
  myFile << Form( "%10u,"  , fEvtPrev[5] );                 // fNclkLiv
  myFile << Form( "%10u,"  , fEvtPrev[6] );                 // fNgpsPPS

  myFile << Form( "%10u,"  , fCoHitRw );                    // fCoHitRw
  myFile << Form( "%10u,"  , fCoHitCD );                    // fCoHitCD
  myFile << Form( "%10u,"  , fHitPatt );                    // fHitPatt
  //  myFile << Form( "%10u,"  , fTsecond );                    // fTsecond
  //  myFile << Form( "%10u,"  , fTmicros );                    // fTmicros

  myFile << Form( "%10u,"  , fEvtPrev[9] );                 // fNtrgAck
  myFile << Form( "%10u,"  , GetLSI( fEvtPrev[10] ) );      // fNswtAck
  myFile << Form( "%10u,"  , GetMSI( fEvtPrev[10] ) );      // fNextAck
  myFile << Form( "%10u,"  , GetLSI( fEvtPrev[11] ) );      // fNlpcAck
  myFile << Form( "%10u,"  , GetMSI( fEvtPrev[11] ) );      // fNhpcAck

  for ( Int_t i = 0; i < N_MODE; i++ ) {
    myFile << Form( "%10u,"  , GetMSI( fEvtPrev[12+i] ) );  // fNcoMode[i]
    myFile << Form( "%10u,"  , GetLSI( fEvtPrev[12+i] ) );  // fNcdMode[i]
  }

  myFile << Form( "%10u,"  , fEvtPrev[28] );                // fNclkACD
  myFile << Form( "%10u,"  , fEvtPrev[29] );                // fNclkCZT
  myFile << Form( "%10u,"  , fEvtPrev[30] );                // fNclkCs1
  myFile << Form( "%10u,"  , fEvtPrev[31] );                // fNclkCs2
  for ( Int_t i = 0; i < N_SITR; i++ ) {
    myFile << Form( "%10u,"  , fEvtPrev[32+i] );            // fNclkSiT[i]
  }
  
  myFile << Form( "%10u,"  , fEvtPrev[44] );                // fNhitACD
  myFile << Form( "%10u,"  , fEvtPrev[45] );                // fNoneSiT
  myFile << Form( "%10u,"  , fEvtPrev[46] );                // fNandSiT
  myFile << Form( "%10u,"  , fEvtPrev[47] );                // fNtwoSiT
  myFile << Form( "%10u,"  , fEvtPrev[48] );                // fNhitCZT
  myFile << Form( "%10u,"  , fEvtPrev[49] );                // fN1stCsI
  myFile << Form( "%10u,"  , fEvtPrev[50] );                // fN2ndCsI

  for ( Int_t i = 0; i < N_SITR; i++ ) {
    myFile << Form( "%10u,"  , GetLSI( fEvtPrev[51+i] ) );  // fNhitXsi[i]
    myFile << Form( "%10u,"  , GetMSI( fEvtPrev[51+i] ) );  // fNhitYsi[i]
  }

  myFile << Form( "%10u"   , 0xFFFFFFFF ) << "\n";          // fEoC

  return;
}
