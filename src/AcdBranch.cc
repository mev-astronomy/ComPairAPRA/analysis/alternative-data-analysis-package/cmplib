#include "AcdBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"

#include "TFile.h"

ClassImp( AcdBranch )

AcdBranch::AcdBranch( void )
{
  Clear();
}

AcdBranch::~AcdBranch()
{
}

Int_t AcdBranch::Process( Int_t sel )
{
  // The main method of AcdBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;
  
  Clear();
  return 1;
}

void AcdBranch::Clear( Option_t *option )
{
}

void AcdBranch::Print( Option_t *option ) const
{
}
