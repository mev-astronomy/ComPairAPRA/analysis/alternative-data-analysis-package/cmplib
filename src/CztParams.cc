#include "CztParams.h"
#include "CztLib.h"
#include "CmpTree.h"

#include "TFile.h"
#include "TMath.h"

ClassImp( CztParams )

CztParams *gCztParams = 0;

CztParams::CztParams()
{
  for ( Int_t idx = 0; idx < CztLib::N_INDX; idx++ ) {
    fAssign[idx] = -1;
  }
  
  for ( Int_t aid = 0; aid < CztLib::N_ASIC; aid++ ) {
    for ( Int_t idx = 0; idx < CztLib::N_INDX; idx++ ) {
      fDead[aid][idx] = kFALSE;
    }
  }

  for ( Int_t aid = 0; aid < CztLib::N_ASIC; aid++ ) {
    fRotate[aid] = kFALSE;

    for ( Int_t cid = 0; cid < CztLib::N_CZTB; cid++ ) {
      fOrigin  [aid][cid] = CztLib::fVnotValid;
      fDimens  [aid][cid] = CztLib::fVnotValid;
      fUsable  [aid][cid] = kTRUE;
      
      fLminPT  [aid][cid] = CztLib::fDnotValid;
      fLmaxPT  [aid][cid] = CztLib::fDnotValid;

      fLminPE  [aid][cid] = CztLib::fDnotValid;
      fLmaxPE  [aid][cid] = CztLib::fDnotValid;

      fPosHistE[aid][cid] = 0;
      fPosHistT[aid][cid] = 0;
      
      fDepHistE[aid][cid] = 0;
      fDepHistT[aid][cid] = 0;

      fMapHistE[aid][cid] = 0;
      fMapHistT[aid][cid] = 0;

      fConforX [aid][cid] = 0;
      fConforY [aid][cid] = 0;
      fConforM [aid][cid] = 0;
      fConforR [aid][cid] = 0;
    }
  }

  for ( Int_t aid = 0; aid < CztLib::N_ASIC; aid++ ) {
    fTempOffs[aid] = 21.5;
    fTempGain[aid] = 0.0350;
    fTempBase[aid] = 12366.0;
  }
  
  for ( Int_t aid = 0; aid < CztLib::N_ASIC; aid++ ) {
    for ( Int_t idx = 0; idx < CztLib::N_INDX; idx++ ) {
      fPeds[aid][idx] = 0;
      fThre[aid][idx] = 0;
      fGain[aid][idx] = 0;
    }
  }

  gCztParams = this;
}

CztParams::~CztParams()
{
  gCztParams = 0;

  for ( Int_t aid = 0; aid < CztLib::N_ASIC; aid++ ) {
    for ( Int_t idx = 0; idx < CztLib::N_INDX; idx++ ) {
      if ( fPeds[aid][idx] ) delete fPeds[aid][idx];
      if ( fThre[aid][idx] ) delete fThre[aid][idx];
      if ( fGain[aid][idx] ) delete fGain[aid][idx];
    }
  }

  for ( Int_t aid = 0; aid < CztLib::N_ASIC; aid++ ) {
    for ( Int_t cid = 0; cid < CztLib::N_CZTB; cid++ ) {
      if ( fPosHistE[aid][cid] ) delete fPosHistE[aid][cid];
      if ( fPosHistT[aid][cid] ) delete fPosHistT[aid][cid];
      if ( fDepHistE[aid][cid] ) delete fDepHistE[aid][cid];
      if ( fDepHistT[aid][cid] ) delete fDepHistT[aid][cid];
      if ( fMapHistE[aid][cid] ) delete fMapHistE[aid][cid];
      if ( fMapHistT[aid][cid] ) delete fMapHistT[aid][cid];
      if ( fConforX [aid][cid] ) delete fConforX [aid][cid];
      if ( fConforY [aid][cid] ) delete fConforY [aid][cid];
      if ( fConforM [aid][cid] ) delete fConforM [aid][cid];
      if ( fConforR [aid][cid] ) delete fConforR [aid][cid];
    }
  }
}

Int_t CztParams::SetAssign( Int_t idx, Int_t chn )
{
  if ( idx < 0 || idx >= CztLib::N_INDX ) return -1;
  if ( chn < 0 || chn >= CztLib::N_CHAN ) return -1;

  fAssign[idx] = chn;

  return 1;
}

Int_t CztParams::SetDead( Int_t aid, Int_t idx, Bool_t dead )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return -1;
  if ( idx < 0 || idx >= CztLib::N_INDX ) return -1;

  fDead[aid][idx] = dead;

  return 1;
}

Int_t CztParams::SetRotate( Int_t aid, Bool_t rot )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return -1;
  fRotate[aid] = rot;

  return 1;
}

Int_t CztParams::SetOrigin( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t z )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return -1;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return -1;

  fOrigin[aid][cid] = TVector3( x, y, z );

  return 1;
}

Int_t CztParams::SetDimens( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t z )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return -1;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return -1;
  
  fDimens[aid][cid] = TVector3( x, y, z );

  return 1;
}

Int_t CztParams::SetUsable( Int_t aid, Int_t cid, Bool_t usable )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return -1;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return -1;
  
  fUsable[aid][cid] = usable;

  return 1;
}

CztParams *CztParams::GetPtr( void )
{
  return gCztParams;
}

CztParams *CztParams::ReadFile( const char *fname, Int_t verb )
{
  CztParams *par = (CztParams *)DetParams::ReadFile( fname, "CztParams" );
  if ( par && verb ) std::cout << "Read CztParams from " << fname << std::endl;

  return par;
}

Int_t CztParams::GetAssign( Int_t idx ) const
{
  if ( idx < 0 || idx >= CztLib::N_INDX ) return -1;
  return fAssign[idx];
}

Int_t CztParams::GetIndex( Int_t cid, Int_t typ, Int_t pol ) const
{
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return -1;
  if ( typ < 0 || typ >= CztLib::N_TYPE ) return -1;
  if ( pol < 0 || pol >= CztLib::N_POLA ) return -1;

  return (cid*CztLib::N_TYPE+typ)*CztLib::N_POLA+pol;
}

Bool_t CztParams::IsDead( Int_t aid, Int_t idx ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return kTRUE;
  if ( idx < 0 || idx >= CztLib::N_INDX ) return kTRUE;

  return fDead[aid][idx];
}

Bool_t CztParams::IsRotate( Int_t aid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return kFALSE;
  return fRotate[aid];
}

TVector3 CztParams::GetOrigin( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fVnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fVnotValid;
  
  return fOrigin[aid][cid];
}

TVector3 CztParams::GetDimens( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fVnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fVnotValid;
  
  return fDimens[aid][cid];
}

Bool_t CztParams::IsUsable( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return kFALSE;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return kFALSE;
  
  return fUsable[aid][cid];
}

void CztParams::Init( Double_t b, Double_t e, Int_t n, Double_t *t )
{
  fTbegun = b;
  fTended = e;
  fNpoint = n;
  fTpoint = new Double_t[fNpoint];
  for ( Int_t ip = 0; ip < fNpoint; ip++ ) {
    fTpoint[ip] = *(t+ip);
  }
  
  if ( n < 0 ) return;
  for ( Int_t aid = 0; aid < CztLib::N_ASIC; aid++ ) {
    for ( Int_t idx = 0; idx < CztLib::N_INDX; idx++ ) {
      fPeds[aid][idx] = new Double_t [fNpoint];
      fThre[aid][idx] = new Double_t [fNpoint];
      fGain[aid][idx] = new Double_t [fNpoint];
      for ( Int_t ip = 0; ip < fNpoint; ip++ ) {
	fPeds[aid][idx][ip] = 0.0;
	fThre[aid][idx][ip] = 0.0;
	fGain[aid][idx][ip] = 1.0;
      }
    }
  }
}

Double_t CztParams::GetTempOffs( Int_t aid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0.0;

  return fTempOffs[aid];
}

Double_t CztParams::GetTempGain( Int_t aid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 1.0;

  return fTempGain[aid];
}

Double_t CztParams::GetTempBase( Int_t aid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0.0;

  return fTempBase[aid];
}

Double_t CztParams::GetPeds( Int_t aid, Int_t idx, Int_t ip ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0.0;
  if ( idx < 0 || idx >= CztLib::N_INDX ) return 0.0;
  if ( ip  < 0 || ip  >= fNpoint        ) return 0.0;

  return fPeds[aid][idx][ip];
}

Double_t CztParams::GetThre( Int_t aid, Int_t idx, Int_t ip ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0.0;
  if ( idx < 0 || idx >= CztLib::N_INDX ) return 0.0;
  if ( ip  < 0 || ip  >= fNpoint        ) return 0.0;

  return fThre[aid][idx][ip];
}

Double_t CztParams::GetGain( Int_t aid, Int_t idx, Int_t ip ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 1.0;
  if ( idx < 0 || idx >= CztLib::N_INDX ) return 1.0;
  if ( ip  < 0 || ip  >= fNpoint        ) return 1.0;

  return fGain[aid][idx][ip];
}

Double_t CztParams::GetLminPT( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fDnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  return fLminPT[aid][cid];
}

Double_t CztParams::GetLmaxPT( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fDnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  return fLmaxPT[aid][cid];
}

Double_t CztParams::GetLminPE( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fDnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  return fLminPE[aid][cid];
}

Double_t CztParams::GetLmaxPE( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fDnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  return fLmaxPE[aid][cid];
}

Double_t CztParams::GetZpos( Int_t aid, Int_t cid, Double_t ene, Double_t tim ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0.0;

  if ( tim > 0.0 ) return GetZposT( aid, cid, tim );
  else             return GetZposE( aid, cid, ene );
}

Double_t CztParams::GetZposE( Int_t aid, Int_t cid, Double_t ene ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0.0;

  Double_t fLmin = fLminPE[aid][cid];
  Double_t fLmax = fLmaxPE[aid][cid];

  if ( ene < fLmin ) ene = fLmin;
  if ( ene > fLmax ) ene = fLmax;
  
  Double_t zdim  = fDimens[aid][cid].Z();
  Double_t diff  = fLmax - fLmin;
  
  if ( fPosHistE[aid][cid] ) {
    Double_t zpos = GetCorr1D( fPosHistE[aid][cid], ene );
    if ( zpos < 0.0 || zpos > zdim ) return 0.0;
    return zpos - zdim/2.0;
  }

  Double_t rati = ( diff > 0.0 ) ? (ene-fLmin)/diff - 0.5 : 0.0;

  return zdim*rati;
}

Double_t CztParams::GetZposT( Int_t aid, Int_t cid, Double_t tim ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0.0;

  Double_t fLmin = fLminPT[aid][cid];
  Double_t fLmax = fLmaxPT[aid][cid];

  if ( tim < fLmin ) tim = fLmin;
  if ( tim > fLmax ) tim = fLmax;

  Double_t zdim  = fDimens[aid][cid].Z();
  Double_t diff  = fLmax - fLmin;

  if ( fPosHistT[aid][cid] ) {
    Double_t zpos = GetCorr1D( fPosHistT[aid][cid], tim );
    if ( zpos < 0.0 || zpos > zdim ) return 0.0;
    return zpos - zdim/2.0;
  }

  Double_t rati = ( diff > 0.0 ) ? (tim-fLmin)/diff - 0.5 : 0.0;

  return zdim*rati;
}

TH1D *CztParams::GetPosHistE( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0;
  if ( fPosHistE[aid][cid] ) return fPosHistE[aid][cid];
  return 0;
}

TH1D *CztParams::GetPosHistT( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0;
  if ( fPosHistT[aid][cid] ) return fPosHistT[aid][cid];
  return 0;
}

Double_t CztParams::GetDepCorr( Int_t aid, Int_t cid, Double_t ene, Double_t tim ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0.0;

  if ( tim > 0.0 ) return GetDepCorrT( aid, cid, tim );
  else             return GetDepCorrE( aid, cid, ene );
}

Double_t CztParams::GetDepCorrT( Int_t aid, Int_t cid, Double_t tim ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 1.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 1.0;
  if ( !fDepHistT[aid][cid] ) return 1.0;

  if ( tim < fLminPT[aid][cid] ) tim = fLminPT[aid][cid];
  if ( tim > fLmaxPT[aid][cid] ) tim = fLmaxPT[aid][cid];
  
  Double_t corr = GetCorr1D( fDepHistT[aid][cid], tim );
  return ( corr ) ? 1.0/corr : 1.0;
}

Double_t CztParams::GetDepCorrE( Int_t aid, Int_t cid, Double_t ene ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 1.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 1.0;
  if ( !fDepHistE[aid][cid] ) return 1.0;
  
  if ( ene < fLminPE[aid][cid] ) ene = fLminPE[aid][cid];
  if ( ene > fLmaxPE[aid][cid] ) ene = fLmaxPE[aid][cid];
  
  Double_t corr = GetCorr1D( fDepHistE[aid][cid], ene );
  return ( corr ) ? 1.0/corr : 1.0;
}

TH1D *CztParams::GetDepHistE( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0;
  if ( fDepHistE[aid][cid] ) return fDepHistE[aid][cid];
  return 0;
}

TH1D *CztParams::GetDepHistT( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0;
  if ( fDepHistT[aid][cid] ) return fDepHistT[aid][cid];
  return 0;
}

Double_t CztParams::GetMapCorr( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t ene, Double_t tim ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 1.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 1.0;

  if ( tim > 0 ) return GetMapCorrT( aid, cid, x, y, tim );
  else           return GetMapCorrE( aid, cid, x, y, ene );
}

Double_t CztParams::GetMapCorrE( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t ene ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 1.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 1.0;
  
  Double_t val = GetCorr3D( fMapHistE[aid][cid], x, y, ene );
  return ( val > 0.0 ) ? 1.0/val : 1.0;
}

Double_t CztParams::GetMapCorrT( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t tim ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 1.0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 1.0;
  
  Double_t val = GetCorr3D( fMapHistT[aid][cid], x, y, tim );
  return ( val > 0.0 ) ? 1.0/val : 1.0;
}

TH3D *CztParams::GetMapHistE( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0;
  if ( fMapHistE[aid][cid] ) return fMapHistE[aid][cid];
  return 0;
}

TH3D *CztParams::GetMapHistT( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return 0;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return 0;
  if ( fMapHistT[aid][cid] ) return fMapHistT[aid][cid];
  return 0;
}

Double_t CztParams::GetConforX( Int_t aid, Int_t cid, Double_t x ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fDnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  if ( x == CztLib::fDnotValid )          return CztLib::fDnotValid;

  if ( !fConforX[aid][cid] ) return x;

  Int_t bin = fConforX[aid][cid]->FindBin( x );
  return fConforX[aid][cid]->GetBinContent( bin );
}

Double_t CztParams::GetConforY( Int_t aid, Int_t cid, Double_t y ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fDnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  if ( y == CztLib::fDnotValid )          return CztLib::fDnotValid;

  if ( !fConforY[aid][cid] ) return y;

  Int_t bin = fConforY[aid][cid]->FindBin( y );
  return fConforY[aid][cid]->GetBinContent( bin );
}

Double_t CztParams::GetConforM( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t z ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fDnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  if ( x == CztLib::fDnotValid )          return CztLib::fDnotValid;
  if ( y == CztLib::fDnotValid )          return CztLib::fDnotValid;

  Double_t zdim  = fDimens[aid][cid].Z();
  if ( z < 0.0  ) z =  0.0;
  if ( z > zdim ) z = zdim;

  Double_t m = (TMath::Abs(x) >= TMath::Abs(y)) ? (TMath::Abs(x)) : (TMath::Abs(y));
  
  if ( !fConforM[aid][cid] ) return 1.0;

  Int_t bin = fConforM[aid][cid]->FindBin( m, z );
  return fConforM[aid][cid]->GetBinContent( bin );
}

Double_t CztParams::GetConforR( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t z ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return CztLib::fDnotValid;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  if ( x == CztLib::fDnotValid )          return CztLib::fDnotValid;
  if ( y == CztLib::fDnotValid )          return CztLib::fDnotValid;

  Double_t zdim  = fDimens[aid][cid].Z();
  if ( z < 0.0  ) z =  0.0;
  if ( z > zdim ) z = zdim;

  Double_t r = TMath::Sqrt( x*x + y*y );
  
  if ( !fConforR[aid][cid] ) return 1.0;

  Int_t bin = fConforR[aid][cid]->FindBin( r, z );
  return fConforR[aid][cid]->GetBinContent( bin );
}

TH1D *CztParams::GetConforX( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return (TH1D *)NULL;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return (TH1D *)NULL;
  if ( fConforX[aid][cid] ) return fConforX[aid][cid];
  return (TH1D *)NULL;
}

TH1D *CztParams::GetConforY( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return (TH1D *)NULL;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return (TH1D *)NULL;
  if ( fConforY[aid][cid] ) return fConforY[aid][cid];
  return (TH1D *)NULL;
}

TH2D *CztParams::GetConforM( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return (TH2D *)NULL;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return (TH2D *)NULL;
  if ( fConforM[aid][cid] ) return fConforM[aid][cid];
  return (TH2D *)NULL;
}

TH2D *CztParams::GetConforR( Int_t aid, Int_t cid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return (TH2D *)NULL;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return (TH2D *)NULL;
  if ( fConforR[aid][cid] ) return fConforR[aid][cid];
  return (TH2D *)NULL;
}

void CztParams::SetTempOffs( Int_t aid, Double_t offs )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  fTempOffs[aid] = offs;
}

void CztParams::SetTempGain( Int_t aid, Double_t gain )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  fTempGain[aid] = gain;
}

void CztParams::SetTempBase( Int_t aid, Double_t base )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  fTempBase[aid] = base;
}

void CztParams::SetPeds( Int_t aid, Int_t idx, Int_t ip, Double_t peds )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( idx < 0 || idx >= CztLib::N_INDX ) return;
  if ( ip  < 0 || ip  >= fNpoint        ) return;

  fPeds[aid][idx][ip] = peds;
}

void CztParams::SetThre( Int_t aid, Int_t idx, Int_t ip, Double_t thre )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( idx < 0 || idx >= CztLib::N_INDX ) return;
  if ( ip  < 0 || ip  >= fNpoint        ) return;

  fThre[aid][idx][ip] = thre;
}

void CztParams::SetGain( Int_t aid, Int_t idx, Int_t ip, Double_t gain )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( idx < 0 || idx >= CztLib::N_INDX ) return;
  if ( ip  < 0 || ip  >= fNpoint        ) return;

  fGain[aid][idx][ip] = gain;
}

void CztParams::SetLminPT( Int_t aid, Int_t cid, Double_t lm )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  fLminPT[aid][cid] = lm;
}

void CztParams::SetLmaxPT( Int_t aid, Int_t cid, Double_t lm )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  fLmaxPT[aid][cid] = lm;
}

void CztParams::SetLminPE( Int_t aid, Int_t cid, Double_t lm )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  fLminPE[aid][cid] = lm;
}

void CztParams::SetLmaxPE( Int_t aid, Int_t cid, Double_t lm )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  fLmaxPE[aid][cid] = lm;
}

void CztParams::SetPosHistE( Int_t aid, Int_t cid, TH1D *pos )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !pos ) return;
  fPosHistE[aid][cid] = SetCorr1D( pos, fPosHistE[aid][cid], Form( "posE_%02d_%02d", aid, cid ) );
}

void CztParams::SetPosHistT( Int_t aid, Int_t cid, TH1D *pos )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !pos ) return;
  fPosHistT[aid][cid] = SetCorr1D( pos, fPosHistT[aid][cid], Form( "posT_%02d_%02d", aid, cid ) );
}

void CztParams::SetDepHistE( Int_t aid, Int_t cid, TH1D *dep )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !dep ) return;
  fDepHistE[aid][cid] = SetCorr1D( dep, fDepHistE[aid][cid], Form( "depE_%02d_%02d", aid, cid ) );
}

void CztParams::SetDepHistT( Int_t aid, Int_t cid, TH1D *dep )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !dep ) return;
  fDepHistT[aid][cid] = SetCorr1D( dep, fDepHistT[aid][cid], Form( "depT_%02d_%02d", aid, cid ) );
}

void CztParams::SetMapHistE( Int_t aid, Int_t cid, TH3D *map )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !map ) return;
  fMapHistE[aid][cid] = SetCorr3D( map, fMapHistE[aid][cid], Form( "mapE_%02d_%02d", aid, cid ) );
}

void CztParams::SetMapHistT( Int_t aid, Int_t cid, TH3D *map )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !map ) return;
  fMapHistT[aid][cid] = SetCorr3D( map, fMapHistT[aid][cid], Form( "mapT_%02d_%02d", aid, cid ) );
}

void CztParams::SetConforX( Int_t aid, Int_t cid, TH1D *map )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !map ) return;
  fConforX[aid][cid] = SetCorr1D( map, fConforX[aid][cid], Form( "confx_%d_%02d", aid, cid ) );
}

void CztParams::SetConforY( Int_t aid, Int_t cid, TH1D *map )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !map ) return;
  fConforY[aid][cid] = SetCorr1D( map, fConforY[aid][cid], Form( "confy_%d_%02d", aid, cid ) );
}

void CztParams::SetConforM( Int_t aid, Int_t cid, TH2D *map )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !map ) return;
  fConforM[aid][cid] = SetCorr2D( map, fConforM[aid][cid], Form( "confm_%d_%02d", aid, cid ) );
}

void CztParams::SetConforR( Int_t aid, Int_t cid, TH2D *map )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( !map ) return;
  fConforR[aid][cid] = SetCorr2D( map, fConforR[aid][cid], Form( "confr_%d_%02d", aid, cid ) );
}

void CztParams::ResetPosHistE( Int_t aid, Int_t cid )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( fPosHistE[aid][cid] ) delete fPosHistE[aid][cid];
  fPosHistE[aid][cid] = 0;
}

void CztParams::ResetPosHistT( Int_t aid, Int_t cid )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( fPosHistT[aid][cid] ) delete fPosHistT[aid][cid];
  fPosHistT[aid][cid] = 0;
}

void CztParams::ResetDepHistE( Int_t aid, Int_t cid )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( fDepHistE[aid][cid] ) delete fDepHistE[aid][cid];
  fDepHistE[aid][cid] = 0;
}

void CztParams::ResetDepHistT( Int_t aid, Int_t cid )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( fDepHistT[aid][cid] ) delete fDepHistT[aid][cid];
  fDepHistT[aid][cid] = 0;
}

void CztParams::ResetMapHistE( Int_t aid, Int_t cid )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( fMapHistE[aid][cid] ) delete fMapHistE[aid][cid];
  fMapHistE[aid][cid] = 0;
}

void CztParams::ResetMapHistT( Int_t aid, Int_t cid )
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return;
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return;
  if ( fMapHistT[aid][cid] ) delete fMapHistT[aid][cid];
  fMapHistT[aid][cid] = 0;
}
