#include "ArduIno.h"

#include <iostream>

ArduIno::ArduIno()
{
  Clear();
}

ArduIno::~ArduIno()
{
}

void ArduIno::Clear( void )
{
  fDataType = 0;
  fEventID  = 0;
  fData1st  = 0;
  fData2nd  = 0;
  fUtcTime  = 0.0;
  fUtcCorr  = 0;
}

void ArduIno::SetArduIno( std::string s )
{
  Clear();

  std::string delimiter = ",";
  std::string token;
  std::vector<std::string> items;

  size_t pos = 0;

  items.clear();
  while ((pos=s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    items.push_back( token );
    s.erase(0, pos+delimiter.length());
  }
  items.push_back( s );
  
  if      ( items[0] == "P" ) fDataType = 1;
  else if ( items[0] == "R" ) fDataType = 2;
  else if ( items[0] == "C" ) fDataType = 3;
  else if ( items[0] == "E" ) fDataType = 4;
  else                        fDataType = 255;
  if ( fDataType != 255 ) {
    fData1st = std::stoul( items[1].c_str() );
    if ( items[0] == "C" ) fData2nd = 0;
    else                   fData2nd = std::stoul( items[2].c_str() );
    fUtcTime = std::stod ( items[3].c_str() );
  }
  if ( fDataType == 4 ) fEventID = fData1st;
}
