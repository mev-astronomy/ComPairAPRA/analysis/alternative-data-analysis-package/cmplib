#include "CztUdpHeader.h"

#include "CztLib.h"

#include "TString.h"

#include <iostream>
#include <fstream>

ClassImp( CztUdpHeader )

CztUdpHeader::CztUdpHeader( void )
{
  fHeader[0] = 0xFF;
  fHeader[1] = 0xFF;
  fHeader[2] = 0xFF;
  fHeader[3] = 0xFF;
  fHeader[4] = 0x5A;
  fHeader[5] = 0x5A;
  fHeader[6] = 0xFF;
  fHeader[7] = 0xFF;
  fHeader[8] = 0xFF;
  fHeader[9] = 0xFF;

  Clear();
}

CztUdpHeader::~CztUdpHeader()
{
}

Int_t CztUdpHeader::ReadData( std::ifstream *fIN, Bool_t speak )
{
  if ( fIN->eof() ) return 0;

  // Check Header word
  fIN->read( (char *)&fBuffer, sizeof(fBuffer) );

  if ( fIN->gcount() == 0 ) return 0;

  for ( Int_t i = 0; i < CztLib::B_UDPH; i++ ) {
    if ( fHeader[i] != fBuffer[i] ) return FindNext( fIN, speak );
  }
  return fIN->gcount();
}

Int_t CztUdpHeader::FindNext( std::ifstream *fIN, Bool_t speak )
{
  Int_t fSize = 0;

  if ( speak ) {
    std::cout << Form( "%02x%02x %02x%02x %02x%02x %02x%02x %02x%02x",
		       fBuffer[0], fBuffer[1],
		       fBuffer[2], fBuffer[3],
		       fBuffer[4], fBuffer[5],
		       fBuffer[6], fBuffer[7],
		       fBuffer[8], fBuffer[9] ) << std::endl;
  }

  Int_t ik = -1;
  for ( Int_t i = 0; i < CztLib::B_UDPH; i++ ) {
    if ( fIN->eof() ) return -1;
    fIN->read( (char *)fBuffer+fSize, 1 );

    if ( ik != i ) {
      std::cout << Form( "%02x%02x %02x%02x %02x%02x %02x%02x %02x%02x",
			 fBuffer[0], fBuffer[1],
			 fBuffer[2], fBuffer[3],
			 fBuffer[4], fBuffer[5],
			 fBuffer[6], fBuffer[7],
			 fBuffer[8], fBuffer[9] ) << std::endl;
      ik = i;
    }

    fSize += fIN->gcount();
    if ( fHeader[i] != fBuffer[i] ) {
      if ( fBuffer[i] == 0xFF && fBuffer[i-1] == 0xFF ) {
	i = i-1;
	fSize = fSize-1;
      }
      else {
	i = -1;
	fSize = 0;
      }
    }
  }
  return fSize;
}

Int_t CztUdpHeader::ReadData( unsigned char *data, Bool_t speak )
{
  Int_t fSize = sizeof(fBuffer);

  // Check Header word
  memcpy( (void *)&fBuffer, data, sizeof(fBuffer) );
  for ( Int_t i = 0; i < CztLib::B_UDPH; i++ ) {
    if ( fHeader[i] != fBuffer[i] ) return FindNext( data, speak );
  }

  return fSize;
}

Int_t CztUdpHeader::FindNext( unsigned char *data, Bool_t speak )
{
  if ( speak ) {
    std::cout << Form( "%02x%02x %02x%02x %02x%02x %02x%02x %02x%02x",
		       fBuffer[0], fBuffer[1],
		       fBuffer[2], fBuffer[3],
		       fBuffer[4], fBuffer[5],
		       fBuffer[6], fBuffer[7],
		       fBuffer[8], fBuffer[9] ) << std::endl;
  }
  
  return -1;
}

void CztUdpHeader::Clear( Option_t *option )
{
  for ( Int_t i = 0; i < CztLib::B_UDPH; i++ ) fBuffer[i] = 0;
}
