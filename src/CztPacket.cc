#include "CmpLib.h"
#include "CztLib.h"
#include "CztPacket.h"

#include "TMath.h"
#include "TString.h"

#include <iostream>

CztPacket::CztPacket( void )
{
  fBuffer = new unsigned char[MAX_PKT_SIZE];
  fVersion = 0;

  Clear();
}

CztPacket::~CztPacket()
{
  if ( fBuffer ) delete [] fBuffer;
}

Int_t CztPacket::GetAsicID( UInt_t hit ) const
{
  return fPktHeader.GetAsicID( hit );
}

Double_t CztPacket::GetUtcTime( void ) const
{
  UInt_t fEpoch   = fEvtHeader.GetEpoch  ();
  UInt_t fEpochNS = fEvtHeader.GetEpochNS();
  return fEpoch+fEpochNS*1.0e-9;
}

CztAsic *CztPacket::GetAsicData( UInt_t hit )
{
  if ( hit >= GetNhitAsic() ) return (CztAsic *)NULL;
  return &fDataAsics[hit];
}

void CztPacket::ChkVersion( void )
{
  UShort_t size = fPktHeader.GetPktSize ();
  UInt_t   asic = fPktHeader.GetNhitAsic();
  // need to subtract 2 bytes for SOE, 2 bytes for COE
  // however, size = total pkt size - 2
  // so instead of subtracting 4, just need to subtract 2
  fVersion = size-CztLib::B_UDPH-CztLib::B_PKTH-asic*CztLib::B_ASIC-2;
  std::cout << Form( "CztPacket::ChkVersion() fVersion = %u, NhitAsic = %u", fVersion, asic ) << std::endl;
}

void CztPacket::SetEventID( UInt_t id )
{
  fEvtHeader.SetEventID( id );
}

void CztPacket::SetTmStamp( UInt_t ts )
{
  fEvtHeader.SetTmStamp( ts );
}

void CztPacket::SetTimePps( UInt_t tp )
{
  fEvtHeader.SetTimePps( tp );
}

void CztPacket::SetEpoch( UInt_t ep )
{
  fEvtHeader.SetEpoch( ep );
}

void CztPacket::SetEpochNS( UInt_t en )
{
  fEvtHeader.SetEpochNS( en );
}

void CztPacket::SetToffset( Double_t t )
{
  fEvtHeader.SetToffset( t );
}

Int_t CztPacket::ReadPacket( std::ifstream *fIN, Bool_t speak )
{
  Clear();
  Int_t ret, fSize = 0;
  
  ret = fUdpHeader.ReadData( fIN, speak );
  if ( ret == 0 ) return 0;
  if ( ret != CztLib::B_UDPH ) return -1;
  fSize += ret;

  ret = fPktHeader.ReadData( fIN, speak );
  if ( ret < 0 ) return -1;
  fSize += ret;

  fPktHeader.GetPktSize();
  
  fIN->read( (char *)&fSOE, 2 );
  if ( fSOE != 0xFFFF ) {
    std::cout << Form( "fSOE = %04x", fSOE ) << std::endl;
    return ReadPacket( fIN, speak );
  }
  fSize += fIN->gcount();

  if ( !fVersion ) ChkVersion();
  
  ret = fEvtHeader.ReadData( fIN, fVersion, speak );
  if ( ret < 0 ) return -1;
  fSize += ret;

  for ( UInt_t i = 0; i < GetNhitAsic(); i++ ) {
    ret = fDataAsics[i].ReadData( fIN, speak );
    if ( ret < 0 ) return -1;
    fSize += ret;
  }
  
  fIN->read( (char *)&fEOE, 2 );
  if ( fEOE != 0x00FF ) {
    std::cout << Form( "fEOE = %04x, PacketSize = %04x", fSOE, fPktHeader.GetPktSize() ) << std::endl;
    return ReadPacket( fIN, speak );
  }
  fSize += fIN->gcount();

  // Check Current Position and read packet data in Buffer
  std::streampos curp = fIN->tellg();
  curp = curp-(std::streampos)(fSize);
  fIN->seekg( curp );

  fIN->read( (char *)fBuffer, fSize );
  fSize = fIN->gcount();

  fNpacket++;
  return fSize;
}

Int_t CztPacket::Fill( BasePacket *pkt, Bool_t speak )
{
  unsigned char *data = pkt->GetData();

  Clear();
  Int_t ret, fSize = 0;

  ret = fUdpHeader.ReadData( data, speak );
  if ( ret != CztLib::B_UDPH ) return -1;
  fSize += ret;
  
  ret = fPktHeader.ReadData( data+fSize, speak );
  if ( ret < 0 ) return -1;
  fSize += ret;
  
  memcpy( (char *)&fSOE, (data+fSize), sizeof(fSOE) );
  if ( fSOE != 0xFFFF ) {
    std::cout << Form( "fSOE = %04x", fSOE ) << std::endl;
    return -1;
  }
  fSize += sizeof(fSOE);

  if ( !fVersion ) ChkVersion();

  ret = fEvtHeader.ReadData( data+fSize, fVersion, speak );
  if ( ret < 0 ) return -1;
  fSize += ret;

  for ( UInt_t i = 0; i < GetNhitAsic(); i++ ) {
    ret = fDataAsics[i].ReadData( data+fSize, speak );
    if ( ret < 0 ) return -1;
    fSize += ret;
  }
  
  memcpy( (char *)&fEOE, (data+fSize), sizeof(fEOE) );
  if ( fEOE != 0x00FF ) {
    std::cout << Form( "fEOE = %04x", fEOE ) << std::endl;
    return -1;
  }
  fSize += sizeof(fEOE);

  fNpacket++;
  return fSize;
}

void CztPacket::Clear( Option_t *option )
{
  fUdpHeader.Clear( option );
  fPktHeader.Clear( option );
  fSOE = 0;
  fEvtHeader.Clear( option );
  for ( Int_t i = 0; i < CztLib::N_ASIC; i++ )
    fDataAsics[i].Clear();
  fEOE = 0;
}
