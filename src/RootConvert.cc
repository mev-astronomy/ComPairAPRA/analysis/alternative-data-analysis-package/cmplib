#include "RootConvert.h"
#include "CmpLib.h"
#include "CmpTree.h"

#include "CmpParams.h"

#include "TrgBranch.h"
#include "AcdBranch.h"
#include "SitBranch.h"
#include "CztBranch.h"
#include "CsIBranch.h"
#include "DstBranch.h"

#include "TTree.h"
#include "TSystem.h"

#include <fstream>
#include <iostream>
#include <signal.h>

Int_t RootConvert::fKill = 0;

RootConvert::RootConvert( void )
{
  fFileSize = 0.0;
  fReadSize = 0.0;
  fData.Clear();
  fExpNum = -1;
  fRunNum = -1;
  fTFcnt  = 0;
  fTFile  = 0;
  fTree   = 0;
  fCtree  = new CmpTree;
}

RootConvert::~RootConvert()
{
  if ( fTree  ) delete fTree;
  if ( fCtree ) delete fCtree;
  if ( fTFile ) delete fTFile;
}

Int_t RootConvert::Add( const char *in_fname )
{
  // get info of raw data file
  Long_t id, flags, mtime;
  Long64_t size;
  if ( gSystem->GetPathInfo( in_fname, &id, &size, &flags, &mtime ) ) {
    std::cout << "Raw data file, \"" << in_fname << "\""
	      << " does not exist." << std::endl;
    return 0;
  }

  fFileSize += size/1024./1024.;
  fReadSize = 0;
  fData.Add( in_fname );

  return 1;
}
      
Int_t RootConvert::Init( void )
{
  if ( !CmpParams ::GetPtr() && !CmpParams::ReadFile() ) return -1;
  fRevision.GetHeads ();
  fRevision.GetStatus();
  return 1;
}

Int_t RootConvert::Open( const char *tr_fname, Int_t flag )
{
  // set output file name and branch flag
  fTFname = tr_fname;
  fTFcnt  = 0;
  fBrFlag = flag;

  return 0;
}


Int_t RootConvert::Loop( Int_t sel, Int_t nproc )
{
  Int_t nevt = 0, nsel = 0;
  Int_t intv = 10000;

  Bool_t speak = kFALSE;
  
  // set signal handler
  signal( SIGINT,  SigHandler );
  signal( SIGTERM, SigHandler );

  while ( ( nproc <= 0 ) || ( nevt < nproc ) ) {
    if ( fKill ) {
      std::cout << std::endl << "*** break ***, process interrupted" << std::endl;
      break;
    }

    // clear root file
    if ( fTFile && !fTFile->IsOpen() ) {
      delete fTFile;
      fTFile = 0;
    }
    
    // open root file
    if ( !fTFile ) {
      TString tfn = fTFname;
      if ( fTFcnt > 0 ) tfn += Form( ".%02d", fTFcnt );
      fTFile = new TFile( tfn, "recreate" );
      if ( !fTFile->IsOpen() ) {
	std::cout << "Error in opening root file: " << tfn.Data() << std::endl;
	return -1;
      }
      std::cout << "Open root file: " << tfn.Data() << std::endl;
      
      // create Tree and set branch
      fTree = new TTree ( "ttree", "ComPair Instrument Data" );
      fTree->SetAutoSave   ( (Long64_t)128*1024*1024*1024 );
      fTree->SetMaxTreeSize( (Long64_t)128*1024*1024*1024 );
      fCtree->Branch( fTree, fBrFlag );
    }

    fCtree->Clear();
    
    if ( fData.Read() <= 0 ) break;
    fReadSize += fData.GetPktSize()+4;
    nevt++;
    
    fCtree->SetUtcTime( fData.GetUtcTime() );
    fCtree->SetEventID( fData.GetEventID() );
    fCtree->SetPktAtrb( fData.GetPktAtrb() );

    if ( speak )
      std::cout << Form( "fData.GetNatrb() = %2X", fData.GetNatrb() ) << std::endl;

    if ( sel == CmpLib::NOEVT ) {
      fTree->Fill();
      nsel++;
    }
    else {
      for ( UInt_t i = 0; i < fData.GetNatrb(); i++ ) {
	fCtree->Fill( fData.GetPacket( i ) );
      }
      if ( sel == CmpLib::RAWEVT ) {
	fTree->Fill();
	nsel++;
      }
      else {
	if ( fCtree->Process( sel ) > 0 ) {
	  //	  std::cout << Form( "nsel = %d", nsel ) << std::endl;
	  //	  CztBranch *fCzt = fCtree->GetCzt();
	  //	  if ( !fCzt ) return -1;
	  fTree->Fill();
	  nsel++;
	}
      }
    }
    
    Stat( nevt, nsel, nproc, 1, intv );
  }
  
  // reset signal handler
  signal( SIGINT,  SIG_DFL );
  signal( SIGTERM, SIG_DFL );

  std::cout << std::endl;
  std::cout << "Processed " << nevt << " events. ";
  if ( nsel > 0 ) std::cout << "Filled " << nsel << " events.";
  std::cout << std::endl;

  if ( Write() ) {
    return 1;
  }
  else {
    return 0;
  }
}

Int_t RootConvert::Write( void )
{
  if ( !fTFile ) return 0;
  fTFile->cd();
  
  if ( fTree->GetEntries() > 0 ) {
    fTree->Write();
    TDirectoryFile *dir = new TDirectoryFile( "params", "calibration parameters" );
    dir->cd();
    fCtree->WritePar();
    fTFile->cd();
  }

  TString srev = "\n";
  srev += "--\n";
  srev += fRevision.Heads ();
  srev += "\n";
  srev += fRevision.Status();
  srev += "\n--";
  TNamed nrev( "Mercurial revision", srev.Data() );
  nrev.Write();
  
  return 1;
}

Double_t RootConvert::GetMemory( void )
{
  std::ifstream fin( Form( "/proc/%d/status", gSystem->GetPid() ) );
  if ( !fin ) return 0;

  Double_t vmsize;
  
  char buf[256];
  while ( !fin.eof() ) {
    fin >> buf;
    if ( buf == TString( "VmSize:" ) ) {
      fin >> vmsize;
      return vmsize;
    }
  }
  
  return 0;
}

Double_t RootConvert::GetFree( const char *path )
{
  Long_t id, bsize, blocks, bfree;

  gSystem->GetFsInfo( path, &id, &bsize, &blocks, &bfree );
  return 1.*bsize*bfree;
}

void RootConvert::SigHandler( int sig )
{
  fKill = 1;
}

Int_t RootConvert::Stat( Int_t nevt, Int_t nsel, Int_t nproc, Int_t ret, Int_t intv )
{
  static Double_t tsta = -1, tpre = -1, tsum = -1;
  //  if ( tsta < 0 ) tsta = (ULong_t)gSystem->Now();
  if ( tsta < 0 ) tsta = (unsigned long long)gSystem->Now();
  if ( tpre < 0 ) tpre = tsta;

  static Int_t pevt = -1, sevt = -1;
  static Double_t pfrc = 0, sfrc = 0;

  if ( tsum > 0 ) tpre = tsum;
  if ( sevt > 0 ) pevt = sevt;
  if ( sfrc > 0 ) pfrc = sfrc;

  Double_t memmax = 2000;

  if ( fTFile->IsOpen() && ret > 0 && (nproc <= 0 || nevt < nproc ) ) {
    if ( nevt <= 0 || nevt%intv > 0 ) return 0;
    if ( pevt >= 0 && nevt <= pevt  ) return 0;
  }

  Double_t isize = fReadSize/1024/1024;
  Double_t osize = fTFile->GetSize()/1024./1024.;
  //  Double_t time  = 1e-3*((ULong_t)gSystem->Now()-tsta);
  Double_t time  = 1e-3*((unsigned long long)gSystem->Now()-tsta);
  Double_t frac  = TMath::Min( isize/fFileSize*100, 99.9 );
  
  if ( nproc > 0 ) frac = TMath::Min( 100.*nevt/nproc, 99.9 );
  std::cout << Form( "\033[100D %7d/%7d: %6.1fMB (%4.1f%%) ",
		     nsel, nevt, osize, frac );

  if      ( time <  600 ) std::cout << Form( "@%4.0fsec ", time );
  else if ( time < 3600 ) std::cout << Form( "@%4.1fmin ", time/60 );
  else                    std::cout << Form( "@%4.1fhr  ", time/3600 );

  Double_t rate = ( time > tpre ) ? (nevt-pevt)/(time-tpre ) : 0;
  if ( rate < 10000 ) std::cout << Form( "(%4.2fkHz) ", rate/1000 );
  else                std::cout << Form( "(%4.1fkHz) ", rate/1000 );

  Double_t mem = GetMemory()/1024.;
  std::cout << Form( "Mem:%5.1fMB ", mem );

  if ( ret == 0 && fData.GetNfile() <= 1 ) std::cout << "                   ";
  else {
    Double_t eta = ( frac > 0 ) ? time*(99.9/frac-1) : 0;
    if ( frac > pfrc && time > tpre )
      eta = (time - tpre)*(99.9-frac)/(frac-pfrc);
    if      ( eta <  600 ) std::cout << Form( "ETA %4.0fsec", eta );
    else if ( eta < 3600 ) std::cout << Form( "ETA %4.1fmin", eta/60 );
    else                   std::cout << Form( "ETA %4.1fhr ", eta/3600 );
  }

  std::cout.flush();

  tpre = time;
  pevt = nevt;
  pfrc = frac;

  // disk free space check
  if ( GetFree( fTFile->GetName() ) < 50.*1024*1024 ) {
    std::cout << std::endl;
    std::cout << "Not enough disk space" << std::endl;
    return -1;
  }

  // memory consumption check
  if ( mem > memmax ) {
    std::cout << std::endl;
    std::cout << "Too much memory consumption" << std::endl;
    return -1;
  }

  if ( !fTFile->IsOpen() || ret == 0 || nevt == nproc ) std::cout << std::endl;
  else if ( nevt > 0 && nevt%(intv*10) == 0 ) {
    std::cout << std::endl;
    tsum = time;
    sevt = nevt;
    sfrc = frac;
  }

  return 0;
}
