#include "SitBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"
#include "DstBranch.h"

#include "TFile.h"

ClassImp( SitBranch )

SitBranch::SitBranch( void )
{
  fSlice = -1;
  for ( Int_t coord = 0; coord < SitLib::N_COORD; coord++ ) {
    fLayer[coord] = new SitLayer();
  }
  Clear();
}

SitBranch::SitBranch( Int_t id )
{
  if ( id < 0 || id >= SitLib::N_SLICE ) fSlice = -1;
  else fSlice = id;

  for ( Int_t coord = 0; coord < SitLib::N_COORD; coord++ ) {
    fLayer[coord] = new SitLayer( fSlice, coord );
  }
  Clear();
}

SitBranch::~SitBranch()
{
}

Int_t SitBranch::GetNhits( Int_t coord ) const
{
  return fLayer[coord]->GetNhits();
}

Double_t SitBranch::GetEnergy( void )
{
  return (fLayer[CmpLib::COORD_Y]->GetNhits() > 0 )
    ? fLayer[CmpLib::COORD_Y]->GetEnergy() 
    : fLayer[CmpLib::COORD_X]->GetEnergy();
}

HitBranch *SitBranch::GetNthHit( Int_t coord, Int_t n )
{
  return fLayer[coord]->GetNthHit( n );
}

HitBranch *SitBranch::NewHit( Int_t coord )
{
  return fLayer[coord]->NewHit();
}

SitLayer *SitBranch::GetLayer( Int_t coord )
{
  if ( coord < 0 || coord >= SitLib::N_COORD ) return (SitLayer *)NULL;
  return fLayer[coord];
}

Int_t SitBranch::NhitStrip( Int_t coord )
{
  return fLayer[coord]->NhitStrip();
}

Int_t SitBranch::Fill( BasePacket *pkt )
{
  fAttrib  = pkt->GetAtrb();
  fEventID = pkt->GetEvid();
  fEpoch   = pkt->GetEpch();
  fEpochNS = pkt->GetEpns();

  fEvent.Fill( pkt );
  return 0;
}

Int_t SitBranch::FindHitID( Int_t coord, Int_t sid )
{
  return fLayer[coord]->FindHitID( sid );
}

void SitBranch::SetSlice( Int_t id )
{
  fSlice = id;
  for ( Int_t coord = SitLib::COORD_X; coord <= SitLib::COORD_Y; coord++ ) {
    fLayer[coord]->SetLyrID( fSlice*2+coord );
    fLayer[coord]->SetCoord( coord );
  }
}

Int_t SitBranch::Process( Int_t sel )
{
  // The main method of SitBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  for ( Int_t coord = 0; coord < SitLib::N_COORD; coord++ ) {
    fLayer[coord]->Clear();
    fLayer[coord]->Process( fSlice );
  }

  CopyHit();

  return 1;
}

void SitBranch::ProcHit( void )
{
  for ( Int_t coord = 0; coord < SitLib::N_COORD; coord++ ) {
    fLayer[coord]->ProcHit( fSlice );
  }
}

void SitBranch::CopyHit( void )
{
  for ( Int_t coord = 0; coord < SitLib::N_COORD; coord++ ) {
    fLayer[coord]->CopyHit();
  }
}

void SitBranch::Clear( Option_t *option )
{
  fAttrib  = 0;
  fEventID = 0;
  fEpoch   = 0;
  fEpochNS = 0;

  fEvent.Clear();

  for ( Int_t coord = SitLib::COORD_X; coord <= SitLib::COORD_Y; coord++ ) {
    fLayer[coord]->Clear();
  }
}

void SitBranch::Print( Option_t *option ) const
{
}
