#include "SitStrip.h"
#include "CmpTree.h"
#include "CmpParams.h"

#include "SitLib.h"
#include "SitAsic.h"
#include "SitEvent.h"
#include "SitParams.h"

#include "TFile.h"
#include "TClonesArray.h"

ClassImp( SitStrip )

SitStrip::SitStrip( void )
{
  Clear();
}

SitStrip::~SitStrip()
{
}

Int_t SitStrip::GetSidx( void ) const
{
  return gCmpParams->GetSecID( fIndx );
}

Double_t SitStrip::GetEdep( Bool_t precise ) const
{
  if ( !precise ) return fEdep;
  return fEdep+fEdepAdj[SitLib::SIDE_L]+fEdepAdj[SitLib::SIDE_R];
}

TVector3 SitStrip::GetHpos( Bool_t precise ) const
{
  if ( !precise ) return fHpos;

  Double_t x = fEdep*fHpos.X();
  x += fEdepAdj[SitLib::SIDE_L]*fHposAdj[SitLib::SIDE_L].X();
  x += fEdepAdj[SitLib::SIDE_R]*fHposAdj[SitLib::SIDE_R].X();
  x /= (fEdep + fEdepAdj[SitLib::SIDE_L] + fEdepAdj[SitLib::SIDE_R]);
  Double_t y = fEdep*fHpos.Y();
  y += fEdepAdj[SitLib::SIDE_L]*fHposAdj[SitLib::SIDE_L].Y();
  y += fEdepAdj[SitLib::SIDE_R]*fHposAdj[SitLib::SIDE_R].Y();
  y /= (fEdep + fEdepAdj[SitLib::SIDE_L] + fEdepAdj[SitLib::SIDE_R]);
  Double_t z = fEdep*fHpos.Z();
  z += fEdepAdj[SitLib::SIDE_L]*fHposAdj[SitLib::SIDE_L].Z();
  z += fEdepAdj[SitLib::SIDE_R]*fHposAdj[SitLib::SIDE_R].Z();
  z /= (fEdep + fEdepAdj[SitLib::SIDE_L] + fEdepAdj[SitLib::SIDE_R]);
  return TVector3( x, y, z );
}

TVector3 SitStrip::GetEpos( Bool_t precise ) const
{
  return fEpos;
}

Int_t SitStrip::GetNhitAdj( Int_t side ) const
{
  if ( side < 0 || side >= SitLib::N_SIDE ) return 0;
  return fNhitAdj[side];
}

Double_t SitStrip::GetEdepAdj( Int_t side ) const
{
  if ( side < 0 || side >= SitLib::N_SIDE ) return SitLib::fDnotValid;
  return fEdepAdj[side];
}

TVector3 SitStrip::GetHposAdj( Int_t side ) const
{
  if ( side < 0 || side >= SitLib::N_SIDE ) return SitLib::fVnotValid;
  return fHposAdj[side];
}

void SitStrip::SetNhitAdj( Int_t side, Int_t nhit )
{
  if ( side < 0 || side >= SitLib::N_SIDE ) return;
  if ( nhit < 0 || nhit >= 2 ) return;
  fNhitAdj[side] = nhit;
}

void SitStrip::SetEdepAdj( Int_t side, Double_t edep )
{
  if ( side < 0 || side >= SitLib::N_SIDE ) return;
  fEdepAdj[side] = edep;
}

void SitStrip::SetHposAdj( Int_t side, TVector3 hpos )
{
  if ( side < 0 || side >= SitLib::N_SIDE ) return;
  fHposAdj[side] = hpos;
}

void SitStrip::Process( void )
{
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return;
}

void SitStrip::Clear( Option_t *option )
{
  fIndx = -1;
  fEdep = 0.0;
  fHpos = SitLib::fVnotValid;
  fEpos = TVector3( 0, 0, 0 );

  for ( Int_t side = 0; side < SitLib::N_SIDE; side++ ) {
    fNhitAdj[side] = 0;
    fEdepAdj[side] = 0.0;
    fHposAdj[side] = SitLib::fVnotValid;
  }
}

void SitStrip::Print( Option_t *option ) const
{
}
