#include "BasePacket.h"

#include "TString.h"

#include <iostream>

BasePacket::BasePacket()
{
  fCapa = 0;
  fHead = 0;
  fData = 0;
  fBuff = 0;
  capaAlloc( PKT_HEAD_SIZE );
  Clear();
}

BasePacket::~BasePacket()
{
  if ( fBuff ) delete [] fBuff;
}

void BasePacket::capaAlloc( int capa )
{
  if ( capa <= fCapa ) return;
  if ( fBuff ) delete [] fBuff;
  fCapa = ( MAX_PKT_SIZE > capa ) ? capa : MAX_PKT_SIZE;
  fBuff = new unsigned char[fCapa];
  fHead = (struct DataHeader *)fBuff;
  fData = (unsigned char *)(fBuff + PKT_HEAD_SIZE);
}

int BasePacket::GetSize( void )
{
  int i_buf_size_a = sizeConvert( buf_size_a );
  int i_buf_size_b = sizeConvert( buf_size_b );

  if ( i_buf_size_a == i_buf_size_b ) return i_buf_size_a;
  else return -1;
}

int BasePacket::GetAtrb( void )
{
  return atrbConvert();
}

double BasePacket::GetTime( void )
{
  return timeConvert();
}

int BasePacket::GetData( unsigned char *buf, int offset, int size )
{
  if ( offset+size >= GetSize() ) return 0;
  memcpy( (void *)buf, (void *)(fData+offset), size );
  return size;
}

UInt_t BasePacket::GetEvid( void )
{
  return fHead->evid[0]+(fHead->evid[1]<<8)+(fHead->evid[2]<<16)+(fHead->evid[3]<<24);
}

UInt_t BasePacket::GetEpch( void )
{
  return fHead->epch[0]+(fHead->epch[1]<<8)+(fHead->epch[2]<<16)+(fHead->epch[3]<<24);
}

UInt_t BasePacket::GetEpns( void )
{
  return fHead->epns[0]+(fHead->epns[1]<<8)+(fHead->epns[2]<<16)+(fHead->epns[3]<<24);
}

int BasePacket::ReadPacket( unsigned char *buf, int offset )
{
  int  i_buf_size_a = 0;

  buf_size_a[0] = buf_size_a[1] = 0;
  buf_size_b[0] = buf_size_b[1] = 0;

  memcpy( (char *)&buf_size_a, (void *)(buf+offset), sizeof( buf_size_a ) );
  i_buf_size_a = sizeConvert( buf_size_a );
  
  if ( i_buf_size_a > MAX_PKT_SIZE ) return -1;
  capaAlloc( i_buf_size_a );

  memcpy( (char *)fBuff, (void *)(buf+offset+PKT_SIZE_SIZE), i_buf_size_a );
  memcpy( (char *)&buf_size_b, (void *)(buf+offset+PKT_SIZE_SIZE+i_buf_size_a), PKT_SIZE_SIZE );
  
  return GetSize();
}

int BasePacket::ReadPacket( std::ifstream *fin, bool speak )
{
  if ( !fin->is_open() ) return 0;

  buf_size_a[0] = buf_size_a[1] = 0;
  buf_size_b[0] = buf_size_b[1] = 0;

  fin->read( (char *)&buf_size_a, sizeof( buf_size_a ) );
  if ( fin->eof() ) return 0;

  if ( fin->gcount() != sizeof( buf_size_a ) ) return -1;

  int i_buf_size_a = sizeConvert( buf_size_a );
  
  if ( i_buf_size_a > MAX_PKT_SIZE ) return -1;
  capaAlloc( i_buf_size_a );

  fin->read( (char *)fBuff, i_buf_size_a );
  if ( fin->gcount() != i_buf_size_a ) return -1;

  if ( speak )
    std::cout << Form( "%02X %02X: %8ld",
		       buf_size_a[0], buf_size_a[1], fin->gcount() ) << std::endl;

  fin->read( (char *)&buf_size_b, sizeof( buf_size_b ) );
  if ( fin->gcount() != sizeof( buf_size_b ) ) return -1;
  
  if ( speak )
    std::cout << Form( "%02X %02X: %8ld",
		       buf_size_b[0], buf_size_b[1], fin->gcount() ) << std::endl;
  return GetSize();
}

int BasePacket::WritePacket( std::ofstream *fout, bool speak )
{
  if ( !fout->is_open() ) return -1;
  
  fout->write( reinterpret_cast<const char*>(&buf_size_a[0]), 2 );
  fout->write( reinterpret_cast<const char*>(fBuff), GetSize() );
  fout->write( reinterpret_cast<const char*>(&buf_size_b[0]), 2 );

  Clear();

  return 1;
}

void BasePacket::SetSize ( UShort_t size )
{
  buf_size_a[0] = buf_size_b[0] = (size >> 8) & 0xFF;
  buf_size_a[1] = buf_size_b[1] = (size%256 ) & 0xFF;
}

void BasePacket::SetAtrb ( UShort_t atrb )
{
  *(fHead->atrb+0) = (atrb>>8 ) & 0xFF;
  *(fHead->atrb+1) = (atrb%256) & 0xFF;
}

void BasePacket::SetEvid ( UInt_t   evid )
{
  memcpy ( fHead->evid, &evid, PKT_EVID_SIZE );
}

void BasePacket::SetEpch ( UInt_t   epch )
{
  memcpy ( fHead->epch, &epch, PKT_EPCH_SIZE );
}

void BasePacket::SetEpns ( UInt_t   epns )
{
  memcpy ( fHead->epns, &epns, PKT_EPNS_SIZE );
}

void BasePacket::SetHead( struct DataHeader head )
{
  for ( int i = 0; i < PKT_ATRB_SIZE; i++ ) *(fHead->atrb+i) = head.atrb[i];
  for ( int i = 0; i < PKT_EVID_SIZE; i++ ) *(fHead->evid+i) = head.evid[i];
  for ( int i = 0; i < PKT_EPCH_SIZE; i++ ) *(fHead->epch+i) = head.epch[i];
  for ( int i = 0; i < PKT_EPNS_SIZE; i++ ) *(fHead->epns+i) = head.epns[i];
}

void BasePacket::SetData( UShort_t size, unsigned char *data )
{
  capaAlloc( size+PKT_HEAD_SIZE );
  memcpy( (void *)fData, data, size );
  SetSize( PKT_HEAD_SIZE+size );
}

void BasePacket::Clear( Option_t *option )
{
  for ( int i = 0; i < PKT_SIZE_SIZE; i++ ) buf_size_a[i] = 0;
  for ( int i = 0; i < PKT_SIZE_SIZE; i++ ) buf_size_b[i] = 0;
  for ( int i = 0; i < PKT_ATRB_SIZE; i++ ) *(fHead->atrb+i) = 0;
  for ( int i = 0; i < PKT_EVID_SIZE; i++ ) *(fHead->evid+i) = 0;
  for ( int i = 0; i < PKT_EPCH_SIZE; i++ ) *(fHead->epch+i) = 0;
  for ( int i = 0; i < PKT_EPNS_SIZE; i++ ) *(fHead->epns+i) = 0;
}

double BasePacket::timeConvert( void )
{
  return GetEpch() + 1e-9*GetEpns();
}

int BasePacket::atrbConvert( void )
{
  return ( fHead->atrb[0] << 8 ) + ( fHead->atrb[1] );
}

int BasePacket::sizeConvert( unsigned char buf[2] )
{
  return ( buf[0] << 8 ) + ( buf[1] );
}
