#include "TrkBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"

#include "DstBranch.h"
#include "HitBranch.h"

#include "TFile.h"
#include "TMath.h"

ClassImp( TrkBranch )

TrkBranch::TrkBranch( void )
{
  Clear();
}

TrkBranch::~TrkBranch()
{
}

Int_t TrkBranch::Process( Int_t sel )
{
  // The main method of TrkBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  Clear();

  for ( Int_t coord = CmpLib::COORD_X; coord <= CmpLib::COORD_Y; coord++ ) {
    fFound[coord] = FindTrack( coord );
    if ( !fFound[coord] ) fFound[coord] = FindTrack( coord, kFALSE );
  }
  
  return 1;
}

void TrkBranch::Clear( Option_t *option )
{
  for ( Int_t coord = CmpLib::COORD_X; coord <= CmpLib::COORD_Y; coord++ ) {
    fFound[coord] = kFALSE;
    fA    [coord] = CmpLib::fDnotValid;
    fB    [coord] = CmpLib::fDnotValid;
    fChisq[coord] = CmpLib::fDnotValid;
    for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
      fHitID[coord][slice] = -1;
    }
    fNhitsLyr[coord] = 0;
    fNusedLyr[coord] = 0;
  }

  fAngle = CmpLib::fDnotValid;
  fDirec = CmpLib::fVnotValid;
  fPerih = CmpLib::fVnotValid;
}

void TrkBranch::Print( Option_t *option ) const
{
}

Bool_t TrkBranch::Found( Int_t coord ) const
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return kFALSE;
  return fFound[coord];
}

Double_t TrkBranch::GetA( Int_t coord ) const
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return CmpLib::fDnotValid;
  return fA[coord];
}

Double_t TrkBranch::GetB( Int_t coord ) const
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return CmpLib::fDnotValid;
  return fB[coord];
}

Int_t TrkBranch::GetNhits ( Int_t coord ) const
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return 0;
  return fNhitsLyr[coord];
}

Int_t TrkBranch::GetNused ( Int_t coord ) const
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return 0;
  return fNusedLyr[coord];
}

Double_t TrkBranch::GetChisq ( Int_t coord ) const
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return CmpLib::fDnotValid;
  return fChisq[coord];
}

Int_t TrkBranch::GetHitID( Int_t coord, Int_t slice ) const
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return -1;
  if ( slice < 0 || slice >= CmpLib::N_SLICE ) return -1;
  return fHitID[coord][slice];
}

TVector3 TrkBranch::GetHitPos( Double_t z )
{
  Double_t x = ( fFound[CmpLib::COORD_X] ) ? fA[0]*z+fB[0] : CmpLib::fDnotValid;
  Double_t y = ( fFound[CmpLib::COORD_Y] ) ? fA[1]*z+fB[1] : CmpLib::fDnotValid;

  return TVector3( x, y, z );
}

void TrkBranch::SetFound ( Int_t coord, Bool_t found )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return;
  fFound[coord] = found;
}

void TrkBranch::SetA( Int_t coord, Double_t a )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return;
  fA[coord] = a;
}

void TrkBranch::SetB( Int_t coord, Double_t b )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return;
  fB[coord] = b;
}

void TrkBranch::SetNhits ( Int_t coord, Int_t hit )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return;
  fNhitsLyr[coord] = hit;
}

void TrkBranch::SetNused ( Int_t coord, Int_t use )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return;
  fNusedLyr[coord] = use;
}

void TrkBranch::SetChisq ( Int_t coord, Double_t csq )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return;
  fChisq[coord] = csq;
}

void TrkBranch::SetHitID( Int_t coord, Int_t slice, Int_t id )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return;
  if ( slice < 0 || slice >= CmpLib::N_SLICE ) return;
  if ( id    < 0 || id    >= CmpLib::N_MHIT  ) return;
  fHitID[coord][slice] = id;
}

Bool_t TrkBranch::FindTrack( Int_t coord, Bool_t flag )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return kFALSE;

  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return kFALSE;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return kFALSE;
  
  Int_t n_try = 1;
  Int_t n_use = 0;
  fNhitsLyr[coord] = 0;

  Int_t fNhit[CmpLib::N_SLICE];
  Int_t ival [CmpLib::N_SLICE];

  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    ival [slice] = -1;
    fNhit[slice] = dst->GetNhits( CmpLib::D_SIT, slice*2+coord );
    if ( fNhit[slice] <= 0 ) continue;
    fNhitsLyr[coord]++;
    n_try *= fNhit[slice];
    if ( n_try < 65535 ) {
      n_use++;
      if ( flag ) ival[slice] = 0;
    }
    else {
      fNhit[slice] = 0;
    }
  }
  if ( n_use < 4 ) return kFALSE;
  
  Int_t ihit[CmpLib::N_SLICE];
  Int_t hsel[CmpLib::N_SLICE];
  
  Int_t    n_max   = 0;
  Double_t csq_min = 10.0;
  Double_t csq_thr = 3.0;
  
  for ( ihit[0] = ival[0]; ihit[0] < fNhit[0]; ihit[0]++ )
    for ( ihit[1] = ival[1]; ihit[1] < fNhit[1]; ihit[1]++ )
      for ( ihit[2] = ival[2]; ihit[2] < fNhit[2]; ihit[2]++ )
	for ( ihit[3] = ival[3]; ihit[3] < fNhit[3]; ihit[3]++ )
	  for ( ihit[4] = ival[4]; ihit[4] < fNhit[4]; ihit[4]++ )
	    for ( ihit[5] = ival[5]; ihit[5] < fNhit[5]; ihit[5]++ )
	      for ( ihit[6] = ival[6]; ihit[6] < fNhit[6]; ihit[6]++ )
		for ( ihit[7] = ival[7]; ihit[7] < fNhit[7]; ihit[7]++ )
		  for ( ihit[8] = ival[8]; ihit[8] < fNhit[8]; ihit[8]++ )
		    for ( ihit[9] = ival[9]; ihit[9] < fNhit[9]; ihit[9]++ ) {
		      Double_t csq = Fitting( coord, &ihit[0] );
		      if ( ( csq >= 0.0 ) &&
			   ( ( n_max <  fNusedLyr[coord] && csq < csq_thr ) ||
			     ( n_max == fNusedLyr[coord] && csq < csq_min ) ||
			     ( n_max >  fNusedLyr[coord] && csq < 0.5*csq_min ) )  ){
			n_max = fNusedLyr[coord];
			csq_min = csq;
			for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
			  hsel[slice] = ihit[slice];
			}
		      }
		    }
  
  if ( csq_min == 10.0 ) return kFALSE;
  
  fChisq[coord] = Fitting( coord, &hsel[0] );
  if ( fChisq[coord] < 0.0 ) return kFALSE;

  if ( MergeHit( coord ) > 0 ) fChisq[coord] = Fitting( coord );
  if ( fChisq[coord] < 0.0 ) {
    fA    [coord] = CmpLib::fDnotValid;
    fB    [coord] = CmpLib::fDnotValid;
    return kFALSE;
  }
  
  return kTRUE;
}

Double_t TrkBranch::Fitting( Int_t coord, Int_t *nth )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return CmpLib::fDnotValid;

  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return 0;
  
  Int_t nhit = 0;
  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    fHitID[coord][slice] = dst->GetHitID( CmpLib::D_SIT, slice*2+coord, nth[slice] );
    if ( fHitID[coord][slice] < 0 ) continue;
    nhit++;
  }
  
  if ( nhit < 4 ) return CmpLib::fDnotValid;

  Double_t csq = 0.0;
  do {
    csq = Fitting( coord );
  } while ( csq >= 0.0 && PurgeHit( coord ) );
  
  return csq;
}

Double_t TrkBranch::Fitting( Int_t coord )
{
  Double_t item[5] = { 0.0, 0.0, 0.0, 0.0, 0.0 };
  
  // Definition of track parameters
  //
  // y = ax + b
  //
  // item[0] = 1.0/sigma/sigma;
  // item[1] = y_i * x_i / sigma / sigma;
  // item[2] = x_i / sigma / sigma;
  // item[3] = y_i / sigma / sigma;
  // item[4] = x_i * x_i / sigma / sigma;
  //
  //      \sum{item[0]}*\sum{item[1]} - \sum{item[2]}*\sum{item[3]}
  // a = -----------------------------------------------------------
  //      \sum{item[0]}*\sum{item[4]} - \sum{item[2]}*\sum{item[2]}
  //
  //      \sum{item[3]}*\sum{item[4]} - \sum{item[1]}*\sum{item[2]}
  // b =  ----------------------------------------------------------
  //      \sum{item[0]}*\sum{item[4]} - \sum{item[2]}*\sum{item[2]}
  //
  
  if ( !gCmpParams ) return CmpLib::fDnotValid;
  CmpParams *p = gCmpParams;

  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return 0;
  
  fNusedLyr[coord] = 0;

  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    if ( fHitID[coord][slice] < 0 ) continue;
    HitBranch *hit = dst->GetHit( fHitID[coord][slice] );
    if ( !hit ) continue;
    fNusedLyr[coord]++;
    Int_t did = hit->GetDetID();
    Int_t pid = hit->GetPriID();
    Int_t sid = hit->GetSecID();
    Int_t idx = p->GetIndex( did, pid, sid );
    TVector3 hpos = hit->GetHitPos();
    TVector3 orgn = p->GetOrigin( idx );
    Double_t r = ( coord == CmpLib::COORD_X ) ? orgn.X() : orgn.Y();
    //    Double_t r = ( coord == CmpLib::COORD_X ) ? hpos.X() : hpos.Y();
    Double_t z = orgn.Z();
    Double_t sigma = 0.15;
    Double_t sig2inv = 1.0/sigma/sigma;
    item[0] += sig2inv;
    item[1] += r*z*sig2inv;
    item[2] += z*sig2inv;
    item[3] += r*sig2inv;
    item[4] += z*z*sig2inv;
  }

  if ( fNusedLyr[coord] < 4 ) return CmpLib::fDnotValid;
  
  if ( item[0]*item[4] == item[2]*item[2] ) return CmpLib::fDnotValid;
  
  fA[coord] = (item[0]*item[1]-item[2]*item[3])/(item[0]*item[4]-item[2]*item[2]);
  fB[coord] = (item[3]*item[4]-item[1]*item[2])/(item[0]*item[4]-item[2]*item[2]);
  
  Double_t csq = 0.0;
  Int_t m = 0;
  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    if ( fHitID[coord][slice] < 0 ) continue;
    HitBranch *hit = dst->GetHit( fHitID[coord][slice] );
    if ( !hit ) continue;
    TVector3 hpos = hit->GetHitPos();
    Int_t did = hit->GetDetID();
    Int_t pid = hit->GetPriID();
    Int_t sid = hit->GetSecID();
    Int_t idx = p->GetIndex( did, pid, sid );
    TVector3 orgn = p->GetOrigin( idx );
    Double_t r = ( coord == CmpLib::COORD_X ) ? orgn.X() : orgn.Y();
    //    Double_t r = ( coord == CmpLib::COORD_X ) ? hpos.X() : hpos.Y();
    Double_t z = orgn.Z();
    Double_t sigma = 0.15;
    Double_t res = (fA[coord]*z+fB[coord])-r;
    TVector3 perr = hit->GetPosErr();
    if      ( coord == CmpLib::COORD_X ) perr.SetX( res );
    else if ( coord == CmpLib::COORD_Y ) perr.SetY( res );
    hit->SetPosErr( perr );
    csq += res*res/sigma/sigma;
    m++;
  }
  csq /= (m*1.0-2.0);
  
  return csq;
}

Int_t TrkBranch::MergeHit( Int_t coord )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return 0;

  Int_t n_merge = 0;
  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    if ( fHitID[coord][slice] >= 0 ) continue;
    fHitID[coord][slice] = FindHitID( coord, slice );
    if ( fHitID[coord][slice] <  0 ) continue;
    n_merge++;
  }
  
  return n_merge;
}

Bool_t TrkBranch::PurgeHit( Int_t coord )
{
  if ( coord < 0 || coord >= CmpLib::N_COORD ) return kFALSE;

  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return 0;
  
  Int_t    n_purge = 0;
  Int_t    s_max = -1;
  Double_t r_thr = 5.0;
  Double_t r_max = 5.0;
  Bool_t   purge = kFALSE;

  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    if ( fHitID[coord][slice] < 0 ) continue;
    HitBranch *hit = dst->GetHit( fHitID[coord][slice] );
    if ( !hit ) continue;
    Double_t res = ( coord == CmpLib::COORD_X ) ? hit->GetPosErr().X() : hit->GetPosErr().Y();
    if ( res > r_thr ) n_purge++;
    if ( res > r_max ) {
      s_max = slice;
      r_max = res;
      purge = kTRUE;
    }
  }

  if ( purge ) fHitID[coord][s_max] = -1;
  
  return purge;
}

Int_t TrkBranch::FindHitID( Int_t coord, Int_t slice )
{
  if ( !gCmpParams ) return -1;
  CmpParams *p = gCmpParams;

  if ( coord < 0 || coord >= CmpLib::N_COORD ) return -1;
  if ( slice < 0 || slice >= CmpLib::N_SLICE ) return -1;
  if ( !Found( coord ) ) return -1;

  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return 0;
  
  Int_t lyr = slice*2+coord;
  
  Int_t idx = p->GetIndex( CmpLib::D_SIT, lyr, 0 );
  if ( idx < 0 ) return -1;

  TVector3 orig = p->GetOrigin( idx );
  Double_t z0 = orig.Z();
  Double_t r0 = ( coord == CmpLib::COORD_X ) ? orig.X() : orig.Y();
  Double_t w =  ( coord == CmpLib::COORD_X ) ? p->GetDimens( idx ).X() : p->GetDimens( idx ).Y();

  Double_t r = fA[coord]*z0 + fB[coord];
  
  Int_t sid = TMath::FloorNint( (r-r0)/w + 0.5 );
  if ( sid < 0 || sid >=CmpLib::N_STRIP ) return -1;
  
  Int_t nhit = dst->GetNhits( CmpLib::D_SIT, lyr );
  for ( Int_t i = 0; i < nhit; i++ ) {
    Int_t hid = dst->GetHitID( CmpLib::D_SIT, lyr, i );
    if ( hid < 0 ) continue;
    HitBranch *hit = dst->GetHit( hid );
    if ( !hit ) continue;
    Int_t fSecID = hit->GetSecID();
    if ( ( (fSecID-2) >= 0 && fSecID-2 == sid ) ||
	 ( (fSecID-1) >= 0 && fSecID-1 == sid ) ||
	 fSecID == sid ||
	 ( (fSecID+1) < CmpLib::N_STRIP && fSecID+1 == sid ) ||
	 ( (fSecID+2) < CmpLib::N_STRIP && fSecID+2 == sid ) )
      return hid;
  }
  
  return -1;
}
