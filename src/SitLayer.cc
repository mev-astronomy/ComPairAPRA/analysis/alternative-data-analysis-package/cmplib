#include "SitLayer.h"
#include "CmpTree.h"
#include "CmpParams.h"

#include "SitLib.h"
#include "SitAsic.h"
#include "SitEvent.h"
#include "SitStrip.h"
#include "SitParams.h"
#include "SitBranch.h"

#include "TFile.h"
#include "TClonesArray.h"

ClassImp( SitLayer )

SitLayer::SitLayer( void ) : DetBranch( CmpLib::D_SIT, -1, SitLib::N_MHIT )
{
  fCoord = -1;
  fArray = new TClonesArray( "SitStrip", SitLib::N_MHIT );
  Clear();
}

SitLayer::SitLayer( Int_t slice, Int_t coord ) : DetBranch( CmpLib::D_SIT, slice*2+coord, SitLib::N_MHIT )
{
  fCoord = coord;
  fArray = new TClonesArray( "SitStrip", SitLib::N_MHIT );
  Clear();
}

SitLayer::~SitLayer()
{
}

SitLayer& SitLayer::operator= (const SitLayer &sit)
{
  fCoord = sit.fCoord;
  for ( Int_t i = 0; i < SitLib::N_STRIP; i++ ) {
    fFlag[i] = sit.fFlag[i];
    fEdep[i] = sit.fEdep[i];
    fHpos[i] = sit.fHpos[i];
  }

  fArray = sit.fArray;

  fDetID = sit.fDetID;
  fLyrID = sit.fLyrID;
  fNhits = sit.fNhits;
  fMhits = sit.fMhits;
  fEne   = sit.fEne;
  
  return *this;
}

void SitLayer::Clear( Option_t *option )
{
  DetBranch::Clear();
  
  for ( Int_t i = 0; i < SitLib::N_STRIP; i++ ) {
    fFlag[i] = kFALSE;
    fEdep[i] = SitLib::fDnotValid;
    fHpos[i] = SitLib::fVnotValid;
  }

  fArray->Clear( "C" );
}

void SitLayer::Print( Option_t *option ) const
{
}

Bool_t SitLayer::GetFlag( Int_t sid ) const
{
  if ( sid < 0 || sid >= SitLib::N_STRIP ) return kFALSE;
  return fFlag[sid];
}

Double_t SitLayer::GetEdep( Int_t sid ) const
{
  if ( sid < 0 || sid >= SitLib::N_STRIP ) return SitLib::fDnotValid;
  return fEdep[sid];
}

TVector3 SitLayer::GetHpos( Int_t sid ) const
{
  if ( sid < 0 || sid >= SitLib::N_STRIP ) return SitLib::fVnotValid;
  return fHpos[sid];
}

SitStrip *SitLayer::GetStrip( Int_t hid )
{
  if ( hid < 0 || hid >= SitLib::N_MHIT ) return (SitStrip *)NULL;
  return (SitStrip *)fArray->At( hid );
}

Int_t SitLayer::FindHitID( Int_t sid )
{
  if ( sid < 0 || sid >= SitLib::N_STRIP ) return -1;

  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return -1;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return -1;

  for ( Int_t i = 0; i < fNhits; i++ ) {
    HitBranch *hit = GetNthHit( i );
    if ( !hit ) continue;
    Int_t fSecID = hit->GetSecID();
    if ( ( (fSecID-2) >= 0 && fSecID-2 == sid ) ||
	 ( (fSecID-1) >= 0 && fSecID-1 == sid ) ||
	 fSecID == sid ||
	 ( (fSecID+1) < SitLib::N_STRIP && fSecID+1 == sid ) ||
	 ( (fSecID+2) < SitLib::N_STRIP && fSecID+2 == sid ) )
      return GetNthHid( i );
  }

  return -1;
}

void SitLayer::SetHit( Int_t idx, Double_t edep, TVector3 hpos )
{
  if ( idx < 0 ) return;
  Int_t sid = gCmpParams->GetSecID( idx );
  if ( sid < 0 ) return;
  fFlag[sid] = kTRUE;
  fEdep[sid] = edep;
  fHpos[sid] = hpos;
  fEne.insert( std::multimap<Double_t, Int_t>::value_type( fEdep[sid], idx ) );
}

void SitLayer::SetFlag( Int_t sid, Bool_t flag )
{
  if ( sid < 0 || sid >= SitLib::N_STRIP ) return;
  fFlag[sid] = flag;
}

void SitLayer::SetEdep( Int_t sid, Double_t edep )
{
  if ( sid < 0 || sid >= SitLib::N_STRIP ) return;
  fEdep[sid] = edep;
}

void SitLayer::SetHpos( Int_t sid, TVector3 hpos )
{
  if ( sid < 0 || sid >= SitLib::N_STRIP ) return;
  fHpos[sid] = hpos;
}

void SitLayer::Process( Int_t slice )
{
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return;
  SitBranch *sit = ctree->GetSit( slice );
  if ( !sit ) return;
  SitEvent  *evt = sit->GetEvent();
  if ( !evt ) return;

  Double_t time = ctree->GetUtcTime();
  Int_t    ip   = gSitParams->GetPoint( time );
  if ( ip < 0 ) return;
  
  for ( Int_t aid = 0; aid < SitLib::N_ASIC; aid++ ) {
    SitAsic *asic = evt->GetAsic( aid );
    for ( Int_t cid = 0; cid < SitLib::N_CHAN; cid++ ) {
      Int_t lidx = gSitParams->GetLindx( slice, aid, cid );
      if ( lidx < 0 || gSitParams->IsDead( lidx ) ) continue;
      Int_t coord = gSitParams->GetCoord( lidx );
      if ( coord != fCoord ) continue;
      Int_t    sid  = gSitParams->GetStrip( lidx );
      Double_t peds = gSitParams->GetPeds ( lidx, ip );
      Double_t gain = gSitParams->GetGain ( lidx, ip );
      Double_t thre = gSitParams->GetThre ( lidx, ip );
      UShort_t data = asic->GetData( cid );
      fEdep[sid] = (data-peds)*gain;
      if ( (data-peds) < thre ) continue;
      Int_t idx = gCmpParams->GetIndex( fDetID, fLyrID, sid );
      fEne.insert( std::multimap<Double_t, Int_t>::value_type( fEdep[sid], idx ) );
      fHpos[sid] = gCmpParams->GetOrigin( idx );
      fFlag[sid] = kTRUE;
    }
  }
  
  ProcHit( slice );
}

void SitLayer::ProcHit( Int_t slice )
{
  for ( it_type it = fEne.begin(); it != fEne.end(); ++it ) {
    Int_t idx = it->second;
    Int_t sid = gCmpParams->GetSecID( idx );
    if ( !GetFlag( sid ) ) continue;
    Int_t nhit = fArray->GetEntries();
    if ( nhit >= SitLib::N_MHIT ) continue;
    SitStrip *strp = (SitStrip *)fArray->ConstructedAt( nhit );
    SetFlag( sid, kFALSE );
    strp->SetIndx( it->second );
    strp->SetEdep( fEdep[sid] ); // it->first
    strp->SetHpos( fHpos[sid] );
    strp->SetEpos( TVector3( 0, 0, 0 ) ); // Temporary set 0
    if ( GetFlag( sid-1 ) ) {
      strp->SetNhitAdj( SitLib::SIDE_L, 1 );
      strp->SetEdepAdj( SitLib::SIDE_L, fEdep[sid-1] );
      strp->SetHposAdj( SitLib::SIDE_L, fHpos[sid-1] );
      SetFlag( sid-1 , kFALSE );
    }
    if ( GetFlag( sid+1 ) ) {
      strp->SetNhitAdj( SitLib::SIDE_R, 1 );
      strp->SetEdepAdj( SitLib::SIDE_R, fEdep[sid+1] );
      strp->SetHposAdj( SitLib::SIDE_R, fHpos[sid+1] );
      SetFlag( sid+1 , kFALSE );
    }
  }
}

void SitLayer::CopyHit( void )
{
  // The main method of CztBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return;

  for ( Int_t i = 0; i < fArray->GetEntries(); i++ ) {
    HitBranch *hit = NewHit();
    if ( !hit ) continue;
    SitStrip *strp = GetStrip( i );
    if ( !strp ) continue;
    hit->SetIndex ( strp->GetIndx() );
    hit->SetHitEne( 1.0e-3*strp->GetEdep( kTRUE ) ); // keV --> MeV
    hit->SetHitPos( strp->GetHpos( kTRUE ) );
  }
}
