#include "SitEventID.h"

#include "TMath.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TGraph.h"

#include <iostream>

SitEventID::SitEventID( void )
{
  fReqTime  = 10.0;
  fSysClock = 10.0e-9;
  
  fAtrb = PKT_ATRB_SIT;
  fBuffer = new unsigned char[MAX_PKT_SIZE];
}

SitEventID::SitEventID( UShort_t atrb ) : DataEventID( atrb )
{
  fReqTime = 10.0;
  fSysClock = 10.0e-9;

  fBuffer = new unsigned char[MAX_PKT_SIZE];
}

SitEventID::~SitEventID()
{
  if ( fBuffer ) delete [] fBuffer;
}

void SitEventID::SetUtcTime( void )
{
  UShort_t iptr = 11;
  ULong64_t time = 0;
  for ( Int_t i = 0; i < 8; i++ ) {
    time *= 256;
    time += fBuffer[iptr-i];
  }

  Long_t epch = time/1000000000;
  Long_t epns = time%1000000000;
  
  fUtcTime = epch*1.0+epns*1.0e-9;
}

void SitEventID::SetEventID( void )
{
  UShort_t iptr = 20;
  fEventID = fBuffer[(iptr+3)]*256*256*256 + fBuffer[(iptr+2)]*256*256 + fBuffer[(iptr+1)]*256 + fBuffer[(iptr+0)];
}

void SitEventID::SetTimePps( void )
{
  UShort_t iptr = 28;
  fTimePps = fBuffer[(iptr+3)]*256*256*256 + fBuffer[(iptr+2)]*256*256 + fBuffer[(iptr+1)]*256 + fBuffer[(iptr+0)];
}

Int_t SitEventID::Branch( void )
{
  if ( !fTree ) return -1;
  fTree->Branch( "fUtcTime", &fUtcTime, "fUtcTime/D" );
  fTree->Branch( "fEventID", &fEventID, "fEventID/i" );
  fTree->Branch( "fTimePps", &fTimePps, "fTimePps/i" );
  return 0;
}

Int_t SitEventID::Process( void )
{
  //  TGraph *gr = new TGraph();

  for ( Int_t ievt = 0; ievt < fTree->GetEntries(); ievt++ ) {
    fTree->GetEvent( ievt );
    Double_t fUtcCorr = TMath::Nint( fUtcTime-fTimePps*fSysClock ) + fTimePps*fSysClock;
    UInt_t   fEpoch   = TMath::FloorNint( fUtcCorr );
    UInt_t   fEpochNS = TMath::FloorNint( ( fUtcCorr - fEpoch )*1e+9 );

    fVevid.push_back( fEventID );
    fVepch.push_back( fEpoch );
    fVepns.push_back( fEpochNS );
    
    //    gr->SetPoint( gr->GetN(), ievt, fUtcTime-fEpoch-fEpochNS*1.0e-9 );
  }

  /*
  TCanvas *c1 = new TCanvas( "c1", "c1" );
  TH2D *hist = new TH2D( "hist", "Time diff. [sec]", 1, -1000.0, fTree->GetEntries(), 1, -1.0, 1.0 );
  hist->SetStats( 0 );
  hist->GetXaxis()->SetTitle( "Event in file" );
  hist->Draw();
  gr->SetMarkerStyle( 20 );
  gr->SetMarkerSize( 0.25 );
  gr->Draw( "AP" );
  c1->SaveAs( "diff.png" );
  delete c1;
  delete hist;
  */
  
  //  delete gr;
  
  return 0;
}

Int_t SitEventID::Read( void )
{
  Clear();

  // Check Dummy word
  fIN.read( (char *)fBuffer+fSize, 2 );
  if ( fIN.eof() ) return 0;
  Int_t pkt_size = fBuffer[0]+fBuffer[1]*256;

  fSize += fIN.gcount();

  // Read Header
  fIN.read( (char *)fBuffer+fSize, pkt_size-2 );
  fSize += fIN.gcount();

  SetUtcTime();
  SetEventID();
  SetTimePps();
  
  return 1;
}

void SitEventID::Clear( Option_t *option )
{
  DataEventID::Clear();
  
  fUtcTime = 0.0;
  fEventID = 0;
  fTimePps = 0;
}
