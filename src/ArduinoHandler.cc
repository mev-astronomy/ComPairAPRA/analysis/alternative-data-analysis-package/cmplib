#include "ArduinoHandler.h"

#include "TMath.h"
#include "TString.h"
#include "TObjString.h"

#include <iostream>
#include <fstream>
#include <string>

ArduinoHandler::ArduinoHandler()
{
  Clear();
}

ArduinoHandler::~ArduinoHandler()
{
}

void ArduinoHandler::Clear( void )
{
  f1stGpsTime = 0;
  fIndex.clear();
  fPacket.clear();
  fArduino.clear();
}

Int_t ArduinoHandler::Add( const char *in_fname )
{
  fList.Add( new TObjString( in_fname ) );

  return 0;
}

Int_t ArduinoHandler::Process( void )
{
  ReadArduino();
  SortArduino();
  FindArduino();
  PairArduino();
  CoinArduino();
  TimeArduino();
  EvidArduino();
  FormArduino();

  return 0;
}

void ArduinoHandler::ReadArduino( void )
{
  UInt_t npacket = 0;
  std::string s;
  std::ifstream fIN;

  ArduIno ardu;
  
  Int_t n_tc = 0;

  do {
    if ( fIN.is_open() && fIN.eof() ) {
      fIN.close();
      fList.RemoveFirst();
    }

    if ( !fIN.is_open() && fList.Last() != 0 ) {
      TObjString *str = (TObjString *)fList.At( 0 );
      if ( !str ) return;

      fIN.open( str->GetName(), std::ios::in );
      if ( !fIN.is_open() ) {
	std::cout << "Error in opening Arduino log file: "
		  << str->GetName() << std::endl;
	return;
      }
    }
  
    while ( 1 ) {
      std::getline( fIN, s );
      if ( fIN.eof() ) break;
      ardu.SetArduIno( s );
      fPacket.push_back( ardu );
      fIndex.push_back( npacket++ );
      if ( ardu.GetDataType() == 3 ) n_tc++;
    }
  } while ( fList.Last() != 0 );

  std::cout << Form( "<ArduinoHandler::ReadArduino> Number of T_C = %d", n_tc ) << std::endl;
}

void ArduinoHandler::SortArduino( void )
{
  std::cout << "<ArduinoHandler::SortArduino>" << std::endl;

  UInt_t fTimeStamp;
  UInt_t fPrevStamp = 0;

  UInt_t fCurrType  = 0;
  
  Bool_t flag = kFALSE;

  Int_t nwrong;
  Int_t npacket = fIndex.size();

  ArduIno ardu;
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu = fPacket[(fIndex[i])];
    fTimeStamp = 0;
    fCurrType = ardu.GetDataType();

    if ( fCurrType != 255 ) {
      if ( fCurrType == 4 ) {
	fTimeStamp = fPrevStamp;
	fPacket[(fIndex[i])].SetData1st( fTimeStamp );	
      }
      else fTimeStamp = ardu.GetData1st();
      fPrevStamp = fTimeStamp;
    }
  }

  Int_t nprev = 0;
  
  do {
    flag = kFALSE;
    nwrong = 0;

    for ( Int_t i = 0; i < npacket; i++ ) {
      ardu = fPacket[(fIndex[i])];
      fTimeStamp = 0;
      
      if ( ardu.GetDataType() != 255 ) {
	fTimeStamp = ardu.GetData1st();
	if ( flag && fPrevStamp - fTimeStamp < 2000000000 && fPrevStamp > fTimeStamp ) {
	  nwrong++;
	  UInt_t p = fIndex[i-1];
	  UInt_t c = fIndex[i];
	  fIndex[i] = p;
	  fIndex[i-1] = c;
	  fTimeStamp = fPrevStamp;
	}

	flag = kTRUE;
	fPrevStamp = fTimeStamp;
      }
    }

    if ( nwrong != nprev ) std::cout << nwrong << std::endl;
    nprev = nwrong;
  } while ( nwrong > 0 );
}

void ArduinoHandler::FindArduino( void )
{
  std::cout << "<ArduinoHandler::FindArduino>" << std::endl;

  UInt_t fTimeStamp;
  UInt_t fPrevStamp = 0;
  UInt_t fPrevIndex = 0;

  UInt_t fFirstC = 0;
  UInt_t fLastC  = 0;

  Int_t ninit = 0;
  Int_t npacket = fIndex.size();

  ArduIno ardu;
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];

    fTimeStamp = 0;

    if ( ninit >= 9 ) continue;
    if ( ninit > 0 ) std::cout << ninit << std::endl;

    if ( ardu.GetDataType() == 3 ) {
      fTimeStamp = ardu.GetData1st();
      if ( fPrevIndex > 0 && fTimeStamp - fPrevStamp < 4205 &&  fTimeStamp - fPrevStamp > 4195 ) {
	fPacket[(fIndex[i])].SetDataType( 5 );
	fPacket[fPrevIndex].SetDataType( 255 );
	ninit++;
      }
      if ( ninit == 0 ) fFirstC = fTimeStamp;
      fLastC = fTimeStamp;
      
      fPrevIndex = fIndex[i];
      fPrevStamp = fTimeStamp;
    }
  }
  
  for ( std::vector<UInt_t>::iterator it = fIndex.begin(); it != fIndex.end(); ++it ) {
    if ( fPacket[*it].GetDataType() == 255 ) it = fIndex.erase( it );
  }

  std::cout << Form( "%u %u %u", fFirstC, fLastC, fLastC-fFirstC ) << std::endl;
}

void ArduinoHandler::PairArduino( void )
{
  std::cout << "<ArduinoHandler::PairArduino>" << std::endl;

  UInt_t fDataType;
  UInt_t fPrevType = 0;
  UInt_t fEventID  = 0;
  
  Int_t npacket = fIndex.size();
  Int_t mpacket = npacket/10;

  ArduIno ardu;

  Int_t n_tc = 0;
  Int_t n_pa = 0;
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    if ( i%mpacket == 0 ) std::cout << Form( "Processed %8d/%8d", i, npacket ) << std::endl;
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];

    fDataType  = 0;
    
    if ( ardu.GetDataType() == 1 ) {
      fDataType = T_P;
    }
    else if ( ardu.GetDataType() == 2 ) {
      fDataType = T_R;
    }
    else if ( ardu.GetDataType() == 3 ) {
      n_tc++;
      fDataType = T_C;
      fPacket[(fIndex[i])].SetEventID ( 0 );
    }
    else if ( ardu.GetDataType() == 4 ) {
      fDataType = T_E;
      fEventID = fPacket[(fIndex[i])].GetEventID();
      if ( fPrevType == T_C ) {
	fPacket[(fIndex[i-1])].SetEventID ( fEventID );
	fPacket[(fIndex[i])].SetDataType( 255 );
	n_pa++;
      }
    }
    
    fPrevType  = fDataType;
  }

  std::cout << Form( "Number of Paired T_C / Number of T_C = %d / %d", n_pa, n_tc ) << std::endl;
}

void ArduinoHandler::CoinArduino( void )
{
  std::cout << "<ArduinoHandler::CoinArduino>" << std::endl;
  
  UInt_t fDataType;
  UInt_t fPrevType = 0;
  UInt_t fGranType = 0;
  UInt_t fTimeStamp;
  UInt_t fPrevStamp = 0;

  Int_t npacket = fIndex.size();

  ArduIno ardu;
  
  Int_t mpacket = npacket/10;
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    if ( i%mpacket == 0 ) std::cout << Form( "Processed %8d/%8d", i, npacket ) << std::endl;

    ardu.Clear();
    ardu = fPacket[(fIndex[i])];

    fDataType  = 0;
    fTimeStamp = 0;

    if ( ardu.GetDataType() == 255 ) continue;
    
    if ( ardu.GetDataType() == 1 ) {
      fDataType = T_P;
      fTimeStamp = ardu.GetData1st();
    }
    else if ( ardu.GetDataType() == 2 ) {
      fDataType = T_R;
      fTimeStamp = ardu.GetData1st();
      UInt_t fDiffStamp = fTimeStamp - fPrevStamp;
      if ( fPrevType == T_R && fDiffStamp < 520 ) {
	ArduIno p = fPacket[(fIndex[i-1])];
	ArduIno c = fPacket[(fIndex[i])];
	fPacket[(fIndex[i])] = p;
	fPacket[(fIndex[i-1])].SetDataType( 255 );
	fTimeStamp = fPrevStamp;
      }
      if ( fPrevType == T_C && fGranType == T_R && fDiffStamp < 520 ) {
	ArduIno g = fPacket[(fIndex[i-2])];
	ArduIno p = fPacket[(fIndex[i-1])];
	ArduIno c = fPacket[(fIndex[i])];
	if ( g.GetDataType() == 255 ) {
	  fPacket[(fIndex[i])] = p;
	  fPacket[(fIndex[i-1])].SetDataType( 255 );
	  fTimeStamp = fPrevStamp;
	}
      }
    }
    else if ( ardu.GetDataType() == 3 ) {
      fDataType = T_C;
      fTimeStamp = ardu.GetData1st();
      UInt_t fDiffStamp = fTimeStamp - fPrevStamp;
      if ( fPrevType == T_R && fDiffStamp < 160 && fDiffStamp > 130 ) {
	fPacket[(fIndex[i])].SetDataType( 6 );
	fPacket[(fIndex[i-1])].SetDataType( 255 );
	fTimeStamp = fPrevStamp;
      }
    }
    else if ( ardu.GetDataType() == 4 ) {
      fDataType = T_E;
      fTimeStamp = ardu.GetData1st();
    }
    
    fGranType  = fPrevType;
    fPrevType  = fDataType;
    fPrevStamp = fTimeStamp;
  }
}

void ArduinoHandler::TimeArduino( void )
{
  UInt_t    fTimeStamp;
  ULong64_t fGpsTime = 0;
  UInt_t    fGpsStamp = 0;
  ULong64_t fUtcTime;

  Bool_t flag = kTRUE;

  Double_t fDecimal = 0.0;
  
  Int_t npacket = fIndex.size();

  ArduIno ardu;
  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];

    fTimeStamp = 0;
    
    if ( ardu.GetDataType() == 5 ) {
      std::cout << "<ArduinoHandler::TimeArduino> ardu.GetDataType() = 5" << std::endl;
      flag = kFALSE;
    }

    if ( ardu.GetDataType() == 255 ) continue;
    
    if ( ardu.GetDataType() == 1 ) {
      fGpsStamp = ardu.GetData1st();
      if ( flag ) {
	fDecimal = TMath::Nint( ardu.GetUtcTime() ) - ardu.GetUtcTime();
	fGpsTime = TMath::Nint( ardu.GetUtcTime() + fDecimal ) * 1e9; // [ns]
	f1stGpsTime = fGpsTime;
      }
      else {
	fGpsTime = TMath::Nint( ardu.GetUtcTime() + fDecimal ) * 1e9; // [ns]
      }
      
      fPacket[(fIndex[i])].SetDataType( 255 );
    }
    else if ( ardu.GetDataType() == 2 || ardu.GetDataType() == 3 || ardu.GetDataType() == 4 || ardu.GetDataType() == 6 ) {
      fTimeStamp = ardu.GetData1st();
      if ( fGpsTime > 0 ) {
	UInt_t diffst = fTimeStamp-fGpsStamp;
	fUtcTime = fGpsTime + TMath::Nint((diffst)*1000.0/42.0); // [ns]
	if ( fUtcTime == 0 ) std::cout << "[Error] fUtcCorr = 0" << std::endl;
	fPacket[(fIndex[i])].SetUtcCorr(fUtcTime );
      }
    }
  }
}

void ArduinoHandler::EvidArduino( void )
{
  Int_t npacket = fIndex.size();

  ArduIno ardu;

  UInt_t fFrstID = 0;
  UInt_t fLastID = 0;
  UInt_t fPrevID = 0;
  UInt_t fEvntID = 0;

  UInt_t ieid = 0;
  UInt_t ievt = 0;

  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];
    if ( ardu.GetDataType() == 3 || ardu.GetDataType() == 6 ) {
      fEvntID = ardu.GetEventID();
      if ( fEvntID != 0 ) {
	fFrstID = fEvntID-ievt;
	break;
      }
      ievt++;
    }
  }

  std::cout << Form( "Number of missed EventID until fFrstID = %d", ievt ) << std::endl;
  
  ievt = 0;
  Int_t jevt = 0;
  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];
    if ( ardu.GetDataType() == 3 || ardu.GetDataType() == 6 ) {
      fEvntID = ardu.GetEventID();
      if ( fEvntID != 0 ) {
	ieid++;
	fLastID = fEvntID;
	jevt=0;
      }
      ievt++;
      jevt++;
    }
  }
  
  std::cout << Form( "Number of missed EventID after fLastID = %d", jevt ) << std::endl;
  std::cout << Form( "Nack = %d, Neid = %d, FirstID = 0x%0X, LastID = 0x%0X", ievt, ieid, fFrstID, fLastID ) << std::endl;
  
  ievt = 0;
  fPrevID = fFrstID - 1;

  Int_t n_event = 0;
  Int_t n_ezero = 0;
  Int_t n_wrong[3] = { 0 };
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];
    if ( ardu.GetDataType() == 3 || ardu.GetDataType() == 6 ) {
      fEvntID = ardu.GetEventID();
      n_event++;
      
      if ( fEvntID < 0x80000000 ) {
	if ( fEvntID != 0 ) std::cout << Form( "[Warning] fEvntID = 0x%08x", fEvntID ) << std::endl;
	fEvntID = 0;
	n_wrong[0]++;
      }
      else if ( fEvntID < fFrstID + n_event - 1 ) {
	std::cout << Form( "[Warning] fEvntID = 0x%08x (0x%08x)", fEvntID, fFrstID+n_event ) << std::endl;
	fEvntID = 0;
	n_wrong[1]++;
      }
      else if ( fEvntID > fLastID - (ievt-n_event) ) {
	std::cout << Form( "[Warning] fEvntID = 0x%08x (0x%08x)", fEvntID, fLastID - (ievt-n_event) ) << std::endl;
	fEvntID = 0;
	n_wrong[2]++;
      }
      
      if ( fEvntID == 0 ) n_ezero++;
      fPacket[(fIndex[i])].SetEventID( fEvntID );
    }
  }

  std::cout << Form( "N_EZERO = %d / %d, %d %d %d", n_ezero, n_event, n_wrong[0], n_wrong[1], n_wrong[2] ) << std::endl;

  Int_t n_zero = 0;
  std::vector<Int_t> ids;
  ids.clear();
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];
    if ( ardu.GetDataType() == 3 || ardu.GetDataType() == 6 ) {
      fEvntID = ardu.GetEventID();
      
      if ( fEvntID == 0 ) {
	ids.push_back(fIndex[i]);
	n_zero++;
      }

      if ( fEvntID != 0 && n_zero != 0 ) {
	if ( fEvntID - fFrstID == n_zero + 1 ) {
	  for ( Int_t j = 0; j < n_zero; j++ ) {
	    ardu.Clear();
	    ardu = fPacket[ids[j]];
	    fPacket[ids[j]].SetEventID( fFrstID+j+1 );	    
	  }
	}
	ids.clear();
	n_zero = 0;
      }

      if ( fEvntID != 0 ) fFrstID = fEvntID;
    }
  }

  fPrevID = fFrstID - 1;
  n_zero = 0;
  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];
    if ( ardu.GetDataType() == 3 || ardu.GetDataType() == 6 ) {
      fEvntID = ardu.GetEventID();

      if ( fEvntID == 0 ) n_zero++;
      else {
	if ( fEvntID <= fPrevID )
	  std::cout << Form( "fEvntID = 0x%08x, fPrevID = 0x%08x", fEvntID, fPrevID ) << std::endl;
	fPrevID = fEvntID;
      }
    }
  }

  std::cout << Form( "N_ZERO = %d / %d", n_zero, n_event ) << std::endl;
}

void ArduinoHandler::FormArduino( void )
{
  Int_t npacket = fIndex.size();
  Bool_t flag  = kFALSE;
  Bool_t speak = kFALSE;
  std::string dt;

  Int_t count[7] = {0};
  
  ArduIno ardu;
  for ( Int_t i = 0; i < npacket; i++ ) {
    ardu.Clear();
    ardu = fPacket[(fIndex[i])];
    
    if ( ardu.GetDataType() == 5 ) flag = kTRUE;
    
    if ( ardu.GetDataType() == 255 ) continue;
    if ( ardu.GetDataType() == 4   ) continue;
    if ( flag ) {
      if ( speak )
	std::cout << Form( "%d,0x%0x,%8u,%8u,%lf,%llu", ardu.GetDataType(), ardu.GetEventID(), ardu.GetData1st(), ardu.GetData2nd(), ardu.GetUtcTime(), ardu.GetUtcCorr() ) << std::endl;
      
      if      ( ardu.GetDataType() == 2 ) { dt = "R";   count[0]++; }
      else if ( ardu.GetDataType() == 3 ) {
	if    ( ardu.GetEventID () != 0 ) { dt = "CE";  count[1]++; }
	else                              { dt = "C";   count[2]++; }
      }
      else if ( ardu.GetDataType() == 4 ) { dt = "E";   count[3]++; }
      else if ( ardu.GetDataType() == 6 ) {
	if    ( ardu.GetEventID () != 0 ) { dt = "CER"; count[4]++; }
	else                              { dt = "CR";  count[5]++; }
      }
      else                                { dt = "N";   count[6]++; std::cout << Form( "ardu.GetDataType() = %d", ardu.GetDataType() ) << std::endl; continue; }
      fArduino.push_back( Form( "%d,%llu,%u", ardu.GetDataType(), ardu.GetUtcCorr(), ardu.GetEventID() ) );
    }
  }

  std::cout << Form( "[R] = %d, [CE] = %d, [C] = %d, [E] = %d, [CER] = %d, [CR] = %d, [N] = %d",
		     count[0], count[1], count[2], count[3], count[4], count[5], count[6] ) << std::endl;
}

void ArduinoHandler::WriteArduino( void )
{
  Int_t npacket = fArduino.size();

  for ( Int_t i = 0; i < npacket; i++ ) {
    std::cout << Form( "%s", fArduino[i].c_str() ) << std::endl;
  }
}
