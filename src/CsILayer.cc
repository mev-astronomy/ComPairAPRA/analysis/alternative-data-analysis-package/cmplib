#include "CsILayer.h"
#include "CsILib.h"
#include "CsIParams.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"
#include "DstBranch.h"
#include "HitBranch.h"

#include "TFile.h"

ClassImp( CsILayer )

CsILayer::CsILayer( void )
{
  fCoord = -1;
  for ( Int_t log = 0; log < CsILib::N_LOGS; log++ ) {
    fLog[log].SetLogID( log );
  }
  Clear();
}

CsILayer::~CsILayer()
{
}

Int_t CsILayer::FindLogID( Int_t hit )
{
  if ( hit < 0 || hit >= fNhits ) return -1;

  HitBranch *h = GetNthHit( hit );
  if ( !h ) return -1;
  
  return h->GetSecID();
}

Int_t CsILayer::FindHitID( Int_t log )
{
  if ( log < 0 || log >= CsILib::N_LOGS ) return -1;

  for ( Int_t i = 0; i < fNhits; i++ ) {
    if ( FindLogID( i ) == log ) return i;
  }

  return -1;
}

CsILog *CsILayer::GetHitLog( Int_t hit )
{
  Int_t log = FindLogID( hit );
  if ( log < 0 ) return (CsILog *)NULL;

  return &fLog[log];
}

Double_t CsILayer::GetHitEne( Int_t hit )
{
  Int_t log = FindLogID( hit );
  if ( log < 0 ) return CmpLib::fDnotValid;

  return fLog[log].GetHitEne();;
}

TVector3 CsILayer::GetHitPos( Int_t hit )
{
  Int_t log = FindLogID( hit );
  if ( log < 0 ) return CmpLib::fVnotValid;

  return fLog[log].GetHitPos();
}

CsILog *CsILayer::GetCsILog( Int_t log )
{
  if ( log < 0 || log >= CsILib::N_LOGS ) return (CsILog *)NULL;
  return &fLog[log];
}

Double_t CsILayer::GetCsIEne( Int_t log )
{
  if ( log < 0 || log >= CsILib::N_LOGS ) return CmpLib::fDnotValid;
  return fLog[log].GetHitEne();;
}

TVector3 CsILayer::GetCsIPos( Int_t log )
{
  if ( log < 0 || log >= CsILib::N_LOGS ) return CmpLib::fVnotValid;
  return fLog[log].GetHitPos();
}

void CsILayer::SetData( CsIEvent *event )
{
  if ( !event ) return;

  for ( Int_t log = 0; log < CsILib::N_LOGS; log++ ) {
    for ( Int_t side = 0; side < CsILib::N_SIDE; side++ ) {
      Int_t indx = gCsIParams->GetIndex( fLyrID, log, side );
      if ( indx < 0 || indx >= CsILib::N_INDX ) continue;
      fLog[log].SetData( side, event->GetData( indx ) );
    }
  }
}

Int_t CsILayer::Process( Int_t sel )
{
  if ( !gCsIParams ) return 0;

  Double_t Cs137 = 661.7; // [keV]
  Double_t eres  = 0.05*Cs137;
  
  for ( Int_t log = 0; log < CsILib::N_LOGS; log++ ) {
    if ( fLog[log].Process( fLyrID ) > 0 ) {
      fEne.insert( std::multimap<Double_t, Int_t>::value_type( fLog[log].GetHitEne(), log ) );
      fPos.insert( std::map     <Int_t, TVector3>::value_type( log, fLog[log].GetHitPos() ) );
      fSta.insert( std::map     <Int_t,    Int_t>::value_type( log, fLog[log].GetHitSta() ) );
    }
  }
  
  Bool_t flag = kFALSE;
  for ( it_type it = fEne.begin(); it != fEne.end(); ++it ) {
    if ( it->first > Cs137-eres && it->first < Cs137+eres ) flag = kTRUE;
  }
  if ( sel == CmpLib::HITEVT && flag == kFALSE ) return 0;

  CopyHit();
  
  return 1;
}

void CsILayer::CopyHit( void )
{
  for ( it_type it = fEne.begin(); it != fEne.end(); ++it ) {
    HitBranch *hit = NewHit();
    if ( !hit ) continue;
    Int_t idx = gCmpParams->GetIndex( fDetID, fLyrID, it->second );
    hit->SetIndex ( idx );
    hit->SetHitEne( 1.0e-3*it->first  ); // keV --> MeV
    hit->SetHitPos( fPos[it->second] );
    hit->SetHitSta( fSta[it->second] );
  }
}

void CsILayer::Clear( Option_t *option )
{
  DetBranch::Clear();

  for ( Int_t log = 0; log < CsILib::N_LOGS; log++ ) {
    fLog[log].Clear();
  }
}

void CsILayer::Print( Option_t *option ) const
{
}
