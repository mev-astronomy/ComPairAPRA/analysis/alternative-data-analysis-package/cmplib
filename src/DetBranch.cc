#include "DetBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"
#include "DstBranch.h"

ClassImp( DetBranch )

DetBranch::DetBranch( void ) : fDetID( -1 ), fLyrID( -1 ), fMhits( -1 )
{
  Clear();
}

DetBranch::DetBranch( Int_t did, Int_t lid, Int_t hit ) : fDetID( did ), fLyrID( lid ), fMhits( hit )
{
  Clear();
}

DetBranch::~DetBranch()
{
}

Double_t DetBranch::GetEnergy( void )
{
  Double_t fEnergy = 0.0;
  for ( Int_t i = 0; i < fNhits; i++ ) {
    HitBranch *hit = GetNthHit( i );
    if ( !hit ) continue;
    fEnergy += hit->GetHitEne();
  }
  return fEnergy;
}

Int_t DetBranch::GetNthHid( Int_t n )
{
  // The main method of DetBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return -1;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return -1;
  
  if ( n < 0 || n >= fNhits ) return -1;

  return dst->GetHitID( fDetID, fLyrID, n );
}

HitBranch *DetBranch::GetNthHit( Int_t n )
{
  // The main method of DetBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return (HitBranch *)NULL;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return (HitBranch *)NULL;
  
  return dst->GetHit( GetNthHid( n ) );
}

HitBranch *DetBranch::NewHit( void )
{
  // The main method of DetBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return (HitBranch *)NULL;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return (HitBranch *)NULL;
  
  if ( fNhits >= fMhits ) return (HitBranch *)NULL;

  HitBranch *hit = dst->NewHit( fDetID, fLyrID );
  if ( !hit ) return (HitBranch *)NULL;

  fNhits++;
  
  return hit;
}

Int_t DetBranch::Process( Int_t sel )
{
  // The main method of DetBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;
  
  return 1;
}

void DetBranch::Clear( Option_t *option )
{
  fNhits = 0;
  fEne.clear();
  fSta.clear();
  fPos.clear();
}

void DetBranch::Print( Option_t *option ) const
{
}
