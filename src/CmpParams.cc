#include "CmpParams.h"
#include "CmpTree.h"

#include "TFile.h"

ClassImp( CmpParams )

CmpParams *gCmpParams = 0;

CmpParams::CmpParams()
{
  for ( Int_t did = 0; did < CmpLib::N_DET; did++ ) {
    for ( Int_t pid = 0; pid < CmpLib::N_PRI; pid++ ) {
      for ( Int_t sid = 0; sid < CmpLib::N_SEC; sid++ ) {
	fIndex[did][pid][sid] = -1;
      }
    }
  }

  for ( Int_t idx = 0; idx < CmpLib::N_GEOM; idx++ ) {
    fDetID[idx] = -1;
    fPriID[idx] = -1;
    fSecID[idx] = -1;
    fOrigin[idx] = CmpLib::fVnotValid;
    fDimens[idx] = CmpLib::fVnotValid;
  }
  
  gCmpParams = this;
}

CmpParams::~CmpParams()
{
  gCmpParams = 0;
}

Int_t CmpParams::GetIndex( Int_t did, Int_t pid, Int_t sid ) const
{
  if ( did < 0 || did >= CmpLib::N_DET ) return -1;
  if ( pid < 0 || pid >= CmpLib::N_PRI ) return -1;
  if ( sid < 0 || sid >= CmpLib::N_SEC ) return -1;

  return fIndex[did][pid][sid];
}

Int_t CmpParams::GetDetID( Int_t idx ) const
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return -1;

  return fDetID[idx];
}

Int_t CmpParams::GetLyrID( Int_t idx ) const
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return -1;

  return ( fDetID[idx] == CmpLib::D_SIT ) ? fPriID[idx] : 0;
}

Int_t CmpParams::GetPriID( Int_t idx ) const
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return -1;

  return fPriID[idx];
}

Int_t CmpParams::GetSecID( Int_t idx ) const
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return -1;

  return fSecID[idx];
}

TVector3 CmpParams::GetOrigin( Int_t idx ) const
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return CmpLib::fVnotValid;

  return fOrigin[idx];
}

TVector3 CmpParams::GetDimens( Int_t idx ) const
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return CmpLib::fVnotValid;

  return fDimens[idx];
}

void CmpParams::SetIndex( Int_t idx, Int_t did, Int_t pid, Int_t sid )
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return;
  if ( did < 0 || did >= CmpLib::N_DET  ) return;
  if ( pid < 0 || pid >= CmpLib::N_PRI  ) return;
  if ( sid < 0 || sid >= CmpLib::N_SEC  ) return;

  fIndex[did][pid][sid] = idx;
  fDetID[idx] = did;
  fPriID[idx] = pid;
  fSecID[idx] = sid;
}

void CmpParams::SetOrigin( Int_t idx, Double_t x, Double_t y, Double_t z )
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return;

  fOrigin[idx] = TVector3( x, y, z );
}

void CmpParams::SetDimens( Int_t idx, Double_t x, Double_t y, Double_t z )
{
  if ( idx < 0 || idx >= CmpLib::N_GEOM ) return;

  fDimens[idx] = TVector3( x, y, z );
}

CmpParams *CmpParams::GetPtr( void )
{
  return gCmpParams;
}

CmpParams *CmpParams::ReadFile( const char *fname, Int_t verb )
{
  CmpParams *par = (CmpParams *)ReadFile( fname, "CmpParams" );
  if ( par && verb ) std::cout << "Read CmpParams from " << fname << std::endl;

  return par;
}

TObject *CmpParams::ReadFile( const char *fname, const char *cname )
{
  TFile *fsave = gFile;

  TString sfn = fname, dir;
  Int_t i1 = sfn.Index( ".root" );
  Int_t i2 = sfn.Index( "/", i1 );
  if ( i1 > 0 && i2 > i1 ) {
    dir = sfn( i2+1, sfn.Length() );
    sfn = sfn( 0, i2 );
    if ( !dir.EndsWith( "/" ) ) dir += "/";
  }

  TFile f( sfn );
  if ( !f.IsOpen() ) return 0;

  TObject *obj = f.Get( dir+cname );
  f.Close();

  if ( fsave ) fsave->cd();

  return obj;
}
