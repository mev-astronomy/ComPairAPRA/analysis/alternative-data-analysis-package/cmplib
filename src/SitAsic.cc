#include "SitAsic.h"

#include "SitLib.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"

#include <iostream>
#include <fstream>

ClassImp( SitAsic )

SitAsic::SitAsic( void )
{
}

SitAsic::~SitAsic()
{
}

UShort_t SitAsic::GetData( UShort_t ch ) const
{
  if ( ch >= SitLib::N_CHAN ) return 0;
  return fData[ch];
}

Int_t SitAsic::readAsic( unsigned char *data, Bool_t speak )
{
  unsigned char f4byte[4];
  unsigned char f8byte[8];
  unsigned char f48byte[48];
  unsigned char f2byte[2];
  
  int size = 0;

  // read asic number
  
  // read event id
  fAsicNum = *(data+size);
  size += 1;

  // read asic tba
  fAsicTBA = *(data+size);
  size += 1;
  
  // read asic header
  memcpy( (char *)&f2byte, (void *)(data+size), sizeof(f2byte) );
  size += sizeof(f2byte);
  fAsicHdr = convData( f2byte );
  
  // read missing event counter
  memcpy( (char *)&f4byte, (void *)(data+size), sizeof(f4byte) );
  size += sizeof(f4byte);
  fNmissEv = convData( f4byte );
  
  // read trigger produced
  memcpy( (char *)&f4byte, (void *)(data+size), sizeof(f4byte) );
  size += sizeof(f4byte);
  fTrigPrd = convData( f4byte );

  // read global counter
  memcpy( (char *)&f8byte, (void *)(data+size), sizeof(f8byte) );
  size += sizeof(f8byte);
  fAsicTBD = convLong( f8byte );

  // read asic data
  memcpy( (char *)f48byte, (void *)(data+size), sizeof(f48byte) );
  size += sizeof(f48byte);
  convAsic( f48byte );

  return size;
}

Bool_t SitAsic::readAsic( std::ifstream *fin, Bool_t speak )
{
  return kFALSE;
}

void SitAsic::Clear( Option_t *option )
{
  fAsicNum = 0;
  fAsicTBA = 0;
  fAsicHdr = 0;
  fNmissEv = 0;
  fTrigPrd = 0;
  fAsicTBD = 0;
  fRefData = 0;
  for ( Int_t i = 0; i < SitLib::N_CHAN; i++ ) {
    fData[i] = 0;
  }
}

UInt_t SitAsic::convData( unsigned char *data )
{
  Int_t order[4] = { 0, 1, 2, 3 };
  UInt_t fData = 0;
  for ( Int_t i = 0; i < 4; i++ ) {
    fData *= 256;
    fData += data[order[i]];
  }
  return fData;
}

ULong64_t SitAsic::convLong( unsigned char *data )
{
  Int_t order[8] = { 0, 1, 2, 3, 4, 5, 6, 7 };
  ULong64_t fData = 0;
  for ( Int_t i = 0; i < 8; i++ ) {
    fData *= 256;
    fData += data[order[i]];
  }
  return fData;
}

void SitAsic::convAsic( unsigned char *data )
{
  fRefData = ( ( *(data+4) & 0xC0 ) >> 6 ) + ( ( *(data+5) & 0xFF ) << 2 );
  Int_t c_id = 0;
  for ( Int_t i = 6; i < 46; i += 5 ) {
    ULong64_t bitarray = 0;
    for ( Int_t j = 4; j >= 0; j-- ) {
      bitarray *= 256;
      bitarray += *(data+i+j);
    }
    for ( Int_t k = 0; k < 4; k++ ) {
      fData[c_id+k] = (UShort_t)(bitarray & 0x3FF);
      bitarray = (bitarray >> 10);
    }
    c_id += 4;
  }
}
