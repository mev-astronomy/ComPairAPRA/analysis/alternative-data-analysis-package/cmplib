#include "Packet.h"
#include "SitMerger.h"
#include "DataHandler.h"

#include <iostream>
#include <signal.h>
#include <fcntl.h>

SitMerger::SitMerger( void )
{
  time( &fTstart );
  fOutFname = "sit_cvt.dat";
  fAtrb = 0;
  fData.clear();
  fFlag.clear();
}

SitMerger::~SitMerger()
{
  for ( it_type it = fData.begin(); it != fData.end(); ++it ) delete it->second;
}

void SitMerger::ReadEvent( void )
{
  Bool_t flag = kFALSE;

  for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
    if ( flag ) {
      it = fData.begin();
      flag = kFALSE;
    }

    UShort_t atrb = it->first;
    while ( fData[atrb]->GetPktSize() == 0 && !fData[atrb]->Eof() ) {
      //      fData[atrb]->Read( kTRUE );
      fData[atrb]->Read();
      UInt_t time = fData[atrb]->GetEpoch();
      if ( fTstart > (time_t)time && time > 160000000 ) fTstart = (time_t)time;
    }
    if ( fData[atrb]->Eof() ) {
      DelAtrb( atrb );
      if ( it == fData.begin() ) {
	it = fData.erase( it );
	flag = kTRUE;
	if ( fData.size() == 0 ) break;
      }
      else {
	it = fData.erase( it );
	if ( it == fData.end() ) break;
	if ( it != fData.begin() ) --it;
      }
    }
  }
}

Int_t SitMerger::WriteEvent( void )
{
  UInt_t evid = 0;
  UInt_t epch = 0;
  UInt_t epns = 0;
  Double_t tutc = 0.0;
    
  Bool_t zero = kFALSE;
  
  for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
    UShort_t atrb = it->first;
    if ( fData[atrb]->GetPktSize() == 0 ) continue;
    if ( fData[atrb]->GetEventID() < 0x80000000 ) zero = kTRUE;
  }

  if ( zero ) {
    for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
      UShort_t atrb = it->first;
      if ( fData[atrb]->GetPktSize() == 0 ) continue;
      if ( fData[atrb]->GetEventID() >= 0x80000000 ) continue;
      if ( tutc == 0 || fData[atrb]->GetUtcTime() < tutc ) {
	tutc = fData[atrb]->GetUtcTime();
	evid = fData[atrb]->GetEventID();
	epch = fData[atrb]->GetEpoch  ();
	epns = fData[atrb]->GetEpochNS();
      }
    }
    for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
      UShort_t atrb = it->first;
      if ( fData[atrb]->GetPktSize() == 0 ) continue;
      if ( fData[atrb]->GetEventID() >= 0x80000000 ) continue;
      if ( fData[atrb]->GetUtcTime() - tutc < 0.5e-3 ) {
	fData[atrb]->SetWflag( kTRUE );
      }
    }
  }
  else {
    for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
      UShort_t atrb = it->first;
      if ( fData[atrb]->GetPktSize() == 0 ) continue;
      if ( ( (fData[atrb]->GetEventID() < evid) && (evid-fData[atrb]->GetEventID()) < 0x40000000 ) ||
	   ( (fData[atrb]->GetEventID() > evid) && (fData[atrb]->GetEventID()-evid) > 0x40000000 ) ) {
	evid = fData[atrb]->GetEventID();
	epch = fData[atrb]->GetEpoch  ();
	epns = fData[atrb]->GetEpochNS();
      }
    }
    for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
      UShort_t atrb = it->first;
      if ( fData[atrb]->GetPktSize() == 0 ) continue;
      if ( fData[atrb]->GetEventID() == evid ) {
	fData[atrb]->SetWflag( kTRUE );
      }
    }
  }
  
  UShort_t npkt = 0;
  UShort_t flag = 0;
  UShort_t size = 0;
  
  for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
    UShort_t atrb = it->first;
    if ( fData[atrb]->GetWflag() ) {
      flag |= fData[atrb]->GetPktAtrb();
      size += fData[atrb]->GetPktSize() + PKT_SIZE_SIZE*2;
      npkt++;
    }
  }
  
  if ( size == 0 ) return 0;

  if ( npkt == 1 ) {
    fData[flag]->Write( &fOUT );
    return 1;
  }
  
  size += PKT_HEAD_SIZE;
  
  unsigned char bsize[2];
  unsigned char battr[2];
  
  bsize[0] = (size >> 8) & 0xFF;
  bsize[1] = (size%256 ) & 0xFF;
  battr[0] = (flag >> 8) & 0xFF;
  battr[1] = (flag%256 ) & 0xFF;
  
  fOUT.write( reinterpret_cast<const char *>(&bsize[0]), 2 );
  fOUT.write( reinterpret_cast<const char *>(&battr[0]), 2 );
  fOUT.write( reinterpret_cast<const char *>(&evid), 4 );
  fOUT.write( reinterpret_cast<const char *>(&epch), 4 );
  fOUT.write( reinterpret_cast<const char *>(&epns), 4 );
  
  for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
    UShort_t atrb = it->first;
    if ( fData[atrb]->GetWflag() ) fData[atrb]->Write( &fOUT );
  }
  
  fOUT.write( reinterpret_cast<const char *>(&bsize[0]), 2 );
  
  return 1;
}

void SitMerger::Clear( Option_t *option )
{
}
