#include "RossPad.h"

#include "TString.h"

#include <iostream>

RossPad::RossPad()
{
  Clear();
}

RossPad::~RossPad()
{
}

void RossPad::Clear( void )
{
  f1stByte = 0;
  f2ndByte = 0;
  
  fFrameCount = 0;

  f3rdByte = 0;
  f4thByte = 0;
  f5thByte = 0;
  f6thByte = 0;

  fTimeStamp = 0;

  f1stWord = 0;
  f2ndWord = 0;
  f3rdWord = 0;

  fGPIO = 0;
  f1stDword = 0;

  for ( Int_t xpos = 0; xpos < N_XPOS; xpos++ ) {
    for ( Int_t ypos = 0; ypos < N_YPOS; ypos++ ) {
      fData[xpos][ypos] = 0;
    }
  }

  fDataType = 0;
  fTimeDiff = 0;
  
  fSize = 0;
  memset( (unsigned char*)&fBuffer, 0, sizeof(fBuffer) );
}

UShort_t RossPad::GetData( Byte_t x, Byte_t y ) const
{
  if ( x < 0 || x >= N_XPOS ) return 0;
  if ( y < 0 || y >= N_YPOS ) return 0;

  return fData[x][y];
}

void RossPad::SetData( Byte_t x, Byte_t y, UShort_t data )
{
  if ( x < 0 || x >= N_XPOS ) return;
  if ( y < 0 || y >= N_YPOS ) return;

  fData[x][y] = data;
}

Int_t RossPad::ReadRossPad( std::ifstream *fin )
{
  if ( !fin->is_open() ) return 0;

  std::streamsize ssize;
  
  fin->read( (char *)&fBuffer, 2 );
  if ( fin->eof() ) return 0;
  fSize += fin->gcount();

  ssize = fBuffer[0]*256+fBuffer[1];
  fin->read( (char *)(fBuffer+2), ssize-2 );
  fSize += fin->gcount();

  Int_t offset = 2;
  f1stByte     = fBuffer[offset++];
  f2ndByte     = fBuffer[offset++];
  fFrameCount  = fBuffer[offset++]*256;
  fFrameCount += fBuffer[offset++];
  f3rdByte     = fBuffer[offset++];
  f4thByte     = fBuffer[offset++];
  f5thByte     = fBuffer[offset++];
  f6thByte     = fBuffer[offset++];
  fTimeStamp   = fBuffer[offset++] << 24;
  fTimeStamp  += fBuffer[offset++] << 16;
  fTimeStamp  += fBuffer[offset++] <<  8;
  fTimeStamp  += fBuffer[offset++];
  f1stWord     = fBuffer[offset++]*256;
  f1stWord    += fBuffer[offset++];
  f2ndWord     = fBuffer[offset++]*256;
  f2ndWord    += fBuffer[offset++];
  f3rdWord     = fBuffer[offset++]*256;
  f3rdWord    += fBuffer[offset++];
  fGPIO        = fBuffer[offset++];
  fGPIO       += fBuffer[offset++] <<  8;
  fGPIO       += fBuffer[offset++] << 16;
  fGPIO       += fBuffer[offset++] << 24;
  f1stDword    = fBuffer[offset++];
  f1stDword   += fBuffer[offset++] <<  8;
  f1stDword   += fBuffer[offset++] << 16;
  f1stDword   += fBuffer[offset++] << 24;
  
  for ( Int_t x = 0; x < N_XPOS; x++ ) {
    for ( Int_t y = 0; y < N_YPOS; y++ ) {
      fData[x][y]  = fBuffer[offset++]*256;
      fData[x][y] += fBuffer[offset++];
    }
  }

  if      ( fGPIO == 0x00000000 ) fDataType = 0;
  else if ( fGPIO == 0x01000000 ) fDataType = 1;
  else if ( fGPIO == 0x02000000 ) fDataType = 2;
  else                            fDataType = 255;
  
  return 1;
}

Int_t RossPad::FillRossPad( BasePacket *pkt, Bool_t speak )
{
  Clear();
  unsigned char *data = pkt->GetData();
  memcpy( (void *)&fBuffer, data, sizeof(fBuffer) );

  Int_t offset = 2;
  f1stByte     = fBuffer[offset++];
  f2ndByte     = fBuffer[offset++];
  fFrameCount  = fBuffer[offset++]*256;
  fFrameCount += fBuffer[offset++];
  f3rdByte     = fBuffer[offset++];
  f4thByte     = fBuffer[offset++];
  f5thByte     = fBuffer[offset++];
  f6thByte     = fBuffer[offset++];
  fTimeStamp   = fBuffer[offset++] << 24;
  fTimeStamp  += fBuffer[offset++] << 16;
  fTimeStamp  += fBuffer[offset++] <<  8;
  fTimeStamp  += fBuffer[offset++];
  f1stWord     = fBuffer[offset++]*256;
  f1stWord    += fBuffer[offset++];
  f2ndWord     = fBuffer[offset++]*256;
  f2ndWord    += fBuffer[offset++];
  f3rdWord     = fBuffer[offset++]*256;
  f3rdWord    += fBuffer[offset++];
  fGPIO        = fBuffer[offset++];
  fGPIO       += fBuffer[offset++] <<  8;
  fGPIO       += fBuffer[offset++] << 16;
  fGPIO       += fBuffer[offset++] << 24;
  f1stDword    = fBuffer[offset++];
  f1stDword   += fBuffer[offset++] <<  8;
  f1stDword   += fBuffer[offset++] << 16;
  f1stDword   += fBuffer[offset++] << 24;
  
  for ( Int_t x = 0; x < N_XPOS; x++ ) {
    for ( Int_t y = 0; y < N_YPOS; y++ ) {
      fData[x][y]  = fBuffer[offset++]*256;
      fData[x][y] += fBuffer[offset++];
    }
  }

  fDataType    = fBuffer[offset++];
  fDataType   += fBuffer[offset++] <<  8;
  fDataType   += fBuffer[offset++] << 16;
  fDataType   += fBuffer[offset++] << 24;

  fTimeDiff    = fBuffer[offset++];
  fTimeDiff   += fBuffer[offset++] <<  8;
  fTimeDiff   += fBuffer[offset++] << 16;
  fTimeDiff   += fBuffer[offset++] << 24;

  return offset;
}

void RossPad::SetRossPad( UInt_t dt, UInt_t td )
{
  memcpy( (void *)(fBuffer+fSize), (void *)&dt, 4 );
  fSize += 4;
  memcpy( (void *)(fBuffer+fSize), (void *)&td, 4 );
  fSize += 4;
}
