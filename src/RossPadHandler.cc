#include "RossPadHandler.h"
#include "RossPad.h"

#include "TMath.h"
#include "TString.h"
#include "TObjString.h"

#include <iostream>
#include <fstream>
#include <string>

RossPadHandler::RossPadHandler()
{
  Clear();
}

RossPadHandler::~RossPadHandler()
{
}

void RossPadHandler::Clear( void )
{
  fList.Clear();

  f1stGpsTime = 0;

  fIndex.clear();
  fTP.clear();
  fTS.clear();
  fDF.clear();
  fRaw.clear();
  fRossPad.clear();
}

UShort_t RossPadHandler::GetSize( UInt_t i ) const
{
  return fRaw[i].GetSize();
}

unsigned char *RossPadHandler::GetData( UInt_t i )
{
  return fRaw[i].GetBuffer();
}

Int_t RossPadHandler::Add( const char *in_fname )
{
  fList.Add( new TObjString( in_fname ) );

  return 0;
}

Int_t RossPadHandler::Process( void )
{
  ReadRossPad();
  SortRossPad();
  FindRossPad();
  CoinRossPad();
  ElimRossPad();
  FormRossPad();

  return 0;
}

void RossPadHandler::ReadRossPad( void )
{
  UInt_t npacket = 0;
  std::ifstream fIN;

  RossPad rp;
  
  do {
    if ( fIN.is_open() && fIN.eof() ) {
      fIN.close();
      fList.RemoveFirst();
    }

    if ( !fIN.is_open() && fList.Last() != 0 ) {
      TObjString *str = (TObjString *)fList.At( 0 );
      if ( !str ) return;

      fIN.open( str->GetName(), std::ios::in|std::ios::binary );
      if ( !fIN.is_open() ) {
	std::cout << "Error in opening RossPad data file: "
		  << str->GetName() << std::endl;
	return;
      }
    }

    rp.Clear();
    
    Int_t ret = rp.ReadRossPad( &fIN );
    if ( ret ) {
      fTP.push_back( rp.GetDataType () );
      fTS.push_back( rp.GetTimeStamp() );
      fDF.push_back( 0 );
      fRaw.push_back( rp );
      fIndex.push_back( npacket++ );
    }

  } while ( fList.Last() != 0 );

  std::cout << Form( "<RossPadHandler::ReadRossPad> n_packet = %d", npacket ) << std::endl;
}

void RossPadHandler::SortRossPad( void )
{
  Bool_t flag;
  Int_t  nwrong;
  
  Int_t  npacket = fIndex.size();

  UInt_t fTimeStamp;
  UInt_t fPrevStamp = 0;
  
  do {
    flag   = kFALSE;
    nwrong = 0;

    for ( Int_t i = 0; i < npacket; i++ ) {
      fTimeStamp = fTS[(fIndex[i])];
      if ( flag && fPrevStamp - fTimeStamp < 2000000000 && fPrevStamp > fTimeStamp ) {
	nwrong++;
	UInt_t p = fIndex[i-1];
	UInt_t c = fIndex[i];
	fIndex[i] = p;
	fIndex[i-1] = c;
	fTimeStamp = fPrevStamp;
      }
      flag = kTRUE;
      fPrevStamp = fTimeStamp;
    }
    std::cout << nwrong << std::endl;
  } while ( nwrong > 0 );
}

void RossPadHandler::FindRossPad( void )
{
  Int_t  npacket = fIndex.size();

  UInt_t fDataType;
  
  UInt_t fTimeStamp;
  UInt_t fPrevStamp = 0;

  UInt_t fPrevIndex = 0;
  
  UInt_t fFirstC = 0;
  UInt_t fLastC  = 0;

  Int_t  ninit = 0;
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    if ( ninit >= 9 ) continue;
    if ( ninit > 0 ) std::cout << ninit << std::endl;

    fDataType  = fTP[(fIndex[i])];
    fTimeStamp = fTS[(fIndex[i])];
    if ( fDataType == 2 ) {
      if ( fPrevIndex > 0 && fTimeStamp - fPrevStamp < 10005 && fTimeStamp - fPrevStamp > 9995 ) {
	fTP[(fIndex[i])] = 8;
	ninit++;
      }
      if ( ninit == 0 ) fFirstC = fTimeStamp;
      fLastC = fTimeStamp;
      
      fPrevIndex = fIndex[i];
      fPrevStamp = fTimeStamp;
    }
  }

  std::cout << Form( "%u %u %u", fFirstC, fLastC, fLastC-fFirstC ) << std::endl;
}

void RossPadHandler::ElimRossPad( void )
{
  UInt_t   fDataType;
  UInt_t   fTimeStamp;
  UInt_t   fPrevStamp = 0;
  
  Int_t npacket = fIndex.size();

  Int_t npps = 0;
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    fDataType  = fTP[(fIndex[i])];
    fTimeStamp = fTS[(fIndex[i])];

    if ( fDataType == 1 ) {
      if ( fTimeStamp - fPrevStamp < 90000000 ) fTP[(fIndex[i])] = 9;
      else {
	fPrevStamp = fTimeStamp;
	npps++;
      }
    }
  }

  std::cout << Form( "<RossPadHandler::ElimRossPad> npps = %d", npps ) << std::endl;
}

void RossPadHandler::CoinRossPad( void )
{
  Int_t npacket = fIndex.size();

  UInt_t   fDataType;
  UInt_t   fTimeStamp;
  UInt_t   fPrevStamp = 0;
  UInt_t   fPrevIndex = 0;
  
  UInt_t   nevt = 0;
  
  for ( Int_t i = 0; i < npacket; i++ ) {
    fDataType  = fTP[(fIndex[i])];
    fTimeStamp = fTS[(fIndex[i])];

    if ( fDataType == 2 ) {
      nevt++;
      // -- 2021Nov08 330 --> 360
      //      if ( fTimeStamp-fPrevStamp > 300 && fTimeStamp-fPrevStamp < 330 ) {
      if ( fTimeStamp-fPrevStamp > 300 && fTimeStamp-fPrevStamp < 360 ) {
	fTP[fPrevIndex ] = 3;
	fTP[(fIndex[i])] = 4;
	fDF[fPrevIndex ] = fTimeStamp-fPrevStamp;
	if ( fPrevIndex < fIndex[i-1] ) {
	  Int_t p = fIndex[i-1];
	  fIndex[fPrevIndex] = p;
	  fIndex[i-1] = fPrevIndex;
	}
      }
    }
    else if ( fDataType == 0 ) {
      fPrevStamp = fTimeStamp;
      fPrevIndex = fIndex[i];
    }
  }

  std::cout << Form( "<RossPadHandler::CoinRossPad> nevt = %d", nevt ) << std::endl;
}

void RossPadHandler::FormRossPad( void )
{
  Int_t npacket = fIndex.size();

  UInt_t    fDataType;
  UInt_t    fTimeStamp;

  ULong64_t fUtcTime;
  ULong64_t fGpsTime = 0;
  UInt_t    fGpsStamp = 0;
  UInt_t    fDiffStamp;
  
  ULong64_t fPrevTime = f1stGpsTime;

  Int_t ninit = 0;
  
  Int_t count[5] = {0};

  for ( Int_t i = 0; i < npacket; i++ ) {
    fDataType  = fTP[(fIndex[i])];
    fTimeStamp = fTS[(fIndex[i])];
    fDiffStamp = fDF[(fIndex[i])];
    
    if ( ninit < 9 ) {
      if ( fDataType == 1 ) {
	fGpsTime  = fPrevTime;
	fGpsStamp = fTimeStamp;
      }

      if ( fDataType == 8 ) {
	ninit++;
      }
    }

    if ( ninit < 9 ) continue;
    
    if ( fDataType == 1 ) {
      UInt_t diffst = fTimeStamp - fGpsStamp;
      if ( fTimeStamp < fGpsStamp ) {
	if ( fGpsStamp-fTimeStamp < 0x80000000 ) {
	  std::cerr << Form( "fGpsTime = %20llu fTimeStamp = %10u, fGpsStamp = %10u", fGpsTime, fTimeStamp, fGpsStamp ) << std::endl;
	}
	else {
	  fGpsTime += TMath::Nint( diffst/100e6 )*1e9; // [ns]
	  fGpsStamp = fTimeStamp;
	}
      }
      else {
	fGpsTime += TMath::Nint( diffst/100e6 )*1e9; // [ns]
	fGpsStamp = fTimeStamp;
      }
    }
    else {

      UInt_t diffst = fTimeStamp-fGpsStamp+fDiffStamp;
      UInt_t limit  = 2e8;
      //      Double_t corr = 1.00000050;
      Double_t corr = 1.00000000;

      fUtcTime = fGpsTime;
      while ( diffst > limit ) {
	fUtcTime += TMath::Nint( limit*10*corr );
	diffst -= limit;
      }
      fUtcTime += TMath::Nint( diffst*10*corr );
      if ( fDataType == 0 || fDataType == 2 || fDataType == 3 ) {
	fRossPad.push_back( Form( "%u,%u,%u,%u,%llu", fIndex[i], fTP[(fIndex[i])], fTS[(fIndex[i])], fDF[(fIndex[i])], fUtcTime ) );
	fRaw[(fIndex[i])].SetRossPad( fDataType, fDF[(fIndex[i])] );
	count[fDataType]++;
      }
    }
  }

  for ( Int_t i = 0; i < 4; i++ ) {
    std::cout << Form( "%d: %d", i, count[i] ) << std::endl;
  }
}
  
void RossPadHandler::WriteRossPad( void )
{
  Int_t npacket = fRossPad.size();

  for ( Int_t i = 0; i < npacket; i++ ) {
    std::cout << Form( "%s", fRossPad[i].c_str() ) << std::endl;
  }
}
