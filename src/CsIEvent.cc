#include "CsIEvent.h"
#include "CsILib.h"
#include "CsIParams.h"
#include "CmpLib.h"
#include "RossPad.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TMath.h"

#include <iostream>
#include <fstream>

ClassImp( CsIEvent )

CsIEvent::CsIEvent( void )
{
  fRossPad = new RossPad();
  Clear();
}

CsIEvent::~CsIEvent()
{
  if ( fRossPad ) delete fRossPad;
}

UShort_t CsIEvent::GetData( Int_t indx ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return 0;
  Byte_t xadr = gCsIParams->GetXaddr( indx );
  Byte_t yadr = gCsIParams->GetYaddr( indx );
  if ( xadr < 0 || xadr >= CsILib::N_XADR ) return 0;
  if ( yadr < 0 || yadr >= CsILib::N_YADR ) return 0;
  //  return fRossPad->GetData( xadr, yadr );
  return fData[xadr][yadr];
}

UShort_t CsIEvent::GetData( Byte_t xadr, Byte_t yadr ) const
{
  if ( xadr < 0 || xadr >= CsILib::N_XADR ) return 0;
  if ( yadr < 0 || yadr >= CsILib::N_YADR ) return 0;
  //  return fRossPad->GetData( xadr, yadr );
  return fData[xadr][yadr];
}

Int_t CsIEvent::Fill( BasePacket *pkt, Bool_t speak )
{
  Clear();

  Int_t fSize = fRossPad->FillRossPad( pkt, speak );
  if ( fSize <= 0 ) return fSize;

  fDataType = fRossPad->GetDataType();
  fTimeDiff = fRossPad->GetTimeDiff();
  
  for ( Byte_t xadr = 0; xadr < CsILib::N_XADR; xadr++ ) {
    for ( Byte_t yadr = 0; yadr < CsILib::N_YADR; yadr++ ) {
      fData[xadr][yadr] = fRossPad->GetData( xadr, yadr );
    }
  }
    
#ifdef DEBUG
  std::cout << Form( "%u %19.9", fEventID, fTime ) << std::endl;
#endif

  return fSize;
}

void CsIEvent::Clear( Option_t *option ) 
{
  fRossPad->Clear();
  
  fDataType = 0;
  fTimeDiff = 0;

  for ( Byte_t xadr = 0; xadr < CsILib::N_XADR; xadr++ ) {
    for ( Byte_t yadr = 0; yadr < CsILib::N_YADR; yadr++ ) {
      fData[xadr][yadr] = 0;
    }
  }
}
