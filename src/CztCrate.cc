#include "CztCrate.h"

#include "CztLib.h"
#include "CztEvent.h"
#include "CztCryst.h"
#include "CztParams.h"
#include "CztBranch.h"
#include "CmpTree.h"

#include "TFile.h"
#include "TClonesArray.h"

ClassImp( CztCrate )

CztCrate::CztCrate( void )
{
  fAsicID = -1;

  for ( Int_t cid = 0; cid < CztLib::N_CZTB; cid++ ) {
    fCryst[cid].SetCztbID( cid );
  }
  Clear();
}

CztCrate::CztCrate( Int_t id )
{
  fAsicID = id;

  for ( Int_t cid = 0; cid < CztLib::N_CZTB; cid++ ) {
    fCryst[cid].SetCztbID( cid );
  }
  Clear();
}

CztCrate::~CztCrate()
{
}

void CztCrate::Clear( Option_t *option )
{
  fTemp = CztLib::fFnotValid;
  for ( Int_t cid = 0; cid < CztLib::N_CZTB; cid++ ) {
    fCryst[cid].Clear();
  }
  fHit.clear();
}

void CztCrate::Print( Option_t *option ) const
{
}

Int_t CztCrate::GetCztbID( UInt_t hit ) const
{
  if ( hit < 0 || hit >= fHit.size() ) return -1;
  it_type it = fHit.begin();
  for ( UInt_t i = 0; i < hit; i++ ) ++it;
  return it->second;
}

CztCryst *CztCrate::GetHitCzt( UInt_t hit )
{
  Int_t cid = GetCztbID( hit );
  if ( cid < 0 ) return (CztCryst *)NULL;

  return &fCryst[cid];
}

CztCryst *CztCrate::GetCztBar( Int_t cid )
{
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return (CztCryst *)NULL;
  return &fCryst[cid];
}

Int_t CztCrate::GetHitNpad( UInt_t hit )
{
  Int_t cid = GetCztbID( hit );
  return GetCztNpad( cid );
}

Double_t CztCrate::GetHitEne( UInt_t hit )
{
  Int_t cid = GetCztbID( hit );
  return GetCztEne( cid );
}

Double_t CztCrate::GetHitTim( UInt_t hit )
{
  Int_t cid = GetCztbID( hit );
  return GetCztTim( cid );
}

TVector3 CztCrate::GetHitPos( UInt_t hit )
{
  Int_t cid = GetCztbID( hit );
  return GetCztPos( cid );
}

TVector3 CztCrate::GetHitTra( UInt_t hit )
{
  TVector3 pos = GetHitPos( hit );

  Double_t angle = TMath::Pi();
  TVector3 shift = TVector3( 0.0, 0.0, -53.850 );

  pos.RotateZ( angle );
  return pos + shift;
}

Int_t CztCrate::GetCztNpad( Int_t cid )
{
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return -1;
  return fCryst[cid].GetNhitPad();
}
   
Double_t CztCrate::GetCztEne( Int_t cid )
{
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  return fCryst[cid].GetHitEne();
}

Double_t CztCrate::GetCztTim( Int_t cid )
{
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fDnotValid;
  return fCryst[cid].GetHitTim();
}

TVector3 CztCrate::GetCztPos( Int_t cid )
{
  if ( cid < 0 || cid >= CztLib::N_CZTB ) return CztLib::fVnotValid;

  TVector3 hitpos = fCryst[cid].GetHitPos();
  TVector3 origin = gCztParams->GetOrigin( fAsicID, cid );

  Bool_t rot = gCztParams->IsRotate( fAsicID );
  
  Double_t z = origin.Z() + hitpos.Z();
  Double_t x = (rot) ? origin.X() - hitpos.X() : origin.X() + hitpos.X();
  Double_t y = (rot) ? origin.Y() - hitpos.Y() : origin.Y() + hitpos.Y();

  return TVector3( x, y, z );
}


void CztCrate::SetAsicID( Int_t id )
{
  if ( id < 0 || id >= CztLib::N_ASIC ) return;
  fAsicID = id;
}

void CztCrate::SetData( CztAsic *data )
{
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return;
  
  Double_t time = ctree->GetUtcTime();
  Int_t    ip   = gCztParams->GetPoint( time );
  if ( ip < 0 ) return;

  fTemp = data->GetTemp();

  for ( Int_t iczt = 0; iczt < CztLib::N_CZTB; iczt++ ) {
    for ( Int_t ityp = 0; ityp < CztLib::N_TYPE; ityp++ ) {
      for ( Int_t ipol = 0; ipol < CztLib::N_POLA; ipol++ ) {
	Int_t idx = gCztParams->GetIndex( iczt, ityp, ipol );
	if ( idx < 0 ) continue;
	if ( gCztParams->IsDead( fAsicID, idx ) ) continue;
	Double_t peds = gCztParams->GetPeds( fAsicID, idx, ip );
	Double_t gain = gCztParams->GetGain( fAsicID, idx, ip );
	Int_t chn = gCztParams->GetAssign( idx );
	if ( chn < 0 ) continue;
	Double_t fEnergy = (Double_t)data->GetData( chn, CztLib::ENERGY );
	Double_t fTiming = (Double_t)data->GetData( chn, CztLib::TIMING );
	fCryst[iczt].SetEnergy( ityp, ipol, gain*( fEnergy - peds ) );
	fCryst[iczt].SetTiming( ityp, ipol, fTiming );
      }
    }
  }
}

void CztCrate::Process( void )
{
  // [Temporary] Need to think how to determine the threshold value
  Double_t E_THRE = 100.0; // [keV]
  //  Double_t E_THRE = 20.0; // [keV]

  for ( Int_t iczt = 0; iczt < CztLib::N_CZTB; iczt++ ) {
    fCryst[iczt].Process( fAsicID );
    Double_t ene = fCryst[iczt].GetHitEne();
    if ( ene > E_THRE ) fHit.insert( std::multimap<Double_t, Int_t>::value_type( ene, iczt ) );
  }
}
