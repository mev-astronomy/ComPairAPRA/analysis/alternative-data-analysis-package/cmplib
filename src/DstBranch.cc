#include "DstBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"
#include "CmpBranch.h"

#include "SitBranch.h"
#include "CztBranch.h"
#include "CsIBranch.h"
#include "TrkBranch.h"

#include "TFile.h"

ClassImp( DstBranch )

DstBranch::DstBranch( void )
{
  fHitArray = new TClonesArray( "HitBranch", CmpLib::N_MHIT );
  Clear();
}

DstBranch::~DstBranch()
{
  if ( fHitArray ) delete fHitArray;
}

Int_t DstBranch::GetNhits( Int_t did ) const
{
  if ( did < 0 || did >= CmpLib::N_DET ) return 0;

  Int_t nhits = 0;
  for ( Int_t lyr = 0; lyr < CmpLib::N_LYR; lyr++ ) {
    nhits += GetNhits( did, lyr );
  }
  return nhits;
}

Int_t DstBranch::GetNhits( Int_t did, Int_t lyr ) const
{
  if ( did < 0 || did >= CmpLib::N_DET ) return 0;
  if ( lyr < 0 || lyr >= CmpLib::N_LYR ) return 0;
  return fHitID[did][lyr].size();
}

Int_t DstBranch::GetHitID( Int_t did, Int_t lyr, Int_t hit ) const
{
  if ( hit < 0 || hit >= GetNhits( did, lyr ) ) return -1;
  return fHitID[did][lyr][hit];
}

HitBranch *DstBranch::GetHit( Int_t hid ) const
{
  if ( hid < 0 || hid >= fNhits ) return (HitBranch *)NULL;
  return (HitBranch *)fHitArray->At( hid );
}

Double_t DstBranch::GetEnergy( Int_t did ) const
{
  if ( did < 0 || did >= CmpLib::N_DET ) return 0.0;

  Double_t ene = 0.0;
  if ( did == CmpLib::D_SIT ) {
    for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
      Int_t lyr_x = slice*2+CmpLib::COORD_X;
      Int_t lyr_y = slice*2+CmpLib::COORD_Y;
      Double_t ene_x = GetEnergy( did, lyr_x );
      Double_t ene_y = GetEnergy( did, lyr_y );
      if ( ene_y > 0.0 ) ene += ene_y;
      else               ene += ene_x;
    }
  }
  else {
    for ( Int_t lyr = 0; lyr < CmpLib::N_LYR; lyr++ ) {
      ene += GetEnergy( did, lyr );
    }
  }
  
  return ene;
}

Double_t DstBranch::GetEnergy( Int_t did, Int_t lyr ) const
{
  if ( did < 0 || did >= CmpLib::N_DET ) return 0.0;
  if ( lyr < 0 || lyr >= CmpLib::N_LYR ) return 0.0;

  Double_t ene = 0.0;
  for ( Int_t hit = 0; hit < GetNhits( did, lyr ); hit++ ) {
    Int_t hid = GetHitID( did, lyr, hit );
    if ( hid < 0 ) continue;
    HitBranch *hitb = GetHit( hid );
    if ( !hitb ) continue;
    ene += hitb->GetHitEne();
  }

  return ene;
}


Int_t DstBranch::Process( Int_t sel )
{
  // The main method of DstBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  // Determine Event Energy
  fEvtEne = 0.0;
  for ( Int_t i = 0; i < ctree->GetNsit(); i++ ) {
    SitBranch *sit = ctree->GetSitAt( i );
    if ( !sit ) continue;
    fEvtEne += sit->GetEnergy();
  }

  CztBranch *czt = ctree->GetCzt();
  if ( czt ) fEvtEne += czt->GetEnergy();

  CsIBranch *csi = ctree->GetCsI();
  if ( csi ) fEvtEne += csi->GetEnergy();
  
  // Find Track
  if ( sel & CmpLib::CREVT ) {
    TrkBranch *trk = ctree->GetTrk();
    if ( trk ) trk->Process( sel );
  }

  // Find Cone
  if ( sel & CmpLib::GREVT ) {
    CmpBranch *cmp = ctree->GetCmp();
    if ( cmp ) cmp->Process( sel );
  }
  
  return 1;
}

HitBranch *DstBranch::NewHit( Int_t did, Int_t lyr )
{
  if ( did < 0 || did >= CmpLib::N_DET  ) return (HitBranch *)NULL;
  if ( lyr < 0 || lyr >= CmpLib::N_LYR  ) return (HitBranch *)NULL;

  if ( fNhits >= CmpLib::N_MHIT ) return (HitBranch *)NULL;

  HitBranch *hit = (HitBranch *)fHitArray->ConstructedAt( fNhits );
  if ( !hit ) return (HitBranch *)NULL;

  fHitID[did][lyr].push_back( fNhits++ );

  return hit;
}

void DstBranch::Clear( Option_t *option )
{
  for ( Int_t did = 0; did < CmpLib::N_DET; did++ ) {
    for ( Int_t lyr = 0; lyr < CmpLib::N_LYR; lyr++ ) {
      fHitID[did][lyr].clear();
    }
  }

  fNhits = 0;
  fHitArray->Clear( "C" );

  fEvtEne = 0.0;
  fEvtAng = CmpLib::fDnotValid;
  fEvtDir = CmpLib::fVnotValid;
  fEvtPer = CmpLib::fVnotValid;
}

void DstBranch::Print( Option_t *option ) const
{
}
