#include "CmpLib.h"

#include "TrgEventID.h"
#include "CztEventID.h"
#include "SitEventID.h"

#include "TString.h"

#include <iostream>
#include <fstream>
#include <string>

void usage( void )
{
  std::cout << std::endl;
  std::cout << "Usage: cevid [atrb] (options) [raw_file].." << std::endl;
  std::cout << std::endl;
  std::cout << "EvID modes:" << std::endl;
  std::cout << "  TRG        TRG                                 " << std::endl;
  std::cout << "  SIT        SiT                                 " << std::endl;
  std::cout << "  CZT        CZT                                 " << std::endl;
  std::cout << "  CSI        CsI                                 " << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "  -o FILE    set output file name as FILE        " << std::endl;
  std::cout << "  -n N       process only N events               " << std::endl;
  std::cout << "  -t TIME    set offset time (double)            " << std::endl;
  std::cout << "  -h         show this help                      " << std::endl;
  std::cout << std::endl;
}

int main( int argc, char *argv[] )
{
  std::cout << "======================================================" << std::endl;
  std::cout << " ComPair data event id (cevid)                        " << std::endl;
  std::cout << Form( " $Revision: 1.1.1.1 $, last update: %s %s", __DATE__, __TIME__ ) << std::endl;
  std::cout << "======================================================" << std::endl;

  // set default parameters
  double oft = 0.0;
  std::string ofn = "cmp_eid.dat";
  int nevt = 0;

  // create main object
  DataEventID *cevid;

  // arguments analysis
  std::string mode = argv[1];

  if      ( mode == "TRG" ) {
    cevid = new TrgEventID();
    ofn  = "trg_eid.dat";
  }
  else if ( mode == "CZT" ) {
    cevid = new CztEventID();
    ofn  = "czt_eid.dat";
  }

  else if ( mode == "SIT" ) {
    cevid = new SitEventID();
    ofn  = "sit_eid.dat";
  }
  else if ( mode == "SI0" ) {
    cevid = new SitEventID( PKT_ATRB_SI0 );
    ofn  = "si0_eid.dat";
  }
  else if ( mode == "SI1" ) {
    cevid = new SitEventID( PKT_ATRB_SI1 );
    ofn  = "si1_eid.dat";
  }
  else if ( mode == "SI2" ) {
    cevid = new SitEventID( PKT_ATRB_SI2 );
    ofn  = "si2_eid.dat";
  }
  else if ( mode == "SI3" ) {
    cevid = new SitEventID( PKT_ATRB_SI3 );
    ofn  = "si3_eid.dat";
  }
  else if ( mode == "SI4" ) {
    cevid = new SitEventID( PKT_ATRB_SI4 );
    ofn  = "si4_eid.dat";
  }
  else if ( mode == "SI5" ) {
    cevid = new SitEventID( PKT_ATRB_SI5 );
    ofn  = "si5_eid.dat";
  }
  else if ( mode == "SI6" ) {
    cevid = new SitEventID( PKT_ATRB_SI6 );
    ofn  = "si6_eid.dat";
  }
  else if ( mode == "SI7" ) {
    cevid = new SitEventID( PKT_ATRB_SI7 );
    ofn  = "si7_eid.dat";
  }
  else if ( mode == "SI8" ) {
    cevid = new SitEventID( PKT_ATRB_SI8 );
    ofn  = "si8_eid.dat";
  }
  else if ( mode == "SI9" ) {
    cevid = new SitEventID( PKT_ATRB_SI9 );
    ofn  = "si9_eid.dat";
  }

  if ( argc == 2 && argv[1][0] == '-' && argv[1][1] == 'h' ) {
    usage();
    return 0;
  }
  else if ( argc < 2 ) {
    std::cout << std::endl;
    std::cout << "You need at least two arguments. " << std::endl;
    std::cout << "Try \"ebuild -h\" for more information" << std::endl;
    return 0;
  }

  // option analysis
  for ( int iarg = 2; iarg < argc; iarg++ ) {
    if ( argv[iarg][0] == '-' && argv[iarg][1] == 'o' ) {
      if ( argc > iarg+1 ) ofn = argv[++iarg];
    }
    
    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'n' ) {
      if ( argc > iarg+1 ) nevt = atoi( argv[++iarg] );
    }
    
    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 't' ) {
      if ( argc > iarg+1 ) oft = std::atof( argv[++iarg] );
    }
    
    else if ( argv[iarg][0] == '-' )
      std::cout << Form( "Unknown option: %s", argv[iarg] ) << std::endl;

    else if ( cevid->Add( argv[iarg] ) ) {
      std::cout << Form( "Raw data file [%02d]: %s, ", cevid->GetNfile(), argv[iarg] );
      Double_t size = cevid->GetFsize();
      if ( size < 1000 ) std::cout << Form( "%4.0f MB", size ) << std::endl;
      else               std::cout << Form( "%4.0f GB", size/1024. ) << std::endl;
    }
  }
  
  if ( nevt > 0 ) std::cout << Form( ", Nevt=%d", nevt );
  std::cout << std::endl;

  // open files
  if ( cevid->Open( ofn.c_str() ) < 0 ) return -1;

  // start process
  if ( oft != 0.0 ) cevid->SetTimeOffset( oft );
  
  cevid->Loop( nevt );
  cevid->Process();
  cevid->Write  ();
  cevid->Close  ();
  
  return 0;
}
