#include "CmpLib.h"
#include "CztLib.h"
#include "CztConvert.h"

#include <iostream>

CztConvert::CztConvert( void )
{
  fAtrb = PKT_ATRB_CZT;
  fCztPacket = new CztPacket;
}

CztConvert::CztConvert( UShort_t atrb ) : DataConvert( atrb )
{
  fCztPacket = new CztPacket;
}

CztConvert::~CztConvert()
{
  if ( fCztPacket ) delete fCztPacket;
}

Int_t CztConvert::Read( void )
{
  Clear();

  fSize = fCztPacket->ReadPacket( &fIN );
  if ( fSize <= 0 ) return fSize;
  
  fEID.read( (char *)&fIdBuff, 12 );
  if ( fEID.eof() ) return 0;
  
  SetEventID();
  SetEpoch  ();
  SetEpochNS();

  if ( fEventID != fCztPacket->GetEventID() ) {
    std::cout << Form( "EventID mismatch: %8x %8x", fEventID, fCztPacket->GetEventID() ) << std::endl;
    return -1;
  }
  
  fPacket->SetData( fSize, fCztPacket->GetBuffer() );
  fPacket->SetAtrb( fAtrb    );
  fPacket->SetEvid( fEventID );
  fPacket->SetEpch( fEpoch   );
  fPacket->SetEpns( fEpochNS );

  fSize += PKT_HEAD_SIZE;

  return 1;
}

void CztConvert::Clear( Option_t *option )
{
  DataConvert::Clear( option );
  fCztPacket->Clear();
}
