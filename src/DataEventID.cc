#include "DataEventID.h"

#include "TSystem.h"
#include "TObjString.h"

#include <iostream>
#include <signal.h>

Int_t DataEventID::fKill = 0;

DataEventID::DataEventID( void )
{
  fAtrb   = 0;
  time( &fTstart );
  fFileSize = 0.0;
  fOutFname = "cmp_eid.dat";
  fTree = new TTree( "ttree", "Event ID" );
  Clear();
}

DataEventID::DataEventID( UShort_t atrb )
{
  fAtrb = atrb;
  time( &fTstart );
  fFileSize = 0.0;
  fTree = new TTree( "ttree", "Event ID" );
  Clear();
}

DataEventID::~DataEventID()
{
  if ( fTree ) delete fTree;
}

Int_t DataEventID::Add( const char *in_fname )
{
  // get info of raw data file
  Long_t id, flags, mtime;
  Long64_t size;
  if ( gSystem->GetPathInfo( in_fname, &id, &size, &flags, &mtime ) ) {
    std::cout << "Raw data file, \"" << in_fname << "\""
	      << " does not exist." << std::endl;
    return 0;
  }

  if ( fTstart > (time_t)mtime ) fTstart = mtime;
  
  fFileSize += size/1024.0/1024.0;
  fList.Add( new TObjString( in_fname ) );
  
  return 1;
}

Int_t DataEventID::Open( const char *tr_fname )
{
  fOutFname = tr_fname;
  fOUT.open( tr_fname, std::ios::out|std::ios::binary );
  if ( !fOUT.is_open() ) return -1;
  std::cout << "Output file \"" << tr_fname << "\" open." << std::endl;
  fERR.open( "dump.bin", std::ios::out|std::ios::binary );
  if ( !fERR.is_open() ) return -1;
  std::cout << "Errout file \"" << "dump.bin" << "\" open." << std::endl;

  Branch();
  
  return 0;
}

Int_t DataEventID::Loop( Int_t nproc )
{
  Int_t nevt = 0;

  // set signal handler
  signal( SIGINT,  SigHandler );
  signal( SIGTERM, SigHandler );

  while ( nproc <= 0 || ( nevt < nproc ) ) {
    if ( fKill ) {
      std::cout << std::endl << "*** break ***, process interrupted" << std::endl;
      break;
    }

    // open raw data file
    if ( !fIN.is_open() ) {
      TObjString *str = (TObjString *)fList.At( 0 );
      if ( !str ) break;
      fIN.open( str->GetName(), std::ios::binary|std::ios::in );
      if ( !fIN.is_open() ) {
	std::cout << "Error in opening raw data file: "
		  << str->GetName() << std::endl;
	continue;
      }
      std::cout << "Open raw data file: " << str->GetName() << std::endl;
    }

    if ( int ret = Read() ) {
      if ( ret < 0 ) break;
      nevt++;
      if ( nevt%1000000 == 1 ) std::cout << Form( "      %8d event processed", nevt ) << std::endl;
      fTree->Fill();
    }
    else {
      if ( fIN.is_open() && fIN.eof() ) {
	fIN.close();
	fList.RemoveFirst();
      }
    }
  }
  
  std::cout << Form( "Total %8d event processed", nevt ) << std::endl;
  // reset signal handler
  signal( SIGINT,  SIG_DFL );
  signal( SIGTERM, SIG_DFL );
  
  return 1;
}

Int_t DataEventID::Write( void )
{
  if ( !fOUT.is_open() ) return -1;
  for ( UInt_t i = 0; i < fVevid.size(); i++ ) {
    UInt_t evid = fVevid[i];
    UInt_t epch = fVepch[i];
    UInt_t epns = fVepns[i];
    if ( fTstart > (time_t)epch && (time_t)epch > 100000000 ) {
      fTstart = (time_t)epch;
      std::cout << Form( "%10u %10u", i, epch ) << std::endl;
    }

    fOUT.write( reinterpret_cast<const char*>(&evid), 4 );
    fOUT.write( reinterpret_cast<const char*>(&epch), 4 );
    fOUT.write( reinterpret_cast<const char*>(&epns), 4 );
  }
  return 0;
}

Int_t DataEventID::Close( void )
{
  if ( !fOUT.is_open() ) return -1;
  if ( Rename() != 0 )   return -1;
  fOUT.close();
  if ( !fERR.is_open() ) return -1;
  fERR.close();
  return 0;
}

Int_t DataEventID::Rename( void )
{
  std::size_t pos = fOutFname.find( ".dat" );
  std::string bfn = fOutFname.substr( 0, pos );

  struct tm *ptr = localtime( &fTstart );
  int year = ptr->tm_year+1900;
  int mon  = ptr->tm_mon+1;
  int mday = ptr->tm_mday;
  int hour = ptr->tm_hour;
  int min  = ptr->tm_min;

  std::string name = Form( "%s-%04d-%02d%02d-%02d%02d", bfn.c_str(),
			   year, mon, mday, hour, min );
  int file_num = 1;
  std::string full_name;

  int fDout;
  do {
    full_name = name+Form( "-%03d.dat", file_num++ );
    fDout = open( full_name.c_str(), O_RDONLY );
  } while( fDout > 0 );

  return std::rename( fOutFname.c_str(), full_name.c_str() );
}

void DataEventID::Clear( Option_t *option )
{
  fSize    = 0;
  fVevid.clear();
  fVepch.clear();
  fVepns.clear();
}

void DataEventID::SigHandler( int sig )
{
  fKill = 1;
}
