#include "CmpLib.h"
#include "DataHandler.h"

#include "TSystem.h"
#include "TObjString.h"

#include <iostream>

DataHandler::DataHandler( void )
{
  fList.Clear();
  fPkt.clear();
  Clear();
}

DataHandler::~DataHandler()
{
  for ( it_type it = fPkt.begin(); it != fPkt.end(); ++it ) delete it->second;
}

UShort_t DataHandler::GetAtrb( UInt_t i )
{
  if ( fNatrb <= i ) return 0;
  if ( fNatrb == 0 ) return 0;
  if ( fNatrb == 1 ) return GetPktAtrb();
  return fAtrbs[i];
}

BasePacket *DataHandler::GetPacket( UInt_t i )
{
  if ( fNatrb <= i ) return (BasePacket *)NULL;
  if ( fNatrb == 0 ) return (BasePacket *)NULL;
  if ( fNatrb == 1 ) return &fPacket;
  return fPkt[(fAtrbs[i])];
}

Int_t DataHandler::Add( const char *fname )
{
  // get info of raw data file
  Long_t id, flags, mtime;
  Long64_t size;
  if ( gSystem->GetPathInfo( fname, &id, &size, &flags, &mtime ) ) {
    std::cout << "Raw data file, \"" << fname << "\""
	      << " does not exist." << std::endl;
    return 0;
  }
  
  fList.Add( new TObjString( fname ) );
  
  return 1;
}

Int_t DataHandler::Read( Bool_t speak )
{
  Int_t    fSize = 0;
  UShort_t fAtrb = 0;
  UInt_t   fEvid = 0;
  
  do {
    fSize = fPacket.ReadPacket( &fIN );
    fAtrb = fPacket.GetAtrb();
    fEvid = fPacket.GetEvid();
    if ( speak )
      std::cout << Form( "<DataHandler::Read> fAtrb = 0x%4X, fEvid = 0x%8X, fSize = %4X", fAtrb, fEvid, fSize ) << std::endl;

    if ( fSize == 0 ) {
      if ( fIN.is_open() && fIN.eof() ) {
	fIN.close();
	fList.RemoveFirst();
      }
      if ( !fIN.is_open() ) {
	TObjString *str = (TObjString *)fList.At( 0 );
	if ( !str ) return 0;
	fIN.open( str->GetName(), std::ios::binary|std::ios::in );
	if ( !fIN.is_open() ) {
	  std::cout << "Error in opening raw datafile: "
		    << str->GetName() << std::endl;
	  return -1;
	}
      }
      else return -1;
    }
  } while ( fSize == 0 );
  
  if ( fSize > 0 ) Split();
  else return -1;
  
  return 1;
}

Int_t DataHandler::Split( void )
{
  UInt_t n = SetNatrb();
  if ( n <= 1 ) return 0;
  //  std::cout << Form( "<DataHandler::Split> n = %d", n ) << std::endl;
  unsigned char siz[2];
  unsigned char atr[2];
  int offset = 0;
  unsigned char *data = fPacket.GetData();
  for ( UInt_t i = 0; i < n; i++ ) {
    fPacket.GetData( &siz[0], offset                , PKT_SIZE_SIZE );
    fPacket.GetData( &atr[0], offset + PKT_SIZE_SIZE, PKT_ATRB_SIZE );
    UShort_t size = siz[0]*256+siz[1];
    UShort_t atrb = atr[0]*256+atr[1];
    UShort_t natr = 0;
    for ( Int_t i = 0; i < CmpLib::N_ATRB; i++ ) {
      UShort_t a = (1<<i);
      if ( atrb & a ) natr++;
    }
    if ( natr > 1 ) {
      // std::cout << Form( "<DataHandler::Split> n = %d: size = %d, atrb = %d", n, size, atrb ) << std::endl;
      offset += PKT_SIZE_SIZE+PKT_HEAD_SIZE;
      for ( Int_t j = 0; j < natr; j++ ) {
	fPacket.GetData( &siz[0], offset                , PKT_SIZE_SIZE );
	fPacket.GetData( &atr[0], offset + PKT_SIZE_SIZE, PKT_ATRB_SIZE );
	UShort_t ssize = siz[0]*256+siz[1];
	UShort_t aatrb = atr[0]*256+atr[1];
	// std::cout << Form( "<DataHandler::Split> n = %d: size = %d, atrb = %d", n, ssize, aatrb ) << std::endl;
	fPkt[aatrb]->ReadPacket( data, offset );
	offset += ssize + 2*PKT_SIZE_SIZE;
      }
      offset += PKT_SIZE_SIZE;
      i += (natr-1);
    }
    else {
      fPkt[atrb]->ReadPacket( data, offset );
      offset += size + 2*PKT_SIZE_SIZE;
    }
  }
  return 1;
}

UInt_t DataHandler::SetNatrb( void )
{
  UShort_t fAtrb = GetPktAtrb();

  fNatrb = 0;
  for ( Int_t i = 0; i < CmpLib::N_ATRB; i++ ) {
    UShort_t atrb = (1<<i);
    if ( fAtrb & atrb ) {
      it_type it = fPkt.find( atrb );
      if ( it == fPkt.end() ) fPkt[atrb] = new BasePacket();
      fAtrbs[fNatrb++] = atrb;
    }
  }
  return fNatrb;
}

Int_t DataHandler::Write( std::ofstream *fOUT )
{
  if ( GetPktSize() > 0 ) fPacket.WritePacket( fOUT );
  Clear();

  return 1;
}

Bool_t DataHandler::Eof( void )
{
  return fList.IsEmpty();
}

void DataHandler::Clear( Option_t *option )
{
  fNatrb = 0;
  for ( Int_t i = 0; i < CmpLib::N_ATRB; i++ ) fAtrbs[i] = 0;

  fPacket.Clear();
  for ( it_type it = fPkt.begin(); it != fPkt.end(); ++it ) {
    (it->second)->Clear();
  }

  fWflag = kFALSE;
}
