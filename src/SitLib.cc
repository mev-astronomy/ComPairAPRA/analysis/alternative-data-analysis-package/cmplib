#include "SitLib.h"

#include "TString.h"

ClassImp( SitLib )

Int_t    SitLib::fThreshold = 0;
TVector3 SitLib::fVnotValid = TVector3( -99999.9, -99999.9, -99999.9 );
Double_t SitLib::fDnotValid = -99999.9;
Float_t  SitLib::fFnotValid = -99999.9;

SitLib::SitLib()
{
}

SitLib::~SitLib()
{
}
