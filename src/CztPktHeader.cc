#include "CztPktHeader.h"
#include "CztLib.h"

#include "TString.h"
#include "TMath.h"

#include <iostream>
#include <fstream>

ClassImp( CztPktHeader )

CztPktHeader::CztPktHeader( void )
{
  Clear();
}

CztPktHeader::~CztPktHeader()
{
}

Int_t CztPktHeader::GetAsicID( UInt_t hit ) const
{
  if ( hit < 0 || hit >= fNhitAsic ) return -1;
  return fAsicID[hit];
}

Int_t CztPktHeader::ReadData( std::ifstream *fIN, Bool_t speak )
{
  Clear();

  Int_t fSize = 0;
  unsigned char f4Byte[4];
  unsigned char f2Byte[2];

  if ( fIN->eof() ) return -1;

  // Read Packet Count
  fIN->read( (char *)&f4Byte, sizeof(f4Byte) );
  fSize += fIN->gcount();
  fPacketCount = conv4Byte( f4Byte );

  // Read Packet Size
  fIN->read( (char *)&f2Byte, sizeof(f2Byte) );
  fSize += fIN->gcount();
  fPacketSize = conv2Byte( f2Byte );

  // Read User Register
  fIN->read( (char *)&f2Byte, sizeof(f2Byte) );
  fSize += fIN->gcount();
  fUserRegister = conv2Byte( f2Byte );

  // Read Event Rate
  fIN->read( (char *)&f4Byte, sizeof(f4Byte) );
  fSize += fIN->gcount();
  fEventRate = conv4Byte( f4Byte );

  // Read 0x0000
  fIN->read( (char *)&f2Byte, sizeof(f2Byte) );
  fSize += fIN->gcount();
  
  // Read Chip In Event
  fIN->read( (char *)&f2Byte, sizeof(f2Byte) );
  fSize += fIN->gcount();
  fChipInEvent = conv2Byte( f2Byte );

  SetHitAsic( fChipInEvent );

  return fSize;
}

Int_t CztPktHeader::ReadData( unsigned char *data, Bool_t speak )
{
  Clear();

  Int_t fSize = 0;
  unsigned char f4Byte[4];
  unsigned char f2Byte[2];

  // Read Packet Count
  memcpy( (char *)&f4Byte, (data+fSize), sizeof(f4Byte) );
  fPacketCount = conv4Byte( f4Byte );
  fSize += sizeof(f4Byte);
  
  // Read Packet Size
  memcpy( (char *)&f2Byte, (data+fSize), sizeof(f2Byte) );
  fPacketSize = conv2Byte( f2Byte );
  fSize += sizeof(f2Byte);

  // Read User Register
  memcpy( (char *)&f2Byte, (data+fSize), sizeof(f2Byte) );
  fUserRegister = conv2Byte( f2Byte );
  fSize += sizeof(f2Byte);

  // Read Event Rate
  memcpy( (char *)&f4Byte, (data+fSize), sizeof(f4Byte) );
  fEventRate = conv4Byte( f4Byte );
  fSize += sizeof(f4Byte);

  // Read Zero data
  memcpy( (char *)&f2Byte, (data+fSize), sizeof(f2Byte) );
  fSize += sizeof(f2Byte);

  // Read Chip In Event
  memcpy( (char *)&f2Byte, (data+fSize), sizeof(f2Byte) );
  fChipInEvent = conv2Byte( f2Byte );
  fSize += sizeof(f2Byte);

  SetHitAsic( fChipInEvent );

  return fSize;
}

void CztPktHeader::SetHitAsic( UShort_t cie ) 
{
  fNhitAsic = 0;
  for ( Int_t i = 0; i < CztLib::N_ASIC; i++ ) {
    if ( ( cie & (1<<i) ) == (1<<i) ) {
      fAsicID[fNhitAsic++] = i;
    }
  }
}

void CztPktHeader::Clear( Option_t *option ) 
{
  fPacketCount  = 0;
  fPacketSize   = 0;
  fUserRegister = 0;
  fEventRate    = 0;
  fChipInEvent  = 0;
  fNhitAsic     = 0;
  for ( UInt_t i = 0; i < CztLib::N_ASIC; i++ ) {
    fAsicID[i] = -1;
  }
}

UInt_t CztPktHeader::conv4Byte( unsigned char *data )
{
  Int_t  order[4] = { 0, 1, 2, 3 };
  UInt_t conv = 0;
  for ( Int_t i = 0; i < 4; i++ ) {
    conv *= 256;
    conv += data[order[i]];
  }
  return conv;
}

UShort_t CztPktHeader::conv2Byte( unsigned char *data )
{
  return data[0]*256+data[1];
}
