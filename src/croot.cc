#include "RootConvert.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"

#include <iostream>
#include <fstream>
#include <string>

void usage( void )
{
  std::cout << std::endl;
  std::cout << "Usage: croot [branch_mode] (options) [raw_file].." << std::endl;
  std::cout << std::endl;
  std::cout << "Branch modes:" << std::endl;
  std::cout << "  HEAD       Header, NOEVT                       " << std::endl;
  std::cout << "  DST        Header+DST, ALLEVT                  " << std::endl;
  std::cout << "  RAW        ALL, RAWEVT                         " << std::endl;
  std::cout << "  HIT        ALL, HITEVT                         " << std::endl;
  std::cout << "  TRG        Header+TRG, ALLEVT              " << std::endl;
  std::cout << "  ACD        Header+ACD+DST, ALLEVT              " << std::endl;
  std::cout << "  SIT        Header+SIT+DST, ALLEVT              " << std::endl;
  std::cout << "  CZT        Header+CZT+DST, ALLEVT              " << std::endl;
  std::cout << "  CSI        Header+CSI+DST, ALLEVT              " << std::endl;
  std::cout << "  ALL        ALL, ALLEVT                         " << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "  -o FILE    set output file name as FILE        " << std::endl;
  std::cout << "  -n N       process only N events               " << std::endl;
  std::cout << "  -h         show this help                      " << std::endl;
  std::cout << std::endl;
}

int main( int argc, char *argv[] )
{
  std::cout << "======================================================" << std::endl;
  std::cout << " ComPair ROOT converter (croot)                       " << std::endl;
  std::cout << Form( " $Revision: 1.1.1.1 $, last update: %s %s", __DATE__, __TIME__ ) << std::endl;
  std::cout << "======================================================" << std::endl;

  if ( argc == 2 && argv[1][0] == '-' && argv[1][1] == 'h' ) {
    usage();
    return 0;
  }
  else if ( argc < 2 ) {
    std::cout << std::endl;
    std::cout << "You need at least two arguments. " << std::endl;
    std::cout << "Try \"croot -h\" for more information" << std::endl;
    return 0;
  }

  // set default parameters
  std::string ofn = "cmp_dst.root";
  int flag = CmpLib::ALL, sel = CmpLib::ALLEVT, nevt = 0;

  // arguments analysis
  std::string mode = argv[1];

  // HEADer mode
  if ( mode == "HEAD" ) {
    flag = CmpLib::HEAD;
    sel  = CmpLib::NOEVT;
    ofn  = "cmp_head.root";
  }

  // RAW mode
  else if ( mode == "RAW" ) {
    flag = CmpLib::ALL;
    sel  = CmpLib::RAWEVT;
    ofn  = "cmp_raw.root";
  }
  
  // HIT mode
  else if ( mode == "HIT" ) {
    flag = CmpLib::ALL;
    sel  = CmpLib::HITEVT;
    ofn  = "cmp_hit.root";
  }
  
  // DST mode
  else if ( mode == "DST" ) {
    flag = CmpLib::HEAD+CmpLib::DST;
    sel  = CmpLib::ALLEVT;
    ofn  = "cmp_dst.root";
  }

  // CRE mode
  else if ( mode == "CRE" ) {
    //    flag = CmpLib::HEAD+CmpLib::DST;
    flag = CmpLib::ALL;
    sel  = CmpLib::CREVT;
    ofn  = "cmp_cre.root";
  }

  // GRE mode
  else if ( mode == "GRE" ) {
    //    flag = CmpLib::HEAD+CmpLib::DST;
    flag = CmpLib::ALL;
    sel  = CmpLib::GREVT;
    ofn  = "cmp_gre.root";
  }

  // TRG mode
  else if ( mode == "TRG" ) {
    flag = CmpLib::HEAD+CmpLib::TRG;
    sel  = CmpLib::ALLEVT;
    ofn  = "cmp_trg.root";
  }

  // ACD mode
  else if ( mode == "ACD" ) {
    flag = CmpLib::HEAD+CmpLib::ACD+CmpLib::DST;
    sel  = CmpLib::ALLEVT;
    ofn  = "cmp_acd.root";
  }

  // SIT mode
  else if ( mode == "SIT" ) {
    flag = CmpLib::HEAD+CmpLib::SIT+CmpLib::DST;
    sel  = CmpLib::ALLEVT;
    ofn  = "cmp_sit.root";
  }

  // CZT mode
  else if ( mode == "CZT" ) {
    flag = CmpLib::HEAD+CmpLib::CZT+CmpLib::DST;
    sel  = CmpLib::ALLEVT;
    ofn  = "cmp_czt.root";
  }

  // CSI mode
  else if ( mode == "CSI" ) {
    flag = CmpLib::HEAD+CmpLib::CSI+CmpLib::DST;
    sel  = CmpLib::ALLEVT;
    ofn  = "cmp_csi.root";
  }

  // ALL mode
  else if ( mode == "ALL" ) {
    flag = CmpLib::ALL;
    sel  = CmpLib::ALLEVT;
    ofn  = "cmp_all.root";
  }

  // create main object
  RootConvert croot;
  croot.Init();

  // option analysis
  for ( int iarg = 2; iarg < argc; iarg++ ) {
    if ( argv[iarg][0] == '-' && argv[iarg][1] == 'o' ) {
      if ( argc > iarg+1 ) ofn = argv[++iarg];
    }
    
    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'n' ) {
      if ( argc > iarg+1 ) nevt = atoi( argv[++iarg] );
    }
    
    else if ( argv[iarg][0] == '-' )
      std::cout << Form( "Unknown option: %s", argv[iarg] ) << std::endl;

    else if ( croot.Add( argv[iarg] ) ) {
      std::cout << Form( "Raw data file [%02d]: %s, ", croot.GetNfile(), argv[iarg] );
      Double_t size = croot.GetFsize();
      if ( size < 1000 ) std::cout << Form( "%4.0f MB", size ) << std::endl;
      else               std::cout << Form( "%4.0f GB", size/1024. ) << std::endl;
    }
  }
  
  std::cout << Form( "Mode: %s", mode.c_str() );
  if ( nevt > 0 ) std::cout << Form( ", Nevt=%d", nevt );
  std::cout << std::endl;

  // open files
  if ( croot.Open( ofn.c_str(), flag ) < 0 ) return -1;

  // start process
  croot.Loop( sel, nevt );

  return 0;
}

