#include "CztAsic.h"

#include "CztLib.h"
#include "CztParams.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"

#include <iostream>
#include <fstream>

ClassImp( CztAsic )

CztAsic::CztAsic( void )
{
}

CztAsic::~CztAsic()
{
}

UShort_t CztAsic::GetData( UInt_t ch ) const
{
  if ( ch >= CztLib::N_DATA ) return 0;
  return fData[ch];
}

UShort_t CztAsic::GetData( Int_t ch, Int_t et ) const
{
  if ( ch*2+et >= CztLib::N_DATA ) return 0;
  return fData[ch*2+et];
}

Float_t CztAsic::GetTemp( void ) const
{
  /*
  unsigned char bTemp[4];
  Float_t fTemp = 0.0;
  if ( i == 0 ) {
    bTemp[0] = fData[104]/256;
    bTemp[1] = fData[104]%256;
    bTemp[2] = fData[105]/256;
    bTemp[3] = fData[105]%256;
    memcpy( &fTemp, &bTemp, sizeof( fTemp ) );
  }
  else {
    bTemp[0] = fData[208]/256;
    bTemp[1] = fData[208]%256;
    bTemp[2] = fData[209]/256;
    bTemp[3] = fData[209]%256;
    memcpy( &fTemp, &bTemp, sizeof( fTemp ) );
  }
  */

  return gCztParams->GetTempOffs(fID)+gCztParams->GetTempGain(fID)*(fData[104]-gCztParams->GetTempBase(fID));
}

Int_t CztAsic::ReadData( std::ifstream *fIN, Bool_t speak )
{
  Clear();
  
  Int_t fSize = 0;
  unsigned char fDummy[CztLib::N_DATA][2];
  unsigned char fDummyID[2];
  
  if ( fIN->eof() ) return -1;
  
  // read Asic ID
  fIN->read( (char *)&fDummyID, sizeof(fDummyID) );
  fSize += fIN->gcount();
  fID = (fDummyID[0]&0x3f)*256+fDummyID[1];
  
  // read Asic Data
  fIN->read( (char *)&fDummy, sizeof( fDummy ) );
  fSize += fIN->gcount();
  
  for ( Int_t i = 0; i < CztLib::N_DATA; i++ ) {
    fData[i] = fDummy[i][0]*256+fDummy[i][1];
  }

  return fSize;
}


Int_t CztAsic::ReadData( unsigned char *data, Bool_t speak )
{
  Clear();
  
  int fSize = 0;
  unsigned char fDummy[CztLib::N_DATA][2];
  unsigned char fDummyID[2];

  // read Asic ID
  memcpy( (char *)&fDummyID, (void *)(data+fSize), sizeof(fDummyID) );
  fSize += sizeof(fDummyID);
  fID = (fDummyID[0]&0x3f)*256+fDummyID[1];
  
  // read Asic Data
  memcpy( (char *)&fDummy, (void *)(data+fSize), sizeof( fDummy ) );
  fSize += sizeof( fDummy );
  
  for ( Int_t i = 0; i < CztLib::N_DATA; i++ ) {
    fData[i] = fDummy[i][0]*256+fDummy[i][1];
  }
  
  return fSize;
}

void CztAsic::Clear( Option_t *option )
{
  fID = 0;
  for ( Int_t i = 0; i < CztLib::N_DATA; i++ ) {
    fData[i] = 0;
  }
}
