#include "TrgEventID.h"

#include "TMath.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TGraph.h"

#include <iostream>

TrgEventID::TrgEventID( void )
{
  fReqTime  = 10.0;
  fSysClock = 20.0e-9;
  
  fAtrb = PKT_ATRB_TRG;
  fBuffer = new unsigned char[MAX_PKT_SIZE];
}

TrgEventID::TrgEventID( UShort_t atrb ) : DataEventID( atrb )
{
  fReqTime = 10.0;
  fSysClock = 20.0e-9;

  fBuffer = new unsigned char[MAX_PKT_SIZE];
}

TrgEventID::~TrgEventID()
{
  if ( fBuffer ) delete [] fBuffer;
}

void TrgEventID::SetUtcTime( void )
{
  Int_t a = 2;
  Long_t epch = fBuffer[a+3]*256*256*256+fBuffer[a+2]*256*256+fBuffer[a+1]*256+fBuffer[a+0];
  Long_t epus = fBuffer[a+7]*256*256*256+fBuffer[a+6]*256*256+fBuffer[a+5]*256+fBuffer[a+4];
  fUtcTime = epch*1.0+epus*1.0e-6;
}

void TrgEventID::SetEventID( void )
{
  Int_t a = 14;
  fEventID = (fBuffer[a+3]*256*256*256+fBuffer[a+2]*256*256+fBuffer[a+1]*256+fBuffer[a+0]) | 0x80000000;
}

void TrgEventID::SetTmStamp( void )
{
  Int_t a = 22;
  fTmStamp = fBuffer[a+3]*256*256*256+fBuffer[a+2]*256*256+fBuffer[a+1]*256+fBuffer[a+0];
}

void TrgEventID::SetTimePps( void )
{
  Int_t a = 18;
  fTimePps = fBuffer[a+3]*256*256*256+fBuffer[a+2]*256*256+fBuffer[a+1]*256+fBuffer[a+0];
  a = 42;
}

Int_t TrgEventID::Branch( void )
{
  if ( !fTree ) return -1;
  fTree->Branch( "fUtcTime", &fUtcTime, "fUtcTime/D" );
  fTree->Branch( "fEventID", &fEventID, "fEventID/i" );
  fTree->Branch( "fTmStamp", &fTmStamp, "fTmStamp/i" );
  fTree->Branch( "fTimePps", &fTimePps, "fTimePps/i" );
  return 0;
}

Int_t TrgEventID::Process( void )
{
  fTree->GetEvent( fTree->GetEntries()-1 );
  Double_t fTended = fUtcTime;

  std::vector<Int_t> ipoint;
  ipoint.clear();

  // GetRang
  Double_t pre_time = -1.0;
  UInt_t pre_pps = 0;
  Bool_t first = kTRUE;
  for ( Int_t i = 0; i < fTree->GetEntries(); i++ ) {
    fTree->GetEvent( i );
    if ( first || pre_pps != fTimePps ) {
      ipoint.push_back( i );
      pre_pps = fTimePps;
      pre_time = fUtcTime;
      first = kFALSE;
    }
  }

  Int_t enough = TMath::Nint( ( fTended - pre_time ) );
  if ( !enough ) {
    ipoint.pop_back();
  }

  Int_t np = ipoint.size();
  if ( np == 0 ) np = 1;

  Int_t *fIevt = new Int_t[np+1];
  for ( Int_t ip = 0; ip < np; ip++ ) {
    fIevt[ip] = ipoint[ip];
  }
  fIevt[np] = fTree->GetEntries();

  // GetSync
  Int_t  *fB   = new Int_t [np];
  UInt_t *fPps = new UInt_t[np];
  for ( Int_t ip = 0; ip < np; ip++ ) {
    fTree->GetEvent( fIevt[ip] );
    Double_t sync = 0.0;
    UInt_t   nclk = 0;
    UInt_t   pps0 = fTimePps;
    Double_t tim0 = fUtcTime;
    for ( Int_t ievt = fIevt[ip]; ievt < fIevt[ip+1]; ievt++ ) {
      fTree->GetEvent( ievt );
      UInt_t   fraw = fTmStamp;
      Double_t time = fUtcTime;
      UInt_t   pps1 = fTimePps;
      
      if ( fraw < pps1 && pps1-fraw < 0x10000 ) continue;
      
      if ( fraw < pps0 ) nclk = 0x100000000 + fraw - pps0;
      else               nclk = fraw - pps0;
      Double_t diff = time - tim0 - nclk*fSysClock;
      if ( diff < sync ) sync = diff;
    }
    sync += tim0;
    fB  [ip] = TMath::Nint( sync );
    fPps[ip] = pps0;
  }

  // GetCorr
  Double_t *fA    = new Double_t[np];
  UInt_t   *fNpps = new UInt_t  [np];
  
  for ( Int_t ip = 1; ip < np; ip++ ) {
    fNpps[ip] = 0;
    UInt_t nclk = fPps[ip]-fPps[ip-1];
    fNpps[ip] = TMath::Nint(nclk*fSysClock);
    fA[ip] = ( fNpps[ip] == 0 || nclk == 0 ) ? 1.0 : fNpps[ip] / (nclk*fSysClock);
  }
  fNpps[0] = fNpps[1];
  fA   [0] = fA   [1];

  TGraph *gr = new TGraph();

  for ( Int_t ip = 0; ip < np; ip++ ) {
    std::cout << Form( "%5d/%5d %10d %10d %10.8lf %9d",
		       ip, np-1, fIevt[ip], fIevt[ip+1]-1, fA[ip], fB[ip] ) << std::endl;
    for ( Int_t ievt = fIevt[ip]; ievt < fIevt[ip+1]; ievt++ ) {
      fTree->GetEvent( ievt );

      UInt_t nclk = ( fTmStamp > fPps[ip] ) ? fTmStamp-fPps[ip] : 0x100000000+fTmStamp-fPps[ip];
      Double_t fUtcCorr = nclk*fSysClock*fA[ip]*1.0e+9+fB[ip]*1.0e+9;
      UInt_t fEpoch   = TMath::FloorNint( fUtcCorr*1.0e-9 );
      UInt_t fEpochNS = TMath::FloorNint( fUtcCorr - fEpoch*1.0e+9 );
      fVevid.push_back( fEventID );
      fVepch.push_back( fEpoch );
      fVepns.push_back( fEpochNS );

      gr->SetPoint( gr->GetN(), ievt, fUtcTime-fEpoch-fEpochNS*1.0e-9 );
      //      std::cout << Form( "%18.0lf %10u %10u %10u", fUtcCorr, fEventID, fEpoch, fEpochNS ) << std::endl;
    }
  }

  TCanvas *c1 = new TCanvas( "c1", "c1" );
  TH2D *hist = new TH2D( "hist", "Time diff. [sec]", 1, -1000.0, fIevt[np]+1000.0, 1, -1.0, 1.0 );
  //  TH2D *hist = new TH2D( "hist", "Time diff. [sec]", 1, -1000.0, fIevt[np]+1000.0, 1, 0.34, 0.38 );
  //  TH2D *hist = new TH2D( "hist", "Time diff. [sec]", 1, -1000.0, fIevt[np]+1000.0, 1, -0.37, -0.33 );
  //  TH2D *hist = new TH2D( "hist", "Time diff. [sec]", 1, -1000.0, fIevt[np]+1000.0, 1, -0.4, -0.3 );
  //  TH2D *hist = new TH2D( "hist", "Time diff. [sec]", 1, -1000.0, fIevt[np]+1000.0, 1, -0.3, -0.2 );
  hist->SetStats( 0 );
  hist->GetXaxis()->SetTitle( "Event in file" );
  hist->Draw();
  gr->SetMarkerStyle( 20 );
  gr->SetMarkerSize( 0.25 );
  gr->Draw( "P" );
  c1->SaveAs( "diff.png" );
  delete c1;
  delete hist;

  delete gr;
  
  ipoint.clear();
  
  delete [] fIevt;
  delete [] fA;
  delete [] fB;
  delete [] fPps;

  return 0;
}

Int_t TrgEventID::Read( void )
{
  Clear();

  int i_buf_size_a;
  int i_buf_size_b;
  unsigned char buf_size_a[2];
  unsigned char buf_size_b[2];
  
  fIN.read( (char *)&buf_size_a, PKT_SIZE_SIZE );
  if ( fIN.eof() ) return 0;
  
  i_buf_size_a = (buf_size_a[0] << 8) + (buf_size_a[1]);

  fIN.read( (char *)fBuffer, i_buf_size_a );
  fSize = i_buf_size_a;
  
  SetUtcTime();
  SetEventID();
  SetTmStamp();
  SetTimePps();

  fIN.read( (char *)&buf_size_b, PKT_SIZE_SIZE );
  i_buf_size_b = (buf_size_b[0] << 8) + (buf_size_b[1]);
  
  if ( i_buf_size_a != i_buf_size_b ) return 0;
  
  return 1;
}

void TrgEventID::Clear( Option_t *option )
{
  fUtcTime = 0.0;
  fEventID = 0;
  fTmStamp = 0;
  fTimePps = 0;
}
