#include "CmpLib.h"
#include "CztConvert.h"
#include "TrgConvert.h"
#include "SitConvert.h"

#include "TString.h"

#include <iostream>
#include <fstream>
#include <string>

void usage( void )
{
  std::cout << std::endl;
  std::cout << "Usage: ccvrt [atrb] (options) [raw_file].." << std::endl;
  std::cout << std::endl;
  std::cout << "Convert modes:" << std::endl;
  std::cout << "  TRG        TRG                                 " << std::endl;
  std::cout << "  SIT        SiT                                 " << std::endl;
  std::cout << "  CZT        CZT                                 " << std::endl;
  std::cout << "  CSI        CsI                                 " << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "  -e FILE    set EventID file name as FILE       " << std::endl;
  std::cout << "  -o FILE    set output file name as FILE        " << std::endl;
  std::cout << "  -n N       process only N events               " << std::endl;
  std::cout << "  -h         show this help                      " << std::endl;
  std::cout << std::endl;
}

int main( int argc, char *argv[] )
{
  std::cout << "======================================================" << std::endl;
  std::cout << " ComPair data converter (ccvrt)                       " << std::endl;
  std::cout << Form( " $Revision: 1.1.1.1 $, last update: %s %s", __DATE__, __TIME__ ) << std::endl;
  std::cout << "======================================================" << std::endl;

  // set default parameters
  std::string ofn = "cmp_cvt.dat";
  std::string efn = "cmp_eid.dat";
  int nevt = 0;

  // create main object
  DataConvert *ccvrt;

  // arguments analysis
  std::string mode = argv[1];

  if      ( mode == "TRG" ) {
    ccvrt = new TrgConvert();
    ofn  = "trg_cvt.dat";
    efn  = "trg_eid.dat";
  }
  else if ( mode == "CZT" ) {
    ccvrt = new CztConvert();
    ofn  = "czt_cvt.dat";
    efn  = "czt_eid.dat";
  }
  else if ( mode == "SIT" ) {
    ccvrt = new SitConvert();
    ofn  = "sit_cvt.dat";
    efn  = "sit_eid.dat";
  }
  else if ( mode == "SI0" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI0 );
    ofn  = "si0_cvt.dat";
    efn  = "si0_eid.dat";
  }
  else if ( mode == "SI1" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI1 );
    ofn  = "si1_cvt.dat";
    efn  = "si1_eid.dat";
  }
  else if ( mode == "SI2" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI2 );
    ofn  = "si2_cvt.dat";
    efn  = "si2_eid.dat";
  }
  else if ( mode == "SI3" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI3 );
    ofn  = "si3_cvt.dat";
    efn  = "si3_eid.dat";
  }
  else if ( mode == "SI4" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI4 );
    ofn  = "si4_cvt.dat";
    efn  = "si4_eid.dat";
  }
  else if ( mode == "SI5" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI5 );
    ofn  = "si5_cvt.dat";
    efn  = "si5_eid.dat";
  }
  else if ( mode == "SI6" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI6 );
    ofn  = "si6_cvt.dat";
    efn  = "si6_eid.dat";
  }
  else if ( mode == "SI7" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI7 );
    ofn  = "si7_cvt.dat";
    efn  = "si7_eid.dat";
  }
  else if ( mode == "SI8" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI8 );
    ofn  = "si8_cvt.dat";
    efn  = "si8_eid.dat";
  }
  else if ( mode == "SI9" ) {
    ccvrt = new SitConvert( PKT_ATRB_SI9 );
    ofn  = "si9_cvt.dat";
    efn  = "si9_eid.dat";
  }
  
  if ( argc == 2 && argv[1][0] == '-' && argv[1][1] == 'h' ) {
    usage();
    return 0;
  }
  else if ( argc < 2 ) {
    std::cout << std::endl;
    std::cout << "You need at least two arguments. " << std::endl;
    std::cout << "Try \"ebuild -h\" for more information" << std::endl;
    return 0;
  }

  // option analysis
  for ( int iarg = 2; iarg < argc; iarg++ ) {
    if ( argv[iarg][0] == '-' && argv[iarg][1] == 'o' ) {
      if ( argc > iarg+1 ) ofn = argv[++iarg];
    }

    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'e' ) {
      if ( argc > iarg+1 ) efn = argv[++iarg];
    }
    
    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'n' ) {
      if ( argc > iarg+1 ) nevt = atoi( argv[++iarg] );
    }
    
    else if ( argv[iarg][0] == '-' )
      std::cout << Form( "Unknown option: %s", argv[iarg] ) << std::endl;

    else if ( ccvrt->Add( argv[iarg] ) ) {
      std::cout << Form( "Raw data file [%02d]: %s, ", ccvrt->GetNfile(), argv[iarg] );
      Double_t size = ccvrt->GetFsize();
      if ( size < 1000 ) std::cout << Form( "%4.0f MB", size ) << std::endl;
      else               std::cout << Form( "%4.0f GB", size/1024. ) << std::endl;
    }
  }
  
  if ( nevt > 0 ) std::cout << Form( ", Nevt=%d", nevt );
  std::cout << std::endl;

  // open files
  if ( ccvrt->Open( efn.c_str(), ofn.c_str() ) < 0 ) return -1;

  // start process
  ccvrt->Loop( nevt );

  ccvrt->Close();
  
  return 0;
}
