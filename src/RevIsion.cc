#include "RevIsion.h"

#include "TSystem.h"
#include "TObjString.h"

#include <iostream>

ClassImp( RevIsion )

RevIsion::RevIsion( void )
{
  Clear();
}

RevIsion::~RevIsion()
{
}

TString RevIsion::Status( void ) const
{
  TString status = "- status:\t";
  for ( Int_t i = 0; i < fList.GetEntries(); i++ ) {
    status += " " + ((TObjString *)fList.At( i ))->String();
  }
  return status;
}

TString RevIsion::Heads( void ) const
{
  TString rev;
  rev  = "- " + fChangeSet;
  if ( fBranch != "" )
    rev += "\n- " + fBranch;
  if ( fTag != "" )
    rev += "\n- " + fTag;
  if ( fDate != "" )
    rev += "\n- " + fDate;
  return rev;
}

void RevIsion::GetHeads( void )
{
  TString s = gSystem->GetFromPipe( "cd $CMPLIB; hg heads" );
  TObjArray *a1 = s.Tokenize( "\n" );
  TObjArray *a2 = ((TObjString *)a1->At( 0 ))->String().Tokenize( ":" );
  fRevNumber    = ((TObjString *)a2->At( 1 ))->String().Atoi();
  fChngSetID    = HexToLong(((TObjString *)a2->At( 2 ))->String());

  for ( Int_t i = 0; i < a1->GetEntries(); i++ ) {
    TObjArray *a = ((TObjString *)a1->At( i ))->String().Tokenize( ":" );
    if      ( (((TObjString *)a->At( 0 ))->String() ).CompareTo( "changeset" ) == 0 )
      fChangeSet = ((TObjString *)a1->At( i ))->String();
    else if ( (((TObjString *)a->At( 0 ))->String() ).CompareTo( "tag"       ) == 0 )
      fTag       = ((TObjString *)a1->At( i ))->String();
    else if ( (((TObjString *)a->At( 0 ))->String() ).CompareTo( "user"      ) == 0 )
      fUser      = ((TObjString *)a1->At( i ))->String();
    else if ( (((TObjString *)a->At( 0 ))->String() ).CompareTo( "date"      ) == 0 )
      fDate      = ((TObjString *)a1->At( i ))->String();
    else if ( (((TObjString *)a->At( 0 ))->String() ).CompareTo( "branch"    ) == 0 )
      fBranch    = ((TObjString *)a1->At( i ))->String();
  }
}

void RevIsion::GetStatus( void )
{
  TString s = gSystem->GetFromPipe( "cd $CMPLIB; hg sta inc src" );
  fList = *(s.Tokenize( "\n" ));
}

void RevIsion::Clear( Option_t *option )
{
  fRevNumber = -1;
  fChngSetID = -1;
  fChangeSet = "";
  fBranch    = "";
  fTag       = "";
  fUser      = "";
  fDate      = "";
  fList.Clear();
}

Long64_t RevIsion::HexToLong( TString s )
{
  Long64_t id = 0;
  Int_t size = s.Sizeof();
  for ( Int_t i = 0; i < size-1; i++ ) {
    TString substr = s[i];
    Int_t val = HexToInt( substr );
    id *= 16;
    id += val;
  }
  return id;
}

Int_t RevIsion::HexToInt( TString s )
{
  if      ( s.IsDec() ) return s.Atoi();
  else if ( s.IsHex() ) {
    s.ToLower();
    if ( s.EqualTo( 'a' ) ) return 10;
    if ( s.EqualTo( 'b' ) ) return 11;
    if ( s.EqualTo( 'c' ) ) return 12;
    if ( s.EqualTo( 'd' ) ) return 13;
    if ( s.EqualTo( 'e' ) ) return 14;
    if ( s.EqualTo( 'f' ) ) return 15;
    else {
      std::cout << "Hex should be within a to f" << std::endl;
      return 0;
    }
  }
  else return 0;
}

