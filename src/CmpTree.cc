#include "CmpTree.h"
#include "CmpLib.h"
#include "SitLib.h"

#include "CmpParams.h"
#include "SitParams.h"
#include "CztParams.h"
#include "CsIParams.h"

#include "TrgBranch.h"
#include "AcdBranch.h"
#include "SitBranch.h"
#include "CztBranch.h"
#include "CsIBranch.h"
#include "SimBranch.h"
#include "TrkBranch.h"
#include "CmpBranch.h"
#include "DstBranch.h"

#include "TTree.h"
#include "TMath.h"

ClassImp( CmpTree )

CmpTree *CmpTree::fPtr = 0;

CmpTree::CmpTree( void )
{
  if ( !fPtr ) fPtr = this;

  fExpNum   = -1;
  fRunNum   = -1;
  fUtcTime  = CmpLib::fDnotValid;
  fEventID  = 0;
  fNdst     = 0;
  fNsit     = 0;
  fTrg  = new TrgBranch;
  fAcd  = new AcdBranch;
  fSit  = new TClonesArray( "SitBranch", CmpLib::N_SLICE );
  fCzt  = new CztBranch;
  fCsI  = new CsIBranch;
  fSim  = new SimBranch;
  fTrk  = new TrkBranch;
  fCmp  = new CmpBranch;
  fDst  = new DstBranch;

  Init ();
  Clear();
}

CmpTree::~CmpTree()
{
  delete fDst;
  delete fCmp;
  delete fTrk;
  delete fSim;
  delete fCsI;
  delete fCzt;
  delete fSit;
  delete fAcd;
  delete fTrg;
}

SitBranch *CmpTree::GetSit( Int_t i )
{
  if ( i < 0 || i >= SitLib::N_SLICE ) return (SitBranch *)NULL;
  for ( Int_t j = 0; j < SitLib::N_SLICE; j++ ) {
    SitBranch *sit = (SitBranch *)fSit->At( j );
    if ( !sit ) continue;
    if ( sit->GetSlice() == i ) return sit;
  }
  return (SitBranch *)NULL;
}

SitBranch *CmpTree::GetSitAt( Int_t i )
{
  if ( i < 0 || i >= SitLib::N_SLICE ) return (SitBranch *)NULL;
  return (SitBranch *)fSit->At( i );
}

Int_t CmpTree::Init( void )
{
  if ( !CmpParams ::GetPtr() && !CmpParams::ReadFile() ) return -1;
  if ( !CztParams ::GetPtr() && !CztParams::ReadFile() ) return -1;
  if ( !CsIParams ::GetPtr() && !CsIParams::ReadFile() ) return -1;
  if ( !SitParams ::GetPtr() && !SitParams::ReadFile() ) return -1;

  //  for ( Int_t i = 0; i < CmpLib::N_SLICE; i++ ) new ( (*fSit)[i] ) SitBranch;

  return 1;
}

Int_t CmpTree::Fill( BasePacket *pkt )
{
  Int_t ret = 0;

  switch ( pkt->GetAtrb() ) {
  case PKT_ATRB_SIM:
    ret = FillSIM( pkt );
    break;
  case PKT_ATRB_SI0:
  case PKT_ATRB_SI1:
  case PKT_ATRB_SI2:
  case PKT_ATRB_SI3:
  case PKT_ATRB_SI4:
  case PKT_ATRB_SI5:
  case PKT_ATRB_SI6:
  case PKT_ATRB_SI7:
  case PKT_ATRB_SI8:
  case PKT_ATRB_SI9:
    ret = FillSIT( pkt );
    break;
  case PKT_ATRB_ACD:
    ret = FillACD( pkt );
    break;
  case PKT_ATRB_CZT:
    ret = FillCZT( pkt );
    break;
  case PKT_ATRB_CSI:
    ret = FillCSI( pkt );
    break;
  case PKT_ATRB_TRG:
    ret = FillTRG( pkt );
    break;
  case PKT_ATRB_DAQ:
    ret = FillDAQ( pkt );
    break;
  default:
    break;
  }
  return ret;
}

Int_t CmpTree::FillSIM( BasePacket *pkt )
{
  return 0;
}

Int_t CmpTree::FillSIT( BasePacket *pkt )
{
  Int_t N_SLICE = gSitParams->GetNslice();

  Int_t slice = N_SLICE - TMath::Nint( TMath::Log2( 1.0*pkt->GetAtrb() ) ) - 1;
  //  Int_t slice = TMath::Nint( TMath::Log2( 1.0*pkt->GetAtrb() ) );
  if ( slice < 0 ) {
    std::cout << Form( "<CmpTree:FillSIT> slice = %d", slice ) << std::endl;
    return -1;
  }
  
  SitBranch *sit = (SitBranch *)fSit->ConstructedAt( fNsit++ );
  if ( !sit ) return -1;
  sit->SetSlice( slice );
  sit->Fill( pkt );

  return 0;
}

Int_t CmpTree::ConstSIT( void )
{
  for ( Int_t slice = 0; slice < CmpLib::N_SLICE; slice++ ) {
    SitBranch *sit = (SitBranch *)fSit->ConstructedAt( fNsit++ );
    if ( !sit ) return -1;
    sit->SetSlice( slice );
  }
  return 0;
}

Int_t CmpTree::FillACD( BasePacket *pkt )
{
  return 0;
}

Int_t CmpTree::FillCZT( BasePacket *pkt )
{
  fCzt->Fill( pkt );
  return 0;
}

Int_t CmpTree::FillCSI( BasePacket *pkt )
{
  fCsI->Fill( pkt );
  return 0;
}

Int_t CmpTree::FillTRG( BasePacket *pkt )
{
  fTrg->Fill( pkt );
  fHitPatt = fTrg->GetHitPatt();
  fTrgMode = fTrg->GetCoHitRw();
  fTcdMode = fTrg->GetCoHitCD();
  //  std::cout << Form( "0x%016x 0x%08x 0x%08x", fHitPatt, fTrgMode, fTcdMode ) << std::endl;
  return 0;
}

Int_t CmpTree::FillDAQ( BasePacket *pkt )
{
  return 0;
}

void CmpTree::Branch( TTree *tree, Int_t bit )
{
  tree->Branch( "fNumber", "CmpTree", &fPtr );
  if ( bit & CmpLib::TRG ) tree->Branch( "fTrg", "TrgBranch", &fTrg );
  if ( bit & CmpLib::ACD ) tree->Branch( "fAcd", "AcdBranch", &fAcd );
  if ( bit & CmpLib::SIT ) tree->Branch( "fSit", &fSit );
  if ( bit & CmpLib::CZT ) tree->Branch( "fCzt", "CztBranch", &fCzt );
  if ( bit & CmpLib::CSI ) tree->Branch( "fCsI", "CsIBranch", &fCsI );
  if ( bit & CmpLib::DST ) tree->Branch( "fDst", "DstBranch", &fDst );
  if ( bit & CmpLib::DST ) tree->Branch( "fTrk", "TrkBranch", &fTrk );
  if ( bit & CmpLib::DST ) tree->Branch( "fCmp", "CmpBranch", &fCmp );
  if ( bit & CmpLib::SIM ) tree->Branch( "fSim", "SimBranch", &fSim );
}

void CmpTree::SetAddr( TTree *tree )
{
  SetAddr( tree, "fNumber", &fPtr  );
  SetAddr( tree, "fTrg"   , &fTrg  );
  SetAddr( tree, "fAcd"   , &fAcd  );
  SetAddr( tree, "fSit"   , &fSit  );
  SetAddr( tree, "fCzt"   , &fCzt  );
  SetAddr( tree, "fCsI"   , &fCsI  );
  SetAddr( tree, "fSim"   , &fSim  );
  SetAddr( tree, "fTrk"   , &fTrk  );
  SetAddr( tree, "fCmp"   , &fCmp  );
  SetAddr( tree, "fDst"   , &fDst  );
}

void CmpTree::SetAddr( TTree *tree, const char *name, void *ptr )
{
  if ( tree->GetBranch( name ) ) tree->SetBranchAddress( name, ptr );
}

Int_t CmpTree::Process( Int_t sel )
{
  fDst->Clear();
  if ( !fTrg->Process( sel ) ) return 0;
  if ( !fCzt->Process( sel ) ) return 0;
  if ( !fCsI->Process( sel ) ) return 0;
  for ( Int_t i = 0; i < fNsit; i++ ) {
    SitBranch *sit = GetSitAt( i );
    if ( !sit ) continue;
    if ( !sit->Process( sel ) ) return 0;
  }
  if ( !fDst->Process( sel ) ) return 0;
  fNdst++;
  
  return 1;
}

Int_t CmpTree::Select( Int_t sel )
{
  return 1;
}

Int_t CmpTree::WritePar( void )
{
  if ( gCmpParams ) gCmpParams->Write();

  return 1;
}

void CmpTree::Clear( Option_t *option )
{
  fUtcTime  = CmpLib::fDnotValid;
  fEventID  = 0;
  fHitPatt  = 0;
  fTrgMode  = 0;
  fTcdMode  = 0;
  fPktAtrb  = 0;
  fNdst     = 0;
  fNsit     = 0;
  if ( fTrg ) fTrg->Clear( "C" );
  if ( fAcd ) fAcd->Clear( "C" );
  if ( fSit ) fSit->Clear( "C" );
  if ( fCzt ) fCzt->Clear( "C" );
  if ( fCsI ) fCsI->Clear( "C" );
  if ( fSim ) fSim->Clear( "C" );
  if ( fTrk ) fTrk->Clear( "C" );
  if ( fCmp ) fCmp->Clear( "C" );
  if ( fDst ) fDst->Clear( "C" );

  return;
}

void CmpTree::Print( Option_t *option ) const
{
}
