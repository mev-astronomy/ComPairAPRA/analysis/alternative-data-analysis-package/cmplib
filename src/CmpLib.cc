#include "CmpLib.h"

#include "TString.h"

ClassImp( CmpLib )

Int_t    CmpLib::fThreshold = 0;
TVector3 CmpLib::fVnotValid = TVector3( -99999.9, -99999.9, -99999.9 );
Double_t CmpLib::fDnotValid = -99999.9;
Float_t  CmpLib::fFnotValid = -99999.9;

CmpLib::CmpLib()
{
}

CmpLib::~CmpLib()
{
}
