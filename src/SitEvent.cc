#include "SitEvent.h"
#include "SitLib.h"
#include "SitAsic.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"

#include <iostream>
#include <fstream>

ClassImp( SitEvent )

SitEvent::SitEvent( void )
{
  Clear();
}

SitEvent::~SitEvent()
{
}

SitAsic *SitEvent::GetAsic( UShort_t aid )
{
  if ( aid < 0 || aid >= SitLib::N_ASIC ) return (SitAsic *)NULL;
  return &fAsic[aid];
}

Int_t SitEvent::Fill( BasePacket *pkt )
{
  return readPacket( pkt->GetData() );
}

Int_t SitEvent::readPacket( unsigned char *data, Bool_t speak )
{
  Clear();

  unsigned char f1byte;
  unsigned char f2byte[2];
  unsigned char f4byte[4];
  unsigned char f8byte[8];

  int size = 0;

  // Read packet size
  memcpy( (void *)&f2byte, (data+size), sizeof(f2byte) );
  size += sizeof( f2byte );
  fPktSize = convData( f2byte );
  
  // Read packet type
  f1byte = *(data+size);
  size += 1;
  fPktType = f1byte;
  
  // Read layer number
  f1byte = *(data+size);
  size += 1;
  fLayerNm = f1byte;
  
  // Read system time
  memcpy( (char *)&f8byte, (data+size), sizeof(f8byte) );
  size += sizeof(f8byte);
  fSysTime = convTime( f8byte );
  
  // Read triggering condition
  f1byte = *(data+size);
  size += 1;
  fTrigCon = f1byte;
    
  // Read Vata Enable
  memcpy( (char *)&f2byte, (data+size), sizeof(f2byte) );
  size += sizeof(f2byte);
  fVataEna = convData( f2byte );

  // Read pl data header
  f1byte = *(data+size);
  size += 1;
  fPlDataH = f1byte;
  
  // Read event count
  memcpy( (char *)&f4byte, (data+size), sizeof(f4byte) );
  size += sizeof(f4byte);
  fEventCt = conv4Byte( f4byte );

  // Read event id
  memcpy( (char *)&f4byte, (data+size), sizeof(f4byte) );
  size += sizeof(f4byte);
  fEventID = conv4Byte( f4byte );
  
  // Read gps pps count
  memcpy( (char *)&f4byte, (data+size), sizeof(f4byte) );
  size += sizeof(f4byte);
  fNgpsPps = conv4Byte( f4byte );

  // Time Since last pps
  memcpy( (char *)&f4byte, (data+size), sizeof(f4byte) );
  size += sizeof(f4byte);
  fTmSince = conv4Byte( f4byte );

  // Read live time
  memcpy( (char *)&f8byte, (data+size), sizeof(f8byte) );
  size += sizeof(f8byte);
  fLivTime = convTime( f8byte );

  // Read running time
  memcpy( (char *)&f8byte, (data+size), sizeof(f8byte) );
  size += sizeof(f8byte);
  fRunTime = convTime( f8byte );

  // Read header TBD
  memcpy( (char *)&f8byte, (data+size), sizeof(f8byte) );
  size += sizeof(f8byte);
  fHeadTBD = convTime( f8byte );
  
  // Read ASIC data
  for ( UShort_t i = 0; i < SitLib::N_ASIC; i++ ) {
    size += fAsic[i].readAsic( data+size );
  }

  return size;
}

void SitEvent::Clear( Option_t *option ) 
{
  fUtcTime = 0;
  fUtcCorr = 0;
  fTmStamp = 0;
  fTimePps = 0;

  fPktSize = 0;
  fPktType = 0;
  fSysTime = 0;
  fTrigCon = 0;
  fVataEna = 0;
  fPlDataH = 0;
  fEventCt = 0;
  fEventID = 0;
  fNgpsPps = 0;
  fTmSince = 0;
  fLivTime = 0;
  fRunTime = 0;
  fHeadTBD = 0;

  for ( Int_t i = 0; i < SitLib::N_ASIC; i++ ) {
    fAsic[i].Clear();
  }
}

UShort_t SitEvent::convData( unsigned char *data )
{
  Int_t order[2] = { 1, 0 };
  UInt_t fData = 0;
  for ( Int_t i = 0; i < 2; i++ ) {
    fData *= 256;
    fData += data[order[i]];
  }
  return fData;
}

UInt_t SitEvent::conv4Byte( unsigned char *data )
{
  Int_t order[4] = { 3, 2, 1, 0 };
  UInt_t fData = 0;
  for ( Int_t i = 0; i < 4; i++ ) {
    fData *= 256;
    fData += data[order[i]];
  }
  return fData;
}

Double_t SitEvent::convTime( unsigned char *time )
{
  Int_t order[8] = { 7, 6, 5, 4, 3, 2, 1, 0 };
  Double_t dtime = 0.0;
  for ( Int_t i = 0; i < 8; i++ ) {
    dtime *= 256.0;
    dtime += time[order[i]];
  }
  dtime /= 1000000000;

  return dtime;
}
