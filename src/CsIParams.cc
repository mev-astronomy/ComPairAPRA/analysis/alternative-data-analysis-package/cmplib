#include "CsIParams.h"
#include "CsILib.h"

#include "TFile.h"
#include "TMath.h"

#include <iostream>

ClassImp( CsIParams )

CsIParams *gCsIParams = 0;

CsIParams::CsIParams()
{
  for ( Int_t xadr = 0; xadr < CsILib::N_XADR; xadr++ ) {
    for ( Int_t yadr = 0; yadr < CsILib::N_YADR; yadr++ ) {
      fAssign[xadr][yadr] = -1;
    }
  }
    
  for ( Int_t layr = 0; layr < CsILib::N_LAYR; layr++ ) {
    for ( Int_t logs = 0; logs < CsILib::N_LOGS; logs++ ) {
      fOrigin[layr][logs] = CsILib::fVnotValid;
      fDimens[layr][logs] = CsILib::fVnotValid;
      fQcorr [layr][logs] = 0;
      fHposi [layr][logs] = 0;
      for ( Int_t side = 0; side < CsILib::N_SIDE; side++ ) {
	fIndex[layr][logs][side] = -1;
      }
    }
  }
  
  for ( Int_t indx = 0; indx < CsILib::N_INDX; indx++ ) {
    fLyrID[indx] = -1;
    fLogID[indx] = -1;
    fSenID[indx] = -1;
    fXaddr[indx] = -1;
    fYaddr[indx] = -1;
    fDead [indx] = kFALSE;

    fPeds [indx] = 0;
    fThre [indx] = 0;
    fGain [indx] = 0;
  }

  gCsIParams = this;
}

CsIParams::~CsIParams()
{
  gCsIParams = 0;

  for ( Int_t indx = 0; indx < CsILib::N_INDX; indx++ ) {
    if ( fPeds[indx] ) delete fPeds[indx];
    if ( fThre[indx] ) delete fThre[indx];
    if ( fGain[indx] ) delete fGain[indx];
  }

  for ( Int_t layr = 0; layr < CsILib::N_LAYR; layr++ ) {
    for ( Int_t logs = 0; logs < CsILib::N_LOGS; logs++ ) {
      if ( fQcorr[layr][logs] ) delete fQcorr[layr][logs];
      if ( fHposi[layr][logs] ) delete fHposi[layr][logs];
    }
  }
}

Int_t CsIParams::SetPixMap( Int_t indx, Int_t xadr, Int_t yadr )
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return -1;
  if ( xadr < 0 || xadr >= CsILib::N_XADR ) return -1;
  if ( yadr < 0 || yadr >= CsILib::N_YADR ) return -1;

  fAssign[xadr][yadr] = indx;
  fXaddr[indx] = xadr;
  fYaddr[indx] = yadr;

  return 1;
}

Int_t CsIParams::SetIndex( Int_t indx, Int_t layr, Int_t logs, Int_t side )
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return -1;
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return -1;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return -1;
  if ( side < 0 || side >= CsILib::N_SIDE ) return -1;

  fIndex[layr][logs][side] = indx;
  fLyrID[indx] = layr;
  fLogID[indx] = logs;
  fSenID[indx] = side;

  return 1;
}

Int_t CsIParams::SetDead( Int_t indx, Bool_t dead )
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return -1;
  fDead[indx] = dead;

  return 1;
}

Int_t CsIParams::SetOrigin( Int_t layr, Int_t logs, Double_t x, Double_t y, Double_t z )
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return -1;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return -1;

  fOrigin[layr][logs] = TVector3( x, y, z );

  return 1;
}

Int_t CsIParams::SetDimens( Int_t layr, Int_t logs, Double_t x, Double_t y, Double_t z )
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return -1;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return -1;
  
  fDimens[layr][logs] = TVector3( x, y, z );

  return 1;
}

Int_t CsIParams::GetAssign( Int_t xadr, Int_t yadr ) const
{
  if ( xadr < 0 || xadr >= CsILib::N_XADR ) return -1;
  if ( yadr < 0 || yadr >= CsILib::N_YADR ) return -1;
  return fAssign[xadr][yadr];
}

Int_t CsIParams::GetIndex( Int_t layr, Int_t logs, Int_t side ) const
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return -1;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return -1;
  if ( side < 0 || side >= CsILib::N_SIDE ) return -1;

  return fIndex[layr][logs][side];
}

Int_t CsIParams::GetLyrID( Int_t indx ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return -1;
  return fLyrID[indx];
}

Int_t CsIParams::GetLogID( Int_t indx ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return -1;
  return fLogID[indx];
}

Int_t CsIParams::GetSenID( Int_t indx ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return -1;
  return fSenID[indx];
}

Byte_t CsIParams::GetXaddr( Int_t indx ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return -1;
  return fXaddr[indx];
}

Byte_t CsIParams::GetYaddr( Int_t indx ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return -1;
  return fYaddr[indx];
}

Bool_t CsIParams::IsDead( Int_t indx ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return kTRUE;
  return fDead[indx];
}

Int_t CsIParams::GetCoord( Int_t layr ) const
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return -1;
  if ( layr%2 == 0 ) return CsILib::COORD_Y;
  else               return CsILib::COORD_X;
}

TVector3 CsIParams::GetOrigin( Int_t layr, Int_t logs ) const
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return CsILib::fVnotValid;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return CsILib::fVnotValid;
  
  return fOrigin[layr][logs];
}

TVector3 CsIParams::GetDimens( Int_t layr, Int_t logs ) const
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return CsILib::fVnotValid;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return CsILib::fVnotValid;
  
  return fDimens[layr][logs];
}

Double_t CsIParams::GetPeds( Int_t indx, Int_t ip ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return 0.0;
  if ( ip   < 0 || ip   >= fNpoint        ) return 0.0;

  return fPeds[indx][ip];
}

Double_t CsIParams::GetThre( Int_t indx, Int_t ip ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return 0.0;
  if ( ip   < 0 || ip   >= fNpoint        ) return 0.0;

  return fThre[indx][ip];
}

Double_t CsIParams::GetGain( Int_t indx, Int_t ip ) const
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return 0.0;
  if ( ip   < 0 || ip   >= fNpoint        ) return 0.0;

  return fGain[indx][ip];
}

Double_t CsIParams::GetQcorr( Int_t layr, Int_t logs, Double_t diff ) const
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return 0.0;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return 0.0;

  if ( !fQcorr[layr][logs] ) return 1.0;

  Double_t corr = GetCorr1D( fQcorr[layr][logs], diff );
  return ( corr > 0.0 ) ? 1.0/corr : 1.0;
}

TH1D *CsIParams::GetQcorHist( Int_t layr, Int_t logs ) const
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return (TH1D *)NULL;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return (TH1D *)NULL;

  if ( fQcorr[layr][logs] ) return fQcorr[layr][logs];
  return (TH1D *)NULL;
}

Double_t CsIParams::GetHposi( Int_t layr, Int_t logs, Double_t diff ) const
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return 0.0;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return 0.0;

  if ( !fHposi[layr][logs] ) return diff;

  Double_t corr = GetCorr1D( fHposi[layr][logs], diff );
  if ( corr < -1.0 || corr > 1.0 ) return diff;

  Double_t length = ( layr%2 == 0 ) ? GetDimens( layr, logs ).X() : GetDimens( layr, logs ).Y();
  return length/2.0*corr;
}

TH1D *CsIParams::GetHposHist( Int_t layr, Int_t logs ) const
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return (TH1D *)NULL;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return (TH1D *)NULL;

  if ( fHposi[layr][logs] ) return fHposi[layr][logs];
  return (TH1D *)NULL;
}

void CsIParams::Init( Double_t b, Double_t e, Int_t n, Double_t *t )
{
  if ( n < 0 ) return;

  DetParams::Init( b, e, n, t );
  
  for ( Int_t indx = 0; indx < CsILib::N_INDX; indx++ ) {
    fPeds[indx] = new Double_t [fNpoint];
    fThre[indx] = new Double_t [fNpoint];
    fGain[indx] = new Double_t [fNpoint];
    for ( Int_t ip = 0; ip < fNpoint; ip++ ) {
      fPeds[indx][ip] = 0.0;
      fThre[indx][ip] = 0.0;
      fGain[indx][ip] = 1.0;
    }
  }
}

void CsIParams::SetPeds( Int_t indx, Int_t ip, Double_t peds )
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return;
  if ( ip   < 0 || ip   >= fNpoint        ) return;

  fPeds[indx][ip] = peds;
}

void CsIParams::SetThre( Int_t indx, Int_t ip, Double_t thre )
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return;
  if ( ip   < 0 || ip   >= fNpoint        ) return;

  fThre[indx][ip] = thre;
}

void CsIParams::SetGain( Int_t indx, Int_t ip, Double_t gain )
{
  if ( indx < 0 || indx >= CsILib::N_INDX ) return;
  if ( ip   < 0 || ip   >= fNpoint        ) return;

  fGain[indx][ip] = gain;
}

void CsIParams::SetQcorHist( Int_t layr, Int_t logs, TH1D *hist )
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return;

  if ( !hist ) return;
  fQcorr[layr][logs] = SetCorr1D( hist, fQcorr[layr][logs], Form( "qcor_%02d_%02d", layr, logs ) );
}

void CsIParams::SetHposHist( Int_t layr, Int_t logs, TH1D *hist )
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return;

  if ( !hist ) return;
  fHposi[layr][logs] = SetCorr1D( hist, fHposi[layr][logs], Form( "hpos_%02d_%02d", layr, logs ) );
}

void CsIParams::ResetQcorHist( Int_t layr, Int_t logs )
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return;

  if ( fQcorr[layr][logs] ) delete fQcorr[layr][logs];
  fQcorr[layr][logs] = 0;
}

void CsIParams::ResetHposHist( Int_t layr, Int_t logs )
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return;
  if ( logs < 0 || logs >= CsILib::N_LOGS ) return;

  if ( fHposi[layr][logs] ) delete fHposi[layr][logs];
  fHposi[layr][logs] = 0;
}

CsIParams *CsIParams::GetPtr( void )
{
  return gCsIParams;
}

CsIParams *CsIParams::ReadFile( const char *fname, Int_t verb )
{
  CsIParams *par = (CsIParams *)DetParams::ReadFile( fname, "CsIParams" );
  if ( par && verb ) std::cout << "Read CsIParams from " << fname << std::endl;

  return par;
}
