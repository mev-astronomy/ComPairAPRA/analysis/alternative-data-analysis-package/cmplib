#include "SitParams.h"
#include "CmpTree.h"

#include "TFile.h"
#include "TMath.h"

ClassImp( SitParams )

SitParams *gSitParams = 0;

SitParams::SitParams()
{
  for ( Int_t slice = 0; slice < SitLib::N_SLICE; slice++ ) {
    for ( Int_t aid = 0; aid < SitLib::N_ASIC; aid++ ) {
      for ( Int_t cid = 0; cid < SitLib::N_CHAN; cid++ ) {
	fLindx[slice][aid][cid] = -1;
      }
    }
  }
  
  for ( Int_t lidx = 0; lidx < SitLib::N_LINDX; lidx++ ) {
    fSlice  [lidx] = -1;
    fCoord  [lidx] = -1;
    fStrip  [lidx] = -1;
    fAsic   [lidx] = -1;
    fChan   [lidx] = -1;
    fDead   [lidx] = kFALSE;
    fOrigin [lidx] = SitLib::fVnotValid;
    fDimens [lidx] = SitLib::fVnotValid;
    fPeds   [lidx] = 0;
    fThre   [lidx] = 0;
    fGain   [lidx] = 0;
  }

  gSitParams = this;
}

SitParams::~SitParams()
{
  gSitParams = 0;
  
  for ( Int_t lidx = 0; lidx < SitLib::N_LINDX; lidx++ ) {
    if ( fPeds[lidx] ) delete fPeds[lidx];
    if ( fThre[lidx] ) delete fThre[lidx];
    if ( fGain[lidx] ) delete fGain[lidx];
  }
}

Int_t SitParams::GetLindx( Int_t slice, Int_t aid, Int_t cid ) const
{
  if ( slice < 0 || slice >= SitLib::N_SLICE ) return -1;
  if ( aid   < 0 || aid   >= SitLib::N_ASIC  ) return -1;
  if ( cid   < 0 || cid   >= SitLib::N_CHAN  ) return -1;
  return fLindx[slice][aid][cid];
}

Int_t SitParams::GetIndex( Int_t layer, Int_t strip ) const
{
  if ( layer < 0 || layer >= SitLib::N_SLICE*2 ) return -1;
  if ( strip < 0 || strip >= SitLib::N_STRIP   ) return -1;

  Int_t slice = layer/2;
  for ( Int_t a_id = 0; a_id < SitLib::N_ASIC; a_id++ ) {
    for ( Int_t c_id = 0; c_id < SitLib::N_CHAN; c_id++ ) {
      Int_t idx = fLindx[slice][a_id][c_id];
      if ( GetStrip( idx ) == strip ) return idx;
    }
  }

  return -1;
}

Int_t SitParams::GetSlice( Int_t lidx ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;
  return fSlice[lidx];
}

Int_t SitParams::GetCoord( Int_t lidx ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;
  return fCoord[lidx];
}

Int_t SitParams::GetStrip( Int_t lidx ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;
  return fStrip[lidx];
}

Int_t SitParams::GetAsic( Int_t lidx ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;
  return fAsic[lidx];
}
 
Int_t SitParams::GetChan( Int_t lidx ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;
  return fChan[lidx];
}

Bool_t SitParams::IsDead( Int_t lidx ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return kFALSE;
  return fDead[lidx];
}

TVector3 SitParams::GetOrigin( Int_t lidx ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return SitLib::fVnotValid;
  return fOrigin[lidx];
}

TVector3 SitParams::GetDimens( Int_t lidx ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return SitLib::fVnotValid;
  return fDimens[lidx];
}

Double_t SitParams::GetPeds( Int_t lidx, Int_t ip ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return 0.0;
  if ( ip   < 0 || ip   >= fNpoint         ) return 0.0;

  return fPeds[lidx][ip];
}

Double_t SitParams::GetThre( Int_t lidx, Int_t ip ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return 0.0;
  if ( ip   < 0 || ip   >= fNpoint         ) return 0.0;

  return fThre[lidx][ip];
}

Double_t SitParams::GetGain( Int_t lidx, Int_t ip ) const
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return 1.0;
  if ( ip   < 0 || ip   >= fNpoint         ) return 1.0;

  return fGain[lidx][ip];
}

Int_t SitParams::SetAssign( Int_t lidx, Int_t slc, Int_t x_y, Int_t srp, Int_t aid, Int_t chn )
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;
  if ( slc  < 0 || slc  >= SitLib::N_SLICE ) return -1;
  if ( x_y  < 0 || x_y  >= SitLib::N_COORD ) return -1;
  if ( srp  < 0 || srp  >= SitLib::N_STRIP ) return -1;
  if ( aid  < 0 || aid  >= SitLib::N_ASIC  ) return -1;
  if ( chn  < 0 || chn  >= SitLib::N_CHAN  ) return -1;

  fLindx[slc][aid][chn] = lidx;
  fSlice[lidx] = slc;
  fCoord[lidx] = x_y;
  fStrip[lidx] = srp;
  fAsic [lidx] = aid;
  fChan [lidx] = chn;

  return 1;
}

Int_t SitParams::SetDead( Int_t lidx, Bool_t dead )
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;

  fDead[lidx] = dead;

  return 1;
}

Int_t SitParams::SetOrigin( Int_t lidx, Double_t x, Double_t y, Double_t z )
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;
  fOrigin[lidx] = TVector3( x, y, z );

  return 1;
}

Int_t SitParams::SetDimens( Int_t lidx, Double_t x, Double_t y, Double_t z )
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return -1;
  fDimens[lidx] = TVector3( x, y, z );

  return 1;
}

void SitParams::Init( Double_t b, Double_t e, Int_t n, Double_t *t )
{
  fTbegun = b;
  fTended = e;
  fNpoint = n;
  fTpoint = new Double_t[fNpoint];
  for ( Int_t ip = 0; ip < fNpoint; ip++ ) {
    fTpoint[ip] = *(t+ip);
  }
  
  if ( n < 0 ) return;
  for ( Int_t lidx = 0; lidx < SitLib::N_LINDX; lidx++ ) {
    fPeds[lidx] = new Double_t [fNpoint];
    fThre[lidx] = new Double_t [fNpoint];
    fGain[lidx] = new Double_t [fNpoint];
    for ( Int_t ip = 0; ip < fNpoint; ip++ ) {
      fPeds[lidx][ip] = 0.0;
      fThre[lidx][ip] = 0.0;
      fGain[lidx][ip] = 1.0;
    }
  }
}

void SitParams::SetPeds( Int_t lidx, Int_t ip, Double_t peds )
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return;
  if ( ip   < 0 || ip   >= fNpoint         ) return;

  fPeds[lidx][ip] = peds;
}

void SitParams::SetThre( Int_t lidx, Int_t ip, Double_t thre )
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return;
  if ( ip   < 0 || ip   >= fNpoint         ) return;

  fThre[lidx][ip] = thre;
}

void SitParams::SetGain( Int_t lidx, Int_t ip, Double_t gain )
{
  if ( lidx < 0 || lidx >= SitLib::N_LINDX ) return;
  if ( ip   < 0 || ip   >= fNpoint         ) return;

  fGain[lidx][ip] = gain;
}

SitParams *SitParams::GetPtr( void )
{
  return gSitParams;
}

SitParams *SitParams::ReadFile( const char *fname, Int_t verb )
{
  SitParams *par = (SitParams *)DetParams::ReadFile( fname, "SitParams" );
  if ( par && verb ) std::cout << "Read SitParams from " << fname << std::endl;

  return par;
}
