#include "CztEvtHeader.h"
#include "CztLib.h"

#include "TString.h"
#include "TMath.h"

#include <iostream>
#include <fstream>

ClassImp( CztEvtHeader )

CztEvtHeader::CztEvtHeader( void )
{
  fPrStamp = 0;
  fTmCarry = 0;
  fUtcTime = 0.0;
  fToffset = 0.0;
  
  Clear();
}

CztEvtHeader::~CztEvtHeader()
{
}

Int_t CztEvtHeader::ReadData( std::ifstream *fIN, UShort_t ver, Bool_t speak )
{
  Clear();

  Int_t fSize = 0;
  unsigned char f4Byte[4];
  unsigned char f8Byte[8];

  if ( fIN->eof() ) return -1;

  // Read Event ID
  fIN->read( (char *)&f4Byte, sizeof(f4Byte) );
  fSize += fIN->gcount();
  fEventID = conv4Byte( f4Byte );
  
  // Read Time Stamp
  fIN->read( (char *)&f4Byte, sizeof(f4Byte) );
  fSize += fIN->gcount();
  fTmStamp = conv4Byte( f4Byte );

  // Temporary UtcTime
  if ( fPrStamp > fTmStamp ) fTmCarry++;
  fPrStamp = fTmStamp;

  fUtcTime = (((Long_t)(fTmCarry)*0x100000000)+fPrStamp)*10.0e-9 + fToffset;
  fEpoch   = TMath::FloorNint( fUtcTime );
  fEpochNS = TMath::Nint( (fUtcTime-fEpoch)*1.0e+9 );
  
  if ( ver > fSize ) {
    
    // Read GPS PPS Time Stamp
    fIN->read( (char *)&f4Byte, sizeof(f4Byte) );
    fSize += fIN->gcount();
    fTimePps = conv4Byte( f4Byte );
    
    // Read Epoch Nano Second
    fIN->read( (char *)&f4Byte, sizeof(f4Byte) );
    fSize += fIN->gcount();
    UInt_t fTempNS = 100*conv4Time( f4Byte );
    f8Byte[0] = f4Byte[0];
    f8Byte[1] = f4Byte[1];
    f8Byte[2] = f4Byte[2];
    f8Byte[3] = f4Byte[3];
    
    // Read Epoch Second
    fIN->read( (char *)&f4Byte, sizeof(f4Byte) );
    fSize += fIN->gcount();
    UInt_t fTemp = conv4Time( f4Byte );
    f8Byte[4] = f4Byte[0];
    f8Byte[5] = f4Byte[1];
    f8Byte[6] = f4Byte[2];
    f8Byte[7] = f4Byte[3];

    if ( f8Byte[0] == 0 && f8Byte[1] == 1 && f8Byte[2] == 0 && f8Byte[3] == 2 &&
	 f8Byte[4] == 0 && f8Byte[5] == 3 && f8Byte[6] == 0 && f8Byte[7] == 4 ) {
      // std::cerr << "EpochNS and Epoch information was not set properly" << std::endl;
    }
    else {
      fEpochNS = fTempNS;
      fEpoch   = fTemp;
    }

    // Real Utc Time
    fUtcTime = fEpoch+fEpochNS*1.0e-9;
    //    std::cout << Form( "%u %u %lf", fEpoch, fEpochNS, fUtcTime ) << std::endl;
  }
  
  return fSize;
}

Int_t CztEvtHeader::ReadData( unsigned char *data, UShort_t ver, Bool_t speak )
{
  Clear();

  Int_t fSize = 0;
  unsigned char f4Byte[4];
  unsigned char f8Byte[8];

  // Read Event ID
  memcpy( (char *)&f4Byte, (data+fSize), sizeof(f4Byte) );
  fEventID = conv4Byte( f4Byte );
  fSize += sizeof(f4Byte);

  // Read Time Stamp
  memcpy( (char *)&f4Byte, (data+fSize), sizeof(f4Byte) );
  fTmStamp = conv4Byte( f4Byte );
  fSize += sizeof(f4Byte);

  // Temporary UtcTime
  if ( fPrStamp > fTmStamp ) fTmCarry++;
  fPrStamp = fTmStamp;

  fUtcTime = (((Long_t)(fTmCarry)*0x100000000)+fPrStamp)*10.0e-9 + fToffset;
  fEpoch   = TMath::FloorNint( fUtcTime );
  fEpochNS = TMath::Nint( (fUtcTime-fEpoch)*1.0e+9 );
  
  if ( ver > fSize ) {
    // Read GPS PPS Time Stamp
    memcpy( (char *)&f4Byte, (data+fSize), sizeof(f4Byte) );
    fTimePps = conv4Byte( f4Byte );
    fSize += sizeof(f4Byte);
    
    // Read Epoch Nano Second
    memcpy( (char *)&f4Byte, (data+fSize), sizeof(f4Byte) );
    UInt_t fTempNS = 100*conv4Time( f4Byte );
    f8Byte[0] = f4Byte[0];
    f8Byte[1] = f4Byte[1];
    f8Byte[2] = f4Byte[2];
    f8Byte[3] = f4Byte[3];
    fSize += sizeof(f4Byte);
    
    // Read Epoch Second
    memcpy( (char *)&f4Byte, (data+fSize), sizeof(f4Byte) );
    UInt_t fTemp = conv4Time( f4Byte );
    f8Byte[4] = f4Byte[0];
    f8Byte[5] = f4Byte[1];
    f8Byte[6] = f4Byte[2];
    f8Byte[7] = f4Byte[3];
    fSize += sizeof(f4Byte);

    if ( f8Byte[0] == 0 && f8Byte[1] == 1 && f8Byte[2] == 0 && f8Byte[3] == 2 &&
	 f8Byte[4] == 0 && f8Byte[5] == 3 && f8Byte[6] == 0 && f8Byte[7] == 4 ) {
      // std::cerr << "EpochNS and Epoch information was not set properly" << std::endl;
    }
    else {
      fEpochNS = fTempNS;
      fEpoch   = fTemp;
    }

    // Real Utc Time
    fUtcTime = fEpoch+fEpochNS*1.0e-9;
  }
  
  //  std::cout << Form( "%u %u %lf", fEpoch, fEpochNS, fUtcTime ) << std::endl;
  return fSize;
}

void CztEvtHeader::Clear( Option_t *option ) 
{
  fEventID = 0;
  fTmStamp = 0;
  fTimePps = 0;
  fEpoch   = 0;
  fEpochNS = 0;

  fUtcTime = 0.0;
}

UInt_t CztEvtHeader::conv4Time( unsigned char *data )
{
  Int_t  order[4] = { 2, 3, 0, 1 };
  UInt_t conv = 0;
  for ( Int_t i = 0; i < 4; i++ ) {
    conv *= 256;
    conv += data[order[i]];
  }
  return conv;
}

UInt_t CztEvtHeader::conv4Byte( unsigned char *data )
{
  Int_t  order[4] = { 0, 1, 2, 3 };
  UInt_t conv = 0;
  for ( Int_t i = 0; i < 4; i++ ) {
    conv *= 256;
    conv += data[order[i]];
  }
  return conv;
}
