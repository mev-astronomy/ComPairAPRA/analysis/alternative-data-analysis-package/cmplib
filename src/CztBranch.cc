#include "CztBranch.h"
#include "CztLib.h"
#include "CztParams.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"
#include "DstBranch.h"
#include "HitBranch.h"

#include "TFile.h"

#define COMPAIR

ClassImp( CztBranch )

CztBranch::CztBranch( void ) : DetBranch( CmpLib::D_CZT, 0, CztLib::N_MHIT )
{
  fArray  = new TClonesArray( "CztCrate", CztLib::N_ASIC );
  Clear();
}

CztBranch::~CztBranch()
{
  delete fArray;
}

Int_t CztBranch::GetHitAsicID( UInt_t hit ) const
{
  if ( hit >= CztLib::N_ASIC || hit >= fNhitsAsic ) return -1;
  return fHitAsicID[hit];
}

CztCrate *CztBranch::GetCrate( Int_t aid ) const
{
  Int_t hid = GetHitID( aid );
  if ( hid < 0 ) return (CztCrate *)NULL;
  
  return (CztCrate *)fArray->At( hid );
}

Int_t CztBranch::GetHitID( Int_t aid ) const
{
  if ( aid < 0 || aid >= CztLib::N_ASIC ) return -1;
  for ( UInt_t i = 0; i < fNhitsAsic; i++ ) {
    if ( fHitAsicID[i] == aid ) return i;
  }
  return -1;
}
  
Int_t CztBranch::GetHitAid( Int_t ith )
{
  if ( ith < 0 || ith >= fNhits ) return -1;

  HitBranch *hit = GetNthHit( ith );
  if ( !hit ) return -1;

  return hit->GetPriID();
}

Int_t CztBranch::GetHitCid( Int_t ith )
{
  if ( ith < 0 || ith >= fNhits ) return -1;

  HitBranch *hit = GetNthHit( ith );
  if ( !hit ) return -1;

  return hit->GetSecID();
}

Int_t CztBranch::GetHitSta( Int_t ith )
{
  if ( ith < 0 || ith >= fNhits ) return -1;

  HitBranch *hit = GetNthHit( ith );
  if ( !hit ) return -1;

  return hit->GetHitSta();
}

Double_t CztBranch::GetHitEne( Int_t ith )
{
  if ( ith < 0 || ith >= fNhits ) return CztLib::fDnotValid;

  HitBranch *hit = GetNthHit( ith );
  if ( !hit ) return CztLib::fDnotValid;

  return hit->GetHitEne();
}

TVector3 CztBranch::GetHitPos( Int_t ith )
{
  if ( ith < 0 || ith >= fNhits ) return CztLib::fVnotValid;

  HitBranch *hit = GetNthHit( ith );
  if ( !hit ) return CztLib::fVnotValid;

  return hit->GetHitPos();
}

Int_t CztBranch::Process( Int_t sel )
{
  if ( !gCmpParams ) return 0;
  
  // The main method of CztBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  DetBranch::Clear();
  fArray->Clear( "C" );

  //  Double_t K = 1504.9; // [keV]
  //  Double_t eres  = 150.49;

  Double_t Cs137 = 661.7;  // [keV]
  Double_t eres  = 2.0*66.17;
  
  //  Double_t ep   = 551.0; // [keV]
  //  Double_t eres = 2.0*55.1;
  
  Bool_t flag = kFALSE;

  for ( UInt_t hit = 0; hit < fNhitsAsic; hit++ ) {
    CztCrate *crate = (CztCrate *)fArray->ConstructedAt( hit );
    if ( !crate ) return 0;
    crate->SetAsicID( fHitAsicID[hit] );
    crate->SetData  ( fEvent.GetAsic( hit ) );
    crate->Process  ();
    Int_t aid = fHitAsicID[hit];
    for ( Int_t i = 0; i < crate->GetNhits(); i++ ) {
      Int_t     cid = crate->GetCztbID( i );
      Double_t  ene = crate->GetHitEne( i );

#ifdef COMPAIR
      TVector3  pos = crate->GetHitTra( i );
#else
      TVector3  pos = crate->GetHitPos( i );
#endif

      Int_t     npad = crate->GetHitNpad( i );

      //      if ( i == 0 && ( ene > ep-eres && ene < ep+eres ) ) flag = kTRUE;
      //      if ( i == 0 && ( ene > K-eres && ene < K+eres ) ) flag = kTRUE;
      if ( i == 0 && ( ene > Cs137-eres && ene < Cs137+eres ) ) flag = kTRUE;

      // if ( npad < 4 ) continue;
      Int_t idx = gCmpParams->GetIndex( fDetID, aid, cid );
      fSta.insert( std::map<Int_t, Int_t>::value_type( idx, npad ) );
      fEne.insert( std::multimap<Double_t, Int_t>::value_type( ene, idx ) );
      fPos.insert( std::map<Int_t, TVector3>::value_type( idx, pos ) );
    }
  }

  CopyHit();

  if ( sel == CmpLib::HITEVT && flag == kFALSE ) return 0;

  return 1;
}

Int_t CztBranch::CopyHit( void )
{
  // The main method of CztBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  DstBranch *dst = ctree->GetDst();
  if ( !dst ) return 0;

  for ( it_type it = fEne.begin(); (it != fEne.end()); ++it ) {
    Int_t idx = it->second;
    HitBranch *hit = NewHit();
    if ( !hit ) continue;
    hit->SetIndex ( it->second );
    hit->SetHitSta( fSta[idx]  );
    hit->SetHitEne( 1.0e-3*it->first  ); // keV --> MeV
    hit->SetHitPos( fPos[idx]  );
  }
  
  return 1;
}

Int_t CztBranch::Fill( BasePacket *pkt )
{
  fAttrib  = pkt->GetAtrb();
  fEventID = pkt->GetEvid();
  fEpoch   = pkt->GetEpch();
  fEpochNS = pkt->GetEpns();

  fEvent.Fill( pkt );
  fNhitsAsic = fEvent.GetNhitAsic();
  for ( UInt_t i = 0; i < fNhitsAsic; i++ ) {
    fHitAsicID[i] = fEvent.GetAsicID( i );
  }
  return 1;
}

void CztBranch::Clear( Option_t *option )
{
  DetBranch::Clear();

  fAttrib  = 0;
  fEventID = 0;
  fEpoch   = 0;
  fEpochNS = 0;
  
  fNhitsAsic = 0;
  for ( Int_t i = 0; i < CztLib::N_ASIC; i++ ) fHitAsicID[i] = -1;

  fArray->Clear( "C" );
  fEvent.Clear();
}

void CztBranch::Print( Option_t *option ) const
{
}
