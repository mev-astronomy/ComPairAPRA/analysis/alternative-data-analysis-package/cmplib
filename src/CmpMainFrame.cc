#include "CmpMainFrame.h"
#include "EventDrawer.h"
#include "CmpLib.h"

#include "TApplication.h"
#include "TStyle.h"
#include "TROOT.h"
#include "TChain.h"
#include "TCanvas.h"
#include "TRootEmbeddedCanvas.h"
#include "TObjArray.h"
#include "TGButton.h"
#include "TH2I.h"
#include "TTimer.h"
#include "TGListBox.h"
#include "TGComboBox.h"
#include "TGNumberEntry.h"
#include "TGLabel.h"

#include <iostream>

ClassImp( CmpMainFrame )

CmpMainFrame::CmpMainFrame( const TGWindow *p, UInt_t w, UInt_t h ) : TGMainFrame( p, w, h )
{
  gROOT->cd();

  //  gStyle->SetPalette( kRainBow );
  //  gStyle->SetPalette( kDarkBodyRadiator );
  //  gStyle->SetPalette( kLightTemperature );
  //  gStyle->SetPalette( kBird );
  gStyle->SetCanvasColor   ( kBlack );
  gStyle->SetTitleTextColor( kWhite );
  gStyle->SetPadLeftMargin  ( 0.04 );
  gStyle->SetPadRightMargin ( 0.04 );
  gStyle->SetPadTopMargin   ( 0.06 );
  gStyle->SetPadBottomMargin( 0.02 );
  
  fCarray = new TObjArray;

  TGVerticalFrame *vframe = new TGVerticalFrame( this, 1200, 650 );

  TGHorizontalFrame *hframe = new TGHorizontalFrame( vframe, 1200, 30 );

  // -- Event Number 
  TGHorizontalFrame *hevnum
    = new TGHorizontalFrame( hframe, 220, 30, kFixedWidth | kFixedHeight | kSunkenFrame );
  
  TGLabel *fLabel = new TGLabel( hevnum, new TGString( "Event Number" ) );
  hevnum->AddFrame( fLabel, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 3, 3, 3, 3 ) );

  fEntry = new TGNumberEntry( hevnum, 0, 7, -1, TGNumberEntry::kNESInteger, TGNumberEntry::kNEANonNegative, TGNumberEntry::kNELLimitMax, 0, 1 );
  hevnum->AddFrame( fEntry, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 3, 3, 3, 3 ) );

  TGTextButton *jump = new TGTextButton( hevnum, "&Jump" );
  jump->Connect( "Clicked()", "CmpMainFrame", this, "Jump()" );
  hevnum->AddFrame( jump, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 1, 1, 1, 1 ) );

  hframe->AddFrame( hevnum, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 0, 0, 0, 0 ) );
  // Event Number --
  
  // -- Energy Threshold
  TGHorizontalFrame *hthre
    = new TGHorizontalFrame( hframe, 220, 30, kFixedWidth | kFixedHeight | kSunkenFrame );

  TGLabel *fThre = new TGLabel( hthre, new TGString( "Edep Total >= " ) );
  hthre->AddFrame( fThre, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 3, 3, 3, 3 ) );

  fEthre = new TGNumberEntry( hthre, 0, 7, -1, TGNumberEntry::kNESRealTwo, TGNumberEntry::kNEANonNegative, TGNumberEntry::kNELLimitMax, 0, 30.0 );
  hthre->AddFrame( fEthre, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 3, 3, 3, 3 ) );

  TGLabel *fUnit = new TGLabel( hthre, new TGString( "[MeV]" ) );
  hthre->AddFrame( fUnit, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 3, 3, 3, 3 ) );

  hframe->AddFrame( hthre, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 0, 0, 0, 0 ) );
  // Energy Threshold --
  
  // -- Trigger Condition
  TGHorizontalFrame *htrig
    = new TGHorizontalFrame( hframe, 340, 30, kFixedWidth | kFixedHeight | kSunkenFrame );
  
  TGLabel *fCoin = new TGLabel( htrig, new TGString( "Trigger" ) );
  htrig->AddFrame( fCoin, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 3, 3, 3, 3 ) );

  fTsel = new TGComboBox( htrig, 1 );
  fTsel->AddEntry( "Exact", 1 );
  fTsel->AddEntry( "Require", 2 );
  fTsel->AddEntry( "Ignore", 3 );
  fTsel->Resize( 75, 20 );
  fTsel->Select( 3 );
  htrig->AddFrame( fTsel, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 2, 2, 2, 2 ) );

  fTrig[0] = new TGCheckButton( htrig, new TGHotString( "ACD" ), -1 );
  fTrig[1] = new TGCheckButton( htrig, new TGHotString( "SiT" ), -1 );
  fTrig[2] = new TGCheckButton( htrig, new TGHotString( "CZT" ), -1 );
  fTrig[3] = new TGCheckButton( htrig, new TGHotString( "CsI" ), -1 );

  for ( Int_t i = 0; i < 4; i++ ) {
    htrig->AddFrame( fTrig[i], new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 0, 0, 0, 0 ) );
    fTrig[i]->SetState( kButtonDown );
  }

  hframe->AddFrame( htrig, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 0, 0, 0, 0 ) );
  // Trigger Condition --

  // -- Mode Control
  TGHorizontalFrame *hmode
    = new TGHorizontalFrame( hframe, 140, 30, kFixedWidth | kFixedHeight | kSunkenFrame );
  
  fMode = new TGComboBox( hmode, 1 );
  fMode->AddEntry( "Cosmic Ray", 1 );
  fMode->AddEntry( "Gamma Ray ", 2 );
  fMode->Resize( 90, 20 );
  fMode->Select( 1 );
  hmode->AddFrame( fMode, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 3, 3, 3, 3 ) );

  TGTextButton *mode = new TGTextButton( hmode, "&Mode" );
  mode->Connect( "Clicked()", "CmpMainFrame", this, "Mode()" );
  hmode->AddFrame( mode, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 2, 2, 2, 2 ) );
  
  hframe->AddFrame( hmode, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 0, 0, 0, 0 ) );
  // Mode Control --
  
  // -- Control Buttons
  TGHorizontalFrame *hcntl
    = new TGHorizontalFrame( hframe, 280, 30, kFixedWidth | kFixedHeight | kSunkenFrame );
  
  TGTextButton *prev = new TGTextButton( hcntl, "&Prev" );
  prev->Connect( "Clicked()", "CmpMainFrame", this, "Prev()" );
  hcntl->AddFrame( prev, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY | kLHintsExpandX, 2, 2, 2, 2 ) );

  TGTextButton *next = new TGTextButton( hcntl, "&Next" );
  next->Connect( "Clicked()", "CmpMainFrame", this, "Next()" );
  hcntl->AddFrame( next, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY | kLHintsExpandX, 2, 2, 2, 2 ) );

  TGTextButton *start = new TGTextButton( hcntl, "&Start" );
  start->Connect( "Clicked()", "CmpMainFrame", this, "Start()" );
  hcntl->AddFrame( start, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY | kLHintsExpandX, 2, 2, 2, 2 ) );

  TGTextButton *stop = new TGTextButton( hcntl, "&Stop" );
  stop->Connect( "Clicked()", "CmpMainFrame", this, "Stop()" );
  hcntl->AddFrame( stop, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY | kLHintsExpandX, 2, 2, 2, 2 ) );

  TGTextButton *exit = new TGTextButton( hcntl, "&Exit", "gApplication->Terminate()" );
  hcntl->AddFrame( exit, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY | kLHintsExpandX, 2, 2, 2, 2 ) );

  hframe->AddFrame( hcntl, new TGLayoutHints( kLHintsCenterX | kLHintsCenterY, 0, 0, 0, 0 ) );
  // Control Buttons --
  
  TGHorizontalFrame *evview = new TGHorizontalFrame( vframe, 1200, 600, kSunkenFrame );
  TRootEmbeddedCanvas *fXZcan = new TRootEmbeddedCanvas( "Ecanvas_xz", evview, 600, 600 );
  fCarray->Add( fXZcan );
  evview->AddFrame( fXZcan, new TGLayoutHints( 0, 0, 0, 0 ) );
  TRootEmbeddedCanvas *fYZcan = new TRootEmbeddedCanvas( "Ecanvas_yz", evview, 600, 600 );
  fCarray->Add( fYZcan );
  evview->AddFrame( fYZcan, new TGLayoutHints( 0, 0, 0, 0 ) );

  vframe->AddFrame( hframe, new TGLayoutHints( kLHintsCenterX, 0, 0, 0, 0 ) );

  vframe->AddFrame( evview, new TGLayoutHints( kLHintsCenterX, 0, 0, 0, 0 ) );
  AddFrame( vframe, new TGLayoutHints( kLHintsCenterX, 2, 2, 2, 2 ) );

  fTimer = new TTimer();
  fTimer->Connect( "Timeout()", "CmpMainFrame", this, "AutoRefresh()" );

  fDrawer = new EventDrawer();
  DrawFrame();

  CheckOption();

  SetWindowName( "ComPair Event Display" );
  MapSubwindows();
  Resize( GetDefaultSize() );
  MapWindow();
}

CmpMainFrame::~CmpMainFrame()
{
}

void CmpMainFrame::CheckOption()
{
  int argc = gApplication->Argc();
  char **argv = gApplication->Argv();

  if ( argc < 2 ) {
    std::cout << "Usage: ./cdisp [DST File]" << std::endl;
    gApplication->Terminate();
  }

  Int_t i = 0;
  for ( int iarg = 1; iarg < argc; iarg++ ) {
    if ( fDrawer->AddFile( argv[iarg] ) ) {
      std::cout << Form( "[%02d]: File %s open", i++, argv[iarg] ) << std::endl;
    }
    else {
      std::cout << "No argument" << std::endl;
    }
  }

  Int_t max = fDrawer->InitTree() - 1;
  fEntry->SetLimits( TGNumberEntry::kNELLimitMax, 0, (Double_t)max );
}

void CmpMainFrame::Mode()
{
  DrawEvent();
}

void CmpMainFrame::Jump()
{
  UInt_t fEvtNum = fEntry->GetIntNumber();
  fDrawer->JumpEntry( fEvtNum );
  DrawEvent();
}

void CmpMainFrame::Prev()
{
  Int_t fTrigSels = fTsel->GetSelected();
  
  Int_t fTrigMode = 0;

  for ( Int_t i = 0; i < 4; i++ ) {
    if ( fTrig[i]->GetState() == kButtonDown ) fTrigMode |= BIT( i );
  }

  Double_t fEneThre = fEthre->GetNumber();
  
  fDrawer->GetPrev( fTrigSels, fTrigMode, fEneThre );
  DrawEvent();
}

void CmpMainFrame::Next()
{
  Int_t fTrigSels = fTsel->GetSelected();
  
  Int_t fTrigMode = 0;
  for ( Int_t i = 0; i < 4; i++ ) {
    if ( fTrig[i]->GetState() == kButtonDown ) fTrigMode |= BIT( i );
  }

  Double_t fEneThre = fEthre->GetNumber();
  
  fDrawer->GetNext( fTrigSels, fTrigMode, fEneThre );
  DrawEvent();
}

void CmpMainFrame::Start()
{
  std::cout << "Start Auto mode" << std::endl;
  Next();
  //  fTimer->Start( 10000, kFALSE );
  fTimer->Start( 2000, kFALSE );
}

void CmpMainFrame::Stop()
{
  fTimer->Stop();
  std::cout << "Stop Auto mode" << std::endl;
}

void CmpMainFrame::AutoRefresh()
{
  Next();
}

void CmpMainFrame::DrawFrame()
{
  for ( Int_t coord = 0; coord < CmpLib::N_COORD; coord++ ) {
    TCanvas *fCanvas = ((TRootEmbeddedCanvas *)fCarray->At( coord ))->GetCanvas();
    fCanvas->cd();
    fCanvas->Clear();
    fDrawer->DrawFrame( coord );
    fCanvas->Update();
  }
}

void CmpMainFrame::DrawEvent()
{
  Int_t id = fMode->GetSelected();

  for ( Int_t coord = 0; coord < CmpLib::N_COORD; coord++ ) {
    TCanvas *fCanvas = ((TRootEmbeddedCanvas *)fCarray->At( coord ))->GetCanvas();
    fCanvas->cd();
    fCanvas->Clear();
    fEntry->SetNumber( fDrawer->GetEventID() );
    fDrawer->DrawEvent( coord, id );
    fCanvas->Update();
  }
}
