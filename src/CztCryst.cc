#include "CztLib.h"
#include "CztCryst.h"
#include "CztParams.h"
#include "CztBranch.h"
#include "CmpTree.h"

#include "TFile.h"
#include "TMath.h"

ClassImp( CztCryst )

CztCryst::CztCryst()
{
  fCztbID = -1;
  Clear();
}

CztCryst::CztCryst( Int_t id )
{
  fCztbID = id;
  Clear();
}

CztCryst::~CztCryst()
{
}

void CztCryst::SetCztbID( Int_t id )
{
  if ( id < 0 || id >= CztLib::N_CZTB ) return;
  fCztbID = id;
}

Double_t CztCryst::GetEnergy( Int_t it, Int_t ip ) const
{
  if ( it < 0 || it >= CztLib::N_TYPE ) return 0;
  if ( ip < 0 || ip >= CztLib::N_POLA ) return 0;
  return fEnergy[it][ip];
}

Double_t CztCryst::GetTiming( Int_t it, Int_t ip ) const
{
  if ( it < 0 || it >= CztLib::N_TYPE ) return 0;
  if ( ip < 0 || ip >= CztLib::N_POLA ) return 0;
  return fTiming[it][ip];
}

void CztCryst::SetEnergy( Int_t ityp, Int_t ipol, Double_t ene )
{
  if ( ityp < 0 || ityp >= CztLib::N_TYPE ) return;
  if ( ipol < 0 || ipol >= CztLib::N_POLA ) return;
  fEnergy[ityp][ipol] = ene;
}

void CztCryst::SetTiming( Int_t ityp, Int_t ipol, Double_t tim )
{
  if ( ityp < 0 || ityp >= CztLib::N_TYPE ) return;
  if ( ipol < 0 || ipol >= CztLib::N_POLA ) return;
  fTiming[ityp][ipol] = tim;
}

void CztCryst::Process( Int_t id )
{
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return;

  CztBranch *czt = ctree->GetCzt();
  if ( !czt ) return;
  
  //  Double_t time = ctree->GetUtcTime();
  Double_t time = czt->GetTime();
  Int_t    ip   = gCztParams->GetPoint( time );
  if ( ip < 0 ) return;

  Double_t x = GetHitX( id, ip );
  Double_t y = GetHitY( id, ip );    

  Double_t fCathT = fTiming[CztLib::CATHODE][CztLib::POSI];
  Double_t fAnodT = fTiming[CztLib::ANODE  ][CztLib::POSI];
  Double_t fCathE = fEnergy[CztLib::CATHODE][CztLib::POSI];
  Double_t fAnodE = fEnergy[CztLib::ANODE  ][CztLib::POSI];

  Double_t hittime = ( fAnodT+fCathT )/2.0;

  Double_t ene = ( fAnodE > 0.0 ) ? fCathE/fAnodE : 0.0;
  Double_t tim = fCathT-fAnodT;
  
  Double_t depcorr = gCztParams->GetDepCorr( id, fCztbID, ene, tim );
  Double_t mapcorr = gCztParams->GetMapCorr( id, fCztbID, x, y, ene, tim );

  fHitEne = depcorr*mapcorr*fEnergy[CztLib::ANODE][CztLib::POSI];
  fHitTim = hittime;

  fHitPos = GetHitPos( id, ip );

  return;
}

Double_t CztCryst::GetHitX( Int_t id, Int_t ip )
{
  Double_t fAnodE = fEnergy[CztLib::ANODE][CztLib::POSI];
  if ( fAnodE == 0.0 ) return CztLib::fDnotValid;
  
  Double_t fPadP0 = fEnergy[CztLib::PAD0 ][CztLib::POSI]/fAnodE;
  Double_t fPadP1 = fEnergy[CztLib::PAD1 ][CztLib::POSI]/fAnodE;
  Double_t fPadN0 = fEnergy[CztLib::PAD0 ][CztLib::NEGA]/fAnodE;
  Double_t fPadN1 = fEnergy[CztLib::PAD1 ][CztLib::NEGA]/fAnodE;

  Int_t    idx_n0 = gCztParams->GetIndex( fCztbID, CztLib::PAD0, CztLib::NEGA );
  Int_t    idx_n1 = gCztParams->GetIndex( fCztbID, CztLib::PAD1, CztLib::NEGA );

  //  if ( TMath::Abs(fPadN0+fPadP0) < 0.05 ) return CztLib::fDnotValid;
  //  if ( TMath::Abs(fPadN1+fPadP1) < 0.05 ) return CztLib::fDnotValid;
  if ( TMath::Abs(fPadN0+fPadP0) < 0.05 ) return 0.0;
  if ( TMath::Abs(fPadN1+fPadP1) < 0.05 ) return 0.0;

  Double_t fPadS0 = fPadN0;
  Double_t fPadS1 = fPadN1;

  //  Double_t x = ( fPadS0 > 0.0 && fPadS1 > 0.0 )
  //    ? ( TMath::Log( fPadS1 / fPadS0 ) ) : CztLib::fDnotValid;
  Double_t x = ( fPadS0 > 0.0 && fPadS1 > 0.0 )
    ? ( TMath::Log( fPadS1 / fPadS0 ) ) : 0.0;

  //  if ( fPadN0*fAnodE < gCztParams->GetThre( id, idx_n0, ip ) &&
  //       fPadN1*fAnodE < gCztParams->GetThre( id, idx_n1, ip ) ) x = CztLib::fDnotValid;
  if ( fPadN0*fAnodE < gCztParams->GetThre( id, idx_n0, ip ) &&
       fPadN1*fAnodE < gCztParams->GetThre( id, idx_n1, ip ) ) x = 0.0;
  
  return x;
}

Double_t CztCryst::GetHitY( Int_t id, Int_t ip )
{
  Double_t fAnodE = fEnergy[CztLib::ANODE][CztLib::POSI];
  if ( fAnodE == 0.0 ) return CztLib::fDnotValid;

  Double_t fPadP2 = fEnergy[CztLib::PAD2][CztLib::POSI]/fAnodE;
  Double_t fPadP3 = fEnergy[CztLib::PAD3][CztLib::POSI]/fAnodE;
  Double_t fPadN2 = fEnergy[CztLib::PAD2][CztLib::NEGA]/fAnodE;
  Double_t fPadN3 = fEnergy[CztLib::PAD3][CztLib::NEGA]/fAnodE;

  Int_t    idx_n2 = gCztParams->GetIndex( fCztbID, CztLib::PAD2, CztLib::NEGA );
  Int_t    idx_n3 = gCztParams->GetIndex( fCztbID, CztLib::PAD3, CztLib::NEGA );

  //  if ( TMath::Abs(fPadN2+fPadP2) < 0.05 ) return CztLib::fDnotValid;
  //  if ( TMath::Abs(fPadN3+fPadP3) < 0.05 ) return CztLib::fDnotValid;
  if ( TMath::Abs(fPadN2+fPadP2) < 0.05 ) return 0.0;
  if ( TMath::Abs(fPadN3+fPadP3) < 0.05 ) return 0.0;

  Double_t fPadS2 = fPadN2;
  Double_t fPadS3 = fPadN3;

  //  Double_t y = ( fPadS2 > 0.0 && fPadS3 > 0.0 )
  //    ? ( TMath::Log( fPadS3 / fPadS2 ) ) : CztLib::fDnotValid;
  Double_t y = ( fPadS2 > 0.0 && fPadS3 > 0.0 )
    ? ( TMath::Log( fPadS3 / fPadS2 ) ) : 0.0;

  //  if ( fPadN2*fAnodE < gCztParams->GetThre( id, idx_n2, ip ) &&
  //       fPadN3*fAnodE < gCztParams->GetThre( id, idx_n3, ip ) ) y = CztLib::fDnotValid;
  if ( fPadN2*fAnodE < gCztParams->GetThre( id, idx_n2, ip ) &&
       fPadN3*fAnodE < gCztParams->GetThre( id, idx_n3, ip ) ) y = 0.0;
  
  return y;
}

Double_t CztCryst::GetHitZ( Int_t id, Int_t ip )
{
  Double_t fAnodE = fEnergy[CztLib::ANODE  ][CztLib::POSI];
  Double_t fCathE = fEnergy[CztLib::CATHODE][CztLib::POSI];
  Double_t fCathT = fTiming[CztLib::CATHODE][CztLib::POSI];
  Double_t fAnodT = fTiming[CztLib::ANODE  ][CztLib::POSI];

  Double_t ene = ( fAnodE > 0.0 ) ? fCathE/fAnodE : 0.0;
  Double_t tim = fCathT-fAnodT;

  return gCztParams->GetZpos( id, fCztbID, ene, tim );
}

TVector3 CztCryst::GetHitPos( Int_t id, Int_t ip )
{
  Double_t fAnodE = fEnergy[CztLib::ANODE  ][CztLib::POSI];
  Double_t fCathE = fEnergy[CztLib::CATHODE][CztLib::POSI];

  Double_t cene = ( fAnodE > 0.0 ) ? fCathE/fAnodE : 0.0;

  Double_t fPadN0 = fEnergy[CztLib::PAD0][CztLib::NEGA]/fAnodE;
  Double_t fPadN1 = fEnergy[CztLib::PAD1][CztLib::NEGA]/fAnodE;
  Double_t fPadN2 = fEnergy[CztLib::PAD2][CztLib::NEGA]/fAnodE;
  Double_t fPadN3 = fEnergy[CztLib::PAD3][CztLib::NEGA]/fAnodE;

  Double_t fPadP0 = fEnergy[CztLib::PAD0][CztLib::POSI]/fAnodE;
  Double_t fPadP1 = fEnergy[CztLib::PAD1][CztLib::POSI]/fAnodE;
  Double_t fPadP2 = fEnergy[CztLib::PAD2][CztLib::POSI]/fAnodE;
  Double_t fPadP3 = fEnergy[CztLib::PAD3][CztLib::POSI]/fAnodE;

  Double_t fPad0 = ( TMath::Abs(fPadN0+fPadP0) < 0.05 ) ? 0.0 : fPadN0;
  Double_t fPad1 = ( TMath::Abs(fPadN1+fPadP1) < 0.05 ) ? 0.0 : fPadN1;
  Double_t fPad2 = ( TMath::Abs(fPadN2+fPadP2) < 0.05 ) ? 0.0 : fPadN2;
  Double_t fPad3 = ( TMath::Abs(fPadN3+fPadP3) < 0.05 ) ? 0.0 : fPadN3;
  
  Int_t idx_n0 = gCztParams->GetIndex( fCztbID, CztLib::PAD0, CztLib::NEGA );
  Int_t idx_n1 = gCztParams->GetIndex( fCztbID, CztLib::PAD1, CztLib::NEGA );
  Int_t idx_n2 = gCztParams->GetIndex( fCztbID, CztLib::PAD2, CztLib::NEGA );
  Int_t idx_n3 = gCztParams->GetIndex( fCztbID, CztLib::PAD3, CztLib::NEGA );

  if ( fPadN0*fAnodE < gCztParams->GetThre( id, idx_n0, ip ) ) fPad0 = 0.0;
  if ( fPadN1*fAnodE < gCztParams->GetThre( id, idx_n1, ip ) ) fPad1 = 0.0;
  if ( fPadN2*fAnodE < gCztParams->GetThre( id, idx_n2, ip ) ) fPad2 = 0.0;
  if ( fPadN3*fAnodE < gCztParams->GetThre( id, idx_n3, ip ) ) fPad3 = 0.0;

  if ( fPad0 > 0.0 && fPad1 > 0.0 && fPad2 > 0.0 && fPad3 > 0.0 ) {
    Double_t x = TMath::Log(fPad1/fPad0)/5.0;
    Double_t y = TMath::Log(fPad3/fPad2)/5.0;
    if ( x >=  1.0 ) x =  1.0;
    if ( y >=  1.0 ) y =  1.0;
    if ( x <= -1.0 ) x = -1.0;
    if ( y <= -1.0 ) y = -1.0;
    
    x = gCztParams->GetConforX( id, fCztbID, x );
    y = gCztParams->GetConforY( id, fCztbID, y );

    Double_t m = gCztParams->GetConforM( id, fCztbID, x, y, cene );
    x *= m;
    y *= m;
    if ( x >=  1.0 ) x =  1.0;
    if ( y >=  1.0 ) y =  1.0;
    if ( x <= -1.0 ) x = -1.0;
    if ( y <= -1.0 ) y = -1.0;
    

    Double_t r = gCztParams->GetConforR( id, fCztbID, x, y, cene );
    x *= r;
    y *= r;
    if ( x >=  1.0 ) x =  1.0;
    if ( y >=  1.0 ) y =  1.0;
    if ( x <= -1.0 ) x = -1.0;
    if ( y <= -1.0 ) y = -1.0;

    x *= 0.5*gCztParams->GetDimens( id, fCztbID ).X();
    y *= 0.5*gCztParams->GetDimens( id, fCztbID ).Y();
    Double_t z = GetHitZ( id, ip );

    return TVector3( x, y, z );
  }
  else {
    Double_t x = 0.0;
    Double_t y = 0.0;
    Double_t z = GetHitZ( id, ip );

    return TVector3( x, y, z );
  }
}

Int_t CztCryst::GetNhitPad( void ) const
{
  Double_t fAnodE = fEnergy[CztLib::ANODE][CztLib::POSI];
  if ( fAnodE == 0.0 ) return -1;
  
  Double_t fPadP0 = fEnergy[CztLib::PAD0 ][CztLib::POSI]/fAnodE;
  Double_t fPadP1 = fEnergy[CztLib::PAD1 ][CztLib::POSI]/fAnodE;
  Double_t fPadP2 = fEnergy[CztLib::PAD2 ][CztLib::POSI]/fAnodE;
  Double_t fPadP3 = fEnergy[CztLib::PAD3 ][CztLib::POSI]/fAnodE;
  Double_t fPadN0 = fEnergy[CztLib::PAD0 ][CztLib::NEGA]/fAnodE;
  Double_t fPadN1 = fEnergy[CztLib::PAD1 ][CztLib::NEGA]/fAnodE;
  Double_t fPadN2 = fEnergy[CztLib::PAD2 ][CztLib::NEGA]/fAnodE;
  Double_t fPadN3 = fEnergy[CztLib::PAD3 ][CztLib::NEGA]/fAnodE;

  Bool_t flags[4];
  flags[0] = (TMath::Abs(fPadN0+fPadP0)<0.05) ? kFALSE : kTRUE;
  flags[1] = (TMath::Abs(fPadN1+fPadP1)<0.05) ? kFALSE : kTRUE;
  flags[2] = (TMath::Abs(fPadN2+fPadP2)<0.05) ? kFALSE : kTRUE;
  flags[3] = (TMath::Abs(fPadN3+fPadP3)<0.05) ? kFALSE : kTRUE;
  
  Int_t npad = 0;
  for ( Int_t i = 0; i < 4; i++ ) {
    if ( flags[i] ) npad++;
  }

  return npad;
}

void CztCryst::Clear( Option_t *option )
{
  for ( Int_t i = 0; i < CztLib::N_TYPE; i++ ) {
    for ( Int_t ip = 0; ip < CztLib::N_POLA; ip++ ) {
      fEnergy[i][ip] = 0;
      fTiming[i][ip] = 0;
    }
  }
  fHitEne = CztLib::fDnotValid;
  fHitTim = CztLib::fDnotValid;
  fHitPos = CztLib::fVnotValid;
}

void CztCryst::Print( Option_t *option ) const
{
}
