#include "HitBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"

#include "TFile.h"

ClassImp( HitBranch )

HitBranch::HitBranch( void )
{
  Clear();
}

HitBranch::~HitBranch()
{
}

Int_t HitBranch::GetDetID( void ) const
{
  if ( !CmpParams ::GetPtr() && !CmpParams::ReadFile() ) return -1;
  return gCmpParams->GetDetID( fIndex );
}

Int_t HitBranch::GetLyrID( void ) const
{
  if ( !CmpParams ::GetPtr() && !CmpParams::ReadFile() ) return -1;
  return gCmpParams->GetLyrID( fIndex );
}

Int_t HitBranch::GetPriID( void ) const
{
  if ( !CmpParams ::GetPtr() && !CmpParams::ReadFile() ) return -1;
  return gCmpParams->GetPriID( fIndex );
}

Int_t HitBranch::GetSecID( void ) const
{
  if ( !CmpParams ::GetPtr() && !CmpParams::ReadFile() ) return -1;
  return gCmpParams->GetSecID( fIndex );
}

/*
TVector3 HitBranch::GetHitTra( void )
{
  return Transform( fHitPos );
}

TVector3 HitBranch::Transform( TVector3 pos )
{
  pos.RotateZ( angle );
  return pos + shift;
}
*/

Int_t HitBranch::Process( Int_t sel )
{
  // The main method of HitBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;
  
  Clear();

  return 1;
}

void HitBranch::CopyHit(const HitBranch* hit)
{
  fIndex  = hit->fIndex;
  fHitSta = hit->fHitSta;
  fHitEne = hit->fHitEne;
  fEneErr = hit->fEneErr;
  fHitTim = hit->fHitTim;
  fHitPos = hit->fHitPos;
  fPosErr = hit->fPosErr;
}

const HitBranch& HitBranch::operator= (const HitBranch& hit)
{
  fIndex  = hit.fIndex;
  fHitSta = hit.fHitSta;
  fHitEne = hit.fHitEne;
  fEneErr = hit.fEneErr;
  fHitTim = hit.fHitTim;
  fHitPos = hit.fHitPos;
  fPosErr = hit.fPosErr;
  return *this;
}

void HitBranch::Clear( Option_t *option )
{
  fIndex  = -1;
  fHitSta = -1;
  fHitEne = 0.0;
  fEneErr = 0.0;
  fHitTim = 0.0;
  fHitPos = CmpLib::fVnotValid;
  fPosErr = TVector3( 0, 0, 0 );
}

void HitBranch::Print( Option_t *option ) const
{
}
