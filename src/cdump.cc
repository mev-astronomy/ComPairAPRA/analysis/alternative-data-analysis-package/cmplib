#include "CmpLib.h"
#include "CztPacket.h"

#include "TString.h"

#include <iostream>
#include <fstream>
#include <string>

int main( int argc, char *argv[] )
{
  // set default parameters
  std::string ofn = "czt_dump.dat";

  CztPacket *pkt = new CztPacket();

  std::ofstream fout;
  fout.open( ofn, std::ios::binary|std::ios::out );
  if ( !fout.is_open() ) return -1;
  
  std::ifstream fin;
  fin.open( argv[1], std::ios::binary|std::ios::in );
  if ( !fin.is_open() ) return -1;

  while( 1 ) {
    Int_t ret = pkt->ReadPacket( &fin, kTRUE );
    if ( ret <= 0 ) break;
    fout.write( reinterpret_cast<const char*>(pkt->GetBuffer()), ret );
  }

  UShort_t fTMP;
  fin.read( (char *)&fTMP, 2 );

  std::cout << Form( "fTMP = 0x%04X", fTMP ) << std::endl;
  return 0;
}
