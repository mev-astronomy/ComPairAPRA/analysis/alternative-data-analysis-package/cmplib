#include "DetParams.h"
#include "CmpLib.h"

#include "TFile.h"
#include "TMath.h"

ClassImp( DetParams )

DetParams::DetParams()
{
  fPeriod = 60.0;
  fNpoint = 0;
  fTbegun = CmpLib::fDnotValid;
  fTended = CmpLib::fDnotValid;
  fTpoint = 0;

  fMapMode = DetParams::EXACT;
}

DetParams::~DetParams()
{
  if ( fTpoint ) delete [] fTpoint;
}

TObject *DetParams::ReadFile( const char *fname, const char *cname )
{
  TFile *fsave = gFile;

  TString sfn = fname, dir;
  Int_t i1 = sfn.Index( ".root" );
  Int_t i2 = sfn.Index( "/", i1 );
  if ( i1 > 0 && i2 > i1 ) {
    dir = sfn( i2+1, sfn.Length() );
    sfn = sfn( 0, i2 );
    if ( !dir.EndsWith( "/" ) ) dir += "/";
  }

  TFile f( sfn );
  if ( !f.IsOpen() ) return 0;

  TObject *obj = f.Get( dir+cname );
  f.Close();

  if ( fsave ) fsave->cd();

  return obj;
}

Double_t DetParams::GetTpoint( Int_t p ) const
{
  if ( p < 0 || p >= fNpoint ) return CmpLib::fDnotValid;
  return fTpoint[p];
}

Int_t DetParams::GetPoint( Double_t t ) const
{
  if ( t < fTbegun || t > fTended ) return -1;
  Int_t guessp = TMath::FloorNint( fNpoint*(t-fTbegun)/(fTended-fTbegun) );
  if ( fTpoint[guessp] > t ) {
    do {
      if ( guessp ) guessp--;
    } while ( fTpoint[guessp] > t && guessp > 0 );
    return guessp;
  }
  else {
    do {
      guessp++;
    } while ( fTpoint[guessp] <= t && guessp < fNpoint );
    return guessp - 1;
  }
}

void DetParams::Init( Double_t b, Double_t e, Int_t n, Double_t *t )
{
  fTbegun = b;
  fTended = e;
  fNpoint = n;
  fTpoint = new Double_t[fNpoint];
  for ( Int_t ip = 0; ip < fNpoint; ip++ ) {
    fTpoint[ip] = *(t+ip);
  }
}

Double_t DetParams::GetCorr1D( TH1D *hist, Double_t x ) const
{
  if ( !hist ) return CmpLib::fDnotValid;

  Int_t ibin = hist->FindBin( x );
  if ( ibin == 0 || ibin == hist->GetNbinsX()+1 ) return CmpLib::fDnotValid;
  
  if ( fMapMode == DetParams::EXACT ) {
    Double_t val = hist->GetBinContent( ibin );
    return val;
  }
  else {
    Double_t w = hist->GetXaxis()->GetBinWidth ( ibin );
    Double_t c = hist->GetXaxis()->GetBinCenter( ibin );
    if ( w == 0.0 ) return CmpLib::fDnotValid;

    Double_t v0 = hist->GetBinContent( ibin );
    
    Double_t corr = 0.0;
    if ( x > c ) {
      Double_t vx = hist->GetBinContent( hist->FindBin( x+w ) );
      corr = ( vx == 0 ) ? 0.0 : (x-c)*(vx-v0)/w;
    }
    else if ( x < c ) {
      Double_t vx = hist->GetBinContent( hist->FindBin( x-w ) );
      corr = ( vx == 0 ) ? 0.0 : (x-c)*(v0-vx)/w;
    }
    else {
      corr = 0.0;
    }

    return v0+corr;
  }
  
  return CmpLib::fDnotValid;
}

Double_t DetParams::GetCorr2D( TH2D *hist, Double_t x, Double_t y ) const
{
  if ( !hist ) return CmpLib::fDnotValid;

  Int_t ibin = hist->FindBin( x, y );
  Int_t xbin, ybin, zbin;
  hist->GetBinXYZ( ibin, xbin, ybin, zbin );
  if ( xbin == 0 || xbin == hist->GetNbinsX()+1 ) return CmpLib::fDnotValid;
  if ( ybin == 0 || ybin == hist->GetNbinsY()+1 ) return CmpLib::fDnotValid;
  
  if ( fMapMode == DetParams::EXACT ) {
    Double_t val = hist->GetBinContent( ibin );
    return val;
  }
  else {
    Double_t wx = hist->GetXaxis()->GetBinWidth( xbin );
    Double_t wy = hist->GetYaxis()->GetBinWidth( ybin );
    if ( wx == 0.0 || wy == 0.0 ) return CmpLib::fDnotValid;

    Double_t cx = hist->GetXaxis()->GetBinCenter( xbin );
    Double_t cy = hist->GetYaxis()->GetBinCenter( ybin );

    Double_t v0 = hist->GetBinContent( ibin );

    if ( v0 == 0.0 ) return CmpLib::fDnotValid;
    
    Double_t corrx = 0.0;
    if ( x > cx ) {
      Double_t vx = hist->GetBinContent( hist->FindBin( x+wx, y ) );
      corrx = ( vx == 0 ) ? 0.0 : (x-cx)*(vx-v0)/wx;
    }
    else if ( x < cx ) {
      Double_t vx = hist->GetBinContent( hist->FindBin( x-wx, y ) );
      corrx = ( vx == 0 ) ? 0.0 : (x-cx)*(v0-vx)/wx;
    }
    else {
      corrx = 0.0;
    }

    Double_t corry = 0.0;
    if ( y > cy ) {
      Double_t vy = hist->GetBinContent( hist->FindBin( x, y+wy ) );
      corry = ( vy == 0 ) ? 0.0 : (y-cy)*(vy-v0)/wy;
    }
    else if ( y < cy ) {
      Double_t vy = hist->GetBinContent( hist->FindBin( x, y-wy ) );
      corry = ( vy == 0 ) ? 0.0 : (y-cy)*(v0-vy)/wy;
    }
    else {
      corry = 0.0;
    }

    Double_t corr = v0 + corrx + corry;
    return corr;
  }
  
  return CmpLib::fDnotValid;
}

Double_t DetParams::GetCorr3D( TH3D *hist, Double_t x, Double_t y, Double_t z ) const
{
  if ( !hist ) return CmpLib::fDnotValid;

  Int_t ibin = hist->FindBin( x, y, z );
  Int_t xbin, ybin, zbin;
  hist->GetBinXYZ( ibin, xbin, ybin, zbin );
  if ( xbin == 0 || xbin == hist->GetNbinsX()+1 ) return CmpLib::fDnotValid;
  if ( ybin == 0 || ybin == hist->GetNbinsY()+1 ) return CmpLib::fDnotValid;
  if ( zbin == 0 || zbin == hist->GetNbinsZ()+1 ) return CmpLib::fDnotValid;

  if ( fMapMode == DetParams::EXACT ) {
    Double_t val = hist->GetBinContent( ibin );
    return val;
  }
  else {
    Double_t wx = hist->GetXaxis()->GetBinWidth( xbin );
    Double_t wy = hist->GetYaxis()->GetBinWidth( ybin );
    Double_t wz = hist->GetZaxis()->GetBinWidth( zbin );
    if ( wx == 0.0 || wy == 0.0 || wz == 0.0 ) return 1.0;

    Double_t cx = hist->GetXaxis()->GetBinCenter( xbin );
    Double_t cy = hist->GetYaxis()->GetBinCenter( ybin );
    Double_t cz = hist->GetZaxis()->GetBinCenter( zbin );

    Double_t v0 = hist->GetBinContent( ibin );

    if ( v0 == 0.0 ) return 1.0;
    
    Double_t corrx = 0.0;
    if ( x > cx ) {
      Double_t vx = hist->GetBinContent( hist->FindBin( x+wx, y, z ) );
      corrx = ( vx == 0 ) ? 0.0 : (x-cx)*(vx-v0)/wx;
    }
    else if ( x < cx ) {
      Double_t vx = hist->GetBinContent( hist->FindBin( x-wx, y, z ) );
      corrx = ( vx == 0 ) ? 0.0 : (x-cx)*(v0-vx)/wx;
    }
    else {
      corrx = 0.0;
    }

    Double_t corry = 0.0;
    if ( y > cy ) {
      Double_t vy = hist->GetBinContent( hist->FindBin( x, y+wy, z ) );
      corry = ( vy == 0 ) ? 0.0 : (y-cy)*(vy-v0)/wy;
    }
    else if ( y < cy ) {
      Double_t vy = hist->GetBinContent( hist->FindBin( x, y-wy, z ) );
      corry = ( vy == 0 ) ? 0.0 : (y-cy)*(v0-vy)/wy;
    }
    else {
      corry = 0.0;
    }

    Double_t corrz = 0.0;
    if ( z > cz ) {
      Double_t vz = hist->GetBinContent( hist->FindBin( x, y, z+wz ) );
      corrz = ( vz == 0 ) ? 0.0 : (z-cz)*(vz-v0)/wz;
    }
    else if ( z < cz ) {
      Double_t vz = hist->GetBinContent( hist->FindBin( x, y, z-wz ) );
      corrz = ( vz == 0 ) ? 0.0 : (z-cz)*(v0-vz)/wz;
    }
    else {
      corrz = 0.0;
    }

    Double_t corr = v0 + corrx + corry + corrz;
    return corr;
  }

  return CmpLib::fDnotValid;
}

TH1D *DetParams::SetCorr1D( TH1D *hsrc, TH1D *hdst, const char *hname )
{
  if ( !hsrc ) return (TH1D *)NULL;
  if ( hdst ) {
    TH1D *fPreCorr = (TH1D *)hdst->Clone( "p_hist" );
    TH1D *fNewCorr = (TH1D *)hsrc->Clone( "n_hist" );
    delete hdst;
    hdst = ( fNewCorr->GetNbinsX() > fPreCorr->GetNbinsX() )
      ? (TH1D *)fNewCorr->Clone( Form( "%s", hname ) )
      : (TH1D *)fPreCorr->Clone( Form( "%s", hname ) );
    hdst->Reset();

    for ( Int_t x = 1; x <= hdst->GetNbinsX(); x++ ) {
      Double_t cx = hdst->GetXaxis()->GetBinCenter( x );
      Double_t preval = fPreCorr->GetBinContent( fPreCorr->FindBin( cx ) );
      Double_t newval = fNewCorr->GetBinContent( fNewCorr->FindBin( cx ) );

      if ( preval && newval ) hdst->SetBinContent( x, preval*newval );
      else if ( preval )      hdst->SetBinContent( x, preval );
      else if ( newval )      hdst->SetBinContent( x, newval );
      else                    hdst->SetBinContent( x, 0.0 );
    }
  }
  else {
    hdst = (TH1D *)hsrc->Clone( Form( "%s", hname ) );
  }

  return hdst;
}

TH2D *DetParams::SetCorr2D( TH2D *hsrc, TH2D *hdst, const char *hname )
{
  if ( !hsrc ) return (TH2D *)NULL;
  if ( hdst ) {
    TH2D *fPreCorr = (TH2D *)hdst->Clone( "p_hist" );
    TH2D *fNewCorr = (TH2D *)hsrc->Clone( "n_hist" );
    delete hdst;
    hdst = ( fNewCorr->GetNbinsX() > fPreCorr->GetNbinsX() )
      ? (TH2D *)fNewCorr->Clone( Form( "%s", hname ) )
      : (TH2D *)fPreCorr->Clone( Form( "%s", hname ) );
    hdst->Reset();

    for ( Int_t x = 1; x <= hdst->GetNbinsX(); x++ ) {
      for ( Int_t y = 1; y <= hdst->GetNbinsY(); y++ ) {
	Int_t bin = hdst->GetBin( x, y );
	Double_t cx = hdst->GetXaxis()->GetBinCenter( x );
	Double_t cy = hdst->GetYaxis()->GetBinCenter( y );
	Double_t preval = fPreCorr->GetBinContent( fPreCorr->FindBin( cx, cy ) );
	Double_t newval = fNewCorr->GetBinContent( fNewCorr->FindBin( cx, cy ) );
	if ( preval && newval ) hdst->SetBinContent( bin, preval*newval );
	else if ( preval )      hdst->SetBinContent( bin, preval );
	else if ( newval )      hdst->SetBinContent( bin, newval );
	else                    hdst->SetBinContent( bin, 0.0 );
      }
    }
  }
  else {
    hdst = (TH2D *)hsrc->Clone( Form( "%s", hname ) );
  }
  
  return hdst;
}

TH3D *DetParams::SetCorr3D( TH3D *hsrc, TH3D *hdst, const char *hname )
{
  if ( !hsrc ) return (TH3D *)NULL;
  if ( hdst ) {
    TH3D *fPreCorr = (TH3D *)hdst->Clone( "p_hist" );
    TH3D *fNewCorr = (TH3D *)hsrc->Clone( "n_hist" );
    delete hdst;
    hdst = ( fNewCorr->GetNbinsX() > fPreCorr->GetNbinsX() )
      ? (TH3D *)fNewCorr->Clone( Form( "%s", hname ) )
      : (TH3D *)fPreCorr->Clone( Form( "%s", hname ) );
    hdst->Reset();

    for ( Int_t x = 1; x <= hdst->GetNbinsX(); x++ ) {
      for ( Int_t y = 1; y <= hdst->GetNbinsY(); y++ ) {
	for ( Int_t z = 1; z <= hdst->GetNbinsZ(); z++ ) {
	  Int_t bin = hdst->GetBin( x, y, z );
	  Double_t cx = hdst->GetXaxis()->GetBinCenter( x );
	  Double_t cy = hdst->GetYaxis()->GetBinCenter( y );
	  Double_t cz = hdst->GetZaxis()->GetBinCenter( z );
	  Double_t preval = fPreCorr->GetBinContent( fPreCorr->FindBin( cx, cy, cz ) );
	  Double_t newval = fNewCorr->GetBinContent( fNewCorr->FindBin( cx, cy, cz ) );
	  if ( preval && newval ) hdst->SetBinContent( bin, preval*newval );
	  else if ( preval )      hdst->SetBinContent( bin, preval );
	  else if ( newval )      hdst->SetBinContent( bin, newval );
	  else                    hdst->SetBinContent( bin, 0.0 );
	}
      }
    }
  }
  else {
    hdst = (TH3D *)hsrc->Clone( Form( "%s", hname ) );
  }
  
  return hdst;
}

TH1D *DetParams::SetHist1D( TH1D *hsrc, TH1D *hdst, const char *hname )
{
  if ( !hsrc ) return (TH1D *)NULL;
  if ( hdst ) {
    TH1D *fPreCorr = (TH1D *)hdst->Clone( "p_hist" );
    TH1D *fNewCorr = (TH1D *)hsrc->Clone( "n_hist" );
    delete hdst;
    hdst = ( fNewCorr->GetNbinsX() > fPreCorr->GetNbinsX() )
      ? (TH1D *)fNewCorr->Clone( Form( "%s", hname ) )
      : (TH1D *)fPreCorr->Clone( Form( "%s", hname ) );
    hdst->Reset();

    for ( Int_t x = 1; x <= hdst->GetNbinsX(); x++ ) {
      Double_t cx = hdst->GetXaxis()->GetBinCenter( x );
      Double_t preval = fPreCorr->GetBinContent( fPreCorr->FindBin( cx ) );
      Double_t newval = fNewCorr->GetBinContent( fNewCorr->FindBin( cx ) );

      if ( preval && newval ) hdst->SetBinContent( x, preval+newval );
      else if ( preval )      hdst->SetBinContent( x, preval );
      else if ( newval )      hdst->SetBinContent( x, newval );
      else                    hdst->SetBinContent( x, 0.0 );
    }
  }
  else {
    hdst = (TH1D *)hsrc->Clone( Form( "%s", hname ) );
  }

  return hdst;
}

TH2D *DetParams::SetHist2D( TH2D *hsrc, TH2D *hdst, const char *hname )
{
  if ( !hsrc ) return (TH2D *)NULL;
  if ( hdst ) {
    TH2D *fPreCorr = (TH2D *)hdst->Clone( "p_hist" );
    TH2D *fNewCorr = (TH2D *)hsrc->Clone( "n_hist" );
    delete hdst;
    hdst = ( fNewCorr->GetNbinsX() > fPreCorr->GetNbinsX() )
      ? (TH2D *)fNewCorr->Clone( Form( "%s", hname ) )
      : (TH2D *)fPreCorr->Clone( Form( "%s", hname ) );
    hdst->Reset();

    for ( Int_t x = 1; x <= hdst->GetNbinsX(); x++ ) {
      for ( Int_t y = 1; y <= hdst->GetNbinsY(); y++ ) {
	Int_t bin = hdst->GetBin( x, y );
	Double_t cx = hdst->GetXaxis()->GetBinCenter( x );
	Double_t cy = hdst->GetYaxis()->GetBinCenter( y );
	Double_t preval = fPreCorr->GetBinContent( fPreCorr->FindBin( cx, cy ) );
	Double_t newval = fNewCorr->GetBinContent( fNewCorr->FindBin( cx, cy ) );
	if ( preval && newval ) hdst->SetBinContent( bin, preval+newval );
	else if ( preval )      hdst->SetBinContent( bin, preval );
	else if ( newval )      hdst->SetBinContent( bin, newval );
	else                    hdst->SetBinContent( bin, 0.0 );
      }
    }
  }
  else {
    hdst = (TH2D *)hsrc->Clone( Form( "%s", hname ) );
  }
  
  return hdst;
}

TH3D *DetParams::SetHist3D( TH3D *hsrc, TH3D *hdst, const char *hname )
{
  if ( !hsrc ) return (TH3D *)NULL;
  if ( hdst ) {
    TH3D *fPreCorr = (TH3D *)hdst->Clone( "p_hist" );
    TH3D *fNewCorr = (TH3D *)hsrc->Clone( "n_hist" );
    delete hdst;
    hdst = ( fNewCorr->GetNbinsX() > fPreCorr->GetNbinsX() )
      ? (TH3D *)fNewCorr->Clone( Form( "%s", hname ) )
      : (TH3D *)fPreCorr->Clone( Form( "%s", hname ) );
    hdst->Reset();

    for ( Int_t x = 1; x <= hdst->GetNbinsX(); x++ ) {
      for ( Int_t y = 1; y <= hdst->GetNbinsY(); y++ ) {
	for ( Int_t z = 1; z <= hdst->GetNbinsZ(); z++ ) {
	  Int_t bin = hdst->GetBin( x, y, z );
	  Double_t cx = hdst->GetXaxis()->GetBinCenter( x );
	  Double_t cy = hdst->GetYaxis()->GetBinCenter( y );
	  Double_t cz = hdst->GetZaxis()->GetBinCenter( z );
	  Double_t preval = fPreCorr->GetBinContent( fPreCorr->FindBin( cx, cy, cz ) );
	  Double_t newval = fNewCorr->GetBinContent( fNewCorr->FindBin( cx, cy, cz ) );
	  if ( preval && newval ) hdst->SetBinContent( bin, preval+newval );
	  else if ( preval )      hdst->SetBinContent( bin, preval );
	  else if ( newval )      hdst->SetBinContent( bin, newval );
	  else                    hdst->SetBinContent( bin, 0.0 );
	}
      }
    }
  }
  else {
    hdst = (TH3D *)hsrc->Clone( Form( "%s", hname ) );
  }
  
  return hdst;
}
