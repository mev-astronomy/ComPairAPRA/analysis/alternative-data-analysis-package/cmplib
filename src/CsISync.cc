#include "CsISync.h"

#include <iostream>

CsISync::CsISync()
{
  Clear();
  fAtrb = PKT_ATRB_CSI;
}

CsISync::~CsISync()
{
}

void CsISync::Clear( void )
{
  fNzero = 0;
  fNtack = 0;
  
  fTrgAck.clear();
  fEventID.clear();
}

UInt_t CsISync::GetEventID( void ) const
{
  return (UInt_t)std::stoul( items_arduino[2].c_str() );
}

Int_t CsISync::GetDataType( void ) const
{
  return std::stoul( items_rosspad[1].c_str() );
}

Bool_t CsISync::GetTrgAck( Int_t i ) const
{
  if ( i < 0 || i >= fTrgAck.size() ) return 0;
  return fTrgAck[i];
}

void CsISync::ProcArduino( void )
{
  a_handler.Process();
  //  std::cout << "----- a_handler.WriteArduino() Begin ------" << std::endl;
  //  a_handler.WriteArduino();
  //  std::cout << "----- a_handler.WriteArduino() End ------" << std::endl;
}

void CsISync::ProcRossPad( void )
{
  r_handler.SetGpsTime( a_handler.GetGpsTime() );
  r_handler.Process();
  //  std::cout << "----- r_handler.WriteRossPad() Begin ------" << std::endl;
  //  r_handler.WriteRossPad();
  //  std::cout << "----- r_handler.WriteRossPad() End ------" << std::endl;
}

void CsISync::ParseArduino( Int_t i )
{
  if ( i < 0 || i >= a_handler.GetSize() ) return;

  size_t pos = 0;
  std::string s;
  std::string delimiter = ",";
  std::string token;

  s = a_handler.GetString( i );
  items_arduino.clear();

  while ((pos=s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    items_arduino.push_back( token );
    s.erase(0, pos+delimiter.length());
  }
  items_arduino.push_back( s );
}

void CsISync::ParseRossPad( Int_t i )
{
  if ( i < 0 || i >= r_handler.GetSize() ) return;

  size_t pos = 0;
  std::string s;
  std::string delimiter = ",";
  std::string token;

  s = r_handler.GetString( i );
  items_rosspad.clear();

  while ((pos=s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    items_rosspad.push_back( token );
    s.erase(0, pos+delimiter.length());
  }
  items_rosspad.push_back( s );
}

UInt_t CsISync::GetIndex( void ) const
{
  return std::stoul( items_rosspad[0].c_str() );
}

ULong64_t CsISync::GetTimeArduino( void ) const
{
  return std::stoll( items_arduino[1].c_str() );
}

ULong64_t CsISync::GetTimeRossPad( void ) const
{
  return std::stoll( items_rosspad[4].c_str() );
}

Int_t CsISync::GetDataArduino( void ) const
{
  if      ( items_arduino[0] == "R"    ) return 0;
  else if ( items_arduino[0] == "E"    ) return 2;
  else if ( items_arduino[0] == "CE"   ) return 2;
  else if ( items_arduino[0] == "C"    ) return 2;
  else if ( items_arduino[0] == "CR"   ) return 3;
  else if ( items_arduino[0] == "CER"  ) return 3;
  else                                   return 4;
}

Int_t CsISync::GetDataRossPad( void ) const
{
  return std::stoul( items_rosspad[1].c_str() );
}

void CsISync::SetData( Bool_t flag )
{
  fTrgAck.push_back( flag );
  fNzero++;
  if ( flag ) fNtack++;
}

void CsISync::SetEventID( UInt_t evid )
{
  Int_t iack = 0;
  if ( fNtack > 0 && evid-fStartID == fNtack+1 ) {
    for ( Int_t ipre = 0; ipre < fNzero; ipre++ ) {
      fEventID.push_back( fTrgAck[ipre] ? (fStartID+1+iack++) : 0 );
    }
  }
  else {
    for ( Int_t ipre = 0; ipre < fNzero; ipre++ ) {
      fEventID.push_back( 0 );
    }
  }
  fEventID.push_back( evid );

  fTrgAck.clear();
  fNzero = 0;
  fNtack = 0;
  fStartID = evid;
}

void CsISync::WritePacket( std::ofstream *fout, bool speak )
{
  Int_t n_rosspad = GetNrosspad();
  Int_t m_rosspad = n_rosspad/10;
  
  for ( Int_t i = 0; i < n_rosspad; i++ ) {
    if ( i%m_rosspad == 0 )
      std::cout << Form( "i = %8d/%8d", i, n_rosspad ) << std::endl;
    
    ParseRossPad( i );
    UInt_t    fIndex       = GetIndex();
    ULong64_t time_rosspad = GetTimeRossPad();
    UInt_t    evid         = fEventID[i];
    UInt_t    fEpoch       = time_rosspad/1000000000;
    UInt_t    fEpochNS     = time_rosspad%1000000000;
	    
    if ( fAtrb == 0 ) std::cout << "fAtrb = 0" << std::endl;
    
    UShort_t fSize = r_handler.GetSize( fIndex );
    fPacket.SetData( fSize, r_handler.GetData( fIndex ) );
    fPacket.SetAtrb( fAtrb );
    fPacket.SetEvid( evid  );
    fPacket.SetEpch( fEpoch   );
    fPacket.SetEpns( fEpochNS );

    fPacket.WritePacket( fout );
  }
}
