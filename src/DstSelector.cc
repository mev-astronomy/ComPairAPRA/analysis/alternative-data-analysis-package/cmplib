//////////////////////////////////////////////////////////////////////////
//
// DstSelector
//
//  DstSelector is a main DST analysis class.
//  This class creates histograms, selects events and fill histograms.
//  For example, you can use DstSelector as follows:
//
//   TFile f("DST_FILE_NAME.root");
//   DstSelector sel;
//   ttree->Process( &sel );
//
//////////////////////////////////////////////////////////////////////////

#include "DstSelector.h"

#include "CmpTree.h"
#include "CmpParams.h"
#include "CztParams.h"
#include "CztBranch.h"
#include "CztCrate.h"
#include "CztCryst.h"
#include "DstBranch.h"
#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH1.h"
#include "TH2.h"
#include "TH3.h"
#include "TMath.h"
#include "TStopwatch.h"

#include <iostream>

using namespace std;

ClassImp( DstSelector )

DstSelector::DstSelector( void )
{
  // Constructor

  fNfill = 0;
  fNproc = 0;
  fIntv  = 100000;
  fTree  = 0;
  fCtree = new CmpTree;
  fTimer = new TStopwatch;
}

DstSelector::~DstSelector()
{
  // Destructor

  delete fCtree;
  delete fTimer;
}

void DstSelector::SlaveBegin( TTree *tree )
{
  // Begin is called everytime a loop on a TTree starts.
  // Histograms are created here

  if ( ( !gCmpParams && !CmpParams::ReadFile() ) || ( !gCztParams && !CztParams::ReadFile() ) ) {
    SetStatus( -1 );
    return;
  }

  // set tree
  fTree  = tree;
  fNfill = 0;
  fNproc = 0;
  fCtree->SetAddr( tree );

  gROOT->cd();
  gROOT->Clear();

  // initialize histograms
  fTreeSel = tree->CopyTree( "", "", 0 );
  LogHist( "hist0000", "Event reduction", 30, 0, 30, 80, 0.1, 1000, 2 );
  InitHist();
  gROOT->GetList()->Remove( fTreeSel );

  if ( !gROOT->IsBatch() || 
       fTree->GetEntries() <= 200000 ) fIntv = 5000;

  fTimer->Start();
}

void DstSelector::Terminate( void )
{
  // Terminate is called at the end of a loop on a TTree
  // fStatus is set as number of events filled (fNfill)

  Print();
  cout << endl;
  SetStatus( fNfill );
  ProcHist();
}

void DstSelector::SetMode( Int_t mode )
{
  // Set mode
  // To be implemented by users (if needed)
}

Bool_t DstSelector::Notify( void )
{
  // Notify is called at the first entry of a new tree in a chain
  // branch lists are created here

  // clear branch list
  Clear();

  // set branch list
  SetBranch();
  return kTRUE;
}

Bool_t DstSelector::Process( Long64_t entry )
{
  // Main process routine
  // Events are selected and histograms are filled here

  // show progress
  if ( fNproc > 0 && fNproc%fIntv == 0 ) {
    Print();
    if ( fNproc%(fIntv*10) == 0 ) cout << endl;
  }

  // set entry number
  fEntry = entry;
  fNproc++;

  // select events
  if ( !Select() ) return 0;

  // fill histograms
  Fill();
  fNfill++;

  return 1;
}

void DstSelector::SetBranch( void )
{
  // Set branch list to be retrived for each loop.
  // In order to maximize the process speed, 2 branch lists 
  // are used with selection levels of 1 and 2

  // pre-selection parameters (level 1 selection)
  Branch( 1, "fNumber" );
  Branch( 1, "fDst"    );

  // fiducial/quality selection parameters (level 2 selection)

  // particle ID parameters
}

void DstSelector::Branch( Int_t lv, const char *bname, const char *pname )
{
  // Find a parameter branch with name PNAME from a branch BNAME, 
  // and add it to the branch list of level LV

  if ( !fTree ) return;

  TList *lst = 0;
  if ( lv == 1 ) lst = &fBrList1;
  if ( lv == 2 ) lst = &fBrList2;
  if ( lv == 3 ) lst = &fBrList3;
  if ( !lst ) return;

  TBranch *br = fTree->GetBranch( bname );
  if ( !br ) {
    cout << "Unknown branch: " << bname;
    if ( pname && pname[0] ) cout << "." << pname << endl;
  }

  if ( pname && pname[0] ) {
    TString spn = pname;
    if ( spn.Index( "." ) >= 0 ) {
      TString sbn = pname;
      sbn.Remove( sbn.Index( "." ) );
      spn.Remove( 0, spn.Index( "." )+1 );
      TBranch *br1 = br->FindBranch( sbn );
      if ( br1 ) br = br1;
    }
    if ( br ) br = br->FindBranch( spn );
  }

  if ( br ) lst->Add( br );
  else {
    cout << "Unknown branch: " << bname;
    if ( pname && pname[0] ) cout << "." << pname << endl;
  }
}

TDirectory *DstSelector::PreProc( void )
{
  TDirectory *pre = 0;
  for ( Int_t i = 0; i < gROOT->GetListOfFiles()->GetSize(); i++ ) {
    TFile *ftmp = (TFile *)gROOT->GetListOfFiles()->At(i);
    TString fname = ftmp->GetName();
    if ( !fname.Contains( "dstsel" ) ) continue;

    for ( Int_t j = 0; j < ftmp->GetListOfKeys()->GetSize(); j++ ) {
      TString dname = ftmp->GetListOfKeys()->At(j)->GetName();
      if ( !dname.Contains( "pre" ) ) continue;
      TDirectory *tmp = (TDirectory *)ftmp->Get( dname );

      if ( !tmp || tmp->GetNkeys() == 0 ) continue;
      pre = tmp;
    }
  }
  return pre;
}

Bool_t DstSelector::Select( void )
{
  // DST selection routine

  TBranch *br;
  TIter next1( &fBrList1 );
  TIter next2( &fBrList2 );
  TIter next3( &fBrList3 );

  // retrive parameters for level 1 selection
  while ( (br = (TBranch *)next1()) ) br->GetEntry( fEntry );

  // pre-fill histograms
  PreFill();

  // level 1 selection
  if ( !DstSelect() ) return kFALSE;
  if ( !PreSelect() ) return kFALSE;

  // retrive parameters for level 2 selection
  while ( (br = (TBranch *)next2()) ) br->GetEntry( fEntry );

  // level 2 selection
  if ( !Fiducial() ) return kFALSE;
  if ( !Quality () ) return kFALSE;

  // retrive parameters for level 3 selection
  while ( (br = (TBranch *)next3()) ) br->GetEntry( fEntry );

  // level 3 selection
  ElossSel();

  return kTRUE;
}

Bool_t DstSelector::DstSelect( void )
{
  // Primary DST selection for multi track events

  fIndex = 0;
  fDst = fCtree->GetDst();

  return ( fCtree->GetNdst() == 0 ) ? kFALSE : kTRUE;
  //  return ( fCtree->GetHasic() == 0 ) ? kFALSE : kTRUE;
}

Bool_t DstSelector::PreSelect( void )
{
  // DST pre-selection

  //  Int_t isel = 0;
  //  Fill2D( "hist0000", isel++, charge );  // [0]

  return kTRUE;
}

Bool_t DstSelector::Fiducial( void )
{
  // Fiducial volume selection

  return kTRUE;
}

Bool_t DstSelector::Quality( void )
{
  // Event quality selection

  return kTRUE;
}

void DstSelector::ElossSel( void )
{
  // Charge number (Z) selection

  return;
}

void DstSelector::InitHist( void )
{
  // Initialize histograms
  // To be implemented by users
}

void DstSelector::PreFill( void )
{
  // Fill histograms before the selections
  // To be implemented by users
}

void DstSelector::Fill( void )
{
  // Fill histograms
  // To be implemented by users
}

void DstSelector::ProcHist( void )
{
  // Process histograms
  // To be implemented by users
}

void DstSelector::Clear( Option_t * )
{
  // Clear branch list

  fBrList1.Clear( "nodelete" );
  fBrList2.Clear( "nodelete" );
  fBrList3.Clear( "nodelete" );
}

void DstSelector::Fill1D( const char *name, Double_t x )
{
  // Fill 1D histogram with x

  TH1D *hist = (TH1D *)gROOT->Get( name );
  if ( hist ) hist->Fill( x );
  else cout << "hist not found: " << name << endl;
}

void DstSelector::Fill1D( const char *name, Double_t x, Double_t w )
{
  // Fill 1D histogram with x

  TH1D *hist = (TH1D *)gROOT->Get( name );
  if ( hist ) hist->Fill( x, w );
  else cout << "hist not found: " << name << endl;
}

void DstSelector::Fill2D( const char *name, Double_t x, Double_t y )
{
  // Fill 2D histogram with x and y

  TH2D *hist = (TH2D *)gROOT->Get( name );
  if ( hist ) hist->Fill( x, y );
  else cout << "hist not found: " << name << endl;
}

void DstSelector::Fill2D( const char *name, 
			  Double_t x, Double_t y, Double_t w )
{
  // Fill 2D histogram with x and y

  TH2D *hist = (TH2D *)gROOT->Get( name );
  if ( hist ) hist->Fill( x, y, w );
  else cout << "hist not found: " << name << endl;
}

void DstSelector::Fill3D( const char *name, 
			  Double_t x, Double_t y, Double_t z )
{
  // Fill 3D histogram with x, y, z

  TH3D *hist = (TH3D *)gROOT->Get( name );
  if ( hist ) hist->Fill( x, y, z );
  else cout << "hist not found: " << name << endl;
}

void DstSelector::Fill3D( const char *name, 
			  Double_t x, Double_t y, Double_t z, Double_t w )
{
  // Fill 3D histogram with x, y, z

  TH3D *hist = (TH3D *)gROOT->Get( name );
  if ( hist ) hist->Fill( x, y, z, w );
  else cout << "hist not found: " << name << endl;
}

void DstSelector::Print( Option_t *option ) const
{
  // Print current process status

  Double_t ntot = fTree->GetEntries();
  Double_t time = fTimer->RealTime();
  Double_t frac = TMath::Min( 99.9, 100.*fNproc/ntot );
  Double_t rate = fNproc/time;
  Double_t eta  = time*(100-frac)/frac;

  cout << "\033[100D";
  cout << Form( "%7d/%8d events (%4.1f%%) @%6.1f sec "
		"(%4.1fkHz) ETA %6.1f sec", 
		(Int_t)fNfill, (Int_t)fNproc, 
		frac, time, rate*1e-3, eta );
  cout.flush();
  fTimer->Continue();
}

Int_t DstSelector::Write( const char *, Int_t, Int_t ) const
{
  // Write histograms

  Int_t ret = 0;
  ret += gROOT->GetList()->Write();
  if ( fTreeSel->GetEntries() > 0 ) ret += fTreeSel->Write();
  if ( gCmpParams ) gCmpParams->Write();
  if ( gCztParams ) gCztParams->Write();

  return ret;
}

Int_t DstSelector::Write( const char *, Int_t, Int_t )
{
  // Write histograms

  return ((const DstSelector *)this)->Write();
}

TH1D *DstSelector::LogHist( const char *name, const char *title,
			    Int_t nbx, Double_t xmin, Double_t xmax )
{
  // Create a 1D histogram with logarithmic bins

  Double_t *bin = GetLogBin( nbx, xmin, xmax );
  TH1D *hist = new TH1D( name, title, nbx, bin );
  delete [] bin;

  return hist;
}

TH2D *DstSelector::LogHist( const char *name, const char *title,
			    Int_t nbx, Double_t xmin, Double_t xmax,
			    Int_t nby, Double_t ymin, Double_t ymax, 
			    Int_t mode )
{
  // Create a 2D histogram with logarithmic/linear bins
  //  mode = 1: liny VS. logx 
  //  mode = 2: logy VS. linx
  //  mode = 3: logy VS. logx 

  Double_t *xbin = 0, *ybin = 0;
  TH2D *hist = 0;

  // logx liny
  if ( mode == 1 ) {
    xbin = GetLogBin( nbx, xmin, xmax );
    hist = new TH2D( name, title, nbx, xbin, nby, ymin, ymax );
  }

  // linx logy
  else if ( mode == 2 ) {
    ybin = GetLogBin( nby, ymin, ymax );
    hist = new TH2D( name, title, nbx, xmin, xmax, nby, ybin );
  }

  // logx logy
  else if ( mode == 3 ) {
    xbin = GetLogBin( nbx, xmin, xmax );
    ybin = GetLogBin( nby, ymin, ymax );
    hist = new TH2D( name, title, nbx, xbin, nby, ybin );
  }

  if ( xbin ) delete [] xbin;
  if ( ybin ) delete [] ybin;

  return hist;
}

TH3D *DstSelector::LogHist( const char *hname, const char *htitle,
			    Int_t nbx, Double_t xmin, Double_t xmax,
			    Int_t nby, Double_t ymin, Double_t ymax,
			    Int_t nbz, Double_t zmin, Double_t zmax,
			    Int_t bit )
{
  // Create a 3D histogram with logarithmic/linear bins
  //  bit | 1: logx
  //  bit | 2: logy
  //  bit | 4: logz

  Double_t *xbin, *ybin, *zbin;

  if ( bit&1 ) xbin = GetLogBin( nbx, xmin, xmax );
  else         xbin = GetLinBin( nbx, xmin, xmax );
  if ( bit&2 ) ybin = GetLogBin( nby, ymin, ymax );
  else         ybin = GetLinBin( nby, ymin, ymax );
  if ( bit&4 ) zbin = GetLogBin( nbz, zmin, zmax );
  else         zbin = GetLinBin( nbz, zmin, zmax );

  TH3D *hist = new TH3D( hname, htitle, nbx, xbin, nby, ybin, nbz, zbin );

  delete [] xbin;
  delete [] ybin;
  delete [] zbin;

  return hist;
}

Double_t *DstSelector::GetLogBin( Int_t nbin, Double_t xmin, Double_t xmax )
{
  // Return an array of logarithmic bin

  Double_t lx1 = TMath::Log10( xmin );
  Double_t lx2 = TMath::Log10( xmax );

  Double_t *bin = new Double_t[nbin+1];
  for ( Int_t i = 0; i <= nbin; i++ )
    bin[i] = TMath::Power( 10, lx1+(lx2-lx1)/nbin*i );

  return bin;
}

Double_t *DstSelector::GetLinBin( Int_t nbin, Double_t xmin, Double_t xmax )
{
  // Return an array of linear bin

  Double_t *bin = new Double_t[nbin+1];
  for ( Int_t i = 0; i <= nbin; i++ ) bin[i] = xmin+(xmax-xmin)/nbin*i;

  return bin;
}
