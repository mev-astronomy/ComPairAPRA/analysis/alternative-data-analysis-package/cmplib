#include "CsISync.h"

#include "TH2D.h"
#include "TCanvas.h"

#include <iostream>
#include <fstream>
#include <string>
#include <fcntl.h>
#include <unistd.h>

void usage( void )
{
  std::cout << std::endl;
  std::cout << "Usage: cproc [options] [raw_file].." << std::endl;
  std::cout << std::endl;
  std::cout << "Options:" << std::endl;
  std::cout << "   -o FILE     set output file name as FILE   " << std::endl;
  std::cout << "   -n N        process only N events          " << std::endl;
  std::cout << "   -a Filename                                " << std::endl;
  std::cout << "   -r Filename                                " << std::endl;
  std::cout << "   -h          show this help                 " << std::endl;
}

int main( int argc, char *argv[] )
{
  std::cout << "==============================================" << std::endl;
  std::cout << " ComPair CsI data handler                     " << std::endl;
  std::cout << "==============================================" << std::endl;

  if ( argc == 2 && argv[1][0] == '-' && argv[1][1] == 'h' ) {
    usage();
    return 0;
  }
  else if ( argc < 2 ) {
    std::cout << std::endl;
    std::cout << "You need at least two arguments. " << std::endl;
    std::cout << "Try \"cprc -h\" for more information" << std::endl;
    return 0;
  }

  CsISync fSync;

  std::string ofn = "csi_cvt.dat";
  int nevt = -1;

  bool aflag = false;
  bool rflag = false;

  // option analysis
  for ( int iarg = 1; iarg < argc; iarg++ ) {

    if ( argv[iarg][0] == '-' ) {
      aflag = false;
      rflag = false;
    }
				  
    if ( argv[iarg][0] == '-' && argv[iarg][1] == 'o' ) {
      if ( argc > iarg+1 ) ofn = argv[++iarg];
    }

    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'n' ) {
      if ( argc > iarg+1 ) nevt = atoi(argv[++iarg]);
    }

    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'a' ) {
      if ( argc > iarg+1 ) fSync.AddArduino( argv[++iarg] );
      aflag = true;
    }
	      
    else if ( argv[iarg][0] == '-' && argv[iarg][1] == 'r' ) {
      if ( argc > iarg+1 ) fSync.AddRossPad( argv[++iarg] );
      rflag = true;
    }

    else if ( aflag ) {
      if ( argc > iarg+1 ) fSync.AddArduino( argv[iarg] );
    }

    else if ( rflag ) {
      if ( argc > iarg+1 ) fSync.AddRossPad( argv[iarg] );
    }
  }

  std::ofstream fOUT;
  fOUT.open( ofn, std::ios::out|std::ios::binary );
  if ( !fOUT.is_open() ) return -1;

  fSync.ProcArduino();
  fSync.ProcRossPad();

  TH1D *hist = new TH1D( "hist", "hist", 6000, -10000.0, 50000.0 );
  TH1D *hist_L = new TH1D( "hist_L", "hist", 200, -100.0, 100.0 );
  TH1D *hist_R = new TH1D( "hist_R", "hist", 200, 38200.0, 38400.0 );
  
  Int_t n_arduino = fSync.GetNarduino();
  Int_t n_rosspad = fSync.GetNrosspad();
  if ( nevt > 0 && n_arduino > nevt ) n_arduino = nevt;
  if ( nevt > 0 && n_rosspad > nevt ) n_rosspad = nevt;
  
  UInt_t fFrstID = 0;
  UInt_t fLastID = 0;
  
  time_t fTstart = 0;

  Int_t m_rosspad = n_rosspad/10;

  std::map<Int_t, UInt_t> fTrigID;
  fTrigID.clear();

  std::map<Int_t, Double_t> fTimeID;
  fTimeID.clear();
  
  Int_t j = 0;
  for ( Int_t i = 0; i < n_rosspad && j < n_arduino; i++ ) {
    if ( i%m_rosspad == 0 )
      std::cout << Form( "i = %8d/%8d, j = %8d/%8d", i, n_rosspad, j, n_arduino ) << std::endl;

    fSync.ParseRossPad( i );
    UInt_t fIndex = fSync.GetIndex();
    
    for ( Int_t k = j; k < n_arduino; k++ ) {
      fSync.ParseArduino( k );

      ULong64_t time_arduino = fSync.GetTimeArduino();
      ULong64_t time_rosspad = fSync.GetTimeRossPad();

      Double_t time_differe = (time_rosspad > time_arduino)
	? +1.0*(time_rosspad - time_arduino)
	: -1.0*(time_arduino - time_rosspad);
      
      UInt_t fEpoch   = time_rosspad/1000000000;

      if ( fTstart <= 0 || fTstart > fEpoch ) fTstart = (time_t)fEpoch;

      Int_t  fDataType    = fSync.GetDataType();
      UInt_t fEventID     = fSync.GetEventID ();

      if ( fFrstID == 0 || fFrstID > fEventID ) fFrstID = fEventID;
      if ( fLastID < fEventID ) fLastID = fEventID;

      if ( time_rosspad >= time_arduino ) {
	if ( time_rosspad - time_arduino < 4e4 ) {
	  
	  if ( fDataType != 0 && fDataType != 4 ) {
	    fTrigID.insert( std::map<Int_t, UInt_t>::value_type( fIndex, fEventID ) );
	    fTimeID.insert( std::map<Int_t, Double_t>::value_type( fIndex, time_differe ) );

	    if ( fEventID == 0 ) {
	      fSync.SetData( kTRUE );
	    }
	    else {
	      fSync.SetEventID( fEventID );
	      hist->Fill( time_differe );
	      hist_L->Fill( time_differe );
	      hist_R->Fill( time_differe );
	    }
	  }
	  else {
	    fSync.SetData( kFALSE );
	    hist->Fill( time_differe );
	    hist_L->Fill( time_differe );
	    hist_R->Fill( time_differe );
	  }
	  j = k + 1;
	  k = n_arduino;
	}
      }
      else if ( time_rosspad < time_arduino ) {

	if ( time_arduino - time_rosspad < 1e4 ) {
	  
	  if ( fDataType != 0 && fDataType != 4 ) {
	    fTrigID.insert( std::map<Int_t, UInt_t>::value_type( fIndex, fEventID ) );
	    fTimeID.insert( std::map<Int_t, Double_t>::value_type( fIndex, time_differe ) );
	    
	    if ( fEventID == 0 ) {
	      fSync.SetData( kTRUE );
	    }
	    else {
	      fSync.SetEventID( fEventID );
	      hist->Fill( time_differe );
	      hist_L->Fill( time_differe );
	      hist_R->Fill( time_differe );
	    }
	  }
	  else {
	    fSync.SetData( kFALSE );
	    hist->Fill( time_differe );
	    hist_L->Fill( time_differe );
	    hist_R->Fill( time_differe );
	  }
	  j = k + 1;
	  k = n_arduino;
	}
	else {
	  fEventID = 0;
	  if ( fDataType != 0 && fDataType != 4 ) {
	    
	    fTrigID.insert( std::map<Int_t, UInt_t>::value_type( fIndex, fEventID ) );
	    fTimeID.insert( std::map<Int_t, Double_t>::value_type( fIndex, time_differe ) );
	    
	    fSync.SetData( kTRUE );
	  }
	  else {
	    fSync.SetData( kFALSE );
	  }
	  j = k;
	  k = n_arduino;
	}
      }
    }
  }
  
  fSync.WritePacket( &fOUT );

  
  
  TCanvas *c1 = new TCanvas( "c1", "c1" );
  hist->Draw();
  c1->SaveAs( "cproc.png" );
  c1->Clear();

  hist_L->Draw();
  c1->SaveAs( "cproc_L.png" );
  c1->Clear();

  hist_R->Draw();
  c1->SaveAs( "cproc_R.png" );
  c1->Clear();
  
  std::size_t pos = ofn.find( ".dat" );
  std::string bfn = ofn.substr( 0, pos );

  struct tm *ptr = localtime( &fTstart );
  int year = ptr->tm_year+1900;
  int mon  = ptr->tm_mon+1;
  int mday = ptr->tm_mday;
  int hour = ptr->tm_hour;
  int min  = ptr->tm_min;
  
  std::string name = Form( "%s-%04d-%02d%02d-%02d%02d", bfn.c_str(),
			   year, mon, mday, hour, min );
  int file_num = 1;
  std::string full_name;
  
  int fDout;
  do {
    full_name = name+Form( "-%03d.dat", file_num++ );
    fDout = open( full_name.c_str(), O_RDONLY );
  } while( fDout > 0 );

  return std::rename( ofn.c_str(), full_name.c_str() );
}
