#include "CztLib.h"

#include "TString.h"

ClassImp( CztLib )

//UInt_t   CztLib::fAsicNum[3] = { 208, 166, 210 };
//UInt_t   CztLib::fAsicNum[3] = { 166, 208, 210 };
Int_t    CztLib::fThreshold = 0;
TVector3 CztLib::fVnotValid = TVector3( -99999.9, -99999.9, -99999.9 );
Double_t CztLib::fDnotValid = -99999.9;
Float_t  CztLib::fFnotValid = -99999.9;

Double_t CztLib::fCztWidth  =  5.9;
Double_t CztLib::fCztHeight =  5.9;
Double_t CztLib::fCztLength = 19.5;

CztLib::CztLib()
{
}

CztLib::~CztLib()
{
}
