#include "TApplication.h"
#include "TGClient.h"
#include "CmpMainFrame.h"

int main( int argc, char **argv ) {
  TApplication theApp( "App", &argc, argv );
  new CmpMainFrame( gClient->GetRoot(), 400, 400 );
  theApp.Run();
  return 0;
}
