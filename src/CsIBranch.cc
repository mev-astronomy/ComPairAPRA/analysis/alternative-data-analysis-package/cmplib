#include "CsIBranch.h"
#include "CsILib.h"
#include "CsIParams.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "DstBranch.h"
#include "HitBranch.h"

#include "TFile.h"

ClassImp( CsIBranch )

CsIBranch::CsIBranch( void )
{
  for ( Int_t lyr = 0; lyr < CsILib::N_LAYR; lyr++ ) {
    fLayer[lyr].SetDetID( CmpLib::D_CSI );
    fLayer[lyr].SetLyrID( lyr );
    fLayer[lyr].SetMhits( CsILib::N_LOGS );
    fLayer[lyr].SetCoord( gCsIParams->GetCoord( lyr ) );
  }
}

CsIBranch::~CsIBranch()
{
}

CsILayer *CsIBranch::GetLayer( Int_t lyr )
{
  if ( lyr < 0 || lyr >= CsILib::N_LAYR ) return (CsILayer *)NULL;
  return &fLayer[lyr];
}

Int_t CsIBranch::Process( Int_t sel )
{
  if ( !gCsIParams ) return 0;
  
  // The main method of CsIBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  Int_t i = 0;
  for ( Int_t lyr = 0; lyr < CsILib::N_LAYR; lyr++ ) {
    i += fLayer[lyr].Process( sel );
    fEnergy += fLayer[lyr].GetEnergy();
  }
  
  if ( sel == CmpLib::HITEVT && i == 0 ) return 0;

  return 1;
}

Int_t CsIBranch::Fill( BasePacket *pkt )
{
  fAttrib  = pkt->GetAtrb();
  fEventID = pkt->GetEvid();
  fEpoch   = pkt->GetEpch();
  fEpochNS = pkt->GetEpns();
  
  fEvent.Fill( pkt );
  for ( Int_t lyr = 0; lyr < CsILib::N_LAYR; lyr++ ) {
    fLayer[lyr].SetData( &fEvent );
  }
  return 1;
}

void CsIBranch::Clear( Option_t *option )
{
  fAttrib  = 0;
  fEventID = 0;
  fEpoch   = 0;
  fEpochNS = 0;
  
  fEvent.Clear();
  for ( Int_t lyr = 0; lyr < CsILib::N_LAYR; lyr++ ) {
    fLayer[lyr].Clear();
  }
  fEnergy = 0.0;
}

void CsIBranch::Print( Option_t *option ) const
{
}
