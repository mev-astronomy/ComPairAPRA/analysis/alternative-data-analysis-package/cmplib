#include "TrgBranch.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "CmpParams.h"

#include "TFile.h"

ClassImp( TrgBranch )

TrgBranch::TrgBranch( void )
{
  Clear();
}

TrgBranch::~TrgBranch()
{
}

Int_t TrgBranch::Process( Int_t sel )
{
  // The main method of TrgBranch
  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  return 1;
}

Int_t TrgBranch::Fill( BasePacket *pkt )
{
  fAttrib  = pkt->GetAtrb();
  fEventID = pkt->GetEvid();
  fEpoch   = pkt->GetEpch();
  fEpochNS = pkt->GetEpns();

  fEvent.Fill( pkt );
  return 1;
}

void TrgBranch::Clear( Option_t *option )
{
  fAttrib  = 0;
  fEventID = 0;
  fEpoch   = 0;
  fEpochNS = 0;

  fEvent.Clear( option );
}

void TrgBranch::Print( Option_t *option ) const
{
}
