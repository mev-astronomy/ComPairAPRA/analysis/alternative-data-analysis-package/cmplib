#include "CztEvent.h"
#include "CztLib.h"
#include "CztAsic.h"

#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include "TMath.h"

#include <iostream>
#include <fstream>

ClassImp( CztEvent )

CztEvent::CztEvent( void )
{
  fCztPacket = new CztPacket();

  fPrStamp = 0;
  fTmCarry = 0;

  Clear();
}

CztEvent::~CztEvent()
{
  if ( fCztPacket ) delete fCztPacket;
}

Int_t CztEvent::GetAsicID( UInt_t hit ) const
{
  if ( hit < 0 || hit >= fNhitAsic ) return -1;
  return fAsicID[hit];
}

CztAsic *CztEvent::GetAsic( UInt_t hit )
{
  if ( hit < 0 || hit >= fNhitAsic ) return (CztAsic *)NULL;
  return &fAsic[hit];
}

Int_t CztEvent::Fill( BasePacket *pkt, Bool_t speak )
{
  Clear();

  Int_t fSize = fCztPacket->Fill( pkt, speak );
  if ( fSize <= 0 ) return fSize;

  fPacketCount  = fCztPacket->GetPktCount();
  fPacketSize   = fCztPacket->GetPktSize ();
  fUserRegister = fCztPacket->GetRegUser ();
  fEventRate    = fCztPacket->GetEvtRate ();
  fChipInEvent  = fCztPacket->GetCIE     ();

  fNhitAsic     = fCztPacket->GetNhitAsic();
  for ( UInt_t hit = 0; hit < fNhitAsic; hit++ ) {
    fAsicID[hit] = fCztPacket->GetAsicID( hit );
  }

  fEventID      = fCztPacket->GetEventID ();
  fTmStamp      = fCztPacket->GetTmStamp ();
  fTimePps      = fCztPacket->GetTimePps ();
  fEpoch        = fCztPacket->GetEpoch   ();
  fEpochNS      = fCztPacket->GetEpochNS ();
  
  for ( UInt_t i = 0; i < fNhitAsic; i++ ) {
    fAsic[i] = *fCztPacket->GetAsicData( i );
  }

  fPrStamp = fCztPacket->GetPrStamp();
  fTmCarry = fCztPacket->GetTmCarry();
  //  fTime = ( fTmCarry*TMath::Power( 2, 32 ) + fPrStamp )*1.0e-8;
  fTime    = fCztPacket->GetUtcTime();

#ifdef DEBUG
  std::cout << Form( "%u %u", fEventID, fTmStamp );
#endif

  return fSize;
}

void CztEvent::SetHitAsic( UShort_t cie ) 
{
  fNhitAsic = 0;
  for ( Int_t i = 0; i < 16; i++ ) {
    if ( ( cie & (1<<i) ) == (1<<i) ) {
      fAsicID[fNhitAsic++] = i;
    }
  }
}

void CztEvent::Clear( Option_t *option ) 
{
  fCztPacket->Clear( option );
  
  fTime = CztLib::fDnotValid;

  fPacketCount  = 0;
  fPacketSize   = 0;
  fUserRegister = 0;
  fEventRate    = 0;
  fChipInEvent  = 0;

  fNhitAsic  = 0;
  fEventID   = 0;
  fTmStamp   = 0;
  fTimePps   = 0;
  fEpoch     = 0;
  fEpochNS   = 0;

  for ( UInt_t i = 0; i < CztLib::N_ASIC; i++ ) {
    fAsicID[i] = -1;
    fAsic  [i].Clear();
  }
}
