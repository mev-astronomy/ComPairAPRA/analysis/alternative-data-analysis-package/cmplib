#include <iostream>
#include <fstream>
#include <string>

int main( int argc, char *argv[] )
{
  std::string efn = "cmp_eid.dat";
  if ( argc == 2 ) efn = argv[argc-1];

  std::ifstream fEID;
  fEID.open( efn, std::ios::in|std::ios::binary );
  if ( !fEID.is_open() ) return -1;

  unsigned char fIdBuff[12];

  int nevt = 0;
  while( 1 ) {
    fEID.read( (char *)&fIdBuff, 12 );
    if ( fEID.eof() ) break;
    nevt++;
  }

  std::cout << efn << ": Nevt = " << nevt << std::endl;
  return 0;
}
