#include "CmpLib.h"
#include "SitLib.h"
#include "SitConvert.h"

#include <iostream>

SitConvert::SitConvert( void )
{
  fAtrb = PKT_ATRB_SIT;
  fBuffer = new unsigned char[MAX_PKT_SIZE];
  fPreID = 0x80000000;
}

SitConvert::SitConvert( UShort_t atrb ) : DataConvert( atrb )
{
  fBuffer = new unsigned char[MAX_PKT_SIZE];
  fPreID = 0x80000000;
}

SitConvert::~SitConvert()
{
  if ( fBuffer ) delete [] fBuffer;
}

void SitConvert::SetPreID( void )
{
  UShort_t iptr = 20;
  fPreID = fBuffer[(iptr+3)]*256*256*256 + fBuffer[(iptr+2)]*256*256 + fBuffer[(iptr+1)]*256 + fBuffer[(iptr+0)];
}

Int_t SitConvert::Read( void )
{
  Clear();

  // Check Dummy word
  fIN.read( (char *)fBuffer+fSize, 2 );
  if ( fIN.eof() ) return 0;
  Int_t pkt_size = fBuffer[0]+fBuffer[1]*256;
  
  fSize += fIN.gcount();

  // Read Header
  fIN.read( (char *)fBuffer+fSize, pkt_size-2 );
  fSize += fIN.gcount();

  fEID.read( (char *)&fIdBuff, 12 );
  if ( fEID.eof() ) return 0;
  
  SetEventID();
  SetEpoch  ();
  SetEpochNS();

  SetPreID();

  if ( fEventID != fPreID ) {
    std::cout << Form( "EventID mismatch: %8x %8x", fEventID, fPreID ) << std::endl;
    return -1;
  }
  
  fPacket->SetData( fSize, fBuffer );
  fPacket->SetAtrb( fAtrb    );
  fPacket->SetEvid( fEventID );
  fPacket->SetEpch( fEpoch   );
  fPacket->SetEpns( fEpochNS );

  fSize += PKT_HEAD_SIZE;

  return 1;
}

void SitConvert::Clear( Option_t *option )
{
  DataConvert::Clear( option );
  fPreID = 0x80000000;
}
