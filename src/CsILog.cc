#include "CsILog.h"
#include "CsILib.h"
#include "CsILayer.h"
#include "CsIBranch.h"
#include "CsIParams.h"

#include "CmpLib.h"
#include "CmpTree.h"
#include "DstBranch.h"
#include "HitBranch.h"

#include "TFile.h"

ClassImp( CsILog )

CsILog::CsILog( void )
{
  fLogID = -1;
  Clear();
}

CsILog::~CsILog()
{
}

UShort_t CsILog::GetData( Int_t side ) const
{
  if ( side < 0 || side >= CsILib::N_SIDE ) return 0;
  return fData[side];
}

Byte_t CsILog::GetAtrb( Int_t side ) const
{
  if ( side < 0 || side >= CsILib::N_SIDE ) return 0;
  return fAtrb[side];
}

Double_t CsILog::GetEdep( Int_t side ) const
{
  if ( side < 0 || side >= CsILib::N_SIDE ) return CmpLib::fDnotValid;
  return fEdep[side];
}

Int_t CsILog::Process( Int_t layr )
{
  if ( layr < 0 || layr >= CsILib::N_LAYR ) return -1;

  if ( !gCsIParams ) return 0;

  CmpTree *ctree = CmpTree::GetPtr();
  if ( !ctree ) return 0;

  CsIBranch *csi = ctree->GetCsI();
  if ( !csi ) return 0;
  
  //  Double_t time = ctree->GetUtcTime();
  Double_t time = csi->GetTime();
  Int_t    ip   = gCsIParams->GetPoint( time );
  if ( ip < 0 ) return 0;

  for ( Int_t side = 0; side < CsILib::N_SIDE; side++ ) {
    Int_t indx = gCsIParams->GetIndex( layr, fLogID, side );
    if ( indx < 0 || indx >= CsILib::N_INDX ) continue;
    if ( gCsIParams->IsDead( indx ) ) continue;
    Double_t peds = gCsIParams->GetPeds( indx, ip );
    Double_t thre = gCsIParams->GetThre( indx, ip );
    Double_t gain = gCsIParams->GetGain( indx, ip );
    if ( fData[side] - peds > thre     ) fAtrb[side] |= CsILib::A_GOOD;
    if ( fData[side] >= CsILib::N_MAXQ ) fAtrb[side] |= CsILib::A_OVFL;
    fEdep[side] = ( fData[side] - peds )*gain;
  }
  
  fHitSta = ( fAtrb[0] == 1 && fAtrb[1] == 1 ) ? 1 : 0;

  if ( fAtrb[0] == 0 || fAtrb[1] == 0 ) return 0;
  
  Double_t summ = TMath::Sqrt( fEdep[0]*fEdep[1] );
  Double_t diff = (fEdep[1]+fEdep[0] > 0.0 ) ? (fEdep[1]-fEdep[0])/(fEdep[1]+fEdep[0]) : 0.0;

  //  fHitEne = gCsIParams->GetQcorr( layr, fLogID, diff )*summ;
  fHitEne = summ;

  fHitPos = ( gCsIParams->GetCoord( layr ) == CsILib::COORD_X )
    ? TVector3( gCsIParams->GetOrigin( layr, fLogID ).X(),
		gCsIParams->GetHposi ( layr, fLogID, diff ),
		gCsIParams->GetOrigin( layr, fLogID ).Z() )
    : TVector3( gCsIParams->GetHposi ( layr, fLogID, diff ),
	        gCsIParams->GetOrigin( layr, fLogID ).Y(),
		gCsIParams->GetOrigin( layr, fLogID ).Z() );
  return 1;
}

void CsILog::SetData( Int_t side, UShort_t data )
{
  if ( side < 0 || side >= CsILib::N_SIDE ) return;
  fData[side] = data;
}

void CsILog::SetAtrb( Int_t side, Byte_t atrb )
{
  if ( side < 0 || side >= CsILib::N_SIDE ) return;
  fAtrb[side] = atrb;
}

void CsILog::AddAtrb( Int_t side, Byte_t atrb )
{
  if ( side < 0 || side >= CsILib::N_SIDE ) return;
  fAtrb[side] |= atrb;
}

void CsILog::DelAtrb( Int_t side, Byte_t atrb )
{
  if ( side < 0 || side >= CsILib::N_SIDE ) return;
  fAtrb[side] &= ~atrb;
}

void CsILog::SetEdep( Int_t side, Double_t edep )
{
  if ( side < 0 || side >= CsILib::N_SIDE ) return;
  fEdep[side] = edep;
}

void CsILog::Clear( Option_t *option )
{
  for ( Int_t side = 0; side < CsILib::N_SIDE; side++ ) {
    fData[side] = 0;
    fAtrb[side] = 0;
    fEdep[side] = CmpLib::fDnotValid;
  }
  fHitEne = CmpLib::fDnotValid;
  fHitPos = CmpLib::fVnotValid;
}

void CsILog::Print( Option_t *option ) const
{
}
