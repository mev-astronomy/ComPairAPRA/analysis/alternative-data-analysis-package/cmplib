#include "Packet.h"
#include "DataMerger.h"
#include "DataHandler.h"

#include <iostream>
#include <signal.h>
#include <fcntl.h>

Int_t DataMerger::fKill = 0;

DataMerger::DataMerger( void )
{
  time( &fTstart );
  fOutFname = "cmp_cvt.dat";
  fAtrb = 0;
  fData.clear();
}

DataMerger::~DataMerger()
{
  for ( it_type it = fData.begin(); it != fData.end(); ++it ) delete it->second;
}

Int_t DataMerger::SetAtrb( UShort_t atrb )
{
  if ( fAtrb & atrb ) {
    std::cout << "<DataMerger::SetAtrb> atrb = " << atrb << " is not exclusive." << std::endl;
    return -1;
  }
  else {
    std::cout << Form( "<DataMerger::SetAtrb> atrb = 0x%04X is set.", atrb ) << std::endl;
    fAtrb |= atrb;
    return 1;
  }
}

Int_t DataMerger::DelAtrb( UShort_t atrb )
{
  if ( !(fAtrb & atrb) ) {
    std::cout << "<DataMerger::DelAtrb> atrb = " << atrb << " is not set." << std::endl;
    return -1;
  }
  else {
    std::cout << Form( "<DataMerger::DelAtrb> atrb = 0x%04X is reset.", atrb ) << std::endl;
    fAtrb &= ~atrb;
    return 1;
  }
}

Int_t DataMerger::Open( const char *fname )
{
  fOutFname = fname;
  fOUT.open( fname, std::ios::out|std::ios::binary );
  if ( !fOUT.is_open() ) return -1;
  
  return 0;
}

Int_t DataMerger::Close( void )
{
  if ( !fOUT.is_open() ) return -1;
  if ( Rename() != 0 )   return -1;
  fOUT.close();

  return 0;
}

Int_t DataMerger::Add( UShort_t atrb, const char *fname )
{
  //  std::cout << Form( "<DataMerger::Add> %d %s", atrb, fname ) << std::endl;
  it_type it = fData.find( atrb );

  if ( it != fData.end() ) {
    fData[atrb]->Add( fname );
  }
  else {
    if ( SetAtrb( atrb ) == 1 ) {
      fData[atrb] = new DataHandler();
      fData[atrb]->Add( fname );
    }
  }

  return 1;
}

void DataMerger::Loop( Int_t nproc )
{
  Int_t nevt = 0;

  // set signal handler
  signal( SIGINT,  SigHandler );
  signal( SIGTERM, SigHandler );

  while ( nproc <= 0 || ( nevt < nproc ) ) {
    if ( fKill ) {
      std::cout << std::endl << "*** break ***, process interrupted" << std::endl;
      break;
    }
    ReadEvent();
    if ( WriteEvent() ) nevt++;
    if ( fData.size() == 0 ) break;
    //    else break;
  }

  // reset signal handler
  signal( SIGINT,  SIG_DFL );
  signal( SIGTERM, SIG_DFL );
}

Int_t DataMerger::Rename( void )
{
  std::size_t pos = fOutFname.find( ".dat" );
  std::string bfn = fOutFname.substr( 0, pos );

  struct tm *ptr = localtime( &fTstart );
  int year = ptr->tm_year+1900;
  int mon  = ptr->tm_mon+1;
  int mday = ptr->tm_mday;
  int hour = ptr->tm_hour;
  int min  = ptr->tm_min;

  std::string name = Form( "%s-%04d-%02d%02d-%02d%02d", bfn.c_str(),
			   year, mon, mday, hour, min );
  int file_num = 1;
  std::string full_name;

  int fDout;
  do {
    full_name = name+Form( "-%03d.dat", file_num++ );
    fDout = open( full_name.c_str(), O_RDONLY );
  } while( fDout > 0 );

  return std::rename( fOutFname.c_str(), full_name.c_str() );
}

void DataMerger::ReadEvent( void )
{
  Bool_t flag = kFALSE;
  
  for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
    if ( flag ) {
      it = fData.begin();
      flag = kFALSE;
    }
    
    UShort_t atrb = it->first;
    while ( fData[atrb]->GetEventID() == 0 && !fData[atrb]->Eof() ) {
      //      fData[atrb]->Read( kTRUE );
      fData[atrb]->Read();
      UInt_t time = fData[atrb]->GetEpoch();
      if ( fTstart > (time_t)time && time > 160000000 ) fTstart = (time_t)time;
      if ( fData[atrb]->GetPktSize() > 0 && fData[atrb]->GetEventID() == 0 ) {
	fData[atrb]->Write( &fOUT );
      }
    }

    if ( fData[atrb]->Eof() ) {
      DelAtrb( atrb );
      if ( it == fData.begin() ) {
	it = fData.erase( it );
	flag = kTRUE;
	if ( fData.size() == 0 ) break;
      }
      else {
	it = fData.erase( it );
	if ( it == fData.end() ) break;
	if ( it != fData.begin() ) --it;
      }
    }
  }
}

Int_t DataMerger::WriteEvent( void )
{
  UInt_t evid = 0;
  UInt_t epch = 0;
  UInt_t epns = 0;
  for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
    UShort_t atrb = it->first;
    if ( fData[atrb]->GetEventID() == 0 ) continue;
    if ( evid == 0 || 
	 ( (fData[atrb]->GetEventID() < evid) && (evid-fData[atrb]->GetEventID()) < 0x40000000 ) ||
	 ( (fData[atrb]->GetEventID() > evid) && (fData[atrb]->GetEventID()-evid) > 0x40000000 ) ) {
      evid = fData[atrb]->GetEventID();
      epch = fData[atrb]->GetEpoch  ();
      epns = fData[atrb]->GetEpochNS();
    }
  }

  if ( evid == 0 ) return 0;

  UShort_t npkt = 0;
  UShort_t flag = 0;
  UShort_t size = 0;

  for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
    UShort_t atrb = it->first;
    if ( fData[atrb]->GetEventID() == evid ) {
      flag |= fData[atrb]->GetPktAtrb();
      size += fData[atrb]->GetPktSize() + PKT_SIZE_SIZE*2;
      npkt++;
    }
  }

  if ( size == 0 ) return 0;

  if ( npkt == 1 ) {
    fData[flag]->Write( &fOUT );
    return 1;
  }

  size += PKT_HEAD_SIZE;
  
  unsigned char bsize[2];
  unsigned char battr[2];

  bsize[0] = (size >> 8) & 0xFF;
  bsize[1] = (size%256 ) & 0xFF;
  battr[0] = (flag >> 8) & 0xFF;
  battr[1] = (flag%256 ) & 0xFF;

  fOUT.write( reinterpret_cast<const char *>(&bsize[0]), 2 );
  fOUT.write( reinterpret_cast<const char *>(&battr[0]), 2 );
  fOUT.write( reinterpret_cast<const char *>(&evid), 4 );
  fOUT.write( reinterpret_cast<const char *>(&epch), 4 );
  fOUT.write( reinterpret_cast<const char *>(&epns), 4 );

  for ( it_type it = fData.begin(); it != fData.end(); ++it ) {
    UShort_t atrb = it->first;
    if ( fData[atrb]->GetEventID() != evid ) continue;
    fData[atrb]->Write( &fOUT );
  }
  
  fOUT.write( reinterpret_cast<const char *>(&bsize[0]), 2 );

  return 1;
}

void DataMerger::Clear( Option_t *option )
{
}

void DataMerger::SigHandler( int sig )
{
  fKill = 1;
}
