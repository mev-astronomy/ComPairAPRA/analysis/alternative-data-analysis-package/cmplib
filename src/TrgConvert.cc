#include "TrgConvert.h"

#include <iostream>

TrgConvert::TrgConvert( void )
{
  fAtrb = PKT_ATRB_TRG;
  fBuffer = new unsigned char[MAX_PKT_SIZE];
}

TrgConvert::TrgConvert( UShort_t atrb ) : DataConvert( atrb )
{
  fBuffer = new unsigned char[MAX_PKT_SIZE];
}

TrgConvert::~TrgConvert()
{
  if ( fBuffer ) delete [] fBuffer;
}

Int_t TrgConvert::Read( void )
{
  Clear();

  int i_buf_size_a;
  int i_buf_size_b;
  unsigned char buf_size_a[2];
  unsigned char buf_size_b[2];
  
  fIN.read( (char *)&buf_size_a, PKT_SIZE_SIZE );
  if ( fIN.eof() ) return 0;
  
  i_buf_size_a = (buf_size_a[0] << 8) + (buf_size_a[1]);

  fIN.read( (char *)fBuffer, i_buf_size_a );
  fSize = i_buf_size_a;
  
  fIN.read( (char *)&buf_size_b, PKT_SIZE_SIZE );
  i_buf_size_b = (buf_size_b[0] << 8) + (buf_size_b[1]);

  if ( i_buf_size_a != i_buf_size_b ) return 0;

  fEID.read( (char *)&fIdBuff, 12 );
  if ( fEID.eof() ) return 0;
  
  SetEventID();
  SetEpoch  ();
  SetEpochNS();
  
  fPacket->SetData( fSize, fBuffer );
  fPacket->SetAtrb( fAtrb    );
  fPacket->SetEvid( fEventID );
  fPacket->SetEpch( fEpoch   );
  fPacket->SetEpns( fEpochNS );
  fSize += PKT_HEAD_SIZE;
  
  return 1;
}

void TrgConvert::Clear( Option_t *option )
{
  DataConvert::Clear( option );
}
