# cmplib

This is a software library developed for data analysis of ComPair instrument.

## Required software

CERN ROOT

## How to install

In case of csh
```csh
setenv CMPLIB [path]
source $CMPLIB/config/env.csh
set path = ( $path $CMPLIB/bin )
setenv LD_LIBRARY_PATH $CMPLIB/lib:${LD_LIBRARY_PATH}
cp $CMPLIB/config/rootrc $HOME/.rootrc
cd $CMPLIB
make
```

If you see error messages when you make the library, please tell google the error messages. You will be informed how to fix them. 
