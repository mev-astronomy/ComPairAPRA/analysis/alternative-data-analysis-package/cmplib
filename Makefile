BASEDIR = $(CMPLIB)
INC_DIR = $(BASEDIR)/inc
SRC_DIR = $(BASEDIR)/src
OBJ_DIR = $(BASEDIR)/obj
DEP_DIR = $(BASEDIR)/dep
BIN_DIR = $(BASEDIR)/bin
LIB_DIR = $(BASEDIR)/lib

ARCH := $(shell uname -s)
KREL := $(shell uname -r)

LIBR = libCmp
CROT = croot
CMRG = cmerg
CCVT = ccvrt
CPRC = cproc
CEID = cevid
CDMP = cdump
DISP = cdisp

ROOTCFLAGS  := $(shell root-config --cflags)
ROOTLIBS    := $(shell root-config --libs)
ROOTGLIBS   := $(shell root-config --glibs) -lEG
ROOTLIBDIR  := $(shell root-config --libdir)
ROOTVERSION := $(shell root-config --version)

ifeq ($(ARCH),Darwin)
CXXDEFS = -D_DARWIN_
CXX     = clang++
CINT    = rootcint6
else
CXXDEFS = -D_FILE_OFFSET_BITS=64
CXX     = g++
CINT    = rootcint
endif

#CXXOPT = -Wall -O1 -fPIC
CXXOPT = -Wall -fPIC
CXXDBG = -ggdb

CXXFLAGS = -I$(BASEDIR) -I$(INC_DIR) $(ROOTCFLAGS) $(CXXOPT) $(CXXDBG) $(CXXDEFS)

LDFLAGS  = -lpthread -ltermcap -lpanel $(ROOTLIBS)
BINFLAGS = -Wl,-rpath,$(LIB_DIR) -Wl,-rpath,$(ROOTLIBDIR)
SOFLAGS  = -shared

OBJSG = 

DICTB = CmpDict
DICTS = $(addsuffix .cc, $(DICTB))

OBJSB = RevIsion.o CmpLib.o CmpTree.o CmpParams.o\
	BasePacket.o DataHandler.o DataMerger.o SitMerger.o\
	CztPacket.o CztUdpHeader.o CztPktHeader.o CztEvtHeader.o\
	DataConvert.o CztConvert.o SitConvert.o TrgConvert.o\
	DataEventID.o CztEventID.o SitEventID.o TrgEventID.o\
	TrgBranch.o TrgEvent.o\
	AcdBranch.o\
	CsILib.o CsIParams.o CsIBranch.o CsIEvent.o CsILog.o CsILayer.o\
	CztLib.o CztParams.o CztBranch.o CztEvent.o CztAsic.o CztCryst.o CztCrate.o\
	SitLib.o SitParams.o SitBranch.o SitEvent.o SitAsic.o SitStrip.o SitLayer.o\
	DetParams.o DetBranch.o TrkBranch.o CmpBranch.o DstBranch.o HitBranch.o\
	SimBranch.o\
	CmpMainFrame.o EventDrawer.o\
	RootConvert.o DstSelector.o\
	ArduIno.o ArduinoHandler.o RossPad.o RossPadHandler.o CsISync.o

INCSB = $(addprefix $(INC_DIR)/, $(addsuffix .h, $(basename $(OBJSB))))\
	$(INC_DIR)/CmpLinkDef.h

ODEP  = $(OBJSG) $(OBJSB)

PROG  = $(CROT) $(CMRG) $(CCVT) $(CPRC) $(CEID) $(CDMP) $(DISP)

DEPS  = $(addsuffix .d, $(PROG)) $(addsuffix .d, $(basename $(ODEP)))
GOBJS = $(addprefix $(OBJ_DIR)/, $(OBJSG))
DOBJS = $(addprefix $(OBJ_DIR)/, $(addsuffix .o, $(basename $(DICTS))))\
	$(addprefix $(OBJ_DIR)/, $(OBJSB))
BINS  = $(addprefix $(BIN_DIR)/, $(PROG))
LIBSR = $(addprefix $(LIB_DIR)/, $(addsuffix .so, $(LIBR)))

ALL: $(LIBSR) $(BINS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(LIBSR): $(DOBJS)
	$(CXX) $(LDFLAGS) $(SOFLAGS) $(LIBS) $(ROOTGLIBS) $^ -o $@

#$(BIN_DIR)/$(CROT): $(OBJ_DIR)/$(CROT).o $(MOBJS) $(GOBJS) $(LIBSR) | $(BIN_DIR)
#	$(CXX) $^ $(BINFLAGS) $(LDFLAGS) $(ROOTGLIBS) -o $@

$(BIN_DIR)/$(CROT): $(OBJ_DIR)/$(CROT).o $(MOBJS) $(GOBJS) $(LIBSR)
	$(CXX) $^ $(BINFLAGS) $(LDFLAGS) $(ROOTGLIBS) -o $@

$(BIN_DIR)/$(CMRG): $(OBJ_DIR)/$(CMRG).o $(MOBJS) $(GOBJS) $(LIBSR)
	$(CXX) $^ $(BINFLAGS) $(LDFLAGS) $(ROOTGLIBS) -o $@

$(BIN_DIR)/$(CCVT): $(OBJ_DIR)/$(CCVT).o $(MOBJS) $(GOBJS) $(LIBSR)
	$(CXX) $^ $(BINFLAGS) $(LDFLAGS) $(ROOTGLIBS) -o $@

$(BIN_DIR)/$(CPRC): $(OBJ_DIR)/$(CPRC).o $(MOBJS) $(GOBJS) $(LIBSR)
	$(CXX) $^ $(BINFLAGS) $(LDFLAGS) $(ROOTGLIBS) -o $@

$(BIN_DIR)/$(CEID): $(OBJ_DIR)/$(CEID).o $(MOBJS) $(GOBJS) $(LIBSR)
	$(CXX) $^ $(BINFLAGS) $(LDFLAGS) $(ROOTGLIBS) -o $@

$(BIN_DIR)/$(CDMP): $(OBJ_DIR)/$(CDMP).o $(MOBJS) $(GOBJS) $(LIBSR)
	$(CXX) $^ $(BINFLAGS) $(LDFLAGS) $(ROOTGLIBS) -o $@

$(BIN_DIR)/$(DISP): $(OBJ_DIR)/$(DISP).o $(MOBJS) $(GOBJS) $(LIBSR)
	$(CXX) $^ $(BINFLAGS) $(LDFLAGS) $(ROOTGLIBS) -o $@

$(SRC_DIR)/$(DICTS) : $(INCSB)
	@echo "Generating dictionary $@..."
#	@rootcint -f $@ $^
#	@rootcint6 -f $@ -c $^
	@$(CINT) -f $@ -c $^
	@mv -f $(SRC_DIR)/$(DICTB)_rdict.pcm $(LIB_DIR)/

$(DEP_DIR)/%.d : $(SRC_DIR)/%.cc | $(DEP_DIR) $(OBJ_DIR) $(LIB_DIR) $(BIN_DIR)
	$(CXX) $(CXXFLAGS) -MM $< | sed 's,$*.o,$(OBJ_DIR)/$*.o $(DEP_DIR)/$*.d,g' > $@

include $(addprefix $(DEP_DIR)/, $(DEPS))

html:	$(LIBSR)
	root -q -l -b 'macro/html.C'

clean:
	rm -f $(BINS) $(OBJ_DIR)/*.o $(DEP_DIR)/*.d $(LIB_DIR)/lib*
	rm -f $(SRC_DIR)/*Dict.* .*~ *~ */*~

.PHONY : clean

$(DEP_DIR):
	mkdir -p $(DEP_DIR)

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(LIB_DIR):
	mkdir -p $(LIB_DIR)

$(BIN_DIR):
	mkdir -p $(BIN_DIR)

