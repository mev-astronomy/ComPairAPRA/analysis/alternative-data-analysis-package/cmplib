#ifndef _SIT_PARAMS_H
#define _SIT_PARAMS_H

#include "SitLib.h"
#include "DetParams.h"

#include "TObject.h"
#include "TVector3.h"
#include "TH2D.h"
#include "TH3D.h"

// SitParams
//  a class which stores general parameters and readout assign data

class SitParams : public DetParams {
  
protected:
  Int_t      fNslice;                        // Number of slices
  
  Int_t      fLindx [SitLib::N_SLICE][SitLib::N_ASIC][SitLib::N_CHAN]; // Local Index
  Int_t      fSlice [SitLib::N_LINDX];       // Slice Number
  Int_t      fCoord [SitLib::N_LINDX];       // Coordinate x/y
  Int_t      fStrip [SitLib::N_LINDX];       // Strip Number
  Int_t      fAsic  [SitLib::N_LINDX];       // Asic Number
  Int_t      fChan  [SitLib::N_LINDX];       // Channel Number
  Bool_t     fDead  [SitLib::N_LINDX];       // Dead Asic channels
  TVector3   fOrigin[SitLib::N_LINDX];       // Origin of Si Strip
  TVector3   fDimens[SitLib::N_LINDX];       // Dimensions of Si Strip

  Double_t  *fPeds  [SitLib::N_LINDX];       // [fNpoint] Pedestal of the channel
  Double_t  *fThre  [SitLib::N_LINDX];       // [fNpoint] Threshold of the channel
  Double_t  *fGain  [SitLib::N_LINDX];       // [fNpoint] Gain of the channel

public:
  SitParams();
  virtual ~SitParams();

  // access functions
  Int_t    GetNslice ( void ) const { return fNslice; }

  Int_t    GetLindx  ( Int_t slice, Int_t aid, Int_t cid ) const;
  Int_t    GetIndex  ( Int_t layer, Int_t strip ) const;
  Int_t    GetSlice  ( Int_t lidx ) const;
  Int_t    GetCoord  ( Int_t lidx ) const;
  Int_t    GetStrip  ( Int_t lidx ) const;
  Int_t    GetAsic   ( Int_t lidx ) const;
  Int_t    GetChan   ( Int_t lidx ) const;
  Bool_t   IsDead    ( Int_t lidx ) const;
  TVector3 GetOrigin ( Int_t lidx ) const;
  TVector3 GetDimens ( Int_t lidx ) const;

  Double_t GetPeds   ( Int_t lidx, Int_t ip ) const;
  Double_t GetThre   ( Int_t lidx, Int_t ip ) const;
  Double_t GetGain   ( Int_t lidx, Int_t ip ) const;
  
  // methods
  void  SetNslice  ( Int_t     n ) { fNslice = n; }

  Int_t SetAssign  ( Int_t lidx, Int_t slc, Int_t x_y, Int_t srp, Int_t aid, Int_t cid );
  Int_t SetDead    ( Int_t lidx, Bool_t dead );
  Int_t SetOrigin  ( Int_t lidx, Double_t x, Double_t y, Double_t z );
  Int_t SetDimens  ( Int_t lidx, Double_t x, Double_t y, Double_t z );

  virtual void Init( Double_t  b, Double_t e, Int_t n, Double_t *t );

  void  SetPeds    ( Int_t lidx, Int_t ip, Double_t peds );
  void  SetThre    ( Int_t lidx, Int_t ip, Double_t thre );
  void  SetGain    ( Int_t lidx, Int_t ip, Double_t gain );
		   
  // access to the global pointer
  static SitParams  *GetPtr( void );
  static SitParams  *ReadFile( const char *fname = FileName(), Int_t v = 1 );
  static const char *FileName( void ) { return "sitpar.root"; }

  // class definition for ROOT
  ClassDef( SitParams, 4 )  // General parameters and assign data class
};

// global object
extern SitParams *gSitParams;

#endif

// Local Variables:
// mode:C++
// End:
