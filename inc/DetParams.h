#ifndef _DET_PARAMS_H
#define _DET_PARAMS_H

#include "TObject.h"
#include "TVector3.h"
#include "TH2D.h"
#include "TH3D.h"

// DetParams
//  a class which stores general parameters and readout assign data

class DetParams : public TObject {
public:
  enum EMode { EXACT = 0, INTRP = 1 };
  
protected:
  Double_t   fPeriod;                                   // Calibration Period
  Int_t      fNpoint;                                   // Number of Data Point
  Double_t   fTbegun;                                   // Data Start
  Double_t   fTended;                                   // Data End
  Double_t  *fTpoint;                                   // [fNpoint] Data time point
  
  Int_t      fMapMode;                                  // MapMode
  
public:
  DetParams();
  virtual ~DetParams();

  // access functions
  Double_t   GetPeriod ( void ) const { return fPeriod; }
  Int_t      GetNpoint ( void ) const { return fNpoint; }
  Double_t   GetTbegun ( void ) const { return fTbegun; }
  Double_t   GetTended ( void ) const { return fTended; }
  Double_t   GetTpoint ( Int_t    p ) const;
  Int_t      GetPoint  ( Double_t t ) const;

  Double_t   GetCorr1D  ( TH1D *hist, Double_t x ) const;
  Double_t   GetCorr2D  ( TH2D *hist, Double_t x, Double_t y ) const;
  Double_t   GetCorr3D  ( TH3D *hist, Double_t x, Double_t y, Double_t z ) const;

  Int_t      GetMapMode( void ) const { return fMapMode; }
  
  // methods
  virtual void Init( Double_t b, Double_t e, Int_t n, Double_t *t );
  void  SetPeriod  ( Double_t  p ) { fPeriod = p; }
  void  SetNpoint  ( Int_t     n ) { fNpoint = n; }
  void  SetTbegun  ( Double_t  t ) { fTbegun = t; }
  void  SetTended  ( Double_t  t ) { fTended = t; }
  void  SetTpoint  ( Double_t *t );

  TH1D *SetCorr1D  ( TH1D *hsrc, TH1D *hdst, const char *fname );
  TH2D *SetCorr2D  ( TH2D *hsrc, TH2D *hdst, const char *fname );
  TH3D *SetCorr3D  ( TH3D *hsrc, TH3D *hdst, const char *fname );
  
  TH1D *SetHist1D  ( TH1D *hsrc, TH1D *hdst, const char *fname );
  TH2D *SetHist2D  ( TH2D *hsrc, TH2D *hdst, const char *fname );
  TH3D *SetHist3D  ( TH3D *hsrc, TH3D *hdst, const char *fname );
  
  void  SetMapMode ( Int_t mode ) { fMapMode = mode; }
		   
  // access to the global pointer
  static TObject    *ReadFile( const char *fname, const char *cname );

  // class definition for ROOT
  ClassDef( DetParams, 1 )  // General parameters and assign data class
};

#endif

// Local Variables:
// mode:C++
// End:
