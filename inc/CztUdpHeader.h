#ifndef _CZT_UDP_HEADER_H
#define _CZT_UDP_HEADER_H

#include "CztLib.h"

#include "TObject.h"

class CztUdpHeader : public TObject {
private:
  unsigned char fHeader[CztLib::B_UDPH];
  unsigned char fBuffer[CztLib::B_UDPH];

public:
  CztUdpHeader( void );
  virtual ~CztUdpHeader();

  unsigned char *GetBuffer( void ) { return fBuffer; };
  
  Int_t ReadData( std::ifstream *fIN,  Bool_t flag = kFALSE );
  Int_t FindNext( std::ifstream *fIN,  Bool_t flag = kFALSE );

  Int_t ReadData( unsigned char *data, Bool_t flag = kFALSE );
  Int_t FindNext( unsigned char *data, Bool_t flag = kFALSE );

  virtual void Clear( Option_t *option = "" );

  ClassDef( CztUdpHeader, 1 )
};

#endif

// Local Variables:
// mode:C++
// End:
