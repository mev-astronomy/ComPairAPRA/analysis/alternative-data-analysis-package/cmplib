#ifndef _REV_ISION_H
#define _REV_ISION_H

#include "TObject.h"
#include "TObjArray.h"
#include "TString.h"

class RevIsion : public TObject {
private:
  Int_t     fRevNumber;
  Long64_t  fChngSetID;
  TString   fChangeSet;
  TString   fBranch;
  TString   fTag;
  TString   fUser;
  TString   fDate;
  TObjArray fList;

public:
  RevIsion( void );
  virtual ~RevIsion();

  // access function
  Int_t    GetRevNumber( void ) const { return fRevNumber; }
  Long64_t GetChngSetID( void ) const { return fChngSetID; }
  TString  GetChangeSet( void ) const { return fChangeSet; }
  TString  GetBranch   ( void ) const { return fBranch;    }
  TString  GetTag      ( void ) const { return fTag;       }
  TString  GetUser     ( void ) const { return fUser;      }
  TString  GetDate     ( void ) const { return fDate;      }

  TString  Heads       ( void ) const;
  TString  Status      ( void ) const;
  
  // methods
  void     GetHeads    ( void );
  void     GetStatus   ( void );

  virtual void Clear   ( Option_t *option = "" );

private:
  Long64_t HexToLong   ( TString s );
  Int_t    HexToInt    ( TString s );
  
  ClassDef( RevIsion, 1 )
};

#endif

// Local Variables:
// mode:C++
// End:
