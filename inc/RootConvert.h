#ifndef _ROOT_CONVERT_H
#define _ROOT_CONVERT_H

#include "RevIsion.h"
#include "CmpLib.h"
#include "CmpTree.h"
#include "DataHandler.h"

#include "TFile.h"

#include <fstream>

class RootConvert {

protected:
  RevIsion     fRevision;
  
  DataHandler  fData;
  Double_t     fFileSize;
  Double_t     fReadSize;

  // Exp, Run number
  Int_t        fExpNum;
  Int_t        fRunNum;

  // ROOT file
  Int_t        fTFcnt;
  TString      fTFname;
  TFile       *fTFile;
  TTree       *fTree;

  // ComPair Tree class;
  CmpTree     *fCtree;
  Int_t        fBrFlag;
  
  static Int_t fKill;

 public:
  RootConvert();
  virtual ~RootConvert();

  // access functions
  Double_t GetFsize( void ) const { return fFileSize; }
  Int_t    GetNfile( void ) const { return fData.GetNfile(); }

  // methods
  Int_t Init ( void );
  Int_t Add  ( const char *in_fname );
  Int_t Open ( const char *tr_fname, Int_t flag = CmpLib::ALLEVT );
  Int_t Loop ( Int_t sel, Int_t nproc = -1 );
  Int_t Stat ( Int_t nevt, Int_t nsel, Int_t nproc, Int_t ret, Int_t intv );
  Int_t Write( void );

  Double_t GetMemory( void );
  Double_t GetFree  ( const char *path );
  
  static void SigHandler( int sig );
};

#endif

// Local Variables:
// mode:C++
// End:
