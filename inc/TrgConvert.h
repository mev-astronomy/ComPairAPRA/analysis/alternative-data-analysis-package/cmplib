#ifndef _TRG_CONVERT_H
#define _TRG_CONVERT_H

#include "DataConvert.h"

class TrgConvert : public DataConvert {
  
protected:
  unsigned char *fBuffer;

public:
  TrgConvert( void );
  TrgConvert( UShort_t atrb );
  virtual ~TrgConvert();

  // methods
  virtual Int_t Read ( void );
  virtual void  Clear( Option_t *option = "" );
};

#endif

// Local Variables:
// mode:C++
// End:
