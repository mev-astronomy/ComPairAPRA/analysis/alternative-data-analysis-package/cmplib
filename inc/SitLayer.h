#ifndef _SIT_LAYER_H
#define _SIT_LAYER_H

#include "SitLib.h"
#include "SitStrip.h"

#include "DetBranch.h"
#include "TObject.h"
#include "TClonesArray.h"

#include <map>

// SitLayer
//  a base class which stores SiT Layer data as a branch

class SitLayer : public DetBranch {

protected:
  Int_t         fCoord;                 // SiT Coord ID

  Bool_t        fFlag[SitLib::N_STRIP]; // Si-Strip Hit Flag
  Double_t      fEdep[SitLib::N_STRIP]; // Si-Strip Hit Energy Deposit
  TVector3      fHpos[SitLib::N_STRIP]; // Si-Strip Hit Position
  
  TClonesArray *fArray;                 // Array of SitStrip
  
public:
  SitLayer( void );
  SitLayer( Int_t slice, Int_t coord );
  virtual ~SitLayer();

  // access functions
  Int_t         GetCoord ( void ) const { return fCoord; }

  Bool_t        GetFlag  ( Int_t sid ) const;
  Double_t      GetEdep  ( Int_t sid ) const;
  TVector3      GetHpos  ( Int_t sid ) const;

  TClonesArray *GetArray ( void ) const { return fArray; }
  SitStrip     *GetStrip ( Int_t hid );
  Int_t         NhitStrip( void ) const { return fEne.size(); }
  Int_t         FindHitID( Int_t sid );

  // methods
  void          SetCoord ( Int_t coord ) { fCoord = coord; }
  void          SetFlag  ( Int_t sid, Bool_t   flag );
  void          SetEdep  ( Int_t sid, Double_t edep );
  void          SetHpos  ( Int_t sid, TVector3 hpos );
  void          SetHit   ( Int_t idx, Double_t edep, TVector3 hpos );
  
  SitLayer& operator= (const SitLayer &sit);
  
  virtual void  Process  ( Int_t slice );
  virtual void  ProcHit  ( Int_t slice );
  virtual void  CopyHit  ( void );
  virtual void  Clear( Option_t *option = "" );
  virtual void  Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( SitLayer, 1 )  // Layer branch base class
};

#endif

// Local Variables:
// mode:C++
// End:
