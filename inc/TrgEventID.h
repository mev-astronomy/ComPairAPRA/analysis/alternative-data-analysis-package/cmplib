#ifndef _TRG_EVENTID_H
#define _TRG_EVENTID_H

#include "DataEventID.h"

class TrgEventID : public DataEventID {
  
protected:
  unsigned char *fBuffer;

  Double_t fUtcTime;
  UInt_t   fEventID;
  UInt_t   fTmStamp;
  UInt_t   fTimePps;

  Double_t fReqTime;
  Double_t fSysClock;
  
public:
  TrgEventID( void );
  TrgEventID( UShort_t atrb );
  virtual ~TrgEventID();

  // access function
  Double_t GetReqTime ( void ) const { return fReqTime;  }
  Double_t GetSysClock( void ) const { return fSysClock; }
  
  // methods
  void SetReqTime ( Double_t time  ) { fReqTime  = time;  }
  void SetSysClock( Double_t clock ) { fSysClock = clock; }

  void SetUtcTime( void );
  void SetEventID( void );
  void SetTmStamp( void );
  void SetTimePps( void );
  
  virtual Int_t Branch ( void );
  virtual Int_t Read   ( void );
  virtual Int_t Process( void );
  virtual void  Clear( Option_t *option = "" );
};

#endif

// Local Variables:
// mode:C++
// End:
