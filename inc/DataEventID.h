#ifndef _DATA_EVENTID_H
#define _DATA_EVENTID_H

#include "Packet.h"

#include "Rtypes.h"
#include "TList.h"
#include "TTree.h"

#include <vector>
#include <fstream>

class DataEventID {

protected:
  UShort_t      fAtrb;
  Int_t         fSize;
  time_t        fTstart;
  double        fToffset;
  
  std::vector< UInt_t > fVevid;
  std::vector< UInt_t > fVepch;
  std::vector< UInt_t > fVepns;
  
  std::ifstream fIN;
  std::ofstream fOUT;
  std::ofstream fERR;
  Double_t      fFileSize;
  TList         fList;
  TTree        *fTree;

  std::string   fOutFname;
  
  static Int_t  fKill;
  
public:
  DataEventID( void );
  DataEventID( UShort_t atrb );
  virtual ~DataEventID();

  // access functions
  Int_t    GetNfile  ( void ) const { return fList.GetSize(); }
  Double_t GetFsize  ( void ) const { return fFileSize; }
  Int_t    GetSize   ( void ) const { return fSize; }
  UShort_t GetAtrb   ( void ) const { return fAtrb; }
  time_t   GetTstart ( void ) const { return fTstart;  }
  Double_t GetToffset( void ) const { return fToffset; }
  
  // methods
  Int_t    Add    ( const char *in_fname );
  Int_t    Open   ( const char *tr_fname );
  Int_t    Close  ( void );
  Int_t    Loop   ( Int_t nproc = -1 );
  Int_t    Stat   ( void );
  Int_t    Write  ( void );
  Int_t    Rename ( void );

  virtual void  SetTimeOffset( double oft ) { fToffset = oft; }
  virtual Int_t Branch ( void ) = 0;
  virtual Int_t Process( void ) = 0;
  virtual Int_t Read   ( void ) = 0;
  virtual void  Clear( Option_t *option = "" );
  
  Double_t GetMemory( void );
  Double_t GetFree  ( const char *path );

  static void SigHandler( int sig );
};

#endif

// Local Variables:
// mode:C++
// End:
