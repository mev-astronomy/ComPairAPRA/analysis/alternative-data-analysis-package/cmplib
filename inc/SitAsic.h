#ifndef _SIT_ASIC_H
#define _SIT_ASIC_H

#include "TObject.h"
#include "TTree.h"

#include "SitLib.h"

class SitAsic : public TObject {
private:
  Byte_t    fAsicNum;               // Asic Number
  Byte_t    fAsicTBA;               // Asic TBD A
  UShort_t  fAsicHdr;               // Asic Header
  UInt_t    fNmissEv;               // Missing Event Counter
  UInt_t    fTrigPrd;               // Triggers Produced
  ULong64_t fAsicTBD;               // Asic TBD
  UShort_t  fRefData;               // Dummy channel pedstal
  UShort_t  fData[SitLib::N_CHAN];  // This is the raw data produced by the asic

public:
  SitAsic( void );
  virtual ~SitAsic();
  
  Byte_t    GetAsicNum( void ) const { return fAsicNum; }
  Byte_t    GetAsicTBA( void ) const { return fAsicTBA; }
  UShort_t  GetAsicHdr( void ) const { return fAsicHdr; }
  UInt_t    GetNmissEv( void ) const { return fNmissEv; }
  UInt_t    GetTrigPrd( void ) const { return fTrigPrd; }
  ULong64_t GetAsicTBD( void ) const { return fAsicTBD; }
  UShort_t  GetRefData( void ) const { return fRefData; }
  UShort_t  GetData   ( UShort_t ch ) const;
  
  Int_t     readAsic  ( unsigned char *buf, Bool_t speak = kFALSE );
  Bool_t    readAsic  ( std::ifstream *fin, Bool_t speak = kFALSE );
  void      SetAsicNum( Byte_t    nm ) { fAsicNum = nm; }
  void      SetAsicTBA( Byte_t    tb ) { fAsicTBA = tb; }
  void      SetAsicHdr( UShort_t  hd ) { fAsicHdr = hd; }
  void      SetNmissEv( UInt_t    nm ) { fNmissEv = nm; }
  void      SetTrigPrd( UInt_t    tp ) { fTrigPrd = tp; }
  void      SetAsicTBD( ULong64_t tb ) { fAsicTBD = tb; }

  UInt_t    convData  ( unsigned char *data );
  ULong64_t convLong  ( unsigned char *data );
  void      convAsic  ( unsigned char *data );
  
  virtual void Clear( Option_t *option = "" );

  ClassDef( SitAsic, 2 )
};

#endif

// Local Variables:
// mode:C++
// End:
