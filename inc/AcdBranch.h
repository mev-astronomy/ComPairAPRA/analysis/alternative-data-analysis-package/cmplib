#ifndef _ACD_BRANCH_H
#define _ACD_BRANCH_H

#include "CmpLib.h"

#include "TObject.h"

// AcdBranch
//  a base class which stores ACD data as a branch

class AcdBranch : public TObject {

protected:
  
public:
  AcdBranch( void );
  virtual ~AcdBranch();

  Int_t    Process  ( Int_t sel = CmpLib::ALL );
  
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( AcdBranch, 1 )  // ACD branch class
};

#endif

// Local Variables:
// mode:C++
// End:
