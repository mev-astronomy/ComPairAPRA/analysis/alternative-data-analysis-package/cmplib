#ifndef _CZT_CRYST_H
#define _CZT_CRYST_H

#include "CztLib.h"

#include "TObject.h"

// CztCryst
//  a class which stores czt crystal data

class CztCryst : public TObject {

 protected:
  Int_t    fCztbID;
  Double_t fEnergy[CztLib::N_TYPE][CztLib::N_POLA];
  Double_t fTiming[CztLib::N_TYPE][CztLib::N_POLA];

  Double_t fHitEne;
  Double_t fHitTim;
  TVector3 fHitPos;

 public:
  CztCryst( void );
  CztCryst( Int_t id );
  virtual ~CztCryst();

  // access functions
  Double_t GetHitX  ( Int_t id, Int_t ip );
  Double_t GetHitY  ( Int_t id, Int_t ip );
  Double_t GetHitZ  ( Int_t id, Int_t ip );

  Int_t    GetCztbID( void ) const { return fCztbID; }
  Double_t GetEnergy( Int_t ityp, Int_t ipol = CztLib::POSI ) const;
  Double_t GetTiming( Int_t ityp, Int_t ipol = CztLib::POSI ) const;

  Int_t    GetNhitPad( void ) const;
  Double_t GetHitEne( void ) const { return fHitEne; }
  Double_t GetHitTim( void ) const { return fHitTim; }
  TVector3 GetHitPos( void ) const { return fHitPos; }
  TVector3 GetHitPos( Int_t id, Int_t ip );

  // meshotds
  CztCryst& operator = (const CztCryst &);
  void Process  ( Int_t id );
  void SetCztbID( Int_t id );
  void SetEnergy( Int_t ityp, Int_t ipol, Double_t ene );
  void SetTiming( Int_t ityp, Int_t ipol, Double_t tim );

  void SetHitEne( Double_t ene ) { fHitEne = ene; }
  void SetHitTim( Double_t tim ) { fHitTim = tim; }
  void SetHitPos( TVector3 pos ) { fHitPos = pos; }
  
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" )  const;

  // class definition for ROOT
  ClassDef( CztCryst, 4 )  // CztCryst class
};

inline CztCryst& CztCryst::operator = ( const CztCryst & czt )
{
  fCztbID = czt.fCztbID;
  for ( Int_t ityp = 0; ityp < CztLib::N_TYPE; ityp++ ) {
    for ( Int_t ipol = 0; ipol < CztLib::N_POLA; ipol++ ) {
      fEnergy[ityp][ipol] = czt.fEnergy[ityp][ipol];
      fTiming[ityp][ipol] = czt.fTiming[ityp][ipol];
    }
  }
  fHitEne = czt.fHitEne;
  fHitTim = czt.fHitTim;
  fHitPos = czt.fHitPos;

  return *this;
}

#endif

// Local Variables:
// mode:C++
// End:
