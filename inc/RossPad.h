#ifndef _ROSS_PAD_H
#define _ROSS_PAD_H

#include "BasePacket.h"

#include "Rtypes.h"

#include <fstream>

class RossPad {
 public:
  enum ENums { N_ASIC = 4, N_CHAN = 16, N_XPOS = 8, N_YPOS = 8 };
  enum EByte { B_DATA = 164 };
  
 protected:
  unsigned int  fSize;   //! binary byte size
  unsigned char fBuffer[B_DATA]; //! Raw binary data
  
  Byte_t   f1stByte;    // temporary 1st byte
  Byte_t   f2ndByte;    // temporary 2nd byte
  
  UShort_t fFrameCount; // the ROSSPAD designated index (zeros every XXX events)

  Byte_t   f3rdByte;    // temporary 3rd byte
  Byte_t   f4thByte;    // temporary 4th byte
  Byte_t   f5thByte;    // temporary 6th byte
  Byte_t   f6thByte;    // temporary 6th byte

  UInt_t   fTimeStamp;  // Timestamp clock

  UShort_t f1stWord;    // temporary 1st word
  UShort_t f2ndWord;    // temporary 2nd word
  UShort_t f3rdWord;    // temporary 3rd word

  UInt_t   fGPIO;       // User defined parameter
  UInt_t   f1stDword;   // temporary 1st Dword

  UShort_t fData[N_XPOS][N_YPOS]; // Sensor data

  UInt_t   fDataType;   // data type
  UInt_t   fTimeDiff;   // time stamp difference
  
public:
  RossPad();
  virtual ~RossPad();

  // access functions
  unsigned int   GetSize  ( void ) const { return fSize;     }
  unsigned char *GetBuffer( void ) { return &fBuffer[0]; }

  Byte_t   Get1stByte   ( void ) const { return f1stByte;    }
  Byte_t   Get2ndByte   ( void ) const { return f2ndByte;    }
  UShort_t GetFrameCount( void ) const { return fFrameCount; }
  Byte_t   Get3rdByte   ( void ) const { return f3rdByte;    }
  Byte_t   Get4thByte   ( void ) const { return f4thByte;    }
  Byte_t   Get5thByte   ( void ) const { return f5thByte;    }
  Byte_t   Get6thByte   ( void ) const { return f6thByte;    }
  UInt_t   GetTimeStamp ( void ) const { return fTimeStamp;  }
  UShort_t Get1stWord   ( void ) const { return f1stWord;    }
  UShort_t Get2ndWord   ( void ) const { return f2ndWord;    }
  UShort_t Get3rdWord   ( void ) const { return f3rdWord;    }
  UInt_t   GetGPIO      ( void ) const { return fGPIO;       }
  UInt_t   Get1stDword  ( void ) const { return f1stDword;   }
  UShort_t GetData      ( Byte_t x, Byte_t y ) const;

  UInt_t   GetDataType  ( void ) const { return fDataType;   }
  UInt_t   GetTimeDiff  ( void ) const { return fTimeDiff;   }

  // methods
  Int_t ReadRossPad  ( std::ifstream *fin  );
  Int_t FillRossPad  ( BasePacket *pkt, Bool_t speak = kFALSE );
  void  SetRossPad   ( UInt_t dt, UInt_t td );

  void  Set1stByte   ( Byte_t   data ) { f1stByte    = data; }
  void  Set2ndByte   ( Byte_t   data ) { f2ndByte    = data; }
  void  SetFrameCount( UShort_t data ) { fFrameCount = data; }
  void  Set3rdByte   ( Byte_t   data ) { f3rdByte    = data; }
  void  Set4thByte   ( Byte_t   data ) { f4thByte    = data; }
  void  Set5thByte   ( Byte_t   data ) { f5thByte    = data; }
  void  Set6thByte   ( Byte_t   data ) { f6thByte    = data; }
  void  SetTimeStamp ( UInt_t   data ) { fTimeStamp  = data; }
  void  Set1stWord   ( UShort_t data ) { f1stWord    = data; }
  void  Set2ndWord   ( UShort_t data ) { f2ndWord    = data; }
  void  Set3rdWord   ( UShort_t data ) { f3rdWord    = data; }
  void  SetGPIO      ( UInt_t   data ) { fGPIO       = data; }
  void  Set1stDword  ( UInt_t   data ) { f1stDword   = data; }
  void  SetData      ( Byte_t x, Byte_t y, UShort_t data );
  void  SetDataType  ( UInt_t   data ) { fDataType   = data; }
  void  SetTimeDiff  ( UInt_t   data ) { fTimeDiff   = data; }

  virtual void Clear ( void );
};

#endif
