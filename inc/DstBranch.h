#ifndef _DST_BRANCH_H
#define _DST_BRANCH_H

#include "CmpLib.h"
#include "HitBranch.h"

#include "TObject.h"
#include "TClonesArray.h"

#include <vector>

// DstBranch
//  a base class which stores dst data as a branch

class DstBranch : public TObject {

protected:
  std::vector< Int_t > fHitID[CmpLib::N_DET][CmpLib::N_LYR]; // Stores HitID for each Detector

  Int_t                fNhits;     // Number of HitBranch
  TClonesArray        *fHitArray;  // Array of HitBranch

  Double_t             fEvtEne;    // Energy of the event [keV]
  Double_t             fEvtAng;    // (Compton) Scattering Angle [rad]
  TVector3             fEvtDir;    // Direction vector ( unit verctor ) [mm]
  TVector3             fEvtPer;    // Perihelion point from origin( 0, 0, 0 ) [mm]
  
public:
  DstBranch( void );
  virtual ~DstBranch();

  // access functions
  Int_t      GetNhits ( Int_t did ) const;
  Int_t      GetNhits ( Int_t did, Int_t lyr ) const;
  Int_t      GetHitID ( Int_t did, Int_t lyr, Int_t hit ) const;
  HitBranch *GetHit   ( Int_t hid ) const;

  Int_t      GetNhits ( void ) const { return fNhits;  }
  TClonesArray *GetHitArray( void )  { return fHitArray; }

  Double_t   GetEnergy( Int_t did ) const;
  Double_t   GetEnergy( Int_t did, Int_t lyr ) const;
  Double_t   GetEvtEne( void ) const { return fEvtEne; }
  Double_t   GetEvtAng( void ) const { return fEvtAng; }
  TVector3   GetEvtDir( void ) const { return fEvtDir; }
  TVector3   GetEvtPer( void ) const { return fEvtPer; }

  // methods
  Int_t      Process  ( Int_t sel = CmpLib::ALL );
  HitBranch *NewHit   ( Int_t did, Int_t lyr );

  void       SetEvtEne( Double_t ene );
  void       SetEvtAng( Double_t ang );
  void       SetEvtDir( TVector3 dir );
  void       SetEvtPer( TVector3 per );
  
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( DstBranch, 2 )  // Dst branch class
};

#endif

// Local Variables:
// mode:C++
// End:
