#ifndef _HIT_BRANCH_H
#define _HIT_BRANCH_H

#include "CmpLib.h"

#include "TObject.h"

// HitBranch
//  a base class which stores Hit data as a branch

class HitBranch : public TObject {

protected:
  Int_t    fIndex;         // Sensor Index
  Int_t    fHitSta;        // Hit status parameter
  Double_t fHitEne;        // Energy deposit of the hit [MeV]
  Double_t fEneErr;        // Energy deposit Error
  Double_t fHitTim;        // Hit Time [?]
  TVector3 fHitPos;        // Hit Position [mm]
  TVector3 fPosErr;        // Hit Position Error [mm]

public:
  HitBranch( void );
  virtual ~HitBranch();

  // access functions
  Int_t    GetIndex ( void ) const { return fIndex;  }
  Int_t    GetDetID ( void ) const;
  Int_t    GetLyrID ( void ) const;
  Int_t    GetPriID ( void ) const;
  Int_t    GetSecID ( void ) const;
  Int_t    GetHitSta( void ) const { return fHitSta; }
  Double_t GetHitEne( void ) const { return fHitEne; }
  Double_t GetEneErr( void ) const { return fEneErr; }
  Double_t GetHitTim( void ) const { return fHitTim; }
  TVector3 GetHitPos( void ) const { return fHitPos; }
  TVector3 GetPosErr( void ) const { return fPosErr; }
  
  // methods
  Int_t    Process  ( Int_t sel );
  void     SetIndex ( Int_t      id ) { fIndex  =   id; }
  void     SetHitSta( Int_t    hsta ) { fHitSta = hsta; }
  void     SetHitEne( Double_t edep ) { fHitEne = edep; }
  void     SetEneErr( Double_t eerr ) { fEneErr = eerr; }
  void     SetHitTim( Double_t time ) { fHitTim = time; }
  void     SetHitPos( TVector3 hpos ) { fHitPos = hpos; }
  void     SetPosErr( TVector3 perr ) { fPosErr = perr; }
  void     CopyHit  (const HitBranch* hit);
  
  const HitBranch& operator= (const HitBranch& hit);
  
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( HitBranch, 2 )  // Hit branch class
};

#endif

// Local Variables:
// mode:C++
// End:
