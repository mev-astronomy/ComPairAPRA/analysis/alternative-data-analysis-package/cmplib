#ifndef _TRK_BRANCH_H
#define _TRK_BRANCH_H

#include "CmpLib.h"
#include "SitLib.h"

#include "TObject.h"

// TrkBranch
//  a base class which stores trk data as a branch

class TrkBranch : public TObject {

protected:
  Bool_t     fFound   [CmpLib::N_COORD]; // Track found flag
  Double_t   fA       [CmpLib::N_COORD]; // Tracking Parameters
  Double_t   fB       [CmpLib::N_COORD]; // X or Y = fA*Z + fB
  Double_t   fChisq   [CmpLib::N_COORD]; // Recuded chisquare of the track fitting
  Int_t      fHitID   [CmpLib::N_COORD][CmpLib::N_SLICE];  // Hit ID in SiT used for the Track
  Int_t      fNhitsLyr[CmpLib::N_COORD]; // Number of hits Layer associated with the track
  Int_t      fNusedLyr[CmpLib::N_COORD]; // Number of hits Layer used for the track fitting
  
  Double_t   fAngle;                     // (Compton) Scattering Angle
  TVector3   fDirec;                     // Direction vector (unit vector) [mm]
  TVector3   fPerih;                     // Periheion point from the origin (0,0,0) [mm]
  
public:
  TrkBranch( void );
  virtual ~TrkBranch();

  // access functions
  Bool_t   Found    ( Int_t coord ) const;
  Double_t GetA     ( Int_t coord ) const;
  Double_t GetB     ( Int_t coord ) const;
  Int_t    GetNhits ( Int_t coord ) const;
  Int_t    GetNused ( Int_t coord ) const;
  Double_t GetChisq ( Int_t coord ) const;
  Int_t    GetHitID ( Int_t coord, Int_t slice ) const;
  TVector3 GetHitPos( Double_t z );
  
  Double_t GetAngle ( void ) const { return fAngle; }
  TVector3 GetDirec ( void ) const { return fDirec; }
  TVector3 GetPerih ( void ) const { return fPerih; }
  
  // methods
  Int_t    Process  ( Int_t sel );
  void     SetFound ( Int_t coord, Bool_t found );
  void     SetA     ( Int_t coord, Double_t   a );
  void     SetB     ( Int_t coord, Double_t   b );
  void     SetNhits ( Int_t coord, Int_t    hit );
  void     SetNused ( Int_t coord, Int_t    use );
  void     SetChisq ( Int_t coord, Double_t csq );
  void     SetHitID ( Int_t coord, Int_t  slice, Int_t id );

  void     SetAngle ( Double_t ang ) { fAngle = ang; }
  void     SetDirec ( TVector3 dir ) { fDirec = dir; }
  void     SetPerih ( TVector3 per ) { fPerih = per; }
  
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

protected:
  // protected methods
  Bool_t   FindTrack( Int_t coord, Bool_t flag = kTRUE );
  Double_t Fitting  ( Int_t coord );
  Double_t Fitting  ( Int_t coord, Int_t *nth );
  Int_t    MergeHit ( Int_t coord );
  Bool_t   PurgeHit ( Int_t coord );
  Int_t    FindHitID( Int_t coord, Int_t slice );

  // class definition for ROOT
  ClassDef( TrkBranch, 1 )  // Trk branch class
};

#endif

// Local Variables:
// mode:C++
// End:
