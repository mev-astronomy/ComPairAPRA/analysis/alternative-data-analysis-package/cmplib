#ifndef _CMP_TREE_H
#define _CMP_TREE_H

#include "CmpLib.h"
#include "BasePacket.h"

#include "TObject.h"
#include "TClonesArray.h"
#include "TTree.h"

#include <iostream>

// CmpTree
//  a class which stores DST as a tree

class TrgBranch;
class AcdBranch;
class SitBranch;
class CztBranch;
class CsIBranch;
class TrkBranch;
class CmpBranch;
class DstBranch;
class SimBranch;

class CmpTree : public TObject {

protected:
  Int_t           fExpNum;   // Exp Number
  Int_t           fRunNum;   // Run Number
  
  Double_t        fUtcTime;  // UTC Time
  UInt_t          fEventID;  // Event ID
  UInt_t          fHitPatt;  // Trigger Hit Pattern
  UInt_t          fTrgMode;  // Trigger Coincidence Mode Rw
  UInt_t          fTcdMode;  // Trigger Coincidence Mode CD
  UShort_t        fPktAtrb;  // Event Packet Attribute
  UInt_t          fNdst;     // Number of Dst branch
  Int_t           fNsit;     // Number of Si-Tracker Branch
  
  TrgBranch      *fTrg;      //! TRG Branch
  AcdBranch      *fAcd;      //! ACD Branch
  TClonesArray   *fSit;      //! Array of SiT Branch
  CztBranch      *fCzt;      //! CZT Branch
  CsIBranch      *fCsI;      //! CsI Branch
  TrkBranch      *fTrk;      //! Trk Branch
  CmpBranch      *fCmp;      //! Cmp Branch
  SimBranch      *fSim;      //! SIM Branch
  
  DstBranch      *fDst;      //! Dst branch
  static CmpTree *fPtr;      //! Self pointer

 public:
  CmpTree( void );
  virtual ~CmpTree();

  // access functions
  Int_t         GetExpNum  ( void ) const { return fExpNum;  }
  Int_t         GetRunNum  ( void ) const { return fRunNum;  }
  
  Double_t      GetUtcTime ( void ) const { return fUtcTime; }
  UInt_t        GetEventID ( void ) const { return fEventID; }
  UInt_t        GetHitPatt ( void ) const { return fHitPatt; }
  UInt_t        GetTrgMode ( void ) const { return fTrgMode; }
  UInt_t        GetTcdMode ( void ) const { return fTcdMode; }
  UShort_t      GetPktAtrb ( void ) const { return fPktAtrb; }
  UInt_t        GetNdst    ( void ) const { return fNdst;    }
  Int_t         GetNsit    ( void ) const { return fNsit;    }
  
  TrgBranch    *GetTrg     ( void ) const { return fTrg;     }
  AcdBranch    *GetAcd     ( void ) const { return fAcd;     }
  TClonesArray *GetSit     ( void ) const { return fSit;     }
  SitBranch    *GetSit     ( Int_t i );
  SitBranch    *GetSitAt   ( Int_t i );
  CztBranch    *GetCzt     ( void ) const { return fCzt;     }
  CsIBranch    *GetCsI     ( void ) const { return fCsI;     }
  SimBranch    *GetSim     ( void ) const { return fSim;     }
  TrkBranch    *GetTrk     ( void ) const { return fTrk;     }
  CmpBranch    *GetCmp     ( void ) const { return fCmp;     }
  DstBranch    *GetDst     ( void ) const { return fDst;     }

  // methods
  Int_t         Init       ( void );
  Int_t         Fill       ( BasePacket *pkt );
  Int_t         FillSIT    ( BasePacket *pkt );
  Int_t         FillACD    ( BasePacket *pkt );
  Int_t         FillCZT    ( BasePacket *pkt );
  Int_t         FillCSI    ( BasePacket *pkt );
  Int_t         FillTRG    ( BasePacket *pkt );
  Int_t         FillSIM    ( BasePacket *pkt );
  Int_t         FillDAQ    ( BasePacket *pkt );
  Int_t         ConstSIT   ( void );
  void          SetExpNum  ( Int_t  exp ) { fExpNum  = exp; }
  void          SetRunNum  ( Int_t  run ) { fRunNum  = run; }
  void          SetUtcTime ( Double_t t ) { fUtcTime =   t; }
  void          SetEventID ( UInt_t  id ) { fEventID =  id; }
  void          SetHitPatt ( UInt_t hit ) { fHitPatt = hit; }
  void          SetTrgMode ( UInt_t mod ) { fTrgMode = mod; }
  void          SetTcdMode ( UInt_t mod ) { fTcdMode = mod; }
  void          SetPktAtrb ( UShort_t a ) { fPktAtrb =   a; }
  void          Branch     ( TTree *tree, Int_t bit = CmpLib::ALL );
  void          SetAddr    ( TTree *tree );
  void          SetAddr    ( TTree *tree, const char *name, void *ptr );
  Int_t         Process    ( Int_t sel = CmpLib::ALLEVT );
  Int_t         Select     ( Int_t sel = CmpLib::ALLEVT );
  Int_t         WritePar   ( void );
  void          Clear      ( Option_t *option = "" );
  void          Print      ( Option_t *option = "" ) const;

  static CmpTree    *GetPtr  ( void ) { return fPtr; }

  // class definition for ROOT
  ClassDef( CmpTree, 1 ) // ComPair Tree class
};

#endif

// Local Variables:
// mode:C++
// End:
