#ifndef _CZT_CONVERT_H
#define _CZT_CONVERT_H

#include "DataConvert.h"
#include "CztPacket.h"

class CztConvert : public DataConvert {
protected:
  CztPacket *fCztPacket;

public:
  CztConvert( void );
  CztConvert( UShort_t atrb );
  virtual ~CztConvert();
  
  virtual Int_t Read ( void );
  virtual void  Clear( Option_t *option = "" );
};

#endif

// Local Variables:
// mode:C++
// End:
