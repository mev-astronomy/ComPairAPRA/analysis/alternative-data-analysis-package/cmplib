#ifndef _CZT_EVT_HEADER_H
#define _CZT_EVT_HEADER_H

#include "CztLib.h"

#include "TObject.h"

class CztEvtHeader : public TObject {
private:
  UInt_t   fEventID;    // Event ID
  UInt_t   fTmStamp;    // Time Stamp
  UInt_t   fTimePps;    // GPS PPS Time Stamp
  UInt_t   fEpoch;      // Epoch Time second part
  UInt_t   fEpochNS;    // Epoch Time nano second part

  UInt_t   fPrStamp;    // Previous Time Stamp
  UInt_t   fTmCarry;    // Number of TmStamp Carry out
  Double_t fUtcTime;    // UTC Time

  Double_t fToffset;    // UTC Time Offset
  
public:
  CztEvtHeader( void );
  virtual ~CztEvtHeader();

  UInt_t   GetEventID ( void ) const { return fEventID; }
  UInt_t   GetTmStamp ( void ) const { return fTmStamp; }
  UInt_t   GetTimePps ( void ) const { return fTimePps; }
  UInt_t   GetEpoch   ( void ) const { return fEpoch;   }
  UInt_t   GetEpochNS ( void ) const { return fEpochNS; }

  UInt_t   GetPrStamp ( void ) const { return fPrStamp; }
  UInt_t   GetTmCarry ( void ) const { return fTmCarry; }
  Double_t GetUtcTime ( void ) const { return fUtcTime; }

  Double_t GetToffset ( void ) const { return fToffset; }

  Int_t    ReadData   ( std::ifstream *fIN,  UShort_t ver, Bool_t speak = kFALSE ); 
  Int_t    ReadData   ( unsigned char *data, UShort_t ver, Bool_t speak = kFALSE ); 

  // methods
  void     SetEventID( UInt_t   id ) { fEventID = id; }
  void     SetTmStamp( UInt_t   ts ) { fTmStamp = ts; }
  void     SetTimePps( UInt_t   tp ) { fTimePps = tp; }
  void     SetEpoch  ( UInt_t   ep ) { fEpoch   = ep; }
  void     SetEpochNS( UInt_t   en ) { fEpochNS = en; }

  void     SetPrStamp( UInt_t   ts ) { fPrStamp = ts; }
  void     SetTmCarry( UInt_t   tc ) { fTmCarry = tc; }
  void     SetUtcTime( Double_t tm ) { fUtcTime = tm; }

  void     SetToffset( Double_t tm ) { fToffset = tm; }
  
  virtual void Clear ( Option_t *option = "" );

private:
  UInt_t   conv4Time ( unsigned char *data );
  UInt_t   conv4Byte ( unsigned char *data );

  ClassDef( CztEvtHeader, 1 )
};

#endif

// Local Variables:
// mode:C++
// End:
