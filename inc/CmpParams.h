#ifndef _CMP_PARAMS_H
#define _CMP_PARAMS_H

#include "CmpLib.h"

#include "TObject.h"
#include "TVector3.h"
#include "TH2D.h"
#include "TH3D.h"

// CmpParams
//  a class which stores general parameters and readout assign data

class CmpParams : public TObject {
  
protected:
  Int_t    fIndex [CmpLib::N_DET][CmpLib::N_PRI][CmpLib::N_SEC];  // Geometry Index

  Int_t    fDetID [CmpLib::N_GEOM];  // Detector ID
  Int_t    fPriID [CmpLib::N_GEOM];  // Primary ID
  Int_t    fSecID [CmpLib::N_GEOM];  // Secondary ID
  TVector3 fOrigin[CmpLib::N_GEOM];  // Origin of Sensors
  TVector3 fDimens[CmpLib::N_GEOM];  // Dimensions of Sensors
  
public:
  CmpParams();
  virtual ~CmpParams();

  // access functions
  Int_t    GetIndex ( Int_t did, Int_t pid, Int_t sid ) const;
  Int_t    GetDetID ( Int_t idx ) const;
  Int_t    GetLyrID ( Int_t idx ) const;
  Int_t    GetPriID ( Int_t idx ) const;
  Int_t    GetSecID ( Int_t idx ) const;
  TVector3 GetOrigin( Int_t idx ) const;
  TVector3 GetDimens( Int_t idx ) const;

  // methods
  void     SetIndex ( Int_t idx, Int_t did, Int_t pid, Int_t sid );
  void     SetOrigin( Int_t idx, Double_t x, Double_t y, Double_t z );
  void     SetDimens( Int_t idx, Double_t x, Double_t y, Double_t z );
  
  // access to the global pointer
  static CmpParams  *GetPtr( void );
  static CmpParams  *ReadFile( const char *fname = FileName(), Int_t v = 1 );
  static TObject    *ReadFile( const char *fname, const char *cname );
  static const char *FileName( void ) { return "cmppar.root"; }

  // class definition for ROOT
  ClassDef( CmpParams, 1 )  // General parameters and assign data class
};

// global object
extern CmpParams *gCmpParams;

#endif

// Local Variables:
// mode:C++
// End:
