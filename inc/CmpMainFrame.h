#ifndef _CMP_MAIN_FRAME_H
#define _CMP_MAIN_FRAME_H

#include "TGFrame.h"

class EventDrawer;

class TObjArray;
class TTimer;
class TGComboBox;
class TGRadioButton;
class TGCheckButton;
class TGNumberEntry;

class CmpMainFrame : public TGMainFrame {
private:
  EventDrawer   *fDrawer;
  TObjArray     *fCarray;
  TTimer        *fTimer;
  TGComboBox    *fMode;
  TGComboBox    *fTsel;
  TGCheckButton *fTrig[4];
  TGNumberEntry *fEntry;
  TGNumberEntry *fEthre;

public:
  CmpMainFrame( const TGWindow *p, UInt_t w, UInt_t h );
  virtual ~CmpMainFrame();

  void CheckOption();
  void DrawFrame  ();
  void DrawEvent  ();
  void Jump       ();
  void Mode       ();
  void Prev       ();
  void Next       ();
  void Start      ();
  void Stop       ();
  void AutoRefresh();

  ClassDef( CmpMainFrame, 1 )
};

#endif

// Local Variables:
// mode:C++
// End:
