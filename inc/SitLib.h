#ifndef _SIT_LIB_H
#define _SIT_LIB_H

#include "TObject.h"
#include "TVector3.h"

class SitLib : public TObject {
 public:
  enum ENums  { N_SLICE = 10, N_COORD =  2, N_STRIP = 192, N_ASIC = 12, N_CHAN = 32, N_LINDX = 3840,
                N_SIDE  =  2, N_MHIT  = 16, N_MFIT  =   9, N_SLICE_Temp = 6 };
  enum ECoords{ COORD_X = 0, COORD_Y = 1 };
  enum ESides { SIDE_L  = 0, SIDE_R  = 1 };
  enum EBits  { HEAD  = BIT( 0 ), EVENT = BIT( 1 ), DET = BIT( 2 ), TRK = BIT( 3 ),
		DST   = BIT( 4 ), ALL   = 0x1F };
  enum ESels  { NOEVT  = 0, ALLEVT = 1, HITEVT = 2, TRGEVT = 3 };

  static TVector3 fVnotValid;
  static Double_t fDnotValid;
  static Float_t  fFnotValid;
  static Int_t    fThreshold;

  SitLib();
  virtual ~SitLib();

  // class definition for ROOT
  ClassDef( SitLib, 1 ) // General parameters and functions class
};

#endif

// Local Variables:
// mode:C++
// End:
