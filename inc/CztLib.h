#ifndef _CZT_LIB_H
#define _CZT_LIB_H

#include "TObject.h"
#include "TVector3.h"

// #define oldversion
#undef  oldversion

class CztLib : public TObject {
 public:
#ifdef oldversion
  enum ENums { N_ASIC = 16,  N_CZTB =  16, N_INDX = 192, N_CHAN = 105, N_DATA = 202,
	       N_MHIT = 256, N_MFIT =   9, N_TYPE =   6, N_POLA =   2, N_PADS = 4 };
  enum EByte { B_HEAD = 26, B_UDPH = 10, B_PKTH = 16, B_ASIC = 406 };
#else
  enum ENums { N_ASIC = 16,  N_CZTB =  16, N_INDX = 192, N_CHAN = 105, N_DATA = 210,
	       N_MHIT = 256, N_MFIT =   9, N_TYPE =   6, N_POLA =   2, N_PADS = 4 };
  enum EByte { B_HEAD = 38, B_UDPH = 10, B_PKTH = 16, B_ASIC = 422 }; // 2020Oct31 B_HEAD 26 --> 38
#endif
  enum EBins { N_XBIN = 16, N_YBIN = 16, N_ZBIN = 8 };
  enum EType { ANODE  = 0, CATHODE = 1, PAD0 = 2, PAD1 = 3, PAD2 = 4, PAD3 = 5 };
  enum EBits { HEAD   = BIT( 0 ), EVENT = BIT( 1 ), ASIC = BIT( 2 ), DST = BIT( 3 ), ALL = 0xF };
  enum ESels { NOEVT  = 0, ALLEVT = 1, HITEVT = 2 };
  enum EData { ENERGY = 0, TIMING = 1, POSI = 0, NEGA = 1 };

  static TVector3 fVnotValid;
  static Double_t fDnotValid;
  static Float_t  fFnotValid;
  static Int_t    fThreshold;

  static Double_t fCztWidth;
  static Double_t fCztHeight;
  static Double_t fCztLength;

  CztLib();
  virtual ~CztLib();

  // class definition for ROOT
  ClassDef( CztLib, 1 ) // General parameters and functions class
};

#endif

// Local Variables:
// mode:C++
// End:
