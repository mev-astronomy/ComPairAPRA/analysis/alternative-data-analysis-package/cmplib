#ifndef _CZT_PACKET_H
#define _CZT_PACKET_H

#include "Packet.h"
#include "CztAsic.h"
#include "CztUdpHeader.h"
#include "CztPktHeader.h"
#include "CztEvtHeader.h"
#include "BasePacket.h"

#include "TObject.h"

#include <fstream>

class CztPacket : public TObject {
protected:
  unsigned char *fBuffer;
  UShort_t       fVersion;
  UInt_t         fNpacket;
  CztUdpHeader   fUdpHeader;
  CztPktHeader   fPktHeader;
  UShort_t       fSOE;
  CztEvtHeader   fEvtHeader;
  CztAsic        fDataAsics[CztLib::N_ASIC];
  UShort_t       fEOE;
  
public:
  CztPacket( void );
  virtual ~CztPacket();

  // access functions
  unsigned char *GetBuffer   ( void ) { return fBuffer;     }
  UShort_t       GetVersion  ( void ) { return fVersion;    }
  CztUdpHeader  *GetUdpHeader( void ) { return &fUdpHeader; }
  CztPktHeader  *GetPktHeader( void ) { return &fPktHeader; }
  CztEvtHeader  *GetEvtHeader( void ) { return &fEvtHeader; }

  UInt_t   GetPktCount( void ) const { return fPktHeader.GetPktCount(); }
  UShort_t GetPktSize ( void ) const { return fPktHeader.GetPktSize (); }
  UShort_t GetRegUser ( void ) const { return fPktHeader.GetRegUser (); }
  UInt_t   GetEvtRate ( void ) const { return fPktHeader.GetEvtRate (); }
  UShort_t GetCIE     ( void ) const { return fPktHeader.GetCIE     (); }
  
  UInt_t   GetNhitAsic( void ) const { return fPktHeader.GetNhitAsic(); }
  Int_t    GetAsicID  ( UInt_t hit ) const;
  UInt_t   GetNpacket ( void ) const { return fNpacket; }
  UInt_t   GetEventID ( void ) const { return fEvtHeader.GetEventID (); }
  UInt_t   GetTmStamp ( void ) const { return fEvtHeader.GetTmStamp (); }
  UInt_t   GetTimePps ( void ) const { return fEvtHeader.GetTimePps (); }
  UInt_t   GetEpoch   ( void ) const { return fEvtHeader.GetEpoch   (); }
  UInt_t   GetEpochNS ( void ) const { return fEvtHeader.GetEpochNS (); }
  Double_t GetUtcTime ( void ) const;
  CztAsic *GetAsicData( UInt_t hit );

  UInt_t   GetPrStamp ( void ) const { return fEvtHeader.GetPrStamp (); }
  UInt_t   GetTmCarry ( void ) const { return fEvtHeader.GetTmCarry (); }
  
  // methods
  void     ChkVersion( void );
  void     SetVersion( UShort_t v ) { fVersion = v; }
  void     SetNpacket( UInt_t np ) { fNpacket = np; }
  void     SetEventID( UInt_t id );
  void     SetTmStamp( UInt_t ts );
  void     SetTimePps( UInt_t tp );
  void     SetEpoch  ( UInt_t ep );
  void     SetEpochNS( UInt_t en );

  void     SetToffset( Double_t t );
  
  Int_t    ReadPacket( std::ifstream *fIN, Bool_t speak = kFALSE );
  Int_t    Fill( BasePacket *pkt, Bool_t speak = kFALSE );

  virtual void  Clear( Option_t *option = "" );

  ClassDef( CztPacket, 1 )
};

#endif

// Local Variables:
// mode:C++
// End:
