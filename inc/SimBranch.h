#ifndef _SIM_BRANCH_H
#define _SIM_BRANCH_H

#include "CmpLib.h"

#include "TObject.h"
#include "TString.h"

#include <vector>
#include <map>

// SimBranch
//  a base class which stores Simulation True Info as a branch

class SimBranch : public TObject {

protected:
  Int_t    fParticle; // Primary Particle ID
  Int_t    fAtomNum;  // Atomic Number
  Int_t    fAtomMass; // Atomic Mass
  Double_t fEkinetic; // Kinetic energy
  TVector3 fPosition; // Position
  TVector3 fMomentum; // Momentum Direction
  std::map<Int_t,TString>  fHitPrName; // Process Name of Hit
  
  std::vector<Int_t>    fTrackID;   // Track ID
  std::vector<Int_t>    fParentID;  // Track ID
  std::vector<TString>  fProcName;  // Process Name
  std::vector<TString>  fPartName;  // Particle Name
  std::vector<Double_t> fTrajEkine; // Kinetic Energy at Interaction point
  std::vector<TVector3> fTrajPoint; // Interaction position

public:
  SimBranch( void );
  virtual ~SimBranch();

  // access functions
  Int_t    GetParticle ( void ) const { return fParticle; }
  Int_t    GetAtomNum  ( void ) const { return fAtomNum;  }
  Int_t    GetAtomMass ( void ) const { return fAtomMass; }
  Double_t GetEkinetic ( void ) const { return fEkinetic; }
  TVector3 GetPosition ( void ) const { return fPosition; }
  TVector3 GetMomentum ( void ) const { return fMomentum; }
  TString  GetHitPrName( Int_t i ) { return fHitPrName[i]; }

  UInt_t   GetNpoint   ( void )     const { return fTrackID.size(); }
  Int_t    GetTrackID  ( UInt_t i ) const { return fTrackID  [i];   }
  Int_t    GetParentID ( UInt_t i ) const { return fParentID [i];   }
  TString  GetProcName ( UInt_t i ) const { return fProcName [i];   }
  TString  GetPartName ( UInt_t i ) const { return fPartName [i];   }
  Double_t GetTrajEkine( UInt_t i ) const { return fTrajEkine[i];   }
  TVector3 GetTrajPoint( UInt_t i ) const { return fTrajPoint[i];   }
  
  // methods
  void     SetParticle ( Int_t    id   ) { fParticle = id;   }
  void     SetAtomNum  ( Int_t    num  ) { fAtomNum  = num;  }
  void     SetAtomMass ( Int_t    mass ) { fAtomMass = mass; }
  void     SetEkinetic ( Double_t ekin ) { fEkinetic = ekin; }
  void     SetPosition ( TVector3 pos  ) { fPosition = pos;  }
  void     SetMomentum ( TVector3 mom  ) { fMomentum = mom;  }
  void     SetHitPrName( Int_t idx, TString  name );

  void     SetTrackID  ( Int_t id )      { fTrackID.push_back( id );    }
  void     SetParentID ( Int_t id )      { fParentID.push_back( id );   }
  void     SetProcName ( TString name  ) { fProcName.push_back( name ); }
  void     SetPartName ( TString name  ) { fPartName.push_back( name ); }
  void     SetTrajEkine( Double_t ene  ) { fTrajEkine.push_back( ene ); }
  void     SetTrajPoint( TVector3 pos  ) { fTrajPoint.push_back( pos ); }
  
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( SimBranch, 1 )  // Sim branch class
};

#endif

// Local Variables:
// mode:C++
// End:
