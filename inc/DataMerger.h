#ifndef _DATA_MERGER_H
#define _DATA_MERGER_H

#include "TList.h"

#include <fstream>
#include <map>

class DataHandler;

class DataMerger {
public:
  typedef std::map< UShort_t, DataHandler* >::iterator it_type;

protected:
  time_t   fTstart;
  UShort_t fAtrb;
  std::map< UShort_t, DataHandler* > fData; // < atrb, datahandler >

  std::ofstream fOUT;
  std::string   fOutFname;

  static Int_t fKill;
  
public:
  DataMerger( void );
  virtual ~DataMerger();

  // methods
  Int_t SetAtrb( UShort_t atrb );
  Int_t DelAtrb( UShort_t atrb );
  Int_t Open   ( const char *fname );
  Int_t Close  ( void );
  Int_t Add    ( UShort_t atrb, const char *fname );
  void  Loop   ( Int_t nproc );
  Int_t Rename ( void );
  
  virtual void  ReadEvent  ( void );
  virtual Int_t WriteEvent ( void );

  virtual void Clear( Option_t *option = "" );
  
  static void SigHandler( int sig );
};

#endif

// Local Variables:
// mode:C++
// End:
