#ifndef _DATA_CONVERT_H
#define _DATA_CONVERT_H

#include "Packet.h"
#include "BasePacket.h"

#include "Rtypes.h"
#include "TList.h"

#include <fstream>

class DataConvert {

protected:
  UShort_t      fAtrb;
  Int_t         fSize;
  UInt_t        fEventID;
  UInt_t        fEpoch;
  UInt_t        fEpochNS;
  time_t        fTstart;
  
  std::ifstream fIN;
  std::ifstream fEID;
  std::ofstream fOUT;
  std::ofstream fERR;
  Double_t      fFileSize;
  TList         fList;
  BasePacket   *fPacket;
  std::string   fEidFname;
  std::string   fOutFname;

  unsigned char fIdBuff[12];

  static Int_t  fKill;
  
public:
  DataConvert( void );
  DataConvert( UShort_t atrb );
  virtual ~DataConvert();

  // access functions
  Int_t    GetNfile  ( void ) const { return fList.GetSize(); }
  Double_t GetFsize  ( void ) const { return fFileSize; }
  Int_t    GetSize   ( void ) const { return fSize; }
  UShort_t GetAtrb   ( void ) const { return fAtrb; }
  UInt_t   GetEventID( void ) const { return fEventID; }
  UInt_t   GetEpoch  ( void ) const { return fEpoch;   }
  UInt_t   GetEpochNS( void ) const { return fEpochNS; }
  time_t   GetTstart ( void ) const { return fTstart;  }
  
  // methods
  void     SetEventID( void );
  void     SetEpoch  ( void );
  void     SetEpochNS( void );

  Int_t    Add   ( const char *in_fname );
  Int_t    Open  ( const char *id_fname, const char *tr_fname );
  Int_t    Close ( void );
  Int_t    Loop  ( Int_t nproc = -1 );
  Int_t    Stat  ( void );
  Int_t    Write ( void );
  Int_t    Rename( void );
  
  virtual Int_t Read ( void ) = 0;
  virtual void  Clear( Option_t *option = "" );
  
  Double_t GetMemory( void );
  Double_t GetFree  ( const char *path );

  static void SigHandler( int sig );
};

#endif

// Local Variables:
// mode:C++
// End:
