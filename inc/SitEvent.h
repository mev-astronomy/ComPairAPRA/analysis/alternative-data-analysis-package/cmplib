#ifndef _SIT_EVENT_H
#define _SIT_EVENT_H

#include "SitLib.h"
#include "SitAsic.h"
#include "BasePacket.h"

#include "TObject.h"

#include <fstream>

class SitEvent : public TObject {
public:
  
protected:
  Double_t  fUtcTime;                // UTC Time
  Double_t  fUtcCorr;                // UTC Corrected Time
  UInt_t    fTmStamp;                // Time Stamp
  UInt_t    fTimePps;                // Time Stamp at Gps PPS

  UShort_t  fPktSize;                // Total size of packet, including this field
  Byte_t    fPktType;                // Packet Type
  Byte_t    fLayerNm;                // Layer Number
  ULong64_t fSysTime;                // System Time
  Byte_t    fTrigCon;                // Triggering condition
  UShort_t  fVataEna;                // vata ro enable
  Byte_t    fPlDataH;                // pl data header
  UInt_t    fEventCt;                // Event count
  UInt_t    fEventID;                // Event ID
  UInt_t    fNgpsPps;                // GPS PPS count
  UInt_t    fTmSince;                // time since last pps pulse
  ULong64_t fLivTime;                // Live time
  ULong64_t fRunTime;                // Running time
  ULong64_t fHeadTBD;                // header TBD
  SitAsic   fAsic[SitLib::N_ASIC];   // asic data
  
public:
  SitEvent();
  virtual ~SitEvent();
  
  // access functions
  Double_t  GetUtcTime( void ) const { return fUtcTime; }
  Double_t  GetUtcCorr( void ) const { return fUtcCorr; }
  UInt_t    GetTmStamp( void ) const { return fTmStamp; }
  UInt_t    GetTimePps( void ) const { return fTimePps; }

  UShort_t  GetPktSize( void ) const { return fPktSize; }
  Byte_t    GetPktType( void ) const { return fPktType; }
  Byte_t    GetLayerNm( void ) const { return fLayerNm; }
  ULong64_t GetSysTime( void ) const { return fSysTime; }
  Byte_t    GetTrigCon( void ) const { return fTrigCon; }
  UShort_t  GetVataEna( void ) const { return fVataEna; }
  Byte_t    GetPlDataH( void ) const { return fPlDataH; }
  UInt_t    GetEventCt( void ) const { return fEventCt; }
  UInt_t    GetEventID( void ) const { return fEventID; }
  UInt_t    GetNgpsPps( void ) const { return fNgpsPps; }
  UInt_t    GetTmSince( void ) const { return fTmSince; }
  ULong64_t GetLivTime( void ) const { return fLivTime; }
  ULong64_t GetRunTime( void ) const { return fRunTime; }
  ULong64_t GetHeadTBD( void ) const { return fHeadTBD; }
  SitAsic  *GetAsic   ( UShort_t aid );
  
  // methods
  void      SetUtcTime( Double_t    t ) { fUtcTime =  t; }
  void      SetUtcCorr( Double_t    t ) { fUtcCorr =  t; }
  void      SetTmStamp( UInt_t     ts ) { fTmStamp = ts; }
  void      SetTimePps( UInt_t     tp ) { fTimePps = tp; }

  void      SetPktSize( UShort_t  siz ) { fPktSize = siz; }
  void      SetPktType( Byte_t    typ ) { fPktType = typ; }
  void      SetLayerNm( Byte_t    num ) { fLayerNm = num; }
  void      SetSysTime( ULong64_t tim ) { fSysTime = tim; }
  void      SetTrigCon( Byte_t    con ) { fTrigCon = con; }
  void      SetVataEna( UShort_t  ena ) { fVataEna = ena; }
  void      SetPlDataH( Byte_t    pld ) { fPlDataH = pld; }
  void      SetEventCt( UInt_t    cnt ) { fEventCt = cnt; }
  void      SetEventID( UInt_t    eid ) { fEventID = eid; }
  void      SetNgpsPps( UInt_t    pps ) { fNgpsPps = pps; }
  void      SetTmSince( UInt_t    tim ) { fTmSince = tim; }
  void      SetLivTime( ULong64_t tim ) { fLivTime = tim; }
  void      SetRunTime( ULong64_t tim ) { fRunTime = tim; }
  void      SetHeadTBD( ULong64_t tbd ) { fHeadTBD = tbd; }
    
  UShort_t  convData  ( unsigned char *data );
  UInt_t    conv4Byte ( unsigned char *data );
  Double_t  convTime  ( unsigned char *time );

  Int_t     Fill      ( BasePacket *pkt );
  Bool_t    readPacket( std::ifstream *fin,  Bool_t speak = kFALSE );
  Int_t     readPacket( unsigned char *data, Bool_t speak = kFALSE );
  
  void      SetEvent  ( unsigned char *data );
  virtual void Clear  ( Option_t *option = "" );
  
  ClassDef( SitEvent, 2 )
};

#endif

// Local Variables:
// mode:C++
// End:
