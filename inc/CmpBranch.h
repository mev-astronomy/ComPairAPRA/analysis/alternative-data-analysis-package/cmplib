#ifndef _CMP_BRANCH_H
#define _CMP_BRANCH_H

#include "CmpLib.h"

#include "TObject.h"

#include <vector>

// CmpBranch
//  a base class which stores compton event data as a branch

class CmpBranch : public TObject {
public:
  const Double_t mec2 = 0.51099895;
  
protected:
  TVector3 fP0;       // position of gamma-ray source
  TVector3 fP1;       // photon scattered position
  TVector3 fP2;       // photon absorbed position

  Double_t fE0;       // source gamma-ray energy
  Double_t fE1;       // deposit energy at scattered point 
  Double_t fE2;       // deposit energy at absorbed point

  Bool_t   fSingle;   // single compton flag
  Bool_t   fDouble;   // double compton flag

  Int_t    fPairing;  // Compton detector pair
  
  Int_t    fNhitSit;  // Number of hit Si-Tracker slice
  Int_t    fNhitCzt;  // Number of CZT Crystal hit
  Int_t    fNhitCsI;  // Number of CsI Crystal hit

  Double_t fConeZen;  // Zenith angle of Cone
  Double_t fConeAzi;  // Azimuth angle of Cone
  Double_t fConeAng;  // Apex Angle of Cone
  Double_t fConePhi;  // angle of cone circle

  std::vector< Int_t > fHitSlice;  // hit slice number
  Double_t fEdep[CmpLib::N_SLICE]; // hit slice energy
  TVector3 fHpos[CmpLib::N_SLICE]; // hit slice position

  Double_t fEdepCzt;               // hit CZT energy
  Double_t fEdepCsI;               // hit CsI energy
  TVector3 fHposCzt;               // hit CZT position
  TVector3 fHposCsI;               // hit CsI position
  
public:
  CmpBranch( void );
  virtual ~CmpBranch();

  // access functions
  TVector3 GetP0      ( void ) const { return fP0;      }
  TVector3 GetP1      ( void ) const { return fP1;      }
  TVector3 GetP2      ( void ) const { return fP2;      }

  Double_t GetE0      ( void ) const { return fE0;      }
  Double_t GetE1      ( void ) const { return fE1;      }
  Double_t GetE2      ( void ) const { return fE2;      }

  Bool_t   Compton    ( void ) const { return fSingle || fDouble; }
  Bool_t   SingleComp ( void ) const { return fSingle;  }
  Bool_t   DoubleComp ( void ) const { return fDouble;  }
  
  Int_t    GetPairing ( void ) const { return fPairing; }
  Int_t    GetNhitSit ( void ) const { return fNhitSit; }
  Int_t    GetNhitCzt ( void ) const { return fNhitCzt; }
  Int_t    GetNhitCsI ( void ) const { return fNhitCsI; }

  Double_t GetConeZen ( void ) const { return fConeZen; }
  Double_t GetConeAzi ( void ) const { return fConeAzi; }
  Double_t GetConeAng ( void ) const { return fConeAng; }
  Double_t GetConePhi ( void ) const { return fConePhi; }

  Double_t GetRadDist ( void ) const;
  
  // methods
  Int_t    Process    ( Int_t sel = CmpLib::ALL );

  void     FindNhitSit( void );
  void     FindNhitCzt( void );
  void     FindNhitCsI( void );
  
  void     SetP0      ( TVector3 pos ) { fP0 = pos; }
  void     SetP1      ( TVector3 pos ) { fP1 = pos; }
  void     SetP2      ( TVector3 pos ) { fP2 = pos; }

  void     SetE0      ( Double_t ene ) { fE0 = ene; }
  void     SetE1      ( Double_t ene ) { fE1 = ene; }
  void     SetE2      ( Double_t ene ) { fE2 = ene; }

  void     SetSingle  ( Bool_t  comp ) { fSingle  = comp; }
  void     SetDouble  ( Bool_t  comp ) { fDouble  = comp; }

  void     SetPairing ( Int_t   pair ) { fPairing = pair; }
  
  void     SetNhitSit ( Int_t   nhit ) { fNhitSit = nhit; }
  void     SetNhitCzt ( Int_t   nhit ) { fNhitCzt = nhit; }
  void     SetNhitCsI ( Int_t   nhit ) { fNhitCsI = nhit; }

  void     SetConeZen ( Double_t zen ) { fConeZen = zen; }
  void     SetConeAzi ( Double_t azi ) { fConeAzi = azi; }
  void     SetConeAng ( Double_t ang ) { fConeAng = ang; }
  void     SetConePhi ( Double_t phi ) { fConePhi = phi; }

  void     SetConeAng ( void );
  TVector3 FindP0     ( Double_t phi );
  TVector3 FindP0     ( TVector3 pos );

  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( CmpBranch, 2 )  // Cmp branch class
};

#endif

// Local Variables:
// mode:C++
// End:
