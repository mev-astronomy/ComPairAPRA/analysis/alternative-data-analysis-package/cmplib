#ifndef _BASE_PACKET_H
#define _BASE_PACKET_H

#include "Packet.h"

#include "Rtypes.h"

#include <fstream>

class BasePacket {
 protected:
  int                fCapa;          // Buffer size in bytes
  unsigned char      buf_size_a[2];  // Packet size put on the first
  unsigned char      buf_size_b[2];  // Packet size put on the last
  struct DataHeader *fHead;          // Head portion beginning ptr
  unsigned char     *fData;          // Data portion beginning ptr
  unsigned char     *fBuff;          // Total Buffer ptr
  
 public:
  BasePacket();
  virtual ~BasePacket();

  int    GetCapa ( void ) const { return fCapa; }
  int    GetSize ( void );
  int    GetAtrb ( void );
  double GetTime ( void );

  struct DataHeader *GetHead( void ) { return fHead; }
  unsigned char     *GetData( void ) { return fData; }
  unsigned char     *GetBuff( void ) { return fBuff; }
  int GetData( unsigned char *buf, int offset, int size );
  
  UInt_t GetEvid ( void );
  UInt_t GetEpch ( void );
  UInt_t GetEpns ( void );
  
  int    ReadPacket ( unsigned char *buf,  int offset );
  int    ReadPacket ( std::ifstream *fin,  bool speak = kFALSE );
  int    WritePacket( std::ofstream *fout, bool speak = kFALSE );

  void   SetSize ( UShort_t size );
  void   SetAtrb ( UShort_t atrb );
  void   SetEvid ( UInt_t   evid );
  void   SetEpch ( UInt_t   epch );
  void   SetEpns ( UInt_t   epns );
  void   SetHead ( struct DataHeader head );
  void   SetData ( UShort_t size, unsigned char *data );

  virtual void Clear( Option_t *option = "" );
  
protected:
  void   capaAlloc( int capa );
  double timeConvert( void );
  int    atrbConvert( void );
  int    sizeConvert( unsigned char buf[2] );
};

#endif

// Local Variables:
// mode:C++
// End:
