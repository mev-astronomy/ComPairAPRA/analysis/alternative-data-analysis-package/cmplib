#ifndef _CZT_EVNT_H
#define _CZT_EVNT_H

#include "CztAsic.h"
#include "CztPacket.h"
#include "BasePacket.h"

#include "TObject.h"

class CztEvent : public TObject {
private:
  CztPacket *fCztPacket;            //! Cztpacket
  
  Double_t fTime;                   // UtcTime

  UInt_t   fPacketCount;            // Packet Count
  UShort_t fPacketSize;             // Packet Size
  UShort_t fUserRegister;           // User Register
  UInt_t   fEventRate;              // Event Rate
  UShort_t fChipInEvent;            // Chip In Event

  UInt_t   fEventID;                // EventID
  UInt_t   fTmStamp;                // Time Stamp
  UInt_t   fTimePps;                // Time Stamp at Gps PPS

  UInt_t   fEpoch;                  // Epoch time
  UInt_t   fEpochNS;                // Epoch nano seconds

  UInt_t   fNhitAsic;               // Number of Hit ASIC
  Int_t    fAsicID[CztLib::N_ASIC]; // Hit ASIC ID
  CztAsic  fAsic  [CztLib::N_ASIC]; // Hit ASIC data

  UInt_t   fPrStamp;                // Previous Time Stamp
  UInt_t   fTmCarry;                // Number of TmStamp Carry out

public:
  CztEvent( void );
  virtual  ~CztEvent();

  Double_t  GetTime     ( void ) { return fTime; }

  UInt_t    GetPktCount ( void ) const { return fPacketCount;  }
  UShort_t  GetPktSize  ( void ) const { return fPacketSize;   }
  UShort_t  GetRegUser  ( void ) const { return fUserRegister; }
  UInt_t    GetEvtRate  ( void ) const { return fEventRate;    }
  UShort_t  GetCIE      ( void ) const { return fChipInEvent;  }

  UInt_t    GetNhitAsic ( void ) const { return fNhitAsic;  }
  Int_t     GetAsicID   ( UInt_t hit ) const;
  UInt_t    GetEventID  ( void ) const { return fEventID;   }
  UInt_t    GetTmStamp  ( void ) const { return fTmStamp;   }
  UInt_t    GetTimePps  ( void ) const { return fTimePps;   }
  UInt_t    GetEpoch    ( void ) const { return fEpoch;     }
  UInt_t    GetEpochNS  ( void ) const { return fEpochNS;   }
  
  CztAsic  *GetAsic     ( UInt_t hit );

  Int_t     Fill        ( BasePacket *pkt, Bool_t speak = kFALSE );

  void      SetPktCount ( UInt_t   pc ) { fPacketCount  = pc; }
  void      SetPktSize  ( UShort_t ps ) { fPacketSize   = ps; }
  void      SetRegUser  ( UShort_t ru ) { fUserRegister = ru; }
  void      SetEvtRate  ( UInt_t   er ) { fEventRate    = er; }
  void      SetCIE      ( UShort_t ci ) { fChipInEvent  = ci; }
  
  void      SetEventID  ( UInt_t   id ) { fEventID      = id; }
  void      SetTmStamp  ( UInt_t   tm ) { fTmStamp      = tm; }
  void      SetTimePps  ( UInt_t   tm ) { fTimePps      = tm; }
  void      SetEpoch    ( UInt_t   ep ) { fEpoch        = ep; }
  void      SetEpochNS  ( UInt_t   ep ) { fEpochNS      = ep; }

  void      SetHitAsic  ( UShort_t cie );

  virtual void Clear( Option_t *option = "" );

  ClassDef( CztEvent, 1 )
};

#endif

// Local Variables:
// mode:C++
// End:
