#ifndef _PACKET_H
#define _PACKET_H

#define PKT_SIZE_SIZE 2
#define PKT_ATRB_SIZE 2
#define PKT_EVID_SIZE 4
#define PKT_EPCH_SIZE 4
#define PKT_EPNS_SIZE 4
#define PKT_TIME_SIZE 8

struct DataHeader
{
  unsigned char atrb[PKT_ATRB_SIZE];
  unsigned char evid[PKT_EVID_SIZE];
  unsigned char epch[PKT_EPCH_SIZE];
  unsigned char epns[PKT_EPNS_SIZE];
};

#define PKT_HEAD_SIZE sizeof( struct DataHeader )

#define PKT_ATRB_SIM 0x0000
#define PKT_ATRB_SI0 0x0001
#define PKT_ATRB_SI1 0x0002
#define PKT_ATRB_SI2 0x0004
#define PKT_ATRB_SI3 0x0008
#define PKT_ATRB_SI4 0x0010
#define PKT_ATRB_SI5 0x0020
#define PKT_ATRB_SI6 0x0040
#define PKT_ATRB_SI7 0x0080
#define PKT_ATRB_SI8 0x0100
#define PKT_ATRB_SI9 0x0200
#define PKT_ATRB_ACD 0x0400
#define PKT_ATRB_SIT 0x0800
#define PKT_ATRB_CZT 0x1000
#define PKT_ATRB_CSI 0x2000
#define PKT_ATRB_TRG 0x4000
#define PKT_ATRB_DAQ 0x8000

#define MAX_PKT_SIZE 0x10000

#endif
