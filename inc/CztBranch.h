#ifndef _CZT_BRANCH_H
#define _CZT_BRANCH_H

#include "CmpLib.h"
#include "BasePacket.h"
#include "DetBranch.h"
#include "CztLib.h"
#include "CztEvent.h"
#include "CztCrate.h"
#include "HitBranch.h"

#include "TObject.h"
#include "TClonesArray.h"

// CztBranch
//  a base class which stores CZT data as a branch

class CztBranch : public DetBranch {
protected:
  UInt_t        fAttrib;                    // CZT Event Attribute
  UInt_t        fEventID;                   // CZT Event ID
  UInt_t        fEpoch;                     // CZT Epoch Time
  UInt_t        fEpochNS;                   // CZT Epoch NS time
 
  UInt_t        fNhitsAsic;                 // Number of Hit Asic
  Int_t         fHitAsicID[CztLib::N_ASIC]; // Hit Asic ID

  CztEvent      fEvent;                     // CZT Raw Event data
  TClonesArray *fArray;                     // Array of Czt Crate

public:
  CztBranch( void );
  virtual ~CztBranch();

  // access functions
  UInt_t        GetAttrib   ( void ) { return fAttrib;  }
  UInt_t        GetEventID  ( void ) { return fEventID; }
  UInt_t        GetEpoch    ( void ) { return fEpoch;   }
  UInt_t        GetEpochNS  ( void ) { return fEpochNS; }
  Double_t      GetTime     ( void ) { return fEpoch + fEpochNS*1.0e-9; }
  
  UInt_t        GetNhitsAsic( void ) const { return fNhitsAsic; }
  Int_t         GetHitAsicID( UInt_t hit ) const;
  
  CztEvent     *GetEvent    ( void ) { return &fEvent; }
  TClonesArray *GetArray    ( void ) { return  fArray; }
  CztCrate     *GetCrate    ( Int_t  aid ) const;
  Int_t         GetHitID    ( Int_t  aid ) const;
  
  Int_t         GetHitAid   ( Int_t ith = 0 );
  Int_t         GetHitCid   ( Int_t ith = 0 );
  Int_t         GetHitSta   ( Int_t ith = 0 );
  Double_t      GetHitEne   ( Int_t ith = 0 );
  TVector3      GetHitPos   ( Int_t ith = 0 );
  
  // methods
  void  SetAttrib ( UInt_t at ) { fAttrib  = at; }
  void  SetEventID( UInt_t id ) { fEventID = id; }
  void  SetEpoch  ( UInt_t ep ) { fEpoch   = ep; }
  void  SetEpochNS( UInt_t ns ) { fEpochNS = ns; }

  Int_t Process   ( Int_t sel = CmpLib::ALL );
  Int_t CopyHit   ( void );
  Int_t Fill      ( BasePacket *pkt );

  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( CztBranch, 3 )  // CZT branch class
};

#endif

// Local Variables:
// mode:C++
// End:
