#ifndef _SIT_MERGER_H
#define _SIT_MERGER_H

#include "DataMerger.h"

#include "TList.h"

#include <fstream>
#include <map>

class SitMerger : public DataMerger {

protected:
  std::map< UShort_t, Bool_t > fFlag; // < atrb, write flag >
  
public:
  SitMerger( void );
  virtual ~SitMerger();

  // methods
  virtual void  ReadEvent  ( void );
  virtual Int_t WriteEvent ( void );

  virtual void Clear( Option_t *option = "" );
  
  static void SigHandler( int sig );
};

#endif

// Local Variables:
// mode:C++
// End:
