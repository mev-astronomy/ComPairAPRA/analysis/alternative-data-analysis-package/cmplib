#ifndef _CZT_EVENTID_H
#define _CZT_EVENTID_H

#include "DataEventID.h"
#include "CztPacket.h"

class CztEventID : public DataEventID {
public:  
  enum ENums { N_ASIC = 16 };

protected:
  CztPacket *fCztPacket;

  Double_t fUtcTime;
  UInt_t   fEventID;
  UInt_t   fTmStamp;
  UInt_t   fTimePps;

  Double_t fReqTime;
  Double_t fSysClock;

public:
  CztEventID( void );
  CztEventID( UShort_t atrb );
  virtual ~CztEventID();

  // access function
  Double_t GetReqTime ( void ) const { return fReqTime;  }
  Double_t GetSysClock( void ) const { return fSysClock; }
  
  // methods
  void SetReqTime ( Double_t time  ) { fReqTime  = time;  }
  void SetSysClock( Double_t clock ) { fSysClock = clock; }

  void SetUtcTime( void );
  void SetEventID( void );
  void SetTmStamp( void );
  void SetTimePps( void );
  
  virtual void  SetTimeOffset( double oft );
  virtual Int_t Branch ( void );
  virtual Int_t Read   ( void );
  virtual Int_t Process( void );
  virtual void  Clear( Option_t *option = "" );
};

#endif

// Local Variables:
// mode:C++
// End:
