#ifndef _CSI_EVNT_H
#define _CSI_EVNT_H

#include "RossPad.h"
#include "BasePacket.h"
#include "CsILib.h"

#include "TObject.h"

class CsIEvent : public TObject {

private:
  UInt_t   fDataType;                             // data type
  UInt_t   fTimeDiff;                             // time stamp difference
  UShort_t fData[CsILib::N_XADR][CsILib::N_YADR]; // CsI data

  RossPad *fRossPad;                              //! RossPad data

public:
  CsIEvent( void );
  virtual  ~CsIEvent();

  // access functions

  UInt_t    GetDataType ( void ) const { return fDataType;  }
  UInt_t    GetTimeDiff ( void ) const { return fTimeDiff;  }
  UShort_t  GetData     ( Int_t indx ) const;
  UShort_t  GetData     ( Byte_t xadr, Byte_t yadr ) const;

  RossPad  *GetRossPad  ( void ) const { return fRossPad;   }
  
  // methods
  Int_t     Fill        ( BasePacket *pkt, Bool_t speak = kFALSE );
  void      SetDataType ( UInt_t   data ) { fDataType   = data; }
  void      SetTimeDiff ( UInt_t   data ) { fTimeDiff   = data; }

  virtual void Clear( Option_t *option = "" );

  ClassDef( CsIEvent, 1 )
};

#endif

// Local Variables:
// mode:C++
// End:
