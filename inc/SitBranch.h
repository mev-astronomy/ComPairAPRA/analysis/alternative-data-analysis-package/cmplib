#ifndef _SIT_BRANCH_H
#define _SIT_BRANCH_H

#include "CmpLib.h"
#include "BasePacket.h"
#include "SitEvent.h"
#include "SitLayer.h"

#include "HitBranch.h"
#include "TrkBranch.h"

#include "TObject.h"

// SitBranch
//  a base class which stores SIT data as a branch

class SitBranch : public TObject {

protected:
  UInt_t    fAttrib;                // CsI Event Attribute
  UInt_t    fEventID;               // CsI Event ID
  UInt_t    fEpoch;                 // CsI Epoch Time
  UInt_t    fEpochNS;               // CsI Epoch NS time

  Int_t     fSlice;                    // SiT Slice ID

  SitEvent  fEvent;                    // SiT Event
  SitLayer *fLayer [SitLib::N_COORD];  // SiT Slice
  
public:
  SitBranch( void );
  SitBranch( Int_t id );
  virtual ~SitBranch();

  // access functions
  UInt_t     GetAttrib ( void ) { return fAttrib;  }
  UInt_t     GetEventID( void ) { return fEventID; }
  UInt_t     GetEpoch  ( void ) { return fEpoch;   }
  UInt_t     GetEpochNS( void ) { return fEpochNS; }
  Double_t   GetTime   ( void ) { return fEpoch + fEpochNS*1.0e-9; }

  Int_t      GetSlice  ( void ) const { return fSlice; }
  Int_t      GetNhits  ( Int_t coord ) const;
  Double_t   GetEnergy ( void );
  HitBranch *GetNthHit ( Int_t coord, Int_t n );
  HitBranch *NewHit    ( Int_t coord );

  SitEvent  *GetEvent  ( void ) { return &fEvent; }
  SitLayer  *GetLayer  ( Int_t coord );
  Int_t      NhitStrip ( Int_t coord );
  
  // methods
  void      SetAttrib ( UInt_t at ) { fAttrib  = at; }
  void      SetEventID( UInt_t id ) { fEventID = id; }
  void      SetEpoch  ( UInt_t ep ) { fEpoch   = ep; }
  void      SetEpochNS( UInt_t ns ) { fEpochNS = ns; }

  Int_t     Process    ( Int_t sel = CmpLib::ALL );
  void      ProcHit    ( void );
  void      CopyHit    ( void );
  Int_t     Fill       ( BasePacket *pkt );
  void      SetNhit    ( Int_t coord, Int_t hit );
  Int_t     FindHitID  ( Int_t coord, Int_t sid );
  
  void      SetSlice   ( Int_t   id );
  
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( SitBranch, 2 )  // SIT branch class
};

#endif

// Local Variables:
// mode:C++
// End:
