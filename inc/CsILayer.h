#ifndef _CSI_LAYER_H
#define _CSI_LAYER_H

#include "CsILib.h"
#include "CsILog.h"
#include "CsIEvent.h"
#include "DetBranch.h"

// CsILayer
//  a base class which stores CsI Layer data as a branch

class CsILayer : public DetBranch {

protected:
  Int_t    fCoord;               // CsI Coord ID
  CsILog   fLog[CsILib::N_LOGS]; // Array of CsI Logs

public:
  CsILayer( void );
  virtual ~CsILayer();

  // access functions
  Int_t    GetCoord ( void ) const { return fCoord; }

  Int_t    FindLogID( Int_t hit );
  Int_t    FindHitID( Int_t log );

  CsILog  *GetHitLog( Int_t hit );
  Double_t GetHitEne( Int_t hit );
  TVector3 GetHitPos( Int_t hit );

  CsILog  *GetCsILog( Int_t log );
  Double_t GetCsIEne( Int_t log );
  TVector3 GetCsIPos( Int_t log );

  // methods
  void     SetCoord ( Int_t coord ) { fCoord = coord; }
  void     SetData  ( CsIEvent *event );
  
  CsILayer& operator= (const CsILayer &csi);
  
  virtual Int_t Process  ( Int_t sel = CmpLib::ALL );
  virtual void  CopyHit  ( void );
  virtual void  Clear( Option_t *option = "" );
  virtual void  Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( CsILayer, 1 )  // CsI Layer branch base class
};

inline CsILayer& CsILayer::operator= (const CsILayer &csi)
{
  fCoord = csi.fCoord;
  for ( Int_t log = 0; log < CsILib::N_LOGS; log++ ) {
    fLog[log] = csi.fLog[log];
  }

  return *this;
}

#endif

// Local Variables:
// mode:C++
// End:
