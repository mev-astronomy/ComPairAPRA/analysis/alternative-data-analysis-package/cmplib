#ifndef _ARDUINO_HANDLER_H
#define _ARDUINO_HANDLER_H

#include "ArduIno.h"

#include "TList.h"

class TList;

class ArduinoHandler {
public:
  enum EType { T_ACK = 1, T_RTC = 2, T_P = 3, T_R = 4, T_C = 5, T_E = 6 };

protected:
  TList fList;                       //! File list
  
  ULong64_t f1stGpsTime;             //! 1st GPS Time
  
  std::vector<UInt_t>      fIndex;   //! index of data
  std::vector<ArduIno>     fPacket;  //! arduino data
  std::vector<std::string> fArduino; //! formatted arduino data
  
public:
  ArduinoHandler();
  virtual ~ArduinoHandler();
  
  // access functions
  ULong64_t   GetGpsTime( void ) const { return f1stGpsTime; }
  UInt_t      GetSize   ( void ) const { return fArduino.size(); }
  std::string GetString ( UInt_t i ) const { return fArduino[i]; }
  
  // methods
  void  SetGpsTime( ULong64_t data ) { f1stGpsTime = data; }
  
  Int_t Add    ( const char *in_fname );
  Int_t Open   ( const char *);
  Int_t Loop   ( Int_t nproc = -1 );
  Int_t Process( void );

  virtual void Clear( void );
  
  void  ReadArduino ( void );
  void  SortArduino ( void );
  void  FindArduino ( void );
  void  PairArduino ( void );
  void  CoinArduino ( void );
  void  TimeArduino ( void );
  void  EvidArduino ( void );
  void  FormArduino ( void );
  void  WriteArduino( void );
};

#endif

// Local Variables:
// mode:C++
// End:
