#ifndef _CMP_LIB_H
#define _CMP_LIB_H

#include "TObject.h"
#include "TVector3.h"

#define GETENV(env,def) ( (getenv( env )) ? getenv( env ) : def )
#define TIM_ZON_ENV "CMPLIB_TIME_ZONE"
#define TIM_ZON_DEF "local"

class CmpLib : public TObject {
 public:
  enum ENums  { N_ATRB  = 16, N_DET = 5, N_SLICE = 10, N_COORD = 2, N_LYR = N_SLICE*N_COORD,
		N_PRI = 20, N_SEC = 192, N_PAIR = 4,
		N_MHIT = 512, N_GEOM = 4126, N_STRIP = 192, N_STHIT = 16 };
  enum EDetIDs{ D_TRG = 0, D_ACD = 1, D_SIT = 2, D_CZT = 3, D_CSI = 4 };
  enum ECoords{ COORD_X = 0, COORD_Y = 1 };
  enum EPair  { SITSIT = 0, SITCZT = 1, SITCSI = 2, CZTCSI = 3 };
  enum EBits  { HEAD = BIT( 0 ), TRG = BIT( 1 ), ACD = BIT( 2 ), SIT = BIT( 3 ),
		CZT  = BIT( 4 ), CSI = BIT( 5 ), DST = BIT( 6 ), SIM = BIT( 7 ),
		ALL = 0xFF };
  enum ESels  { NOEVT  = 0, RAWEVT = 1, ALLEVT = 2, TRGEVT = 3, HITEVT = 4, CREVT = 5, GREVT = 6 };

  static TVector3 fVnotValid;
  static Double_t fDnotValid;
  static Float_t  fFnotValid;
  static Int_t    fThreshold;

  CmpLib();
  virtual ~CmpLib();

  // class definition for ROOT
  ClassDef( CmpLib, 2 ) // General parameters and functions class
};

#endif

// Local Variables:
// mode:C++
// End:
