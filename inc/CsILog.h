#ifndef _CSI_LOG_H
#define _CSI_LOG_H

#include "CsILib.h"
#include "CsIEvent.h"

#include "TObject.h"

// CsILog
//  a base class which stores CsI Log data as a branch

class CsILog : public TObject {

protected:
  Int_t    fLogID;
  
  UShort_t fData[CsILib::N_SIDE]; // Sensor data
  Byte_t   fAtrb[CsILib::N_SIDE]; // Sensor data attribute
  Double_t fEdep[CsILib::N_SIDE]; // Energy deposit

  Int_t    fHitSta;               // CsI Log Hit Status
  Double_t fHitEne;               // CsI Log Hit Energy Deposit
  TVector3 fHitPos;               // CsI Log Hit Position

public:
  CsILog( void );
  virtual ~CsILog();

  // access functions
  Int_t    GetLogID ( void ) const { return fLogID;  }
  UShort_t GetData  ( Int_t side ) const;
  Byte_t   GetAtrb  ( Int_t side ) const;
  Double_t GetEdep  ( Int_t side ) const;
  Int_t    GetHitSta( void ) const { return fHitSta; }
  Double_t GetHitEne( void ) const { return fHitEne; }
  TVector3 GetHitPos( void ) const { return fHitPos; }
  
  // methods
  Int_t    Process  ( Int_t layr );
  void     SetLogID ( Int_t id ) { fLogID = id; }
  void     SetData  ( Int_t side, UShort_t data );
  void     SetAtrb  ( Int_t side, Byte_t   atrb );
  void     AddAtrb  ( Int_t side, Byte_t   atrb );
  void     DelAtrb  ( Int_t side, Byte_t   atrb );
  void     SetEdep  ( Int_t side, Double_t edep );
  void     SetHitSta( Int_t    sta ) { fHitSta = sta; }
  void     SetHitEne( Double_t ene ) { fHitEne = ene; }
  void     SetHitPos( TVector3 pos ) { fHitPos = pos; }
  
  CsILog& operator= (const CsILog &log);

  virtual void  Clear  ( Option_t *option = "" );
  virtual void  Print  ( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( CsILog, 1 )  // CsI Log branch base class
};

inline CsILog& CsILog::operator= (const CsILog &log)
{
  fLogID = log.fLogID;

  for ( Int_t side = 0; side < CsILib::N_SIDE; side++ ) {
    fData[side] = log.fData[side];
    fAtrb[side] = log.fAtrb[side];
    fEdep[side] = log.fEdep[side];
  }

  fHitSta = log.fHitSta;
  fHitEne = log.fHitEne;
  fHitPos = log.fHitPos;

  return *this;
}

#endif

// Local Variables:
// mode:C++
// End:
