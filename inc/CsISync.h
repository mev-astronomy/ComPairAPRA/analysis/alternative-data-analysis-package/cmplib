#ifndef _CSI_SYNC_H
#define _CSI_SYNC_H

#include "BasePacket.h"
#include "ArduinoHandler.h"
#include "RossPadHandler.h"

#include "Rtypes.h"

#include <fstream>
#include <vector>

class CsISync {
 public:
  
 protected:
  Int_t  fAtrb;
  
  Int_t  fNzero;
  Int_t  fNtack;

  UInt_t fStartID;

  BasePacket fPacket;       // Data Packet

  ArduinoHandler a_handler; // Arduino Handler
  RossPadHandler r_handler; // RossPad Handler
  std::vector<UInt_t> fEventID;

  std::vector<Bool_t> fTrgAck;
  
  std::vector<std::string> items_arduino;
  std::vector<std::string> items_rosspad;
  
public:
  CsISync();
  virtual ~CsISync();

  // access functions
  Int_t     GetNzero      ( void ) const { return fNzero; }
  Int_t     GetNtack      ( void ) const { return fNtack; }

  UInt_t    GetIndex      ( void ) const;
  ULong64_t GetTimeArduino( void ) const;
  ULong64_t GetTimeRossPad( void ) const;

  Int_t     GetDataArduino( void ) const;
  Int_t     GetDataRossPad( void ) const;
  
  Int_t     GetDataType   ( void ) const;
  UInt_t    GetEventID    ( void ) const;

  Bool_t    GetTrgAck     ( Int_t i ) const;

  Int_t     GetNarduino   ( void ) { return a_handler.GetSize(); }
  Int_t     GetNrosspad   ( void ) { return r_handler.GetSize(); }

  // methods
  void      AddArduino    ( const char *in_fname ) { a_handler.Add( in_fname ); }
  void      AddRossPad    ( const char *in_fname ) { r_handler.Add( in_fname ); }

  void      ProcArduino   ( void );
  void      ProcRossPad   ( void );

  void      ParseArduino  ( Int_t i );
  void      ParseRossPad  ( Int_t i );

  void      SetData       ( Bool_t flag );
  void      SetEventID    ( UInt_t evid );

  void      WritePacket   ( std::ofstream *fout, bool speak = kFALSE );

  void      SetNzero     ( Int_t n ) { fNzero = n; }
  void      SetNtack     ( Int_t n ) { fNtack = n; }

  virtual void Clear ( void );
};

#endif
