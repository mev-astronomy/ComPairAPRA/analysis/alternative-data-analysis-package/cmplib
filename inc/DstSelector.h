#ifndef _DST_SELECTOR_H
#define _DST_SELECTOR_H

#include "TSelector.h"
#include "TList.h"

class CmpTree;

class TH1D;
class TH2D;
class TH3D;
class TStopwatch;
class DstBranch;

class DstSelector : public TSelector {

protected:
  // Basic parameters
  Long64_t    fEntry;       // Local entry number
  Long64_t    fNproc;       // Number of events processed
  Long64_t    fNfill;       // Number of events filled
  Long64_t    fIntv;        // Event interval to show status

  // Pointers
  TTree      *fTree;        // TTree object
  TTree      *fTreeSel;     // TTree of selected events
  CmpTree    *fCtree;       // CMP tree object
  TStopwatch *fTimer;       // Timer

  // Branch lists
  TList       fBrList1;     // List of branches for level 1 selection
  TList       fBrList2;     // List of branches for level 2 selection
  TList       fBrList3;     // List of branches for level 3 selection


  // Primary track index
  Int_t       fIndex;       // Primary track index
  DstBranch  *fDst;         // Primary track DST

public:
  DstSelector( void );
  virtual ~DstSelector();

  virtual Int_t  Version    ( void ) const { return 1; }
  virtual void   SlaveBegin ( TTree *tree );
  virtual Bool_t Notify     ( void );
  virtual Bool_t Process    ( Long64_t entry );
  virtual void   Terminate  ( void );
  virtual void   SetMode    ( Int_t mode );

protected:
  virtual TDirectory* PreProc  ( void );

  virtual Bool_t Select   ( void );
  virtual Bool_t DstSelect( void );
  virtual Bool_t PreSelect( void );
  virtual Bool_t Fiducial ( void );
  virtual Bool_t Quality  ( void );
  virtual void   ElossSel ( void );

  virtual void   SetBranch( void );
  virtual void   InitHist ( void );
  virtual void   PreFill  ( void );
  virtual void   Fill     ( void );
  virtual void   ProcHist ( void );

  virtual void   Clear( Option_t *option = "" );
  virtual void   Print( Option_t *option = "" ) const;
  virtual Int_t  Write( const char *name = 0, Int_t opt = 0, Int_t bsiz = 0 );
  virtual Int_t  Write( const char *name = 0, 
			Int_t opt = 0, Int_t bsiz = 0 ) const;

  void Branch( Int_t level, const char *bname, const char *pname = 0 );

  void Fill1D( const char *name, Double_t x );
  void Fill1D( const char *name, Double_t x, Double_t w );
  void Fill2D( const char *name, Double_t x, Double_t y );
  void Fill2D( const char *name, Double_t x, Double_t y, Double_t w );
  void Fill3D( const char *name, Double_t x, Double_t y, Double_t z );
  void Fill3D( const char *name, Double_t x, Double_t y, 
	                         Double_t z, Double_t w );

public:
  static TH1D *LogHist( const char *name, const char *title,
			Int_t nbx, Double_t xmin, Double_t xmax );
  static TH2D *LogHist( const char *name, const char *title,
			Int_t nbx, Double_t xmin, Double_t xmax,
			Int_t nby, Double_t ymin, Double_t ymax, 
			Int_t mode = 1 );
  static TH3D *LogHist( const char *name, const char *title,
			Int_t nbx, Double_t xmin, Double_t xmax,
			Int_t nby, Double_t ymin, Double_t ymax, 
			Int_t nbz, Double_t zmin, Double_t zmax, 
			Int_t bit = 1 );
  static Double_t *GetLogBin( Int_t nbin, Double_t xmin, Double_t xmax );
  static Double_t *GetLinBin( Int_t nbin, Double_t xmin, Double_t xmax );

  // class definition for ROOT
  ClassDef( DstSelector, 1 )  // DST Selector class
};

#endif

// Local Variables:
// mode:C++
// End:

