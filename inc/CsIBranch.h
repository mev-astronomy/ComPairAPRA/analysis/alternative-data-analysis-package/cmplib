#ifndef _CSI_BRANCH_H
#define _CSI_BRANCH_H

#include "CmpLib.h"
#include "CsILib.h"
#include "CsIEvent.h"
#include "CsILayer.h"
#include "BasePacket.h"
#include "DetBranch.h"
#include "HitBranch.h"

#include "TObject.h"

// CsIBranch
//  a base class which stores CsI data as a branch

class CsIBranch : public TObject {

protected:
  UInt_t    fAttrib;                // CsI Event Attribute
  UInt_t    fEventID;               // CsI Event ID
  UInt_t    fEpoch;                 // CsI Epoch Time
  UInt_t    fEpochNS;               // CsI Epoch NS time
  
  CsIEvent  fEvent;                 // CsI Raw Event data
  CsILayer  fLayer[CsILib::N_LAYR]; // Array of CsI Layer
  Double_t  fEnergy;                // CsI calorimeter total Energy
  
public:
  CsIBranch( void );
  virtual ~CsIBranch();

  // access functions
  UInt_t    GetAttrib ( void ) { return fAttrib;  }
  UInt_t    GetEventID( void ) { return fEventID; }
  UInt_t    GetEpoch  ( void ) { return fEpoch;   }
  UInt_t    GetEpochNS( void ) { return fEpochNS; }
  Double_t  GetTime   ( void ) { return fEpoch + fEpochNS*1.0e-9; }
  
  CsIEvent *GetEvent  ( void ) { return &fEvent;  }
  CsILayer *GetLayer  ( Int_t lyr );
  Double_t  GetEnergy ( void ) const { return fEnergy; }
  
  // methods
  void      SetAttrib ( UInt_t at ) { fAttrib  = at; }
  void      SetEventID( UInt_t id ) { fEventID = id; }
  void      SetEpoch  ( UInt_t ep ) { fEpoch   = ep; }
  void      SetEpochNS( UInt_t ns ) { fEpochNS = ns; }
  
  Int_t     Process   ( Int_t sel = CmpLib::ALL );
  Int_t     Fill      ( BasePacket *pkt );
  void      SetEnergy ( Double_t ene ) { fEnergy = ene; }
  
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( CsIBranch, 1 )  // CsI branch class
};

#endif

// Local Variables:
// mode:C++
// End:
