#ifndef _CSI_PARAMS_H
#define _CSI_PARAMS_H

#include "CsILib.h"
#include "DetParams.h"

// CsIParams
//  a class which stores general parameters and readout assign data

class CsIParams : public DetParams {
public:
  
protected:
  Int_t      fAssign [CsILib::N_XADR][CsILib::N_YADR];  // Pixelmap assignment
  Int_t      fIndex  [CsILib::N_LAYR][CsILib::N_LOGS][CsILib::N_SIDE]; // Index
  Int_t      fLyrID  [CsILib::N_INDX];                  // Layer ID
  Int_t      fLogID  [CsILib::N_INDX];                  // LOG ID
  Int_t      fSenID  [CsILib::N_INDX];                  // Sensor ID
  Byte_t     fXaddr  [CsILib::N_INDX];                  // PixelMap X address
  Byte_t     fYaddr  [CsILib::N_INDX];                  // PixelMap Y address
  Bool_t     fDead   [CsILib::N_INDX];                  // Dead Sensor channels

  TVector3   fOrigin [CsILib::N_LAYR][CsILib::N_LOGS];  // Origin of CsI Logs
  TVector3   fDimens [CsILib::N_LAYR][CsILib::N_LOGS];  // Dimensions of CsI Logs
  
  Double_t  *fPeds   [CsILib::N_INDX];                  // [fNpoint] Pedestal of the channel
  Double_t  *fThre   [CsILib::N_INDX];                  // [fNpoint] Threshold of the channel
  Double_t  *fGain   [CsILib::N_INDX];                  // [fNpoint] Gain of the channel

  TH1D      *fQcorr  [CsILib::N_LAYR][CsILib::N_LOGS];  // Q correction by hit position
  TH1D      *fHposi  [CsILib::N_LAYR][CsILib::N_LOGS];  // Hit Position determined by Q
  
public:
  CsIParams();
  virtual ~CsIParams();

  // access functions
  Int_t      GetAssign ( Int_t xadr, Int_t yadr ) const;
  Int_t      GetIndex  ( Int_t layr, Int_t logs, Int_t side ) const;
  Int_t      GetLyrID  ( Int_t indx ) const;
  Int_t      GetLogID  ( Int_t indx ) const;
  Int_t      GetSenID  ( Int_t indx ) const;
  Byte_t     GetXaddr  ( Int_t indx ) const;
  Byte_t     GetYaddr  ( Int_t indx ) const;
  Bool_t     IsDead    ( Int_t indx ) const;
  Int_t      GetCoord  ( Int_t layr ) const;
  
  TVector3   GetOrigin ( Int_t layr, Int_t logs ) const;
  TVector3   GetDimens ( Int_t layr, Int_t logs ) const;
  
  Double_t   GetPeds    ( Int_t indx, Int_t ip ) const;
  Double_t   GetThre    ( Int_t indx, Int_t ip ) const;
  Double_t   GetGain    ( Int_t indx, Int_t ip ) const;

  Double_t   GetQcorr   ( Int_t layr, Int_t logs, Double_t diff ) const;
  TH1D      *GetQcorHist( Int_t layr, Int_t logs ) const;

  Double_t   GetHposi   ( Int_t layr, Int_t logs, Double_t diff ) const;
  TH1D      *GetHposHist( Int_t layr, Int_t logs ) const;

  // methods
  Int_t SetPixMap  ( Int_t indx, Int_t xadr, Int_t yadr );
  Int_t SetIndex   ( Int_t indx, Int_t layr, Int_t logs, Int_t side );
  Int_t SetDead    ( Int_t indx, Bool_t dead );

  Int_t SetOrigin  ( Int_t layr, Int_t logs, Double_t x, Double_t y, Double_t z );
  Int_t SetDimens  ( Int_t lyar, Int_t logs, Double_t x, Double_t y, Double_t z );

  virtual void Init( Double_t b, Double_t e, Int_t n, Double_t *t );

  void  SetPeds    ( Int_t indx, Int_t ip, Double_t peds );
  void  SetThre    ( Int_t indx, Int_t ip, Double_t thre );
  void  SetGain    ( Int_t indx, Int_t ip, Double_t gain );

  void  SetQcorHist( Int_t layr, Int_t logs, TH1D *hist );
  void  SetHposHist( Int_t layr, Int_t logs, TH1D *hist );
  
  void  ResetQcorHist( Int_t layr, Int_t logs );
  void  ResetHposHist( Int_t layr, Int_t logs );
  
  // access to the global pointer
  static CsIParams  *GetPtr( void );
  static CsIParams  *ReadFile( const char *fname = FileName(), Int_t v = 1 );
  static const char *FileName( void ) { return "csipar.root"; }

  // class definition for ROOT
  ClassDef( CsIParams, 1 )  // General parameters and assign data class
};

// global object
extern CsIParams *gCsIParams;

#endif

// Local Variables:
// mode:C++
// End:
