#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class RevIsion+;
#pragma link C++ class CmpLib+;
#pragma link C++ class CmpTree+;
#pragma link C++ class CmpParams+;
#pragma link C++ class DetParams+;
#pragma link C++ class DetBranch+;
#pragma link C++ class CztLib+;
#pragma link C++ class CztAsic+;
#pragma link C++ class CztUdpHeader+;
#pragma link C++ class CztPktHeader+;
#pragma link C++ class CztEvtHeader+;
#pragma link C++ class CztPacket+;
#pragma link C++ class CztEvent+;
#pragma link C++ class CztCryst+;
#pragma link C++ class CztCrate+;
#pragma link C++ class CztBranch+;
#pragma link C++ class CztParams+;
#pragma link C++ class TrgEvent+;
#pragma link C++ class TrgBranch+;
#pragma link C++ class AcdBranch+;
#pragma link C++ class SimBranch+;
#pragma link C++ class SitLib+;
#pragma link C++ class SitAsic+;
#pragma link C++ class SitEvent+;
#pragma link C++ class SitStrip+;
#pragma link C++ class SitLayer+;
#pragma link C++ class SitBranch+;
#pragma link C++ class SitParams+;
#pragma link C++ class CsILib+;
#pragma link C++ class CsILog+;
#pragma link C++ class CsILayer+;
#pragma link C++ class CsIEvent+;
#pragma link C++ class CsIParams+;
#pragma link C++ class CsIBranch+;
#pragma link C++ class TrkBranch+;
#pragma link C++ class CmpBranch+;
#pragma link C++ class HitBranch+;
#pragma link C++ class DstBranch+;
#pragma link C++ class DstSelector+;
#pragma link C++ class CmpMainFrame+;
#pragma link C++ class EventDrawer+;

#endif
