#ifndef _DATA_HANDLER_H
#define _DATA_HANDLER_H

#include "Packet.h"
#include "CmpLib.h"
#include "BasePacket.h"

#include "TList.h"

#include <map>
#include <fstream>

class DataHandler {
public:
  typedef std::map< UShort_t, BasePacket * >::iterator it_type;

protected:
  UInt_t         fNatrb;
  UInt_t         fAtrbs[CmpLib::N_ATRB];
  Bool_t         fWflag;
  std::ifstream  fIN;
  TList          fList;
  BasePacket     fPacket;

  std::map< UShort_t, BasePacket * > fPkt;
  
public:
  DataHandler( void );
  virtual ~DataHandler();

  // access functions
  Int_t       GetPktSize( void ) { return fPacket.GetSize(); }
  UShort_t    GetPktAtrb( void ) { return fPacket.GetAtrb(); }
  Double_t    GetUtcTime( void ) { return fPacket.GetTime(); }
  UInt_t      GetEventID( void ) { return fPacket.GetEvid(); }
  UInt_t      GetEpoch  ( void ) { return fPacket.GetEpch(); }
  UInt_t      GetEpochNS( void ) { return fPacket.GetEpns(); }
  Int_t       GetNfile  ( void ) const { return fList.GetSize(); }
  UInt_t      GetNatrb  ( void ) const { return fNatrb; }
  UShort_t    GetAtrb   ( void ) { return GetPktAtrb(); }
  UShort_t    GetAtrb   ( UInt_t i );
  Bool_t      GetWflag  ( void ) { return fWflag; }
  BasePacket *GetPacket ( void ) { return &fPacket; }
  BasePacket *GetPacket ( UInt_t i );
  
  // methods
  Int_t    Init    ( UShort_t atrb );
  Int_t    Add     ( const char *fname );
  Int_t    Read    ( Bool_t speak = kFALSE );
  Int_t    Split   ( void );
  Int_t    Write   ( std::ofstream *fOUT );
  UInt_t   SetNatrb( void );
  Bool_t   Eof     ( void );
  void     SetWflag( Bool_t flag ) { fWflag = flag; }
  
  virtual void Clear( Option_t *option = "" );
};

#endif

// Local Variables:
// mode:C++
// End:
