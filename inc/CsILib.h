#ifndef _CSI_LIB_H
#define _CSI_LIB_H

#include "TObject.h"
#include "TVector3.h"

class CsILib : public TObject {
 public:
  enum ENums { N_ASIC = 4, N_CHAN = 16, N_LAYR = 5, N_LOGS = 6, N_SIDE = 2, N_XADR = 8, N_YADR = 8, N_INDX = 64, N_MAXQ = 16384 };
  enum EPars { COORD_X = 0, COORD_Y = 1, SIDE_L = 0, SIDE_R = 1, A_GOOD = 1, A_OVFL = 2 };

  static TVector3 fVnotValid;
  static Double_t fDnotValid;
  static Float_t  fFnotValid;
  static Int_t    fThreshold;

  static Double_t fCsIWidth;
  static Double_t fCsIHeight;
  static Double_t fCsILength;

  CsILib();
  virtual ~CsILib();

  // class definition for ROOT
  ClassDef( CsILib, 1 ) // General parameters and functions class
};

#endif

// Local Variables:
// mode:C++
// End:
