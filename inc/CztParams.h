#ifndef _CZT_PARAMS_H
#define _CZT_PARAMS_H

#include "CztLib.h"
#include "DetParams.h"

#include "TObject.h"
#include "TVector3.h"
#include "TH2D.h"
#include "TH3D.h"

// CztParams
//  a class which stores general parameters and readout assign data

class CztParams : public DetParams {
  
protected:
  Int_t      fAssign [CztLib::N_INDX];                  // Channel Assign
  Bool_t     fDead   [CztLib::N_ASIC][CztLib::N_INDX];  // Dead Asic channels
  Bool_t     fRotate [CztLib::N_ASIC];                  // Crate Rotation Flag
  TVector3   fOrigin [CztLib::N_ASIC][CztLib::N_CZTB];  // Origin of CZT bars
  TVector3   fDimens [CztLib::N_ASIC][CztLib::N_CZTB];  // Dimensions of CZT bars
  Bool_t     fUsable [CztLib::N_ASIC][CztLib::N_CZTB];  // Usable for Compton Reconstruction
  
  Double_t   fTempOffs[CztLib::N_ASIC];                 // Temperature Sensor Offset
  Double_t   fTempGain[CztLib::N_ASIC];                 // Temperature Sensor Gain
  Double_t   fTempBase[CztLib::N_ASIC];                 // Temperature Sensor Baseline
  
  Double_t  *fPeds    [CztLib::N_ASIC][CztLib::N_INDX]; // [fNpoint] Pedestal of the channel
  Double_t  *fThre    [CztLib::N_ASIC][CztLib::N_INDX]; // [fNpoint] Threshold of the channel
  Double_t  *fGain    [CztLib::N_ASIC][CztLib::N_INDX]; // [fNpoint] Gain of the channel

  Double_t   fLminPT  [CztLib::N_ASIC][CztLib::N_CZTB]; // Minimum Time difference CT-AT
  Double_t   fLmaxPT  [CztLib::N_ASIC][CztLib::N_CZTB]; // Maximum Time difference CT-AT

  Double_t   fLminPE  [CztLib::N_ASIC][CztLib::N_CZTB]; // Minimum CE/AE
  Double_t   fLmaxPE  [CztLib::N_ASIC][CztLib::N_CZTB]; // Maximum CE/AE

  TH1D      *fPosHistE[CztLib::N_ASIC][CztLib::N_CZTB]; // Z position determined by Energy
  TH1D      *fPosHistT[CztLib::N_ASIC][CztLib::N_CZTB]; // Z position determined by Timing
  
  TH1D      *fDepHistE[CztLib::N_ASIC][CztLib::N_CZTB]; // Depth correction Histogram E
  TH1D      *fDepHistT[CztLib::N_ASIC][CztLib::N_CZTB]; // Depth correction Histogram T
  
  TH3D      *fMapHistE[CztLib::N_ASIC][CztLib::N_CZTB]; // XY correction Histogram E
  TH3D      *fMapHistT[CztLib::N_ASIC][CztLib::N_CZTB]; // XY correction Histogram T

  TH1D      *fConforX [CztLib::N_ASIC][CztLib::N_CZTB]; // Conformal map X
  TH1D      *fConforY [CztLib::N_ASIC][CztLib::N_CZTB]; // Conformal map Y
  TH2D      *fConforM [CztLib::N_ASIC][CztLib::N_CZTB]; // Conformal map (Max<X,Y>)
  TH2D      *fConforR [CztLib::N_ASIC][CztLib::N_CZTB]; // Conformal map R
  
public:
  CztParams();
  virtual ~CztParams();

  // access functions
  Int_t      GetAssign ( Int_t idx ) const;
  Int_t      GetIndex  ( Int_t cid, Int_t typ, Int_t pol ) const;
  Bool_t     IsDead    ( Int_t aid, Int_t idx ) const;
  Bool_t     IsRotate  ( Int_t aid ) const;
  TVector3   GetOrigin ( Int_t aid, Int_t cid ) const;
  TVector3   GetDimens ( Int_t aid, Int_t cid ) const;
  Bool_t     IsUsable  ( Int_t aid, Int_t cid ) const;
  
  Double_t   GetTempOffs( Int_t aid ) const;
  Double_t   GetTempGain( Int_t aid ) const;
  Double_t   GetTempBase( Int_t aid ) const;
  
  Double_t   GetPeds    ( Int_t aid, Int_t idx, Int_t ip ) const;
  Double_t   GetThre    ( Int_t aid, Int_t idx, Int_t ip ) const;
  Double_t   GetGain    ( Int_t aid, Int_t idx, Int_t ip ) const;

  Double_t   GetLminPT  ( Int_t aid, Int_t cid ) const;
  Double_t   GetLmaxPT  ( Int_t aid, Int_t cid ) const;

  Double_t   GetLminPE  ( Int_t aid, Int_t cid ) const;
  Double_t   GetLmaxPE  ( Int_t aid, Int_t cid ) const;

  Double_t   GetZpos    ( Int_t aid, Int_t cid, Double_t ene, Double_t tim ) const;
  Double_t   GetZposE   ( Int_t aid, Int_t cid, Double_t ene ) const;
  Double_t   GetZposT   ( Int_t aid, Int_t cid, Double_t tim ) const;
  TH1D      *GetPosHistE( Int_t aid, Int_t cid ) const;
  TH1D      *GetPosHistT( Int_t aid, Int_t cid ) const;
  
  Double_t   GetDepCorr ( Int_t aid, Int_t cid, Double_t ene, Double_t tim ) const;
  Double_t   GetDepCorrT( Int_t aid, Int_t cid, Double_t ene ) const;
  Double_t   GetDepCorrE( Int_t aid, Int_t cid, Double_t tim ) const;
  TH1D      *GetDepHistT( Int_t aid, Int_t cid ) const;
  TH1D      *GetDepHistE( Int_t aid, Int_t cid ) const;

  Double_t   GetMapCorr ( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t ene, Double_t tim ) const;
  Double_t   GetMapCorrE( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t ene ) const;
  Double_t   GetMapCorrT( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t tim ) const;
  TH3D      *GetMapHistE( Int_t aid, Int_t cid ) const;
  TH3D      *GetMapHistT( Int_t aid, Int_t cid ) const;

  Double_t   GetConforX ( Int_t aid, Int_t cid, Double_t x ) const;
  Double_t   GetConforY ( Int_t aid, Int_t cid, Double_t y ) const;
  Double_t   GetConforM ( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t z ) const;
  Double_t   GetConforR ( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t z ) const;

  TH1D      *GetConforX ( Int_t aid, Int_t cid ) const;
  TH1D      *GetConforY ( Int_t aid, Int_t cid ) const;
  TH2D      *GetConforM ( Int_t aid, Int_t cid ) const;
  TH2D      *GetConforR ( Int_t aid, Int_t cid ) const;
  
  // methods
  Int_t SetAssign  ( Int_t idx, Int_t chn );
  Int_t SetDead    ( Int_t aid, Int_t idx, Bool_t dead );
  Int_t SetRotate  ( Int_t aid, Bool_t rot );
  Int_t SetOrigin  ( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t z );
  Int_t SetDimens  ( Int_t aid, Int_t cid, Double_t x, Double_t y, Double_t z );
  Int_t SetUsable  ( Int_t aid, Int_t cid, Bool_t usable );

  virtual void Init( Double_t b, Double_t e, Int_t n, Double_t *t );

  void  SetTempOffs( Int_t aid, Double_t offs );
  void  SetTempGain( Int_t aid, Double_t gain );
  void  SetTempBase( Int_t aid, Double_t base );
  
  void  SetPeds    ( Int_t aid, Int_t idx, Int_t ip, Double_t peds );
  void  SetThre    ( Int_t aid, Int_t idx, Int_t ip, Double_t thre );
  void  SetGain    ( Int_t aid, Int_t idx, Int_t ip, Double_t gain );

  void  SetLminPT  ( Int_t aid, Int_t cid, Double_t lm );
  void  SetLmaxPT  ( Int_t aid, Int_t cid, Double_t lm );

  void  SetLminPE  ( Int_t aid, Int_t cid, Double_t lm );
  void  SetLmaxPE  ( Int_t aid, Int_t cid, Double_t lm );

  void  SetPosHistE( Int_t aid, Int_t cid, TH1D *pos );
  void  SetPosHistT( Int_t aid, Int_t cid, TH1D *pos );

  void  SetDepHistE( Int_t aid, Int_t cid, TH1D *dep );
  void  SetDepHistT( Int_t aid, Int_t cid, TH1D *dep );
  
  void  SetMapHistE( Int_t aid, Int_t cid, TH3D *map );
  void  SetMapHistT( Int_t aid, Int_t cid, TH3D *map );

  void  SetConforX ( Int_t aid, Int_t cid, TH1D *map );
  void  SetConforY ( Int_t aid, Int_t cid, TH1D *map );
  void  SetConforM ( Int_t aid, Int_t cid, TH2D *map );
  void  SetConforR ( Int_t aid, Int_t cid, TH2D *map );

  void  ResetPosHistE( Int_t aid, Int_t cid );
  void  ResetPosHistT( Int_t aid, Int_t cid );
  void  ResetDepHistE( Int_t aid, Int_t cid );
  void  ResetDepHistT( Int_t aid, Int_t cid );
  void  ResetMapHistE( Int_t aid, Int_t cid );
  void  ResetMapHistT( Int_t aid, Int_t cid );
		   
  // access to the global pointer
  static CztParams  *GetPtr( void );
  static CztParams  *ReadFile( const char *fname = FileName(), Int_t v = 1 );
  static const char *FileName( void ) { return "cztpar.root"; }

  // class definition for ROOT
  ClassDef( CztParams, 4 )  // General parameters and assign data class
};

// global object
extern CztParams *gCztParams;

#endif

// Local Variables:
// mode:C++
// End:
