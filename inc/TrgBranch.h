#ifndef _TRG_BRANCH_H
#define _TRG_BRANCH_H

#include "CmpLib.h"
#include "BasePacket.h"
#include "TrgEvent.h"

#include "TObject.h"

// TrgBranch
//  a base class which stores TRG data as a branch

class TrgBranch : public TObject {
protected:
  UInt_t   fAttrib;     // TRG Event Attribute
  UInt_t   fEventID;    // TRG Event ID
  UInt_t   fEpoch;      // TRG Epoch Time
  UInt_t   fEpochNS;    // TRG Epoch NS time

  TrgEvent fEvent;      // TRG Event Data

public:
  TrgBranch( void );
  virtual ~TrgBranch();

  // access functions
  UInt_t   GetAttrib ( void ) { return fAttrib;  }
  UInt_t   GetEventID( void ) { return fEventID; }
  UInt_t   GetEpoch  ( void ) { return fEpoch;   }
  UInt_t   GetEpochNS( void ) { return fEpochNS; }
  Double_t GetTime   ( void ) { return fEpoch + fEpochNS*1.0e-9; }

  TrgEvent *GetEvent ( void ) { return &fEvent;  }
  UInt_t   GetHitPatt( void ) { return fEvent.GetHitPatt(); }
  UInt_t   GetCoHitRw( void ) { return fEvent.GetCoHitRw(); }
  UInt_t   GetCoHitCD( void ) { return fEvent.GetCoHitCD(); }

  // methods
  void  SetAttrib ( UInt_t at ) { fAttrib  = at; }
  void  SetEventID( UInt_t id ) { fEventID = id; }
  void  SetEpoch  ( UInt_t ep ) { fEpoch   = ep; }
  void  SetEpochNS( UInt_t ns ) { fEpochNS = ns; }

  Int_t Process   ( Int_t sel = CmpLib::ALL );
  Int_t Fill      ( BasePacket *pkt );

  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( TrgBranch, 2 )  // Trg branch class
};

#endif

// Local Variables:
// mode:C++
// End:
