#ifndef _CZT_ASIC_H
#define _CZT_ASIC_H

#include "TObject.h"

#include "CztLib.h"

#include <fstream>

class CztAsic : public TObject {
private:
  UShort_t fID;
  UShort_t fData[CztLib::N_DATA];

public:
  CztAsic( void );
  virtual ~CztAsic();
  
  UShort_t GetID   ( void ) const { return fID; }
  UShort_t GetData ( UInt_t ch ) const;
  UShort_t GetData ( Int_t ch, Int_t et ) const;
  Float_t  GetTemp ( void ) const;
  
  Int_t    ReadData ( std::ifstream *fIN , Bool_t speak );
  Int_t    ReadData ( unsigned char *data, Bool_t speak );

  CztAsic& operator = (const CztAsic &);
  virtual void Clear( Option_t *option = "" );

  ClassDef( CztAsic, 1 )
};

inline CztAsic& CztAsic::operator = ( const CztAsic & asic )
{
  fID = asic.fID;
  for ( Int_t i = 0; i < CztLib::N_DATA; i++ ) {
    fData[i] = asic.fData[i];
  }

  return *this;
}

#endif

// Local Variables:
// mode:C++
// End:
