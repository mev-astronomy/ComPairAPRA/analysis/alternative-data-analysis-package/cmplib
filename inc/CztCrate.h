#ifndef _CZT_CRATE_H
#define _CZT_CRATE_H

#include "CztLib.h"
#include "CztAsic.h"
#include "CztCryst.h"

#include "TObject.h"

#include <map>

// CztCrate
//  a base class which stores CZT Crate hit data

class CztCrate : public TObject {

public:
  typedef std::multimap< Double_t, Int_t, std::greater<Double_t> >::const_iterator it_type;
  
protected:
  Int_t     fAsicID;                // Asic ID
  Float_t   fTemp;                  // Temperature
  CztCryst  fCryst[CztLib::N_CZTB]; // Array of Czt Crystal

  std::multimap< Double_t, Int_t, std::greater<Double_t> > fHit; // MultiMap < Energy, CZT# >
  
public:
  CztCrate( void );
  CztCrate( Int_t id );
  virtual ~CztCrate();
  
  // access functions
  Int_t      GetAsicID     ( void ) const { return fAsicID;     }
  Float_t    GetTemp       ( void ) const { return fTemp;       }
  Int_t      GetNhits      ( void ) const { return fHit.size(); }
  Int_t      GetCztbID     ( UInt_t hit ) const;
  CztCryst  *GetHitCzt     ( UInt_t hit );
  CztCryst  *GetCztBar     ( Int_t  cid );

  Int_t      GetHitNpad    ( UInt_t hit );
  Double_t   GetHitEne     ( UInt_t hit );
  Double_t   GetHitTim     ( UInt_t hit );
  TVector3   GetHitPos     ( UInt_t hit );
  TVector3   GetHitTra     ( UInt_t hit );
  
  Int_t      GetCztNpad    ( Int_t  cid );
  Double_t   GetCztEne     ( Int_t  cid );
  Double_t   GetCztTim     ( Int_t  cid );
  TVector3   GetCztPos     ( Int_t  cid );
  
  void       SetData       ( CztAsic *data );
  void       SetAsicID     ( Int_t id );
  
  virtual void Process    ( void );
  virtual void Clear( Option_t *option = "" );
  virtual void Print( Option_t *option = "" ) const;
  
  // class definition for ROOT
  ClassDef( CztCrate, 1 )  // CztCrate base class
};

#endif

// Local Variables:
// mode:C++
// End:
