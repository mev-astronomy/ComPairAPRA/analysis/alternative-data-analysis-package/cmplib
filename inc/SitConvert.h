#ifndef _SIT_CONVERT_H
#define _SIT_CONVERT_H

#include "DataConvert.h"

class SitConvert : public DataConvert {
  
protected:
  unsigned char *fBuffer;
  UInt_t fPreID;
  
public:
  SitConvert( void );
  SitConvert( UShort_t atrb );
  virtual ~SitConvert();

  // methods
  void SetPreID( void );
  
  virtual Int_t Read ( void );
  virtual void  Clear( Option_t *option = "" );
};

#endif

// Local Variables:
// mode:C++
// End:
