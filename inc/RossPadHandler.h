#ifndef _ROSS_PAD_HANDLER_H
#define _ROSS_PAD_HANDLER_H

#include "TList.h"

#include <vector>
#include <fstream>

class RossPad;
class TList;

class RossPadHandler {
protected:
  TList fList;
  
  ULong64_t f1stGpsTime;         //! GPS Time
  
  std::vector<UInt_t>      fIndex;   //! index of data
  std::vector<UInt_t>      fTP;      //! data type
  std::vector<UInt_t>      fTS;      //! time stamp of data
  std::vector<UInt_t>      fDF;      //! time stamp difference
  std::vector<RossPad>     fRaw;     //! RossPad data
  std::vector<std::string> fRossPad; //! RossPad data
  
public:
  RossPadHandler();
  virtual ~RossPadHandler();
  
  // access functions
  ULong64_t      GetGpsTime( void ) const { return f1stGpsTime; }
  UInt_t         GetSize   ( void ) const { return fRossPad.size(); }
  std::string    GetString ( UInt_t i ) const { return fRossPad[i]; }

  UShort_t       GetSize   ( UInt_t i ) const;
  unsigned char *GetData   ( UInt_t i );

  // methods
  void  SetGpsTime( ULong64_t data ) { f1stGpsTime = data; }
  
  Int_t Add    ( const char *in_fname );
  Int_t Open   ( const char *);
  Int_t Loop   ( Int_t nproc = -1 );
  Int_t Process( void );

  virtual void Clear( void );
  
  void  ReadRossPad ( void );
  void  SortRossPad ( void );
  void  FindRossPad ( void );
  void  ElimRossPad ( void );
  void  CoinRossPad ( void );
  void  FormRossPad ( void );
  void  WriteRossPad( void );
};

#endif

// Local Variables:
// mode:C++
// End:
