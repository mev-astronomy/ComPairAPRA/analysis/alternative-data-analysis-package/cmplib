#ifndef _EVENT_DRAWER_H
#define _EVENT_DRAWER_H

#include "TObject.h"
#include "TObjArray.h"

#include "CmpLib.h"

#include <map>

class TF1;
class TH1I;
class TH2D;
class TGaxis;
class TLine;
class TFile;
class TChain;
class CmpTree;
class TBox;
class TObjArray;
class TCanvas;

class EventDrawer : public TObject {
public:
  typedef std::multimap< Double_t, Double_t, std::greater<Double_t> >::const_iterator it_type;
  enum ENums { B_ACD = BIT( 0 ), B_SIT = BIT( 1 ), B_CZT = BIT( 2 ), B_CSI = BIT( 3 ) };
  
private:
  Int_t     iEvent;
  TChain   *fTrees;
  CmpTree  *fCtree;

  TH2D     *fWorld  [CmpLib::N_COORD];
  TGaxis   *fAxis   [CmpLib::N_COORD];
  TLine    *fLine   [CmpLib::N_COORD];
  TBox     *fSensor [CmpLib::N_COORD][CmpLib::N_GEOM];
  
  TObjArray fHitArray;
  std::multimap< Double_t, Double_t, std::greater<Double_t> > fRes; //! Multimap < zpos, res >

  TH1I     *fTRKhist[CmpLib::N_COORD][CmpLib::N_SLICE];

  Int_t     fNumberOfColors;

  Int_t     fNhitSitStr[CmpLib::N_SLICE];
  Int_t     fNhitCztBar;
  Int_t     fNhitCsiBar;
  Double_t  fSitMaxEdep[CmpLib::N_SLICE];
  Double_t  fCztMaxEdep;
  Double_t  fCsiMaxEdep;
  Double_t  fSitTotEdep[CmpLib::N_SLICE];
  Double_t  fCztTotEdep;
  Double_t  fCsiTotEdep;
  
public:
  EventDrawer();
  virtual ~EventDrawer();

  Int_t  GetEventID ( void );

  Bool_t AddFile    ( const char *in_fname );
  void   GetPrev    ( Int_t tsels = 3, Int_t tmode = 0, Double_t ethre = 0.0 );
  void   GetNext    ( Int_t tsels = 3, Int_t tmode = 0, Double_t ethre = 0.0 );
  void   JumpEntry  ( Int_t entry );
  Bool_t TrigMode   ( Int_t tsels, Int_t tmode );
  Bool_t BackSplash ( void );
  Int_t  InitTree   ( void );
  void   SetGeometry( void );
  void   DrawFrame  ( Int_t coord );
  void   DrawEvent  ( Int_t coord, Int_t mode );
  void   DrawTrack  ( Int_t coord, Int_t mode );
  void   DrawSimTrk ( Int_t coord, Int_t mode );
  void   DrawInfo   ( Int_t coord );
  void   DrawText   ( Double_t x, Double_t y, const char *str,
		      Double_t size, Int_t col = 1, Int_t align = 11,
		      Double_t angle = 0 );

  virtual void Clear( Option_t *option = "" );

private:
  TLine *SimTrack  ( Int_t coord );
  TLine *FitTrack  ( Int_t coord );
  TLine *GetTrack  ( Int_t coord, Double_t z0, Double_t z1, TVector3 pos, TVector3 mom );
  
  ClassDef( EventDrawer, 1 )
};

#endif

// Local Variables:
// mode: C++
// End:
