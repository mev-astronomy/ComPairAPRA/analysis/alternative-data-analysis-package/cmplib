#ifndef _ARDU_INO_H
#define _ARDU_INO_H

#include "BasePacket.h"

#include "Rtypes.h"

#include <fstream>

class ArduIno {
 public:
  
 protected:
  UInt_t    fDataType;   // data type
  UInt_t    fEventID;    // Event ID
  UInt_t    fData1st;    // 1st data
  UInt_t    fData2nd;    // 2nd data
  Double_t  fUtcTime;    // Utc Time
  ULong64_t fUtcCorr;    // Corrected UTC Time
  
public:
  ArduIno();
  virtual ~ArduIno();

  // access functions
  UInt_t    GetDataType  ( void ) const { return fDataType;   }
  UInt_t    GetEventID   ( void ) const { return fEventID;    }
  UInt_t    GetData1st   ( void ) const { return fData1st;    }
  UInt_t    GetData2nd   ( void ) const { return fData2nd;    }
  Double_t  GetUtcTime   ( void ) const { return fUtcTime;    }
  ULong64_t GetUtcCorr   ( void ) const { return fUtcCorr;    }

  // methods
  ArduIno& operator= (const ArduIno &ardu);

  void     SetArduIno   ( std::string s );
  void     SetDataType  ( UInt_t    type ) { fDataType = type; }
  void     SetEventID   ( UInt_t    evid ) { fEventID  = evid; }
  void     SetData1st   ( UInt_t    data ) { fData1st  = data; }
  void     SetData2nd   ( UInt_t    data ) { fData2nd  = data; }
  void     SetUtcTime   ( Double_t  time ) { fUtcTime  = time; }
  void     SetUtcCorr   ( ULong64_t time ) { fUtcCorr  = time; }
  
  virtual void Clear ( void );
};

inline ArduIno& ArduIno::operator= (const ArduIno &ardu)
{
  fDataType = ardu.fDataType;
  fEventID  = ardu.fEventID;
  fData1st  = ardu.fData1st;
  fData2nd  = ardu.fData2nd;
  fUtcTime  = ardu.fUtcTime;
  fUtcCorr  = ardu.fUtcCorr;

  return *this;
}

#endif
