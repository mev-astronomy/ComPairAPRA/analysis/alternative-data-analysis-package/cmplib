#ifndef _CZT_PKT_HEADER_H
#define _CZT_PKT_HEADER_H

#include "CztLib.h"

#include "TObject.h"

class CztPktHeader : public TObject {
private:
  UInt_t   fPacketCount;            // Packet Count
  UShort_t fPacketSize;             // Packet Size
  UShort_t fUserRegister;           // User Register
  UInt_t   fEventRate;              // Event Rate
  UShort_t fChipInEvent;            // Chip In Event

  UInt_t   fNhitAsic;               // Number of Hit ASIC
  Int_t    fAsicID[CztLib::N_ASIC]; // Hit ASIC ID

public:
  CztPktHeader( void );
  virtual ~CztPktHeader();

  UInt_t    GetPktCount ( void ) const { return fPacketCount;  }
  UShort_t  GetPktSize  ( void ) const { return fPacketSize;   }
  UShort_t  GetRegUser  ( void ) const { return fUserRegister; }
  UInt_t    GetEvtRate  ( void ) const { return fEventRate;    }
  UShort_t  GetCIE      ( void ) const { return fChipInEvent;  }

  UInt_t    GetNhitAsic ( void ) const { return fNhitAsic;  }
  Int_t     GetAsicID   ( UInt_t hit ) const;

  Int_t     ReadData    ( std::ifstream *fIN,  Bool_t speak = kFALSE ); 
  Int_t     ReadData    ( unsigned char *data, Bool_t speak = kFALSE ); 

  // methods
  void      SetPktCount ( UInt_t   pc ) { fPacketCount  = pc; }
  void      SetPktSize  ( UShort_t ps ) { fPacketSize   = ps; }
  void      SetRegUser  ( UShort_t ru ) { fUserRegister = ru; }
  void      SetEvtRate  ( UInt_t   er ) { fEventRate    = er; }
  void      SetCIE      ( UShort_t ci ) { fChipInEvent  = ci; }
  void      SetHitAsic  ( UShort_t cie );

  virtual void Clear( Option_t *option = "" );

 private:
  UInt_t    conv4Byte   ( unsigned char *data );
  UShort_t  conv2Byte   ( unsigned char *data );

  ClassDef( CztPktHeader, 1 )
};

#endif

// Local Variables:
// mode:C++
// End:
