#ifndef _DET_BRANCH_H
#define _DET_BRANCH_H

#include "CmpLib.h"
#include "HitBranch.h"

#include "TObject.h"

#include <map>

// DetBranch
//  a base class which stores Detector data as a branch

class DetBranch : public TObject {
public:
  typedef std::multimap< Double_t, Int_t, std::greater<Double_t> >::const_iterator it_type;

protected:
  Int_t     fDetID;                     // Detector ID
  Int_t     fLyrID;                     // Layer ID
  Int_t     fNhits;                     // Number of Sensor Hits
  Int_t     fMhits;                     // Number of Maximum hits

  std::multimap< Double_t, Int_t, std::greater<Double_t> > fEne; //! Multimap < Energy, ID#>
  std::map< Int_t, Int_t    > fSta; //! map < PID#, HitSta >
  std::map< Int_t, TVector3 > fPos; //! map < PID#, HitPos >

public:
  DetBranch( void );
  DetBranch( Int_t did, Int_t lid, Int_t hit );
  virtual ~DetBranch();

  // access functions
  Int_t      GetDetID    ( void ) { return fDetID; }
  Int_t      GetLyrID    ( void ) { return fLyrID; }
  Int_t      GetNhits    ( void ) { return fNhits; }
  Int_t      GetMhits    ( void ) { return fMhits; }
  Double_t   GetEnergy   ( void ); 
  Int_t      GetNthHid   ( Int_t n );
  HitBranch *GetNthHit   ( Int_t n );
  HitBranch *NewHit      ( void );

  // methods
  Int_t Process ( Int_t sel = CmpLib::ALL );
  Int_t CopyHit ( void );

  void  SetDetID( Int_t  id ) { fDetID =  id; }
  void  SetLyrID( Int_t  id ) { fLyrID =  id; }
  void  SetMhits( Int_t hit ) { fMhits = hit; }
		   
  virtual void  Clear( Option_t *option = "" );
  virtual void  Print( Option_t *option = "" ) const;

  // class definition for ROOT
  ClassDef( DetBranch, 1 )  // Detector branch class
};

#endif

// Local Variables:
// mode:C++
// End:
