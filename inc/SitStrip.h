#ifndef _SIT_STRIP_H
#define _SIT_STRIP_H

#include "SitLib.h"

#include "TObject.h"

// SitStrip
//  a base class which stores SiT Stip Hit data as a branch

class SitStrip : public TObject {

protected:
  Int_t    fIndx;                     // Index
  Double_t fEdep;                     // Hit Energy deposit
  TVector3 fHpos;                     // Hit Position
  TVector3 fEpos;                     // Hit Position Error
  
  Int_t    fNhitAdj[SitLib::N_SIDE];  // Number of Adjacent strip hits
  Double_t fEdepAdj[SitLib::N_SIDE];  // Adjacent hit Energy deposit
  TVector3 fHposAdj[SitLib::N_SIDE];  // Adjacent hit Position
  
public:
  SitStrip( void );
  virtual ~SitStrip();
  
  // access functions
  Int_t      GetIndx   ( void ) const { return fIndx; }
  Int_t      GetSidx   ( void ) const;
  Double_t   GetEdep   ( Bool_t precise = kFALSE ) const;
  TVector3   GetHpos   ( Bool_t precise = kFALSE ) const;
  TVector3   GetEpos   ( Bool_t precise = kFALSE ) const;
  
  Int_t      GetNhitAdj( Int_t side ) const;
  Double_t   GetEdepAdj( Int_t side ) const;
  TVector3   GetHposAdj( Int_t side ) const;
  
  // methods
  void       SetIndx   ( Int_t    indx ) { fIndx = indx; }
  void       SetEdep   ( Double_t edep ) { fEdep = edep; }
  void       SetHpos   ( TVector3 hpos ) { fHpos = hpos; }
  void       SetEpos   ( TVector3 epos ) { fEpos = epos; }
  void       SetNhitAdj( Int_t side, Int_t    nhit );
  void       SetEdepAdj( Int_t side, Double_t edep );
  void       SetHposAdj( Int_t side, TVector3 hpos );
  
  virtual void Process ( void );
  virtual void Clear   ( Option_t *option = "" );
  virtual void Print   ( Option_t *option = "" ) const;
  
  // class definition for ROOT
  ClassDef( SitStrip, 1 )  // SiT Strip Hit branch base class
};

#endif

// Local Variables:
// mode:C++
// End:
