setenv CMPLIB_LOCAL_SCRATCH  $HOME/scratch
setenv CMPLIB_LOCAL_DATA     $HOME/scratch/Data
setenv CMPLIB_MACRO_PATH     $CMPLIB/macro
setenv CMPLIB_SEL_DIRECTORY  $CMPLIB/sel

if ( $?PERLLIB ) then
    setenv PERLLIB $CMPLIB/sh:$PERLLIB
else
    setenv PERLLIB $CMPLIB/sh
endif

if ( $?PERL5LIB ) then
    setenv PERL5LIB $CMPLIB/sh:$PERL5LIB
else
    setenv PERL5LIB $CMPLIB/sh
endif

setenv CMPLIB_TIME_ZONE local

setenv CMPLIB_PARAMS_DATABASE  $CMPLIB/dat/params_db.dat
setenv CMPLIB_PARAMS_DIRECTORY $CMPLIB/scratch/ComPair/czt/exp01/params
