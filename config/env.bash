export CMPLIB_LOCAL_SCRATCH=$HOME/scratch
export CMPLIB_LOCAL_DATA=$HOME/scratch/Data
export CMPLIB_MACRO_PATH=$CMPLIB/macro
export CMPLIB_SEL_DIRECTORY=$CMPLIB/sel

if [ -z $PERLLIB ]; then
    export PERLLIB=$CMPLIB/sh:$PERLLIB
else
    export PERLLIB=$CMPLIB/sh
fi

if [ -z $PERL5LIB]; then
    export PERL5LIB=$CMPLIB/sh:$PERL5LIB
else
    export PERL5LIB=$CMPLIB/sh
fi

export CMPLIB_TIME_ZONE=local

export CMPLIB_PARAMS_DATABASE=$CMPLIB/dat/params_db.dat
export CMPLIB_PARAMS_DIRECTORY=$CMPLIB/scratch/ComPair/czt/exp01/params
